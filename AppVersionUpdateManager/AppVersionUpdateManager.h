//
//  AppVersionUpdateManager.h
//  Thuis today
//
//  Created by Apple on 21/02/18.
//  Copyright © 2018 Offshore Software Solutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <SystemConfiguration/SystemConfiguration.h>

@protocol AppVersionUpdateManagerDelegate <NSObject>
@optional

/** Will be called when the update alert has shown. */
- (void)appVersionUpdateManagerDidShowUpdateDialog;

/** Will be called when the user selected to update now. */
- (void)appVersionUpdateManagerUserDidLaunchAppStore;

/** Will be called when the user selected not to update now. */
- (void)appVersionUpdateManagerUserDidCancel;

@end

@interface AppVersionUpdateManager : NSObject

/** Delegate to handle the user's actions when prompted to update. */
@property (nonatomic, weak) id <AppVersionUpdateManagerDelegate> delegate;

/** Set the UIAlertView title. NSLocalizedString() supported. */
@property (nonatomic, weak) NSString *alertTitle;

/** Set the UIAlertView alert message. NSLocalizedString() supported. */
@property (nonatomic, weak) NSString *alertMessage;

/** Set the UIAlertView update button's title. NSLocalizedString() supported. */
@property (nonatomic, weak) NSString *alertUpdateButtonTitle;

/** Set the UIAlertView cancel button's title. NSLocalizedString() supported. */
@property (nonatomic, weak) NSString *alertCancelButtonTitle;


/** Shared instance. [ATAppUpdater sharedUpdater] */
+ (id)sharedManager;

/** Checks for newer version and show alert without a cancel button. */
- (void)showUpdateWithForce;

/** Checks for newer version and show alert with a cancel button. */
- (void)showUpdateWithConfirmation;

@end
