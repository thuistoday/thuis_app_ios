//
//  NewShopDetailViewController.m
//  Thuis today
//
//  Created by offshore_mac_1 on 05/01/18.
//  Copyright © 2018 IMMANENT. All rights reserved.
//
#define ACCEPTABLE_CHARACTERS @"0123456789,"
#define ACCEPTABLE_CHARACTERSWITHOUT @"0123456789"
#define RGB(r, g, b) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:1]
#define RGBA(r, g, b, a) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:a]
#import <SDWebImage/UIImageView+WebCache.h>
#import "NewShopDetailViewController.h"
#import "MFSideMenu.h"
@interface NewShopDetailViewController ()<UISearchBarDelegate,UIGestureRecognizerDelegate>
{
    UITextField *textfld;
    float animatedViewHeight;
    NSMutableArray *arrayForSelectedIndexes ,*arrayForProductCategories,*arrayForStoreCategoriesIds,*arrayForProducts,*arrayForCoreDatavalues,*arrayForShop;
    BOOL checkViewForPopUpIsHidden,checkForPopUp;
    NSString *categorieIds;
    NSMutableDictionary *dicForTextFieldvalues;
    NSMutableArray *filteredDevices;
    BOOL isFiltered;
    int tag;
    int tag1;
    BOOL isSearchBarHidden;
    NSIndexPath * newIndexPath;
    CGRect imgProductImageframe;
    CGRect imgViewProdBackgroundframe;
    CGRect lblPriceframe;
    int PAGE_OFFSET, PAGE_NUMBER;
     int PAGE_LIMIT;
    BOOL isPageRefreshing, shouldStopPagination;
}
@property (weak, nonatomic) IBOutlet UILabel *lblForShopName;
@property (nonatomic, strong) UITapGestureRecognizer *tapGesture;
@end

@implementation NewShopDetailViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [CommonMethods configureNavigationBarForViewController:self withTitle:@""];
    tag=0;
    tag1=0;
    PAGE_LIMIT = 10;
    PAGE_NUMBER = 0;
    PAGE_OFFSET = 0;
    shouldStopPagination = NO;
    viewForCategory.hidden=YES;
    isFiltered = false;
    searchBar.delegate= self;
    //viewSearchBar.hidden=true;
    viewPopProductDetail.hidden=true;
    animatedViewHeight =viewForCategory.frame.size.height;
    arrayForSelectedIndexes =[[NSMutableArray alloc]init];
    arrayForProductCategories = [[NSMutableArray alloc]init];
    arrayForStoreCategoriesIds =[[NSMutableArray alloc]init];
    arrayForProducts = [[NSMutableArray alloc]init];
    dicForTextFieldvalues = [[NSMutableDictionary alloc]init];
    arrayForCoreDatavalues = [[NSMutableArray alloc]init];
    arrayForShop=[[NSMutableArray alloc]init];
    NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:@"restaurantData"];
    arrayForShop = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    lblShopAddress.text =[arrayForShop valueForKey:@"address_ar"];
    if (IDIOM ==  IPAD) {
        self.lblForShopName.text = [arrayForShop valueForKey:@"restaurant_name_ar"];
    }
    else {
    [CommonMethods addTitleLabelForNavigationBarForViewController:self withTitle:[arrayForShop valueForKey:@"restaurant_name_ar"]];
    }
    lblShopDescription.text =[arrayForShop valueForKey:@"restaurant_description_ar"];
    imgProductImage.layer.cornerRadius = imgProductImage.frame.size.width/2;
    imgProductImage.layer.masksToBounds = YES;
    
    NSLog(@"%@",arrayForShop);
    [imgShopLogo sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",baseUrlForImage,[arrayForShop valueForKey:@"logo"]]]
                   placeholderImage:[UIImage imageNamed:@"place-holder@1x"]];
    NSCalendar* calender = [NSCalendar currentCalendar];
    NSDateComponents* component = [calender components:NSWeekdayCalendarUnit fromDate:[NSDate date]];
    NSLog(@"%ld",(long)component.weekday);
    
    if (component.weekday == 1) {
        lblOpenCLose.text = [NSString stringWithFormat:@"%@:%@",NSLocalizedString(@"Open-Close", @"Cancel"),[arrayForShop valueForKey:@"sun"]];
    }
    else if(component.weekday == 2)
    {
        lblOpenCLose.text = [NSString stringWithFormat:@"%@:%@",NSLocalizedString(@"Open-Close", @"Cancel"),[arrayForShop valueForKey:@"mon"]];
    }
    else if(component.weekday == 3)
    {
        lblOpenCLose.text = [NSString stringWithFormat:@"%@:%@",NSLocalizedString(@"Open-Close", @"tue"),[arrayForShop valueForKey:@"mon"]];
        
    }
    else if(component.weekday == 4)
    {
        lblOpenCLose.text = [NSString stringWithFormat:@"%@:%@",NSLocalizedString(@"Open-Close", @"tue"),[arrayForShop valueForKey:@"wed"]];
    }
    else if(component.weekday == 5)
    {
        lblOpenCLose.text = [NSString stringWithFormat:@"%@:%@",NSLocalizedString(@"Open-Close", @"tue"),[arrayForShop valueForKey:@"thr"]];
    }
    else if(component.weekday == 6)
    {
        lblOpenCLose.text = [NSString stringWithFormat:@"%@:%@",NSLocalizedString(@"Open-Close", @"tue"),[arrayForShop valueForKey:@"fri"]];
    }
    else if(component.weekday == 7)
    {
        lblOpenCLose.text = [NSString stringWithFormat:@"%@:%@",NSLocalizedString(@"Open-Close", @"tue"),[arrayForShop valueForKey:@"sat"]];
    }
  //  tableViewForShopsList.frame = CGRectMake(0,viewTop.frame.size.height+1,self.view.frame.size.width,self.view.frame.size.height-(viewTop.frame.size.height+1+(viewbottom.frame.size.height-30)));
      // imgTableviewSep.frame = CGRectMake(0,viewTop.frame.size.height,self.view.frame.size.width,1);
    imgProductImageframe = CGRectMake(imgProductImage.frame.origin.x,imgProductImage.frame.origin.y,imgProductImage.frame.size.width,imgProductImage.frame.size.height);
    imgViewProdBackgroundframe=CGRectMake(imgViewProdBackground.frame.origin.x,imgViewProdBackground.frame.origin.y,imgViewProdBackground.frame.size.width,imgViewProdBackground.frame.size.height);
    lblPriceframe=CGRectMake(lblPrice.frame.origin.x,lblPrice.frame.origin.y,lblPrice.frame.size.width,lblPrice.frame.size.height);
    for (UIView *view in searchBar.subviews){
        if ([view isKindOfClass: [UITextField class]]) {
            UITextField *tf = (UITextField *)view;
            tf.delegate = self;
            break;
        }
    }
    [self callWebServiceForGetAllProducts:@"" forPage:0];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
   
    lblCount.layer.cornerRadius = lblCount.frame.size.width/2;
    lblCount.layer.masksToBounds=YES;
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"cartCount"]!=nil) {
        lblCount.text=[[NSUserDefaults standardUserDefaults] valueForKey:@"cartCount"];
    }
    else
    {
        lblCount.text=@"";
    }
    if (IDIOM ==  IPAD) {
        self.lblForShopName.text = [arrayForShop valueForKey:@"restaurant_name_ar"];
    }
    else {
        [CommonMethods addTitleLabelForNavigationBarForViewController:self withTitle:[arrayForShop valueForKey:@"restaurant_name_ar"]];
    }
    [[NSUserDefaults standardUserDefaults]setObject:@"no" forKey:@"isCartView"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    [self callWebServiceForGetAllProductCategories];
    //[self callWebServiceForGetAllProductCategories];
    
    // [self callWebServiceForGetAllProducts:@""];
    checkForPopUp = FALSE;
    [dicForTextFieldvalues removeAllObjects];
    [tableViewForShopsList reloadData];
}
- (void)callWebServiceForGetAllProductCategories
{
    NSDictionary *params = @{
                             @"shop_id":[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"restaurant_id"]]
                             };
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [[SingletonClass sharedManager] m_PostApiResponse:@"fetchProductCategory" paramDiction:params islogout:NO  onCompletion:^(NSDictionary *response)
         {
             NSDictionary *responseDic = (NSDictionary *)response;
             if ([[[responseDic objectForKey:@"payload"]valueForKey:@"status"]integerValue]==1)
             {
                 arrayForProductCategories = [[responseDic objectForKey:@"payload"] objectForKey:@"data"];
                 
                 [tableViewForCategory reloadData];
                 if (checkViewForPopUpIsHidden == true)
                 {
                     viewForCategory.hidden=NO;
                     checkViewForPopUpIsHidden = false;
                 }
             }
             else
             {
                 if (checkForPopUp != false)
                 {
                     viewForCategory.hidden=YES;
                     tableViewForShopsList.hidden=NO;
                     
                     [[AppDelegate shareInstance]showAlertWithErrorMessage:[[responseDic objectForKey:@"payload"] objectForKey:@"message"]];
                 }
                 checkForPopUp = true;
             }
         }];
    });
}
-(void)callWebServiceForGetAllProducts:(NSString *)categorieId forPage:(int)page_number
{
    // api call to get product list for given category id
    
    [[AppDelegate shareInstance]showActivityIndicator];
    NSDictionary *params;
    PAGE_OFFSET = (PAGE_NUMBER * PAGE_LIMIT);
    if (categorieIds == nil)
    {
        params = @{
                   @"shop_id":[[NSUserDefaults standardUserDefaults] valueForKey:@"restaurant_id"],
                   @"category_id":@"",
                   @"offset": [NSString stringWithFormat:@"%d", PAGE_OFFSET],
                   @"limit": @"10"//[NSString stringWithFormat:@"%d", PAGE_LIMIT]
                   };
    }
    else
    {
        params = @{
                   @"shop_id":[[NSUserDefaults standardUserDefaults] valueForKey:@"restaurant_id"],
                   @"category_id":categorieId,
                   @"offset": [NSString stringWithFormat:@"%d", PAGE_OFFSET],
                   @"limit": @"10"//[NSString stringWithFormat:@"%d", PAGE_LIMIT]
                   };
    }
    NSLog(@"%@",params);
    dispatch_async(dispatch_get_main_queue(), ^{
        [[SingletonClass sharedManager] m_PostApiResponse:@"fetchProduct" paramDiction:params islogout:NO  onCompletion:^(NSDictionary *response)
         {
             [[AppDelegate shareInstance]hideActivityIndicator];
             NSDictionary *responseDic = (NSDictionary *)response;
             if ([[[responseDic objectForKey:@"payload"]valueForKey:@"status"]integerValue]==1)
             {
                 NSArray *offsetArrayOfProducts = [[responseDic objectForKey:@"payload"] objectForKey:@"data"];
                 NSLog(@"Product Count - %lu",(unsigned long)offsetArrayOfProducts.count);
                 if (offsetArrayOfProducts.count < PAGE_LIMIT) {
                     shouldStopPagination = YES;
                 }
                 else {
                     shouldStopPagination = NO;
                 }
                 isPageRefreshing=NO;
                 if(PAGE_NUMBER == 0) {
                     
                     arrayForProducts = [NSMutableArray arrayWithArray:offsetArrayOfProducts];
                 }
                 else {
                     [arrayForProducts addObjectsFromArray:offsetArrayOfProducts];
                 }
                 //arrayForProducts = [[responseDic objectForKey:@"payload"] objectForKey:@"data"];
                 for (int i=0; i<arrayForProducts.count; i++) {
                     [dicForTextFieldvalues setValue:@"" forKey:[NSString stringWithFormat:@"%d",i]];
                 }
                 NSLog(@"%@",dicForTextFieldvalues);
                 [tableViewForShopsList reloadData];
                 viewForCategory.hidden=YES;
             }
             else
             {
               
                 shouldStopPagination = NO;
                 dispatch_async(dispatch_get_main_queue(), ^{
                     [[AppDelegate shareInstance]hideActivityIndicator];
                     [[AppDelegate shareInstance] showAlertWithErrorMessage:[[responseDic objectForKey:@"payload"]valueForKey:@"message"]];
                     [arrayForProducts removeAllObjects];
                     [tableViewForShopsList reloadData];
                 });
             }
         }];
    });
}

- (IBAction)btnCartAction:(id)sender
{
    [self performSegueWithIdentifier:@"NavigateToCartViewController" sender:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    
    
    if(searchText.length==0)
    {
        isFiltered=false;
    }
    else
    {
        isFiltered= true;
        filteredDevices=[[NSMutableArray alloc]init];
        for(int i=0 ;i<[arrayForProducts count];i++)
        {
            NSDictionary *dict=[arrayForProducts objectAtIndex:i];
            NSString *mealName=[dict objectForKey:@"meal_name_ar"];
            NSRange nameRange=[mealName rangeOfString:searchText options:NSCaseInsensitiveSearch];
            if(nameRange.location!=NSNotFound)
            {
                [filteredDevices addObject:dict];
            }
        }
    }
    [tableViewForShopsList reloadData];
}
#pragma mark - UISearchBarDelegate
- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar
{
    shouldStopPagination = YES;
    viewForCategory.hidden =YES;
    tag1=0;
    [self addTapGesture];
    [searchBar setShowsCancelButton:YES animated:YES];
    return YES;
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar {
    shouldStopPagination = NO;

}
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
    [searchBar setShowsCancelButton:NO animated:YES];

//    isFiltered=false;
//    isSearchBarHidden=true;
//    tableViewForShopsList.frame = CGRectMake(0,viewTop.frame.size.height+1,self.view.frame.size.width,self.view.frame.size.height-(viewTop.frame.size.height+(viewbottom.frame.size.height-30)));
//    imgTableviewSep.frame= CGRectMake(0,viewTop.frame.size.height,self.view.frame.size.width,1);
//    viewSearchBar.hidden=true;
    searchBar.text = @"";
//    tag=0;
    [tableViewForShopsList reloadData];
}

- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    return YES;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView== tableViewForShopsList)
    {
        if(isFiltered)
        {
            return filteredDevices.count;
        }
        else
        {
            return arrayForProducts.count;
        }
        
    }
    else {
        return arrayForProductCategories.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier;
    UITableViewCell *cell ;
    if (tableView== tableViewForShopsList) {
        if(isFiltered)
        {
            CellIdentifier = @"cell";
            cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            UILabel *lblTitle = (UILabel*)[cell viewWithTag:102];
            UILabel *lblPrice = (UILabel*)[cell viewWithTag:104];
            UILabel *lblProductUnit = (UILabel*)[cell viewWithTag:105];
            UILabel *lblVat = (UILabel*)[cell viewWithTag:202];
            UITextField *txtFld = (UITextField *)[cell viewWithTag:103];
            UIImageView *imgForShop = (UIImageView*)[cell viewWithTag:201];
            UIButton *btnAddCart = (UIButton *)[cell viewWithTag:501];
             UIButton *btnInfo = (UIButton *)[cell viewWithTag:1];
            imgForShop.layer.cornerRadius = imgForShop.frame.size.width/2;
            imgForShop.layer.masksToBounds=YES;
            UIColor *color = [UIColor colorWithRed:82/255.0 green:139/255.0 blue:11/255.0 alpha:1];
            
            //UIColor *color = [UIColor colorWithRed:82 green:139 blue:11 alpha:1];
            //        let borderColor = UIColor(red:82, green:139, blue:11, alpha:1.0);
            //viewImage.layer.borderColor = borderColor.CGColor
            [imgForShop.layer setBorderColor: [color CGColor]];
            [imgForShop.layer setBorderWidth: 1.0];
            
           //[btnAddCart addTarget:self action:@selector(addCartButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
           
            [btnInfo addTarget:self action:@selector(infoButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            txtFld.text = [dicForTextFieldvalues valueForKey:[NSString stringWithFormat:@"%ld",(long)indexPath.row]];
            float sum=0.00;
            float vat=0.00;
            float orgPrice=0.00;
            NSString* cleanedString = [[[[filteredDevices objectAtIndex:indexPath.row] valueForKey:@"meal_price"] stringByReplacingOccurrencesOfString:@"," withString:@"."]
                                       stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
            
            NSString *strTextField =[[txtFld.text stringByReplacingOccurrencesOfString:@"," withString:@"."]
                                     stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
            
            vat = ([cleanedString floatValue]*[[[filteredDevices objectAtIndex:indexPath.row] valueForKey:@"vat_name"] floatValue])/100;
            NSString *str =@"%";
            lblVat.text=[NSString stringWithFormat:@"VAT:%@%@ %@",[[filteredDevices objectAtIndex:indexPath.row] valueForKey:@"vat_name"],str,[[arrayForProducts objectAtIndex:indexPath.row] valueForKey:@"type"]];
            if ([[[filteredDevices objectAtIndex:indexPath.row] valueForKey:@"type"] isEqualToString:@"Excl."]) {
                // sum = ([cleanedString floatValue]+vat)*[txtFld.text floatValue]  ;
                sum = [cleanedString floatValue]+vat;
                orgPrice =[cleanedString floatValue]+vat*1;
                NSString *roundOff = [NSString stringWithFormat:@"%.2f",sum];
                sum = [roundOff floatValue] *[strTextField floatValue];
            }
            else
            {
                sum = [strTextField floatValue] * [cleanedString floatValue];
                orgPrice =[cleanedString floatValue]*1;
            }
            NSString* strForOriginalPriceWithComma = [[[NSString stringWithFormat:@"%@: €%.2f",NSLocalizedString(@"Price", @"Cancel"),orgPrice] stringByReplacingOccurrencesOfString:@"." withString:@","]
                                                      stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
            
            lblVat.text = strForOriginalPriceWithComma;
            
            
            NSString* strForPriceWithComma = [[[NSString stringWithFormat:@"%.2f",sum] stringByReplacingOccurrencesOfString:@"." withString:@","]
                                              stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
            lblPrice.text = [NSString stringWithFormat:@"€ %@",strForPriceWithComma];
            
            lblTitle.text =[[filteredDevices objectAtIndex:indexPath.row] valueForKey:@"meal_name_ar"];
            [imgForShop sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",baseUrlForImage,[[filteredDevices objectAtIndex:indexPath.row] valueForKey:@"meal_image"]]]
                          placeholderImage:[UIImage imageNamed:@"place-holder@1x"]];
            //        if ([[[arrayForProducts objectAtIndex:indexPath.row] valueForKey:@"product_unit"] isEqualToString:@"weight"]) {
            //            lblProductUnit.text=@"Kg";
            //        }
            //        else{
            //            lblProductUnit.text=@"Pcs";
            //        }
            lblProductUnit.text = [[filteredDevices objectAtIndex:indexPath.row] valueForKey:@"actual_unit"];
            return cell;
        }
        else
        {
            CellIdentifier = @"cell";
            cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            UILabel *lblTitle = (UILabel*)[cell viewWithTag:102];
            UILabel *lblPrice = (UILabel*)[cell viewWithTag:104];
            UILabel *lblProductUnit = (UILabel*)[cell viewWithTag:105];
            UILabel *lblVat = (UILabel*)[cell viewWithTag:202];
            UITextField *txtFld = (UITextField *)[cell viewWithTag:103];
            UIImageView *imgForShop = (UIImageView*)[cell viewWithTag:201];
            UIButton *btnAddCart = (UIButton *)[cell viewWithTag:501];
            UIButton *btnInfo = (UIButton *)[cell viewWithTag:1];
            imgForShop.layer.cornerRadius = imgForShop.frame.size.width/2;
            imgForShop.layer.masksToBounds=YES;
            UIColor *color = [UIColor colorWithRed:82/255.0 green:139/255.0 blue:11/255.0 alpha:1];

            //UIColor *color = [UIColor colorWithRed:82 green:139 blue:11 alpha:1];
            //        let borderColor = UIColor(red:82, green:139, blue:11, alpha:1.0);
                    //viewImage.layer.borderColor = borderColor.CGColor
                    [imgForShop.layer setBorderColor: [color CGColor]];
            [imgForShop.layer setBorderWidth: 1.0];
            
            //[btnAddCart addTarget:self action:@selector(addCartButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            
            [btnInfo addTarget:self action:@selector(infoButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
            txtFld.text = [dicForTextFieldvalues valueForKey:[NSString stringWithFormat:@"%ld",(long)indexPath.row]];
            float sum=0.00;
            float vat=0.00;
            float orgPrice=0.00;
            NSString* cleanedString = [[[[arrayForProducts objectAtIndex:indexPath.row] valueForKey:@"meal_price"] stringByReplacingOccurrencesOfString:@"," withString:@"."]
                                       stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
            
            NSString *strTextField =[[txtFld.text stringByReplacingOccurrencesOfString:@"," withString:@"."]
                                     stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
            
            vat = ([cleanedString floatValue]*[[[arrayForProducts objectAtIndex:indexPath.row] valueForKey:@"vat_name"] floatValue])/100;
            NSString *str =@"%";
            lblVat.text=[NSString stringWithFormat:@"VAT:%@%@ %@",[[arrayForProducts objectAtIndex:indexPath.row] valueForKey:@"vat_name"],str,[[arrayForProducts objectAtIndex:indexPath.row] valueForKey:@"type"]];
            if ([[[arrayForProducts objectAtIndex:indexPath.row] valueForKey:@"type"] isEqualToString:@"Excl."]) {
                // sum = ([cleanedString floatValue]+vat)*[txtFld.text floatValue]  ;
                sum = [cleanedString floatValue]+vat;
                orgPrice =[cleanedString floatValue]+vat*1;
                NSString *roundOff = [NSString stringWithFormat:@"%.2f",sum];
                sum = [roundOff floatValue] *[strTextField floatValue];
            }
            else
            {
                sum = [strTextField floatValue] * [cleanedString floatValue];
                orgPrice =[cleanedString floatValue]*1;
            }
            NSString* strForOriginalPriceWithComma = [[[NSString stringWithFormat:@"%@: €%.2f",NSLocalizedString(@"Price", @"Cancel"),orgPrice] stringByReplacingOccurrencesOfString:@"." withString:@","]
                                                      stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
            
            lblVat.text = strForOriginalPriceWithComma;
            
            
            NSString* strForPriceWithComma = [[[NSString stringWithFormat:@"%.2f",sum] stringByReplacingOccurrencesOfString:@"." withString:@","]
                                              stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
            lblPrice.text = [NSString stringWithFormat:@"€ %@",strForPriceWithComma];
            
            lblTitle.text =[[arrayForProducts objectAtIndex:indexPath.row] valueForKey:@"meal_name_ar"];
            [imgForShop sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",baseUrlForImage,[[arrayForProducts objectAtIndex:indexPath.row] valueForKey:@"meal_image"]]]
                          placeholderImage:[UIImage imageNamed:@"place-holder@1x"]];
            //        if ([[[arrayForProducts objectAtIndex:indexPath.row] valueForKey:@"product_unit"] isEqualToString:@"weight"]) {
            //            lblProductUnit.text=@"Kg";
            //        }
            //        else{
            //            lblProductUnit.text=@"Pcs";
            //        }
            lblProductUnit.text = [[arrayForProducts objectAtIndex:indexPath.row] valueForKey:@"actual_unit"];
            return cell;
        }
        
    }
    else
    {
        CellIdentifier = @"Cell";
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        UIImageView *imgForCheck = (UIImageView*)[cell viewWithTag:101];
        
        UILabel *lblTitle = (UILabel*)[cell viewWithTag:102];
        lblTitle.text = [[arrayForProductCategories objectAtIndex:indexPath.row] valueForKey:@"category_name_ar"];
        if ([arrayForSelectedIndexes containsObject:[NSString stringWithFormat:@"%ld",(long)indexPath.row]])
        {
            imgForCheck.image=[UIImage imageNamed:@"check-checked.png"];
        }
        else
        {
            imgForCheck.image=[UIImage imageNamed:@"check-empty.png"];
            
        }
        return cell;
    }
}
-(void)addCartButtonClicked:(id)sender
{
    CGPoint buttonPosition = [sender convertPoint:CGPointZero
                                           toView:tableViewForShopsList];
    NSIndexPath *tappedIP = [tableViewForShopsList indexPathForRowAtPoint:buttonPosition];
    UITableViewCell* cell = [tableViewForShopsList cellForRowAtIndexPath:tappedIP];
    UITextField *txtFld = (UITextField *)[cell viewWithTag:103];
    if([textfld.text isEqualToString:@""])
    {
        [[AppDelegate shareInstance] showAlertWithErrorMessage: NSLocalizedString(@"Price should not be zero", @"Cancel")];
       // [self performSelector:@selector(addToCart:) withObject:txtFld afterDelay:0.5];
    }
    else
    {
        [self performSelector:@selector(addToCart:) withObject:textfld afterDelay:0.5];
    }
    
    //txtFld.text=@"";
}
-(void)infoButtonClicked:(id)sender
{
    CGPoint buttonPosition = [sender convertPoint:CGPointZero
                                           toView:tableViewForShopsList];
    NSIndexPath *tappedIP = [tableViewForShopsList indexPathForRowAtPoint:buttonPosition];
    imgProductImage.frame=imgProductImageframe;
    imgViewProdBackground.frame=imgViewProdBackgroundframe;
    lblPrice.frame=lblPriceframe;
    NSInteger inte =tappedIP.row ;
    float sum=0.00;
    float vat=0.00;
    float orgPrice=0.00;
    [imgProductImage sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",baseUrlForImage,[[arrayForProducts objectAtIndex:inte] valueForKey:@"meal_image"]]]
                  placeholderImage:[UIImage imageNamed:@"place-holder@1x"]];
    
    NSString* cleanedString = [[[[arrayForProducts objectAtIndex:inte] valueForKey:@"meal_price"] stringByReplacingOccurrencesOfString:@"," withString:@"."]
                               stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
    
    NSString *unit=[[arrayForProducts objectAtIndex:inte] valueForKey:@"actual_unit"];
    
    vat = ([cleanedString floatValue]*[[[arrayForProducts objectAtIndex:inte] valueForKey:@"vat_name"] floatValue])/100;
    NSString *str =@"%";
    if ([[[arrayForProducts objectAtIndex:inte] valueForKey:@"type"] isEqualToString:@"Excl."]) {
        // sum = ([cleanedString floatValue]+vat)*[txtFld.text floatValue]  ;
        sum = [cleanedString floatValue]+vat;
        orgPrice =[cleanedString floatValue]+vat*1;
        NSString *roundOff = [NSString stringWithFormat:@"%.2f",sum];
        sum = [roundOff floatValue];
    }
    else
    {
        sum = [cleanedString floatValue];
        orgPrice =[cleanedString floatValue]*1;
    }
    NSString* strForPriceWithComma = [[[NSString stringWithFormat:@"%.2f",sum] stringByReplacingOccurrencesOfString:@"." withString:@","]
                                      stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
    lblPrice.text = [NSString stringWithFormat:@"Prijs € %@ / %@",strForPriceWithComma,unit ];
    lblproductDesc.text=[[arrayForProducts objectAtIndex:inte] valueForKey:@"meal_description_ar"];
    lblProductName.text=[[arrayForProducts objectAtIndex:inte] valueForKey:@"meal_name_ar"];
    viewPopProductDetail.hidden=false;
    if([lblproductDesc.text isEqualToString:@""]){
        if(IDIOM==IPAD){
            lblproductDesc.hidden=true;    imgProductImage.frame=CGRectMake(imgProductImage.frame.origin.x,imgProductImage.frame.origin.y+60,imgProductImage.frame.size.width,imgProductImage.frame.size.height);
            imgViewProdBackground.frame=CGRectMake(imgViewProdBackground.frame.origin.x,imgViewProdBackground.frame.origin.y+60,imgViewProdBackground.frame.size.width,imgViewProdBackground.frame.size.height);
            lblPrice.frame=CGRectMake(lblPrice.frame.origin.x,lblPrice.frame.origin.y+90,lblPrice.frame.size.width,lblPrice.frame.size.height);
            
        }
        else
        {
            lblproductDesc.hidden=true;    imgProductImage.frame=CGRectMake(imgProductImage.frame.origin.x,imgProductImage.frame.origin.y+40,imgProductImage.frame.size.width,imgProductImage.frame.size.height);
            imgViewProdBackground.frame=CGRectMake(imgViewProdBackground.frame.origin.x,imgViewProdBackground.frame.origin.y+40,imgViewProdBackground.frame.size.width,imgViewProdBackground.frame.size.height);
            lblPrice.frame=CGRectMake(lblPrice.frame.origin.x,lblPrice.frame.origin.y+60,lblPrice.frame.size.width,lblPrice.frame.size.height);
        }
    }
    else{
        lblproductDesc.hidden=false;    imgProductImage.frame=imgProductImageframe;
        imgViewProdBackground.frame=imgViewProdBackgroundframe;
        lblPrice.frame=lblPriceframe;
    }

}

#pragma mark -
#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView ==tableViewForCategory) {
        PAGE_OFFSET = 0;
        PAGE_NUMBER = 0;

        if ([arrayForSelectedIndexes containsObject:[NSString stringWithFormat:@"%ld",(long)indexPath.row]]) {
            [arrayForSelectedIndexes removeObject:[NSString stringWithFormat:@"%ld",(long)indexPath.row]];
            [arrayForStoreCategoriesIds removeObject:[[arrayForProductCategories objectAtIndex:indexPath.row] valueForKey:@"category_id"]];
        }
        else
        {
            [arrayForSelectedIndexes addObject:[NSString stringWithFormat:@"%ld",(long)indexPath.row]];
            [arrayForStoreCategoriesIds addObject:[[arrayForProductCategories objectAtIndex:indexPath.row] valueForKey:@"category_id"]];
        }
        [tableView reloadData];
        
    }
    else{
        ProductDetailViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"ProductDetailViewController"];
        newView.arrayForProductInfo = [arrayForProducts objectAtIndex:indexPath.row];
        [self.navigationController pushViewController:newView animated:YES];
    }
    
}

#pragma mark - UIScrollViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if(tableViewForShopsList.contentOffset.y >= (tableViewForShopsList.contentSize.height - tableViewForShopsList.bounds.size.height))
    {
        if(isPageRefreshing == NO && shouldStopPagination == NO) {
            isPageRefreshing = YES;
            [self callWebServiceForGetAllProducts:categorieIds forPage:PAGE_NUMBER++];
            NSLog(@"called %d",PAGE_NUMBER);
        }
    }
}


#pragma mark -
- (IBAction)btnCategoryAction:(id)sender
{
    // viewForCategory.hidden=NO;
    PAGE_OFFSET = 0;
    PAGE_NUMBER = 0;
    [searchBar resignFirstResponder];
    if(tag1==0)
    {
        if (arrayForProductCategories.count)
        {
            viewForCategory.hidden =NO;
            [tableViewForCategory reloadData];
        }
        else
        {
            checkViewForPopUpIsHidden=true;
            [self callWebServiceForGetAllProductCategories];
        }
        tag1=1;
    }
    else
    {
        viewForCategory.hidden =YES;
        tag1=0;
    }
    
    
}

- (IBAction)BtnOkAction:(id)sender
{
    viewForCategory.hidden=YES;
    tag1=0;
    tableViewForShopsList.hidden=NO;
    if (arrayForStoreCategoriesIds.count)
    {
        categorieIds= @"";
        categorieIds= [arrayForStoreCategoriesIds componentsJoinedByString:@","];
    }
    else
    {
        categorieIds= @"";
    }
    [self callWebServiceForGetAllProducts:categorieIds forPage:PAGE_NUMBER];
}

- (IBAction)btnAddToCartAction:(id)sender
{
    UITableViewCell* cell = (UITableViewCell*)[sender superview];
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:tableViewForShopsList];
    NSIndexPath *indexPath = [tableViewForShopsList indexPathForRowAtPoint:buttonPosition];
    NSString *type=[[arrayForProducts objectAtIndex:indexPath.row] valueForKey:@"type"] ;
    UILabel *lblPrice = (UILabel*)[cell viewWithTag:104];
    UITextField *txtFld = (UITextField *)[cell viewWithTag:103];
    [txtFld resignFirstResponder];
        float sum=0.00;
        float vat=0.00;
    float price=0.00;
        NSString* cleanedString = [[[[arrayForProducts objectAtIndex:indexPath.row] valueForKey:@"meal_price"] stringByReplacingOccurrencesOfString:@"," withString:@"."]
                                   stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
    
        NSString *strTextField =[[txtFld.text stringByReplacingOccurrencesOfString:@"," withString:@"."]
                                 stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
    
        vat = ([cleanedString floatValue]*[[[arrayForProducts objectAtIndex:indexPath.row] valueForKey:@"vat_name"] floatValue])/100;
    
        NSLog(@"%f",vat);
    
        if ([[[arrayForProducts objectAtIndex:indexPath.row] valueForKey:@"type"] isEqualToString:@"Excl."]) {
            // sum = ([cleanedString floatValue]+vat)*[txtFld.text floatValue]  ;
            sum = [cleanedString floatValue]+vat;
            price=sum;
            NSString *roundOff = [NSString stringWithFormat:@"%.2f",sum];
            sum = [roundOff floatValue] *[strTextField floatValue];
        }
        else
        {
            price=[cleanedString floatValue];
             sum = [strTextField floatValue] * [cleanedString floatValue];
        }


    NSString* strForPriceWithComma = [[[NSString stringWithFormat:@"%.2f",sum] stringByReplacingOccurrencesOfString:@"." withString:@","]
                                      stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
    lblPrice.text = [NSString stringWithFormat:@"€ %@",strForPriceWithComma];

    if (![lblPrice.text isEqualToString:@"€ 0,00"]) {
        [[CustomClasssesForCoreData sharedManagerForCoreData] methodForSaveValues:[[arrayForProducts objectAtIndex:indexPath.row] copy] price:lblPrice.text quantity:txtFld.text singlePrice:[NSString stringWithFormat:@"%.2f",price] type:type onCompletion:^(NSArray *response){
            NSLog(@"%lu",(unsigned long)response.count);
        }];
        [self methodForGetTotalPrice];
    }
        else
        {
    
            [[AppDelegate shareInstance] showAlertWithErrorMessage: NSLocalizedString(@"Price should not be zero", @"Cancel")];
    
        }
}

-(void)methodForGetTotalPrice
{
    [self.view endEditing:YES];
    [[CustomClasssesForCoreData sharedManagerForCoreData] methodForFetchValues:nil onCompletion:^(NSArray *response)
     {
         NSLog(@"%@",response);
         arrayForCoreDatavalues = [response mutableCopy];
     }];
    NSString *count = [NSString stringWithFormat:@"%lu",(unsigned long)arrayForCoreDatavalues.count];
    [[NSUserDefaults standardUserDefaults]setObject:count forKey:@"cartCount"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"cartCount"]!=nil) {
        lblCount.text=[[NSUserDefaults standardUserDefaults] valueForKey:@"cartCount"];
    }
    else
    {
        lblCount.text=@"";
    }
}

- (IBAction)btnMenuAction:(id)sender
{
    [self.menuContainerViewController toggleLeftSideMenuCompletion:nil];
    
}

- (IBAction)btnActionClosePopUp:(id)sender
{
    viewPopProductDetail.hidden=true;
}

- (IBAction)btnHomeAction:(id)sender
{
    
    ViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
    [self.navigationController pushViewController:newView animated:YES];
}

- (IBAction)btnProfileAction:(id)sender
{
    if ( [[[NSUserDefaults standardUserDefaults] objectForKey:@"userData"] count])
    {
        UserProfileViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"UserProfileViewController"];
        [self.navigationController pushViewController:newView animated:YES];
    }
    else
    {
        LoginRegistrationViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginRegistrationViewController"];
        [self.navigationController pushViewController:newView animated:YES];
    }
    
}


#pragma UITextField delegate
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    //textfld.text=@"";
}
- (void)textFieldDidEndEditing:(UITextField *)textField
{
 [tableViewForShopsList reloadData];
}
-(void) addToCart:(UITextField *) textField
{
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"is_recipe"];
    UITableViewCell* cell = (UITableViewCell*)textField.superview.superview;
    CGPoint buttonPosition = [textField.superview.superview convertPoint:CGPointZero toView:tableViewForShopsList];
    NSIndexPath *indexPath = [tableViewForShopsList indexPathForRowAtPoint:buttonPosition];
    UILabel *lblPrice = (UILabel*)[cell viewWithTag:104];
    UITextField *txtFld = (UITextField *)[cell viewWithTag:103];
    float sum=0.00;
    float vat=0.00;
    NSString *type=[[arrayForProducts objectAtIndex:indexPath.row] valueForKey:@"type"] ;
    NSString* cleanedString = [[[[arrayForProducts objectAtIndex:indexPath.row] valueForKey:@"meal_price"] stringByReplacingOccurrencesOfString:@"," withString:@"."]
                               stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
    
    NSString *strTextField =[[txtFld.text stringByReplacingOccurrencesOfString:@"," withString:@"."]
                             stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
    
    vat = ([cleanedString floatValue]*[[[arrayForProducts objectAtIndex:indexPath.row] valueForKey:@"vat_name"] floatValue])/100;
    
    NSLog(@"%f",vat);
    
    if ([[[arrayForProducts objectAtIndex:indexPath.row] valueForKey:@"type"] isEqualToString:@"Excl."]) {
        // sum = ([cleanedString floatValue]+vat)*[txtFld.text floatValue]  ;
        sum = [cleanedString floatValue]+vat;
        NSString *roundOff = [NSString stringWithFormat:@"%.2f",sum];
    }
    else
    {
        sum = [cleanedString floatValue];
    }
    
    
    
    
    if (![lblPrice.text isEqualToString:@"€ 0,00"]) {
        [[CustomClasssesForCoreData sharedManagerForCoreData] methodForSaveValues:[[arrayForProducts objectAtIndex:indexPath.row] copy] price:lblPrice.text quantity:txtFld.text singlePrice:[NSString stringWithFormat:@"%.2f",sum] type:type onCompletion:^(NSArray *response){
            NSLog(@"%lu",(unsigned long)response.count);
        }];
        [self methodForGetTotalPrice];
    }
    else
    {
        
        [[AppDelegate shareInstance] showAlertWithErrorMessage: NSLocalizedString(@"Price should not be zero", @"Cancel")];
        
    }
    textfld.text = @"";
    
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    if (textField.text.length >=5 && range.length == 0)
    {
        return NO; // return NO to not change text
    }
    else
    {
        CGPoint origin = textField.frame.origin;
        CGPoint point = [textField.superview convertPoint:origin toView:tableViewForShopsList];
        newIndexPath = [tableViewForShopsList indexPathForRowAtPoint:point];
        
        // [tableViewForShopsList setContentOffset:CGPointMake(0, point.y+100)];
        
        //    [tableViewForShopsList scrollToRowAtIndexPath:newIndexPath
        //                         atScrollPosition:UITableViewScrollPositionNone
        //                                 animated:NO];
        
//        if ([[[arrayForProducts objectAtIndex:newIndexPath.row] valueForKey:@"product_unit"] isEqualToString:@"weight"])
//        {
//            if ([[[arrayForProducts objectAtIndex:newIndexPath.row] valueForKey:@"actual_unit"] isEqualToString:@"per stuk"] || [[[arrayForProducts objectAtIndex:newIndexPath.row] valueForKey:@"actual_unit"] isEqualToString:@"per zak"] || [[[arrayForProducts objectAtIndex:newIndexPath.row] valueForKey:@"actual_unit"] isEqualToString:@"per doos"])
                if ([[[arrayForProducts objectAtIndex:newIndexPath.row] valueForKey:@"product_unit"] isEqualToString:@"stuk"] || [[[arrayForProducts objectAtIndex:newIndexPath.row] valueForKey:@"product_unit"] isEqualToString:@"zak"] || [[[arrayForProducts objectAtIndex:newIndexPath.row] valueForKey:@"product_unit"] isEqualToString:@"doos"])
            {
                if ([self checkForValidCharacterForWithout:string]) {
                    
                    string = [[string stringByReplacingOccurrencesOfString:@"." withString:@","]
                              stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
                    
                    NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
                    newString =     [newString stringByTrimmingCharactersInSet:
                                     [NSCharacterSet whitespaceCharacterSet]];
                    
                    [dicForTextFieldvalues setObject:newString forKey:[NSString stringWithFormat:@"%ld",(long)newIndexPath.row]];
                    
                    //[tableViewForShopsList reloadData];
                    
                    double delayInSeconds = 0.1;
                    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
                    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                        [textField becomeFirstResponder];
                    });
                    return true;
                }
                else{
                    return false;
                }
            }
            else
            {
                if ([self checkForValidCharacter:string]) {
                    
                    string = [[string stringByReplacingOccurrencesOfString:@"." withString:@","]
                              stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
                    
                    NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
                    newString =     [newString stringByTrimmingCharactersInSet:
                                     [NSCharacterSet whitespaceCharacterSet]];
                    
                    [dicForTextFieldvalues setObject:newString forKey:[NSString stringWithFormat:@"%ld",(long)newIndexPath.row]];
                    
                    //[tableViewForShopsList reloadData];
                    
                    double delayInSeconds = 0.1;
                    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
                    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                        [textField becomeFirstResponder];
                    });
                    return true;
                }
                else{
                    return false;
                }
            }
        //}
//        else
//        {
//            if ([self checkForValidCharacterForWithout:string]) {
//
//                string = [[string stringByReplacingOccurrencesOfString:@"." withString:@","]
//                          stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
//
//                NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
//                newString =     [newString stringByTrimmingCharactersInSet:
//                                 [NSCharacterSet whitespaceCharacterSet]];
//
//                [dicForTextFieldvalues setObject:newString forKey:[NSString stringWithFormat:@"%ld",(long)newIndexPath.row]];
//
//                // [tableViewForShopsList reloadData];
//
//                double delayInSeconds = 0.1;
//                dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
//                dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
//                    [textField becomeFirstResponder];
//                });
//                return true;
//            }
//            else
//            {
//                return false;
//            }
//
//        }
    }
    
   
}

-(BOOL)checkForValidCharacter:(NSString*)str
{
    NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:ACCEPTABLE_CHARACTERS] invertedSet];
    NSString *filtered = [[str componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
    if ([str isEqualToString:filtered])
    {
        return true;
    }
    else
    {
        return false;
    }
}
-(BOOL)checkForValidCharacterForWithout:(NSString*)str
{
    NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:ACCEPTABLE_CHARACTERSWITHOUT] invertedSet];
    NSString *filtered = [[str componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
    if ([str isEqualToString:filtered])
    {
        return true;
    }
    else
    {
        return false;
    }
}
- (IBAction)btnBottomBackAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)dismissSearchBarKeyBoard:(UIGestureRecognizer *)gestureRecognizer {
    [searchBar setShowsCancelButton:NO animated:YES];

    [self.view endEditing:YES];
    [self.view removeGestureRecognizer:self.tapGesture];
}
- (IBAction)btnBottomCartAction:(id)sender
{
    [searchBar becomeFirstResponder];
    
//    CartViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"CartViewController"];
//    [self.navigationController pushViewController:newView animated:YES];
    
   /* if(tag==0)
    {
        isSearchBarHidden=false;
        tableViewForShopsList.frame = CGRectMake(0,viewTop.frame.size.height+60,self.view.frame.size.width,self.view.frame.size.height-(viewTop.frame.size.height+(viewbottom.frame.size.height)));
        imgTableviewSep.frame= CGRectMake(0,viewTop.frame.size.height+59,self.view.frame.size.width,1);
        viewSearchBar.hidden=false;
        tag=1;
    }
    else
    {
        isSearchBarHidden=true;
        viewForCategory.hidden=YES;
        tableViewForShopsList.frame = CGRectMake(0,viewTop.frame.size.height+1,self.view.frame.size.width,self.view.frame.size.height-(viewTop.frame.size.height+(viewbottom.frame.size.height-30)));
        imgTableviewSep.frame= CGRectMake(0,viewTop.frame.size.height,self.view.frame.size.width,1);
        viewSearchBar.hidden=true;
        tag=0;
    }
    */
}

- (void)addTapGesture {
    self.tapGesture = [[UITapGestureRecognizer alloc] init];
    self.tapGesture.delegate = self;
    [self.tapGesture addTarget:self action:@selector(dismissSearchBarKeyBoard:)];
    [self.view addGestureRecognizer:self.tapGesture];
}
- (IBAction)btnBottomHomeAction:(id)sender
{
    
    ViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
    [self.navigationController pushViewController:newView animated:YES];
}


@end
