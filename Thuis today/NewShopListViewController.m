//
//  NewShopListViewController.m
//  Thuis today
//
//  Created by offshore_mac_1 on 28/12/17.
//  Copyright © 2017 IMMANENT. All rights reserved.
//

#import "NewShopListViewController.h"

@interface NewShopListViewController ()
{
    NSMutableArray *arrayForSelectedIndexes,*arrayForCategories,*arrayForStoreCategoriesIds,*arrayForShops;
    NSString *categorieIds;
    BOOL checkViewForPopUpIsHidden;
    BOOL onceDone;
    CGRect frame;
    CGRect frameimgViewCatgBorder;
    CGRect frameimgShopImage;
    CGRect frameimgViewProdBorder;
    CGRect frameimgViewIcon;
    CGRect framelblCategoryName;
    CGRect framelbladdress;
    CGRect frameblTime;
    CGRect framelblOpenCloseTime;
}

@end

@implementation NewShopListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [CommonMethods configureNavigationBarForViewController:self withTitle:@""];
    viewForPopUP.hidden=YES;
    arrayForSelectedIndexes=[[NSMutableArray alloc]init];
    arrayForCategories = [[NSMutableArray alloc]init];
    arrayForStoreCategoriesIds = [[NSMutableArray alloc]init];
    arrayForShops = [[NSMutableArray alloc]init];
    
    [self callWebServiceForGetAllCategories];
    ViewForInfoPopUp.hidden=true;
    categorieIds = [_categorieIds stringByTrimmingCharactersInSet:
                    [NSCharacterSet whitespaceCharacterSet]];
    if (categorieIds == nil) {
        categorieIds = @"";
    }
    
    [self callWebServiceForGetAllShopes:categorieIds];
    btnChooseCategory.layer.cornerRadius= 4;
    imgCategory.layer.cornerRadius = imgCategory.frame.size.width/2;
    imgCategory.layer.masksToBounds =YES;
    imgShopImage.layer.cornerRadius = imgShopImage.frame.size.width/2;
    imgShopImage.layer.masksToBounds =YES;
    
    txtViewDescription.hidden=false;
     frame=CGRectMake(imgCategory.frame.origin.x,imgCategory.frame.origin.y,imgCategory.frame.size.width,imgCategory.frame.size.height);
     frameimgViewCatgBorder=CGRectMake(imgViewCatgBorder.frame.origin.x,imgViewCatgBorder.frame.origin.y,imgViewCatgBorder.frame.size.width,imgViewCatgBorder.frame.size.height);
   frameimgShopImage=CGRectMake(imgShopImage.frame.origin.x,imgShopImage.frame.origin.y,imgShopImage.frame.size.width,imgShopImage.frame.size.height);
   frameimgViewProdBorder=CGRectMake(imgViewProdBorder.frame.origin.x,imgViewProdBorder.frame.origin.y,imgViewProdBorder.frame.size.width,imgViewProdBorder.frame.size.height);
  frameimgViewIcon=CGRectMake(imgViewIcon.frame.origin.x,imgViewIcon.frame.origin.y,imgViewIcon.frame.size.width,imgViewIcon.frame.size.height);
 framelblCategoryName=CGRectMake(lblCategoryName.frame.origin.x,lblCategoryName.frame.origin.y,lblCategoryName.frame.size.width,lblCategoryName.frame.size.height);
  framelbladdress=CGRectMake(lbladdress.frame.origin.x,lbladdress.frame.origin.y,lbladdress.frame.size.width,lbladdress.frame.size.height);
    frameblTime=CGRectMake(lblTime.frame.origin.x,lblTime.frame.origin.y,lblTime.frame.size.width,lblTime.frame.size.height);
 framelblOpenCloseTime=CGRectMake(lblOpenCloseTime.frame.origin.x,lblOpenCloseTime.frame.origin.y,lblOpenCloseTime.frame.size.width,lblOpenCloseTime.frame.size.height);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)btnCartAction:(id)sender
{
    [self performSegueWithIdentifier:@"NavigateToCartViewController" sender:nil];
}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    //[self callWebServiceForGetAllCategories];
    //    categorieIds = [_categorieIds stringByTrimmingCharactersInSet:
    //                   [NSCharacterSet whitespaceCharacterSet]];
    //    if (categorieIds == nil) {
    //        categorieIds = @"";
    //    }
    lblCount.layer.cornerRadius = lblCount.frame.size.width/2;
    lblCount.layer.masksToBounds=YES;
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"cartCount"]!=nil) {
        lblCount.text=[[NSUserDefaults standardUserDefaults] valueForKey:@"cartCount"];
    }
    else
    {
        lblCount.text=@"";
    }
    [[NSUserDefaults standardUserDefaults]setObject:@"no" forKey:@"isCartView"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    //[self callWebServiceForGetAllShopes:categorieIds];
    
}
-(void)callWebServiceForGetAllCategories
{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [[SingletonClass sharedManager] m_PostApiResponse:@"shopType" paramDiction:nil islogout:NO  onCompletion:^(NSDictionary *response)
         {
             NSDictionary *responseDic = (NSDictionary *)response;
             if ([[[responseDic objectForKey:@"payload"]valueForKey:@"status"]integerValue]==1)
             {
                 arrayForCategories = [[responseDic objectForKey:@"payload"] objectForKey:@"data"];
                 
                 [tableViewForCategories reloadData];
                 if (checkViewForPopUpIsHidden == true)
                 {
                     viewForPopUP.hidden=NO;
                     checkViewForPopUpIsHidden = false;
                 }
             }
             else
             {
                 
             }
         }];
    });
}

-(void)callWebServiceForGetAllShopes:(NSString *)categorieId{
    
    [[AppDelegate shareInstance]showActivityIndicator];
    
    _cityName =  [_cityName stringByTrimmingCharactersInSet:
                  [NSCharacterSet whitespaceCharacterSet]];
    _zipCode =  [_zipCode stringByTrimmingCharactersInSet:
                 [NSCharacterSet whitespaceCharacterSet]];
    if (_zipCode == nil) {
        _zipCode = @"";
    }
    categorieId = [categorieId stringByTrimmingCharactersInSet:
                   [NSCharacterSet whitespaceCharacterSet]];
    NSDictionary *params;
    if (![_zipCode isEqualToString:@""] && ![categorieId isEqualToString:@""])
    {
        params = @{
                   @"zipcode":[NSString stringWithFormat:@"%@",_zipCode],
                   @"category":[NSString stringWithFormat:@"%@",categorieId]
                   };
    }
    else if (![_zipCode isEqualToString:@""]) {
        params = @{
                   @"zipcode":[NSString stringWithFormat:@"%@",_zipCode],
                   };
    }
    else if (![_cityName isEqualToString:@""] && ![categorieId isEqualToString:@""])
    {
        params = @{
                   @"cityname":[NSString stringWithFormat:@"%@",_cityName],
                   @"category":[NSString stringWithFormat:@"%@",categorieId]
                   };
    }
    else if (![_cityName isEqualToString:@""]) {
        params = @{
                   @"cityname":[NSString stringWithFormat:@"%@",_cityName],
                   };
    }
    
    [[NSUserDefaults standardUserDefaults] setValue:_zipCode forKey:@"Zipcode"];
    [[NSUserDefaults standardUserDefaults] setValue:_cityName forKey:@"Cityname"];
    
    NSLog(@"%@",params);
    dispatch_async(dispatch_get_main_queue(), ^{
        [[SingletonClass sharedManager] m_PostApiResponse:@"findShop" paramDiction:params islogout:NO  onCompletion:^(NSDictionary *response)
         {
             NSDictionary *responseDic = (NSDictionary *)response;
             if ([[[responseDic objectForKey:@"payload"]valueForKey:@"status"]integerValue]==1)
             {
                 lblShopName.hidden = NO;
                 arrayForShops = [[responseDic objectForKey:@"payload"] objectForKey:@"data"];
                 self.navigationItem.title = [[responseDic objectForKey:@"payload"] objectForKey:@"town_name"];
                 NSString *str_serviceCharge = [NSString stringWithFormat:@"%@",[[responseDic objectForKey:@"payload"] objectForKey:kService_Charge]];
                 [[NSUserDefaults standardUserDefaults] setObject:str_serviceCharge forKey:kService_Charge];
                 [[NSUserDefaults standardUserDefaults] synchronize];
                 [collectionViewForShopList reloadData];
                 viewForPopUP.hidden=YES;
                 [[AppDelegate shareInstance]hideActivityIndicator];
             }
             else
             {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     [[AppDelegate shareInstance]hideActivityIndicator];
                     [[AppDelegate shareInstance] showAlertWithErrorMessage:[[responseDic objectForKey:@"payload"]valueForKey:@"message"]];
                 });
             }
         }];
    });
}

#pragma mark - CollectionView

- (NSInteger)numberOfSectionsInCollectionView: (UICollectionView *)collectionView
{
    return 1;
}
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return arrayForShops.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    UIImageView *imgForShopLogo = (UIImageView*)[cell viewWithTag:101];
    UIImageView *imgForShop = (UIImageView*)[cell viewWithTag:100];
    UILabel *lblNameOfShop = (UILabel*)[cell viewWithTag:102];
    UILabel *lblShopTitle = (UILabel*)[cell viewWithTag:103];
     UILabel *lblCategoryName = (UILabel*)[cell viewWithTag:104];
    UIButton *btnInfo = (UIButton *)[cell viewWithTag:105];
    btnInfo.tag= indexPath.row;
    [btnInfo addTarget:self action:@selector(infoButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    //imgForShopLogo.layer.cornerRadius = imgForShopLogo.frame.size.width/2;
    //imgForShopLogo.layer.masksToBounds =YES;
    
    
//    imgForShop.layer.cornerRadius = imgForShop.frame.size.width/2;
//    imgForShop.clipsToBounds = YES;
    
    lblNameOfShop.text =[[arrayForShops objectAtIndex:indexPath.row] valueForKey:@"restaurant_name_ar"];
    lblShopTitle.text =[[arrayForShops objectAtIndex:indexPath.row] valueForKey:@"address_ar"];
    lblCategoryName.text =[[arrayForShops objectAtIndex:indexPath.row] valueForKey:@"category_name"];
//    [lblNameOfShop setFont: [lblNameOfShop.font fontWithSize: 10]];
    [lblShopTitle setFont: [lblShopTitle.font fontWithSize: 14]];
    [lblCategoryName setFont: [lblNameOfShop.font fontWithSize: 14]];
    NSLog(@"%ld", (long)indexPath.row);
    [imgForShopLogo sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",baseUrlForImageProfile,[[arrayForShops objectAtIndex:indexPath.row] valueForKey:@"image"]]]
                      placeholderImage:[UIImage imageNamed:@"place-holder@1x"]];
    NSLog(@"%@%@",baseUrlForImageProfile,[[arrayForShops objectAtIndex:indexPath.row] valueForKey:@"image"]);
    [imgForShop sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",baseUrlForImageProfile,[[arrayForShops objectAtIndex:indexPath.row] valueForKey:@"logo"]]]
                  placeholderImage:[UIImage imageNamed:@"place-holder@1x"]];
    return cell;

    //return cell;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(collectionView.frame.size.width/2-2.5, collectionView.frame.size.width/2);

}

-(void)infoButtonClicked:(UIButton*)sender
{
    imgCategory.frame= frame;
    imgViewCatgBorder.frame=frameimgViewCatgBorder;;
    imgShopImage.frame=frameimgShopImage;
    imgViewProdBorder.frame=frameimgViewProdBorder;
    imgViewIcon.frame=frameimgViewIcon;
    lblCategoryName.frame=framelblCategoryName;
    lbladdress.frame=framelbladdress;
    lblTime.frame=frameblTime;
    lblOpenCloseTime.frame=framelblOpenCloseTime;
    NSInteger inte =sender.tag ;
//    NSDate *now = [NSDate date];
//    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
//    [formatter setDateFormat:@"dd"];
//    NSString *day = [formatter stringFromDate:now];
    // get the current date
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    NSDate *now = [[NSDate alloc] init];
    
    // get the weekday of the current date
    NSCalendar* cal = [NSCalendar currentCalendar];
    NSDateComponents* components = [cal components:NSWeekdayCalendarUnit fromDate:now];
    NSInteger weekday = [components weekday]; // 1 = Sunday, 2 = Monday, etc.
    ViewForInfoPopUp.hidden=false;
    NSString *monday =[[arrayForShops objectAtIndex:inte] valueForKey:@"mo"];
    NSString *tuesday =[[arrayForShops objectAtIndex:inte] valueForKey:@"tu"];
    NSString *wednesday =[[arrayForShops objectAtIndex:inte] valueForKey:@"we"];
    NSString *thursday =[[arrayForShops objectAtIndex:inte] valueForKey:@"th"];
    NSString *friday =[[arrayForShops objectAtIndex:inte] valueForKey:@"fr"];
    NSString *saturday =[[arrayForShops objectAtIndex:inte] valueForKey:@"sa"];
    NSString *sunday =[[arrayForShops objectAtIndex:inte] valueForKey:@"su"];
    
    if(weekday==1)
    {
        lblTime.text =sunday;
    }
    if(weekday==2)
    {
        lblTime.text =monday;
    }
    if(weekday==3)
    {
        lblTime.text =tuesday;
    }
    if(weekday==4)
    {
        lblTime.text =wednesday;
    }
    if(weekday==5)
    {
        lblTime.text =thursday;
    }
    if(weekday==6)
    {
       lblTime.text =friday;
    }
    if(weekday==7)
    {
        lblTime.text =saturday;
    }
    
    if([lblTime.text isEqualToString:@""])
    {
        lblTime.hidden=true;
        lblOpenCloseTime.hidden=true;
        
    }
    else
    {
        lblTime.hidden=false;
        lblOpenCloseTime.hidden=false;
    }
    [imgCategory sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",baseUrlForImageProfile,[[arrayForShops objectAtIndex:inte] valueForKey:@"image"]]]
                      placeholderImage:[UIImage imageNamed:@"place-holder@1x"]];
    NSLog(@"%@%@",baseUrlForImage,[[arrayForShops objectAtIndex:inte] valueForKey:@"image"]);
    [imgShopImage sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",baseUrlForImageProfile,[[arrayForShops objectAtIndex:inte] valueForKey:@"logo"]]]
                  placeholderImage:[UIImage imageNamed:@"place-holder@1x"]];
    NSLog(@"%@%@",baseUrlForImageProfile,[[arrayForShops objectAtIndex:inte] valueForKey:@"logo"]);
    lbladdress.text =[[arrayForShops objectAtIndex:inte] valueForKey:@"address_ar"];
    lblCategoryName.text=[[arrayForShops objectAtIndex:inte] valueForKey:@"category_name"];
    txtViewDescription.text=[[arrayForShops objectAtIndex:inte] valueForKey:@"restaurant_description_ar"];
    if([txtViewDescription.text isEqualToString:@""]){
        if(onceDone==NO){
            onceDone=YES;
        }
        
        if(IDIOM==IPAD){
       txtViewDescription.hidden=true;
            imgCategory.frame=CGRectMake(imgCategory.frame.origin.x,imgCategory.frame.origin.y+20,imgCategory.frame.size.width,imgCategory.frame.size.height);
            imgViewCatgBorder.frame=CGRectMake(imgViewCatgBorder.frame.origin.x,imgViewCatgBorder.frame.origin.y+20,imgViewCatgBorder.frame.size.width,imgViewCatgBorder.frame.size.height);
            imgShopImage.frame=CGRectMake(imgShopImage.frame.origin.x,imgShopImage.frame.origin.y+70,imgShopImage.frame.size.width,imgShopImage.frame.size.height);
            imgViewProdBorder.frame=CGRectMake(imgViewProdBorder.frame.origin.x,imgViewProdBorder.frame.origin.y+70,imgViewProdBorder.frame.size.width,imgViewProdBorder.frame.size.height);
            imgViewIcon.frame=CGRectMake(imgViewIcon.frame.origin.x,imgViewIcon.frame.origin.y+80,imgViewIcon.frame.size.width,imgViewIcon.frame.size.height);
            lblCategoryName.frame=CGRectMake(lblCategoryName.frame.origin.x,lblCategoryName.frame.origin.y+80,lblCategoryName.frame.size.width,lblCategoryName.frame.size.height);
            lbladdress.frame=CGRectMake(lbladdress.frame.origin.x,lbladdress.frame.origin.y+70,lbladdress.frame.size.width,lbladdress.frame.size.height);
            lblTime.frame=CGRectMake(lblTime.frame.origin.x,lblTime.frame.origin.y-30,lblTime.frame.size.width,lblTime.frame.size.height);
            lblOpenCloseTime.frame=CGRectMake(lblOpenCloseTime.frame.origin.x,lblOpenCloseTime.frame.origin.y-30,lblOpenCloseTime.frame.size.width,lblOpenCloseTime.frame.size.height);
        }
        else{
            txtViewDescription.hidden=true; imgCategory.frame=CGRectMake(imgCategory.frame.origin.x,imgCategory.frame.origin.y+10,imgCategory.frame.size.width,imgCategory.frame.size.height);
            imgViewCatgBorder.frame=CGRectMake(imgViewCatgBorder.frame.origin.x,imgViewCatgBorder.frame.origin.y+10,imgViewCatgBorder.frame.size.width,imgViewCatgBorder.frame.size.height);
            imgShopImage.frame=CGRectMake(imgShopImage.frame.origin.x,imgShopImage.frame.origin.y+30,imgShopImage.frame.size.width,imgShopImage.frame.size.height);
            imgViewProdBorder.frame=CGRectMake(imgViewProdBorder.frame.origin.x,imgViewProdBorder.frame.origin.y+30,imgViewProdBorder.frame.size.width,imgViewProdBorder.frame.size.height);
            imgViewIcon.frame=CGRectMake(imgViewIcon.frame.origin.x,imgViewIcon.frame.origin.y+30,imgViewIcon.frame.size.width,imgViewIcon.frame.size.height);
            lblCategoryName.frame=CGRectMake(lblCategoryName.frame.origin.x,lblCategoryName.frame.origin.y+30,lblCategoryName.frame.size.width,lblCategoryName.frame.size.height);
            lbladdress.frame=CGRectMake(lbladdress.frame.origin.x,lbladdress.frame.origin.y+30,lbladdress.frame.size.width,lbladdress.frame.size.height);
            lblTime.frame=CGRectMake(lblTime.frame.origin.x,lblTime.frame.origin.y-30,lblTime.frame.size.width,lblTime.frame.size.height);
            lblOpenCloseTime.frame=CGRectMake(lblOpenCloseTime.frame.origin.x,lblOpenCloseTime.frame.origin.y-30,lblOpenCloseTime.frame.size.width,lblOpenCloseTime.frame.size.height);
           
        }
    }
    else{
        txtViewDescription.hidden=false;
        imgCategory.frame= frame;
        imgViewCatgBorder.frame=frameimgViewCatgBorder;;
        imgShopImage.frame=frameimgShopImage;
        imgViewProdBorder.frame=frameimgViewProdBorder;
        imgViewIcon.frame=frameimgViewIcon;
        lblCategoryName.frame=framelblCategoryName;
        lbladdress.frame=framelbladdress;
        lblTime.frame=frameblTime;
        lblOpenCloseTime.frame=framelblOpenCloseTime;
    }
    lblShopOwnerName.text=[[arrayForShops objectAtIndex:inte] valueForKey:@"restaurant_name_ar"];
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if (![[[NSUserDefaults standardUserDefaults]valueForKey:@"restaurant_id"]isEqualToString:[[arrayForShops objectAtIndex:indexPath.row] valueForKey:@"restaurant_id"]] && [[NSUserDefaults standardUserDefaults]valueForKey:@"restaurant_id"]!= nil)
    {
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:nil
                                      message:NSLocalizedString(@"Change Shop", @"Cancel")
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            [[CustomClasssesForCoreData sharedManagerForCoreData] methodForDeleteAllValues:nil onCompletion:^(NSArray *response)
             {
                 [[NSUserDefaults standardUserDefaults]setObject:@"0" forKey:@"cartCount"];
                 
                 [[NSUserDefaults standardUserDefaults]synchronize];
                 if ([[NSUserDefaults standardUserDefaults] valueForKey:@"cartCount"]!=nil) {
                     lblCount.text=[[NSUserDefaults standardUserDefaults] valueForKey:@"cartCount"];
                 }
                 else
                 {
                     lblCount.text=@"";
                 }
                 
             }];
            ShopDetailViewController*newView = [self.storyboard instantiateViewControllerWithIdentifier:@"ShopDetailViewController"];
            
            [[NSUserDefaults standardUserDefaults] setObject:[[arrayForShops objectAtIndex:indexPath.row] valueForKey:@"restaurant_id"] forKey:@"restaurant_id"];
            NSData *data = [NSKeyedArchiver archivedDataWithRootObject:[arrayForShops objectAtIndex:indexPath.row]];
            
            [[NSUserDefaults standardUserDefaults] setObject:data forKey:@"restaurantData"];
            
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            
            
            newView.stringForShopId =[[arrayForShops objectAtIndex:indexPath.row] valueForKey:@"restaurant_id"];
            newView.strForShopName =[[arrayForShops objectAtIndex:indexPath.row] valueForKey:@"restaurant_name_ar"];
            [self.navigationController pushViewController:newView animated:YES];
            
            
        }];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){
            
        }];
        [alert addAction:cancelAction];
        [alert addAction:okAction];
        [self presentViewController:alert animated:YES completion:nil];
        
    }
    else
    {
        ShopDetailViewController*newView = [self.storyboard instantiateViewControllerWithIdentifier:@"ShopDetailViewController"];
        
        [[NSUserDefaults standardUserDefaults] setObject:[[arrayForShops objectAtIndex:indexPath.row] valueForKey:@"restaurant_id"] forKey:@"restaurant_id"];
        NSData *data = [NSKeyedArchiver archivedDataWithRootObject:[arrayForShops objectAtIndex:indexPath.row]];
        
        [[NSUserDefaults standardUserDefaults] setObject:data forKey:@"restaurantData"];
        
        [[NSUserDefaults standardUserDefaults] synchronize];
        newView.stringForShopId =[[arrayForShops objectAtIndex:indexPath.row] valueForKey:@"restaurant_id"];
        newView.strForShopName =[[arrayForShops objectAtIndex:indexPath.row] valueForKey:@"restaurant_name_ar"];
        [self.navigationController pushViewController:newView animated:YES];
    }
    //cell.selected = YES;
}
#pragma mark -
#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
   
        return arrayForCategories.count;
        
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier;
    UITableViewCell *cell ;
    
        CellIdentifier = @"Cell";
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        UIImageView *imgForCheck = (UIImageView*)[cell viewWithTag:101];
        UILabel *lblTitle = (UILabel*)[cell viewWithTag:102];
        lblTitle.text =[[arrayForCategories objectAtIndex:indexPath.row] valueForKey:@"category_name"];
        if ([arrayForSelectedIndexes containsObject:[NSString stringWithFormat:@"%ld",(long)indexPath.row]])
        {
            imgForCheck.image=[UIImage imageNamed:@"green-checkbox@1x"];
        }
        else
        {
            imgForCheck.image=[UIImage imageNamed:@"check-box@1x"];
            
        }
        return cell;
    
    
}

#pragma mark -
#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
   
        if ([arrayForSelectedIndexes containsObject:[NSString stringWithFormat:@"%ld",(long)indexPath.row]]) {
            [arrayForSelectedIndexes removeObject:[NSString stringWithFormat:@"%ld",(long)indexPath.row]];
            [arrayForStoreCategoriesIds removeObject:[[arrayForCategories objectAtIndex:indexPath.row] valueForKey:@"category_id"]];
        }
        else
        {
            [arrayForSelectedIndexes addObject:[NSString stringWithFormat:@"%ld",(long)indexPath.row]];
            [arrayForStoreCategoriesIds addObject:[[arrayForCategories objectAtIndex:indexPath.row] valueForKey:@"category_id"]];
        }
        [tableView reloadData];
    
}

- (IBAction)btnCloseInfoWndowPopUp:(id)sender
{
    ViewForInfoPopUp.hidden=TRUE;
}

- (IBAction)btnCloseAction:(id)sender
{
    viewForPopUP.hidden=YES;
    //tableViewForShopsList.hidden=NO;
}

- (IBAction)btnChooseCategoriePop:(id)sender
{
    viewForPopUP.hidden=YES;
    //tableViewForShopsList.hidden=NO;
    if (arrayForStoreCategoriesIds.count)
    {
        categorieIds= @"";
        categorieIds= [arrayForStoreCategoriesIds componentsJoinedByString:@","];
    }
    else
    {
        categorieIds= @"";
    }
    [self callWebServiceForGetAllShopes:categorieIds];
}

- (IBAction)btnOpenCategorieList:(id)sender
{
    viewForPopUP.hidden=NO;
    //tableViewForShopsList.hidden=YES;
    if (arrayForCategories.count)
    {
        viewForPopUP.hidden =NO;
        [tableViewForCategories reloadData];
    }
    else
    {
        checkViewForPopUpIsHidden=true;
        [self callWebServiceForGetAllCategories];
    }
}

- (IBAction)btnMenuAction:(id)sender
{
    [self.menuContainerViewController toggleLeftSideMenuCompletion:nil];
    
}

- (IBAction)btnBottomBackAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnBottomCartAction:(id)sender
{
    CartViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"CartViewController"];
    [self.navigationController pushViewController:newView animated:YES];
}

- (IBAction)btnBottomHomeAction:(id)sender
{
    ViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
    [self.navigationController pushViewController:newView animated:YES];
}

- (IBAction)btnActionStore:(id)sender
{
    kookboekView.hidden = YES;
    imgOrange.frame = CGRectMake(btnStore.frame.origin.x, imgOrange.frame.origin.y, btnStore.frame.size.width, imgOrange.frame.size.height);
}

- (IBAction)btnActionKookboek:(id)sender
{
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"No_Recipes"])
    {
        [[AppDelegate shareInstance] showAlertWithErrorMessage:@"No recipes available in this area"];
    }
    else
    {
        kookboekView.hidden = NO;
        imgOrange.frame = CGRectMake(btnKookboek.frame.origin.x, imgOrange.frame.origin.y, btnKookboek.frame.size.width, imgOrange.frame.size.height);
    }
}

- (IBAction)btnHomeAction:(id)sender {
    
    ViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
    [self.navigationController pushViewController:newView animated:YES];
}

- (IBAction)btnProfileAction:(id)sender
{
    if ( [[[NSUserDefaults standardUserDefaults] objectForKey:@"userData"] count])
    {
        UserProfileViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"UserProfileViewController"];
        [self.navigationController pushViewController:newView animated:YES];
    }
    else
    {
        LoginRegistrationViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginRegistrationViewController"];
        [self.navigationController pushViewController:newView animated:YES];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
