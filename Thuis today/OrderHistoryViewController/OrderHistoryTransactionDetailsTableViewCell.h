//
//  OrderHistoryTransactionDetailsTableViewCell.h
//  Thuis today
//
//  Created by Apple on 19/03/18.
//  Copyright © 2018 OFFSHORE SOFTECH CENTRE PVT LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrderHistoryTransactionDetailsTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lbl_deliveryCharges;
@property (weak, nonatomic) IBOutlet UILabel *lbl_serviceCharges;

@property (weak, nonatomic) IBOutlet UILabel *lbl_totalPrice;
@end
