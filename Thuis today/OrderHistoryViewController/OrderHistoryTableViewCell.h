//
//  OrderHistoryTableViewCell.h
//  Thuis today
//
//  Created by Apple on 19/03/18.
//  Copyright © 2018 OFFSHORE SOFTECH CENTRE PVT LTD. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrderHistoryTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lbl_productName;
@property (weak, nonatomic) IBOutlet UILabel *lbl_productQuantity;
@property (weak, nonatomic) IBOutlet UILabel *lbl_preicePerUnit;
@property (weak, nonatomic) IBOutlet UILabel *lbl_totalPrice;

@end
