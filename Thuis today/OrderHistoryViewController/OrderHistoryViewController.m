//
//  OrderHistoryViewController.m
//  Thuis today
//
//  Created by IMMANENT on 28/02/17.
//  Copyright © 2017 IMMANENT. All rights reserved.
//
#define RGB(r, g, b) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:1]
#define RGBA(r, g, b, a) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:a]

#import "OrderHistoryViewController.h"
#import "OrderHistoryTableViewCell.h"
#import "OrderHistoryTransactionDetailsTableViewCell.h"

@interface OrderHistoryViewController ()
{
    NSMutableArray *arrayForOrderDetail,*arrayForUserInfo,*arrayForValues;
    NSArray *values;
    float subtotal;
    NSString *strForServiceCharge;
    NSString *strForDeliveryCharge;
    NSString* cleanString;
}
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *headerViewTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *headerViewTopConstraint_iphone;

@property (weak, nonatomic) IBOutlet UILabel *lbl_remark;
@end

@implementation OrderHistoryViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [CommonMethods configureNavigationBarForViewController:self withTitle:@"Order Detail"];
    arrayForOrderDetail=[[NSMutableArray alloc]init];
    //OrderDetail=[[NSMutableArray alloc]init];
    //_OrderDetail=[[NSMutableArray alloc]init];
    arrayForUserInfo = [[NSMutableArray alloc]init];
    arrayForValues = [[NSMutableArray alloc]init];
    subtotal = 0;
    // Do any additional setup after loading the view.
    [self methodForCallWebServiceOrderHistory];
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    lblCount.layer.cornerRadius = lblCount.frame.size.width/2;
    lblCount.layer.masksToBounds=YES;
    lblStatus.layer.cornerRadius = 5;
    lblStatus.layer.masksToBounds=YES;

    lblPrice.layer.borderWidth= 5;
    lblPrice.layer.borderColor = (__bridge CGColorRef _Nullable)([UIColor lightGrayColor]);
    lblPrice.layer.masksToBounds = YES;
    
    
    
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"cartCount"]!=nil) {
        lblCount.text=[[NSUserDefaults standardUserDefaults] valueForKey:@"cartCount"];
    }
    else
    {
        lblCount.text=@"";
    }
    [[NSUserDefaults standardUserDefaults]setObject:@"no" forKey:@"isCartView"];
    [[NSUserDefaults standardUserDefaults]synchronize];
}

- (void)showRemarkView {
     if (IDIOM == IPAD) {
    [UIView animateWithDuration:0 animations:^{
        self.headerViewTopConstraint.constant = 321;
        [self.view setNeedsLayout];
        [self.view layoutIfNeeded];
    }];
     }
     else {
         [UIView animateWithDuration:0 animations:^{
             self.headerViewTopConstraint_iphone.constant = 207;
             [self.view setNeedsLayout];
             [self.view layoutIfNeeded];
         }];
     }
}

- (void)hideRemarkView {
    if (IDIOM == IPAD) {
        [UIView animateWithDuration:0 animations:^{
            self.headerViewTopConstraint.constant = 221;
            [self.view setNeedsLayout];
            [self.view layoutIfNeeded];
        }];
    }
    else {
        [UIView animateWithDuration:0 animations:^{
            self.headerViewTopConstraint_iphone.constant = 148;
            [self.view setNeedsLayout];
            [self.view layoutIfNeeded];
        }];
    }
}
-(void)methodForDisplayValues
{
    lblOrderNumber.text= [NSString stringWithFormat:@"#%@", [[_OrderDetail objectAtIndex:0]  valueForKey:@"id"]];
    
    lblOrderPlaceDate.text=[[_OrderDetail objectAtIndex:0] valueForKey:@"date"];
    
    
    lblStatus.text=NSLocalizedString(@"Inprogress", @"Cancel");
    
   
    
    if ([[[_OrderDetail objectAtIndex:0]  valueForKey:@"status"] isEqualToString:@"8"])
    {
        lblStatus.text =NSLocalizedString(@"Delivered", @"Cancel");
     
        lblStatus.backgroundColor = [UIColor colorWithRed:0/255.0f green:159/255.0f blue:14/255.0f alpha:1];
    }
    else if([[[_OrderDetail objectAtIndex:0]valueForKey:@"status"] isEqualToString:@"5"])
    {
        lblStatus.text =NSLocalizedString(@"Canceled", @"Cancel");
        lblStatus.backgroundColor = [UIColor colorWithRed:219/255.0f green:38/255.0f blue:54/255.0f alpha:1];
    }
    else if([[[_OrderDetail objectAtIndex:0] valueForKey:@"status"] isEqualToString:@"6"])
    {
        lblStatus.text =NSLocalizedString(@"Order picked", @"Cancel");
        lblStatus.backgroundColor = [UIColor colorWithRed:153/255.0f green:126/255.0f blue:229/255.0f alpha:1];
    }
    else if([[[_OrderDetail objectAtIndex:0]  valueForKey:@"status"] isEqualToString:@"2"])
    {
        lblStatus.text =NSLocalizedString(@"Fresh Order", @"Cancel");
        lblStatus.backgroundColor = [UIColor colorWithRed:0/255.0f green:152/255.0f blue:163/255.0f alpha:1];
    }
    else if([[[_OrderDetail objectAtIndex:0]  valueForKey:@"status"] isEqualToString:@"3"])
    {
        lblStatus.text =NSLocalizedString(@"Order approved", @"Cancel");
        lblStatus.backgroundColor = [UIColor colorWithRed:254/255.0f green:102/255.0f blue:3/255.0f alpha:1];
    }
    else if([[[_OrderDetail objectAtIndex:0] valueForKey:@"status"] isEqualToString:@"4"])
    {
        lblStatus.text =NSLocalizedString(@"Order ready", @"Cancel");
        lblStatus.backgroundColor = [UIColor colorWithRed:106/255.0f green:239/255.0f blue:116/255.0f alpha:1];
    }
    else if([[[_OrderDetail objectAtIndex:0]  valueForKey:@"status"] isEqualToString:@"7"])
    {
        lblStatus.text =NSLocalizedString(@"Transit", @"Cancel");
        lblStatus.backgroundColor = [UIColor colorWithRed:50/255.0f green:154/255.0f blue:207/255.0f alpha:1];
    }
    else if([[[_OrderDetail objectAtIndex:0] valueForKey:@"status"] isEqualToString:@"9"])
    {
        lblStatus.text =NSLocalizedString(@"Not Delivered", @"Cancel");
        lblStatus.backgroundColor = [UIColor colorWithRed:246/255.0f green:44/255.0f blue:63/255.0f alpha:1];
    }
    else if([[[_OrderDetail objectAtIndex:0]  valueForKey:@"status"] isEqualToString:@"11"])
    {
        lblStatus.text =NSLocalizedString(@"Payment Canceled", @"Cancel");
        lblStatus.backgroundColor = [UIColor colorWithRed:219/255.0f green:38/255.0f blue:54/255.0f alpha:1];
    }
    else if([[[_OrderDetail objectAtIndex:0] valueForKey:@"status"] isEqualToString:@"12"])
    {
        lblStatus.text =NSLocalizedString(@"Payment Processing", @"Cancel");
        lblStatus.backgroundColor = [UIColor colorWithRed:223/255.0f green:128/255.0f blue:64/255.0f alpha:1];
    }
    //NSLog(@"%@",arrayForOrderDetail);
    lblPaymentMethod.text=[[_OrderDetail objectAtIndex:0] valueForKey:@"payment_method"];
    
    
    lblDeliveredDate.text = [[_OrderDetail objectAtIndex:0] valueForKey:@"delivery_date"];
    
    lblAddress.text=[[_OrderDetail objectAtIndex:0] valueForKey:@"delivery_address"];
    

}
#pragma mark -
#pragma mark - Method for call webservice
-(void)methodForCallWebServiceOrderHistory
{
     NSLog(@"%@",_OrderDetail);
    
    
    [[AppDelegate shareInstance] showActivityIndicator];
    NSDictionary *params = @{
                             @"order_id":[[_OrderDetail objectAtIndex:0] valueForKey:@"id"]
                             };
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [[SingletonClass sharedManager] m_PostApiResponse:@"orderDetail" paramDiction:params  islogout:NO onCompletion:^(NSDictionary *response)
         {
             NSDictionary *responseDic = (NSDictionary *)response;
             if ([[[responseDic objectForKey:@"payload"]valueForKey:@"status"]integerValue]==1)
             {
                 arrayForOrderDetail=[[responseDic objectForKey:@"payload"] objectForKey:@"data"];
//                NSLog(@"%@",arrayForOrderDetail);
//                 NSData* data = [[arrayForOrderDetail valueForKey:@"cart"] dataUsingEncoding:NSUTF8StringEncoding];
//              NSDictionary   *dic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
//                 for (id key in dic) {
//                     NSLog(@"key: %@, value: %@", key, [dic objectForKey:key]);
//                     [arrayForValues addObject:[dic objectForKey:key]];
//                 }
//                    // NSLog(@"%@",arrayForValues);
//                
//
                 //for (int i=0; i<arrayForValues.count; i++)
                     //                 {
                     //                     NSLog(@"%@",[[arrayForValues objectAtIndex:i] valueForKey:@"subtotal"]);
                     //
                     //                   if ([[NSString stringWithFormat:@"%@",[[arrayForValues objectAtIndex:i] valueForKey:@"subtotal"]] containsString:@","])
                     //                   {
                     //                       NSString* cleanedString = [[[[arrayForValues objectAtIndex:i] valueForKey:@"subtotal"] stringByReplacingOccurrencesOfString:@"," withString:@"."]
                     //                                                  stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
                     //                       subtotal =subtotal+[cleanedString floatValue];
                     //
                     //                   }
                     //                    else
                     //                    {
                     //                          subtotal =subtotal+[[[arrayForValues objectAtIndex:i] valueForKey:@"subtotal"] floatValue];
                     //                    }
                     //
                     //                }
                     //                 //////////////////////////
                     //                 NSLog(@"%@",arrayForValues);
                     //
                     //
                     //                 float floadDelivery = [[NSString stringWithFormat:@"%@",[arrayForOrderDetail valueForKey:@"delivery_charge"]] floatValue];
                     //                 NSString *strDeliveryCharges=NSLocalizedString(@"Delivery charges", @"Cancel");
                     //                 strForDeliveryCharge = [[[NSString stringWithFormat:@"%@:%.2f",strDeliveryCharges,floadDelivery] stringByReplacingOccurrencesOfString:@"." withString:@","]
                     //                                                   stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
                     //                 lblDeliveryCharges.text =strForDeliveryCharge;
                     //                 ///////////////////////////////
                     //                 float floadService = [[NSString stringWithFormat:@"%@",[arrayForOrderDetail valueForKey:@"service_charge"]] floatValue];
                     //                 NSString *strServiceCharge=NSLocalizedString(@"Service charges", @"Cancel");
                     //
                     //                 strForServiceCharge = [[[NSString stringWithFormat:@"%@: %.2f Cent",strServiceCharge,floadService] stringByReplacingOccurrencesOfString:@"." withString:@","]
                     //                                                  stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
                     //                 lblServiceCharges.text=strForServiceCharge;
                     //
                     //
                     //
                     //
                     //                 NSString *str =NSLocalizedString(@"Total Price", @"Cancel");
                     //                 cleanString = [[[NSString stringWithFormat:@"%@:€%@",str,[arrayForOrderDetail valueForKey:@"total_payment"]] stringByReplacingOccurrencesOfString:@"." withString:@","]
                     //                                           stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
                     //                 lblPrice.text = cleanString ;
                     //                // dispatch_async(dispatch_get_main_queue(), ^{
                     //
                     //                 [tblForOrderDetail reloadData];
                     //                 [self methodForDisplayValues];
                 
//                 for (int i=0; i<_OrderDetail.count; i++)
//                                  {
//                                      NSLog(@"%@",[[_OrderDetail objectAtIndex:i] valueForKey:@"subtotal"]);
//                 
//                                    if ([[NSString stringWithFormat:@"%@",[[_OrderDetail objectAtIndex:i] valueForKey:@"subtotal"]] containsString:@","])
//                                    {
//                                        NSString* cleanedString = [[[[_OrderDetail objectAtIndex:i] valueForKey:@"subtotal"] stringByReplacingOccurrencesOfString:@"," withString:@"."]
//                                                                   stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
//                                        subtotal =subtotal+[cleanedString floatValue];
//                 
//                                    }
//                                     else
//                                     {
//                                           subtotal =subtotal+[[[_OrderDetail objectAtIndex:i] valueForKey:@"subtotal"] floatValue];
//                                     }
//                 
//                                 }
                                  //////////////////////////
                                  NSLog(@"%@",_OrderDetail);
                 if([[[arrayForOrderDetail objectAtIndex:0] valueForKey:@"agent_status"]isEqualToString:@""])
                 {
                     txtViewRemark.hidden=YES;
                     self.lbl_remark.hidden = YES;
                     [self hideRemarkView];
                 }
                 else
                 {
                     self.lbl_remark.hidden = NO;
                     txtViewRemark.hidden= NO;
                     [self showRemarkView];
                     txtViewRemark.text=[[arrayForOrderDetail objectAtIndex:0] valueForKey:@"agent_status"];
                 }
                 
                                  float floadDelivery = [[NSString stringWithFormat:@"%@",[[_OrderDetail objectAtIndex:0] valueForKey:@"delivery_charge"]] floatValue];
//                                  NSString *strDeliveryCharges=NSLocalizedString(@"Delivery charges", @"Cancel");
                                  strForDeliveryCharge = [[[NSString stringWithFormat:@"€ %.2f ",floadDelivery] stringByReplacingOccurrencesOfString:@"." withString:@","]
                                                                    stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
                                  //lblDeliveryCharges.text =strForDeliveryCharge;
                                  ///////////////////////////////
                                  float floadService = [[NSString stringWithFormat:@"%@",[[_OrderDetail objectAtIndex:0] valueForKey:@"service_charge"]] floatValue];
                                //  NSString *strServiceCharge=NSLocalizedString(@"Service charges", @"Cancel");
                 
                                  strForServiceCharge = [[[NSString stringWithFormat:@"€ %f ",floadService] stringByReplacingOccurrencesOfString:@"." withString:@","]
                                                                   stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
                                  //lblServiceCharges.text=strForServiceCharge;
                 
                                //  NSString *str =NSLocalizedString(@"Total Price", @"Cancel");
                                  cleanString = [[[NSString stringWithFormat:@"€ %@",[[_OrderDetail objectAtIndex:0] valueForKey:@"total_payment"]] stringByReplacingOccurrencesOfString:@"." withString:@","]
                                                            stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
                                  //lblPrice.text = cleanString ;
                                 // dispatch_async(dispatch_get_main_queue(), ^{
                 
                                  [tblForOrderDetail reloadData];
                                  [self methodForDisplayValues];
                 [[AppDelegate shareInstance] hideActivityIndicator];
                // });
             }
             else
             {
                 [[AppDelegate shareInstance] hideActivityIndicator];
                 UIAlertController * alert=   [UIAlertController
                                               alertControllerWithTitle:nil
                                               message:NSLocalizedString(@"Unable to get detail please try again.", @"Cancel")
                                               preferredStyle:UIAlertControllerStyleAlert];
                 UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                     [self methodForCallWebServiceOrderHistory];
                     
                 }];
                 [alert addAction:okAction];
                 [self presentViewController:alert animated:YES completion:nil];
             }
         }];
    });
    
}
#pragma mark -
#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSLog(@"%lu", (unsigned long)arrayForOrderDetail.count);
    return arrayForOrderDetail.count+1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(IDIOM==IPAD){
        if (indexPath.row == [arrayForOrderDetail count])
        {
            return 200;
        }
        
        else
        {
            return 100;
        }
    }
    else{
        
        if (indexPath.row == [arrayForOrderDetail count])
        {
            return 100;
        }
        
        else
        {
            return 60;
        }
    }
   
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier;
    if(indexPath.row == [arrayForOrderDetail count])
    {
        CellIdentifier = @"OrderHistoryTransactionDetailsTableViewCell";
        OrderHistoryTransactionDetailsTableViewCell *transactionCell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        transactionCell.lbl_serviceCharges.text = [CommonMethods replaceDecimalWithComma:[NSString stringWithFormat:@"€ %@",[[_OrderDetail objectAtIndex:0] valueForKey:kService_Charge]]];
        transactionCell.lbl_deliveryCharges.text = [CommonMethods replaceDecimalWithComma:[NSString stringWithFormat:@"€ %@",[[_OrderDetail objectAtIndex:0] valueForKey:kDelivery_Charge]]];
        transactionCell.lbl_totalPrice.text = [CommonMethods replaceDecimalWithComma:[NSString stringWithFormat:@"€ %@",[[_OrderDetail objectAtIndex:0] valueForKey:@"total_payment"]]];
        transactionCell.backgroundColor = [UIColor whiteColor];
        return transactionCell;
        
    }
    else
    {
        CellIdentifier = @"OrderHistoryTableViewCell";
        OrderHistoryTableViewCell *orderDetailsCell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        NSDictionary *orderDetails = [arrayForOrderDetail objectAtIndex:indexPath.row];
        orderDetailsCell.lbl_productName.text = [orderDetails valueForKey:@"name"];
        orderDetailsCell.lbl_productQuantity.text = [NSString stringWithFormat:@"%@",[orderDetails valueForKey:@"qty"]];
        
        orderDetailsCell.lbl_preicePerUnit.text = [CommonMethods replaceDecimalWithComma:[NSString stringWithFormat:@"€ %@",[orderDetails valueForKey:@"price"]]];
        
         orderDetailsCell.lbl_totalPrice.text = [CommonMethods replaceDecimalWithComma:[NSString stringWithFormat:@"€ %@",[orderDetails valueForKey:@"total_price_pro"]]];
        orderDetailsCell.backgroundColor = RGB(241, 241, 241);
        
        orderDetailsCell.selectionStyle = UITableViewCellSelectionStyleNone;
        return orderDetailsCell;
    }
    
}
//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//
//}
#pragma mark -
#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)btnCartAction:(id)sender
{
    CartViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"CartViewController"];
    [self.navigationController pushViewController:newView animated:YES];
}

- (IBAction)btnMenuAction:(id)sender
{
    [self.menuContainerViewController toggleLeftSideMenuCompletion:nil];

}
- (IBAction)btnGalleryAction:(id)sender
{
    
}

- (IBAction)btnCameraAction:(id)sender
{
    
}

@end
