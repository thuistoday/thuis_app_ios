//
//  OrderHistoryViewController.h
//  Thuis today
//
//  Created by IMMANENT on 28/02/17.
//  Copyright © 2017 IMMANENT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MFSideMenu.h"
@interface OrderHistoryViewController : UIViewController
{
    
     IBOutlet UILabel *lblCount;
    
     IBOutlet UILabel *lblOrderNumber;
    
     IBOutlet UILabel *lblOrderPlaceDate;
    
     IBOutlet UILabel *lblStatus;
     IBOutlet UILabel *lblPaymentMethod;
    
     IBOutlet UILabel *lblDeliveredDate;
    
     IBOutlet UILabel *lblAddress;
    
     IBOutlet UITableView *tblForOrderDetail;
    
    IBOutlet UILabel *lblPrice;
    
     IBOutlet UILabel *lblDeliveryCharges;
    
     IBOutlet UILabel *lblServiceCharges;
    
     IBOutlet UIView *viewForTotalPrice;
   
    __weak IBOutlet UITextView *txtViewRemark;
}

@property NSString *strForOrderId;
//@property  NSMutableArray *OrderDetail;
@property (nonatomic, strong) IBOutlet NSMutableArray *OrderDetail;
- (IBAction)btnCartAction:(id)sender;

- (IBAction)btnMenuAction:(id)sender;

@end
