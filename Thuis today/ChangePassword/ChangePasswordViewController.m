//
//  ChangePasswordViewController.m
//  Thuis today
//
//  Created by IMMANENT on 18/03/17.
//  Copyright © 2017 IMMANENT. All rights reserved.
//

#import "ChangePasswordViewController.h"

@interface ChangePasswordViewController ()
{
    NSMutableArray *arrayForPlaceholder,*arrayForHideFields,*arrayForuserData;
    NSMutableDictionary *dicForTextFieldvalues;
    NSIndexPath *newIndexPath;
    BOOL checkForEmptyField;
    int noOfRows;
}
@end

@implementation ChangePasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [CommonMethods configureNavigationBarForViewController:self withTitle:@"Change Password"];
    arrayForPlaceholder = [[NSMutableArray alloc]initWithObjects:NSLocalizedString(@"Old Password", @"Cancel"),NSLocalizedString(@"New Password", @"Cancel"),NSLocalizedString(@"Confirm Password", @"Cancel"), nil];
//    arrayForHideFields = [[NSMutableArray alloc]initWithObjects:@"New Password",@"Confirm Password", nil];
    dicForTextFieldvalues = [[NSMutableDictionary alloc]init];
    noOfRows=0;
    checkForEmptyField=false;
    
    // Do any additional setup after loading the view.

}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    arrayForuserData =[[NSMutableArray alloc]init];
    arrayForuserData =[[NSUserDefaults standardUserDefaults] objectForKey:@"userData"];
    [[NSUserDefaults standardUserDefaults]setObject:@"no" forKey:@"isCartView"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    if (_checkForFields ==false)
    {
        noOfRows =5;
        arrayForPlaceholder = [[NSMutableArray alloc]initWithObjects:NSLocalizedString(@"Old Password", @"Cancel"),NSLocalizedString(@"New Password", @"Cancel"),NSLocalizedString(@"Confirm Password", @"Cancel"), nil];
        [dicForTextFieldvalues setObject:@"" forKey:@"oldPassword"];
        [dicForTextFieldvalues setObject:@"" forKey:@"newPassword"];
        [dicForTextFieldvalues setObject:@"" forKey:@"confirmPassword"];

    }
    else
    {
        noOfRows =4;
        arrayForPlaceholder = [[NSMutableArray alloc]initWithObjects:NSLocalizedString(@"New Password", @"Cancel"),NSLocalizedString(@"Confirm Password", @"Cancel"), nil];
        
        [dicForTextFieldvalues setObject:@"" forKey:@"newPassword"];
        [dicForTextFieldvalues setObject:@"" forKey:@"confirmPassword"];

    }
    
    lblCount.layer.cornerRadius = lblCount.frame.size.width/2;
    lblCount.layer.masksToBounds=YES;
    
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"cartCount"]!=nil) {
        lblCount.text=[[NSUserDefaults standardUserDefaults] valueForKey:@"cartCount"];
    }
    else
    {
        lblCount.text=@"";
    }
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
           return noOfRows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier;
    UITableViewCell *cell ;
    if(indexPath.row ==0)
    {
        CellIdentifier = @"cell1";
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        return cell;
    }
    else if(indexPath.row ==noOfRows-1)
    {
        CellIdentifier = @"cell3";
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        return cell;
    }
    else
    { CellIdentifier = @"cell2";
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        UITextField *txtField = (UITextField*)[cell viewWithTag:101];
        txtField.placeholder =[arrayForPlaceholder objectAtIndex:indexPath.row-1];
        
        ///UIColor *color = [UIColor colorWithRed:210/255.0f green:45/255.0f blue:62/255.0f alpha:1];
        UIColor *color = [UIColor lightGrayColor];
      
        [txtField setValue:color forKeyPath:@"_placeholderLabel.textColor"];
        if (_checkForFields == false) {
            txtField.placeholder =[arrayForPlaceholder objectAtIndex:indexPath.row-1];
            
            if (indexPath.row ==1) {
                txtField.text =[dicForTextFieldvalues valueForKey:@"oldPassword"];
            }
            else if(indexPath.row ==2)
            {
                txtField.text = [dicForTextFieldvalues valueForKey:@"newPassword"];
            }
            else if(indexPath.row ==3)
            {
                txtField.text = [dicForTextFieldvalues valueForKey:@"confirmPassword"];
            }
        }
        else
        {
            txtField.placeholder =[arrayForPlaceholder objectAtIndex:indexPath.row-1];
            if (indexPath.row ==1) {
                txtField.text =[dicForTextFieldvalues valueForKey:@"newPassword"];
            }
            else if(indexPath.row ==2)
            {
                txtField.text = [dicForTextFieldvalues valueForKey:@"confirmPassword"];
            }
        }
        return cell;
       
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ( IDIOM == IPAD )
    {
        if (indexPath.row ==0 || indexPath.row == noOfRows-1 )
        {
            return 110;
        }
        else
        {
            return 90;
        }
    }
    else
    {
//        if (indexPath.row ==0 || indexPath.row == 1)
//        {
//            return 70;
//        }
//        else
//        {
//            return 40;
//        }
         return 54;
    }
}
#pragma -mark UITextField delegates

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    CGPoint origin = textField.frame.origin;
    CGPoint point = [textField.superview convertPoint:origin toView:tblFields];
    newIndexPath = [tblFields indexPathForRowAtPoint:point];
    
    NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    newString =     [newString stringByTrimmingCharactersInSet:
                     [NSCharacterSet whitespaceCharacterSet]];
    
    if (_checkForFields ==false)
    {
        if (newIndexPath.row ==1)
        {
            [dicForTextFieldvalues setObject:newString forKey:@"oldPassword"];
        }
        else if(newIndexPath.row ==2)
        {
            [dicForTextFieldvalues setObject:newString forKey:@"newPassword"];
           
            
        }
        else if(newIndexPath.row ==3)
        {
            [dicForTextFieldvalues setObject:newString forKey:@"confirmPassword"];
        }
    }
    else
    {
        if(newIndexPath.row ==1)
        {
            [dicForTextFieldvalues setObject:newString forKey:@"newPassword"];
        }
        else if(newIndexPath.row ==2)
        {
            [dicForTextFieldvalues setObject:newString forKey:@"confirmPassword"];
        }
    }
    [tblFields beginUpdates];
    [tblFields endUpdates];
    
    return true;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
- (IBAction)btnSavePasswordAction:(id)sender
{   BOOL isRightPass=NO;
    BOOL isOnceDone=NO;
    NSLog(@"%@",[dicForTextFieldvalues allValues]);
//    for (id currentValue in [dicForTextFieldvalues allValues])
//    {
    for (id currentValue in [dicForTextFieldvalues allValues])
        {
        if (![currentValue isEqualToString:@""])
        {
            checkForEmptyField = true;
    }
        else
        {
            checkForEmptyField = false;
            [[AppDelegate shareInstance] showAlertWithErrorMessage:  NSLocalizedString(@"Please fill all fields.", @"Cancel")];
            if (_checkForFields ==false)
            {
                [dicForTextFieldvalues setObject:@"" forKey:@"oldPassword"];
                [dicForTextFieldvalues setObject:@"" forKey:@"newPassword"];
                [dicForTextFieldvalues setObject:@"" forKey:@"confirmPassword"];
            }
            else
            {
              
                [dicForTextFieldvalues setObject:@"" forKey:@"newPassword"];
                [dicForTextFieldvalues setObject:@"" forKey:@"confirmPassword"];
            }
            return;
        }
    }
    if (checkForEmptyField == true)
    {
         [self methodForCallWebservice];
//        NSString *pass =[dicForTextFieldvalues valueForKey:@"newPassword"];
//                        if([pass length] >= 8)
//                        {
//                            BOOL lowerCaseLetter=0,upperCaseLetter=0,digit=0,specialCharacter = 0;
//                            for (int i = 0; i < [pass length]; i++)
//                            {
//                                unichar c = [pass characterAtIndex:i];
//                                if(!lowerCaseLetter)
//                                {
//                                    lowerCaseLetter = [[NSCharacterSet lowercaseLetterCharacterSet] characterIsMember:c];
//                                }
////                                if(!upperCaseLetter)
////                                {
////                                    upperCaseLetter = [[NSCharacterSet uppercaseLetterCharacterSet] characterIsMember:c];
////                                }
//                                if(!digit)
//                                {
//                                    digit = [[NSCharacterSet decimalDigitCharacterSet] characterIsMember:c];
//                                }
//                                if(!specialCharacter)
//                                {
//                                    NSRange range;
//                                    NSString* s = [NSString stringWithCharacters:&c length:1];
//                                    NSCharacterSet *lowerCaseChars = [NSCharacterSet characterSetWithCharactersInString:@"!@#$%^&*()_+"];
//                                    range = [s rangeOfCharacterFromSet:lowerCaseChars];
//                                    if ( !range.length )
//                                    {
//                                        specialCharacter=NO;
//                                    }
//                                    else{
//                                        specialCharacter= YES;
//                                    }
//                                    //specialCharacter = [[NSCharacterSet symbolCharacterSet] characterIsMember:c];
//                                }
//                            }
//        
//                            if(specialCharacter && digit && lowerCaseLetter )
//                            {
//
//                            }
//                            else
//                            {
//                                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
//                                                                                message:@"Please Ensure that newpassword have at least one lower case letter, one upper case letter, one digit and one special character"
//                                                                               delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//                                [alert show];
//                                
//                            }
//        
//                        }
//                        else
//                        {
//                            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
//                                                                            message:@"Please Enter at least 8 password"
//                                                                           delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//                            [alert show];
//                            
//                        }
    }
    else{
       [[AppDelegate shareInstance]showAlertWithErrorMessage:NSLocalizedString(@"Please fill all fields.", @"Cancel")];
        if (_checkForFields ==false)
        {
            [dicForTextFieldvalues setObject:@"" forKey:@"oldPassword"];
            [dicForTextFieldvalues setObject:@"" forKey:@"newPassword"];
            [dicForTextFieldvalues setObject:@"" forKey:@"confirmPassword"];
        }
        else
        {
            
            [dicForTextFieldvalues setObject:@"" forKey:@"newPassword"];
            [dicForTextFieldvalues setObject:@"" forKey:@"confirmPassword"];
        }
        return;
    }
}

- (IBAction)btnMenuAction:(id)sender
{
    [self.menuContainerViewController toggleLeftSideMenuCompletion:nil];
}

- (IBAction)btnCartAction:(id)sender
{
    CartViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"CartViewController"];
    [self.navigationController pushViewController:newView animated:YES];
}
-(void)methodForCallWebservice
{
    [self.view endEditing:YES];
    
        if ([[dicForTextFieldvalues valueForKey:@"newPassword"] isEqualToString:[dicForTextFieldvalues valueForKey:@"confirmPassword"]]) {
            [[AppDelegate shareInstance] showActivityIndicator];
            NSDictionary *params;
            if (_checkForFields ==false)
            {
                params = @{
                           @"user_id":[arrayForuserData valueForKey:@"id"],
                           @"password":[dicForTextFieldvalues valueForKey:@"newPassword"],
                           @"old_password":[dicForTextFieldvalues valueForKey:@"oldPassword"]
                           };

            }
            else
            {
                params = @{
                            @"user_id":_textUserId,
                            @"password":[dicForTextFieldvalues valueForKey:@"newPassword"]
                        };
            }
            
           dispatch_async(dispatch_get_main_queue(), ^{
                
                [[SingletonClass sharedManager] m_PostApiResponse:@"new_password?" paramDiction:params  islogout:NO onCompletion:^(NSDictionary *response)
                 {
                     NSDictionary *responseDic = (NSDictionary *)response;
                     if ([[[responseDic objectForKey:@"payload"]valueForKey:@"status"]integerValue]==1)
                     {
                         dispatch_async(dispatch_get_main_queue(), ^{
                             [[AppDelegate shareInstance] hideActivityIndicator];
                             UIAlertController * alert=   [UIAlertController
                                                           alertControllerWithTitle:nil
                                                           message:[[responseDic objectForKey:@"payload"] valueForKey:@"message"]
                                                           preferredStyle:UIAlertControllerStyleAlert];
                             UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                                 ViewController  *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
                                 [self.navigationController pushViewController:newView animated:YES];
                                 
                             }];
                             [alert addAction:okAction];
                             [self presentViewController:alert animated:YES completion:nil];
                             
                         });
                         
                     }
                     else
                     {
                         dispatch_async(dispatch_get_main_queue(), ^{
                                [[AppDelegate shareInstance] hideActivityIndicator];
                             [[AppDelegate shareInstance] showAlertWithErrorMessage:[[responseDic objectForKey:@"payload"]valueForKey:@"message"]];
                         });
                     }
                 }];
            });
        }
        else
        {
            
            [[AppDelegate shareInstance] hideActivityIndicator];
            [[AppDelegate shareInstance] showAlertWithErrorMessage:  NSLocalizedString(@"Password do not match.", @"Cancel")];
            if (_checkForFields ==false)
            {
                [dicForTextFieldvalues setObject:@"" forKey:@"oldPassword"];
                [dicForTextFieldvalues setObject:@"" forKey:@"newPassword"];
                [dicForTextFieldvalues setObject:@"" forKey:@"confirmPassword"];
            }
            else
            {
                [dicForTextFieldvalues setObject:@"" forKey:@"newPassword"];
                [dicForTextFieldvalues setObject:@"" forKey:@"confirmPassword"];
            }
            [tblFields reloadData];
        }
}

@end
