//
//  ChangePasswordViewController.h
//  Thuis today
//
//  Created by IMMANENT on 18/03/17.
//  Copyright © 2017 IMMANENT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChangePasswordViewController : UIViewController<UITextFieldDelegate>
{
    
     IBOutlet UITableView *tblFields;
     IBOutlet UILabel *lblCount;
    
}
- (IBAction)btnSavePasswordAction:(id)sender;
- (IBAction)btnMenuAction:(id)sender;
- (IBAction)btnCartAction:(id)sender;
@property BOOL checkForFields;
@property NSString *textUserId;
@end
