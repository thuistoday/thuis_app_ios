//
//  AboutUsViewController.h
//  Thuis today
//
//  Created by offshore_mac_1 on 18/08/17.
//  Copyright © 2017 IMMANENT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
@interface AboutUsViewController : UIViewController<MFMailComposeViewControllerDelegate>
{
    IBOutlet UILabel *lblCount;
    
}

- (IBAction)btnCartAction:(id)sender;
- (IBAction)btnMenuAction:(id)sender;


- (IBAction)btnBottomBackAction:(id)sender;
- (IBAction)btnBottomCartAction:(id)sender;
- (IBAction)btnBottomHomeAction:(id)sender;
@end
