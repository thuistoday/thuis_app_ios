//
//  ViewController.h
//  Thuis today
//
//  Created by IMMANENT on 11/01/17.
//  Copyright © 2017 IMMANENT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
{
    IBOutlet UIView *viewForPopUp;
    
     IBOutlet UITableView *tblForCategories;
    
     IBOutlet UITextField *txtCity;
     IBOutlet UILabel *lblCount;
    
    __weak IBOutlet UILabel *lblChooseTheShopTpe;
    
}
- (IBAction)btnCloseAction:(id)sender;
- (IBAction)btnChooseAction:(id)sender;
- (IBAction)btnActionMenu:(id)sender;
- (IBAction)btnChooseCategorieAction:(id)sender;
- (IBAction)btnSearchShop:(id)sender;
- (IBAction)btnBottomCartAction:(id)sender;
- (IBAction)btnFBClick:(id)sender;


@end

