//
//  AppDelegate.h
//  Thuis today
//
//  Created by IMMANENT on 11/01/17.
//  Copyright © 2017 IMMANENT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
@import UserNotifications;
#endif
@interface AppDelegate : UIResponder <UIApplicationDelegate,UNUserNotificationCenterDelegate>
#pragma mark -
#pragma mark - AppDelegate Shared Method
+(AppDelegate*)shareInstance;

#pragma mark -
#pragma mark - Alert View Method
-(void) showAlertWithErrorMessage:(NSString *)message;
#pragma mark -
#pragma mark - Show ActivityIndicator
-(void)showActivityIndicator;
#pragma mark -
#pragma mark - Hide ActivityIndicator
-(void)hideActivityIndicator;
@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong) NSPersistentContainer *persistentContainer;

- (void)saveContext;
-(void)reloadSideMenu;

@end

