//
//  NeedAHelpViewController.m
//  Thuis today
//
//  Created by IMMANENT on 30/06/17.
//  Copyright © 2017 IMMANENT. All rights reserved.
//

#import "NeedAHelpViewController.h"

@interface NeedAHelpViewController ()<UICollectionViewDelegate,UICollectionViewDataSource>
{
    NSMutableArray *imgArray;
    NSMutableArray *arrayForUrl,*arrayForTitle;
}

@end

@implementation NeedAHelpViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [CommonMethods configureNavigationBarForViewController:self withTitle:@"Need Help?"];
    // Do any additional setup after loading the view.
    imgArray = [[NSMutableArray alloc]initWithObjects:@"buy",@"pay",@"quality",@"delivery",@"services",@"aaccount", nil];
//    arrayForUrl = [[NSMutableArray alloc]initWithObjects:@"http://thuistoday.demodemo.ga/help/app#how-to-buy",@"http://thuistoday.demodemo.ga/help/app#how-to-pay",@"http:////thuistoday.demodemo.ga/help/app#q-guaranteed",@"http://thuistoday.demodemo.ga/help/app#about-delivery",@"http://thuistoday.demodemo.ga/help/app#service", nil];
     arrayForUrl = [[NSMutableArray alloc]initWithObjects:@"http://thuis.today/help/app#how-to-buy",@"http://thuis.today/help/app#how-to-pay",@"http://thuis.today/help/app#q-guaranteed",@"http://thuis.today/help/app#about-delivery",@"http://thuis.today/help/app#service", nil];
       arrayForTitle = [NSMutableArray arrayWithObjects:NSLocalizedString(@"How to buy", @"Cancel"),NSLocalizedString(@"How to pay", @"Cancel"),NSLocalizedString(@"Quality guaranteed", @"Cancel"),NSLocalizedString(@"About delivery", @"Cancel"),NSLocalizedString(@"Services & Conditions", @"Cancel"),NSLocalizedString(@"My account", @"Cancel"),nil];
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [[NSUserDefaults standardUserDefaults]setObject:@"no" forKey:@"isCartView"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    lblCount.layer.cornerRadius = lblCount.frame.size.width/2;
    lblCount.layer.masksToBounds=YES;
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"cartCount"]!=nil) {
        lblCount.text=[[NSUserDefaults standardUserDefaults] valueForKey:@"cartCount"];
    }
    else
    {
        lblCount.text=@"";
    }
}
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    return 6;
}

-(UICollectionViewCell*)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *identifier = @"ReuseCell";
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    UIImageView *imgHelp = (UIImageView*)[cell viewWithTag:101];
    UILabel *lblTitle = (UILabel*)[cell viewWithTag:102];
    lblTitle.text =[arrayForTitle objectAtIndex:indexPath.row];
    imgHelp.image = [UIImage imageNamed:[imgArray objectAtIndex:indexPath.row]];
    return cell;
    
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row == 5)
    {
        if ( [[[NSUserDefaults standardUserDefaults] objectForKey:@"userData"] count])
        {
            UserProfileViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"UserProfileViewController"];
            [self.navigationController pushViewController:newView animated:YES];
        }
        else
        {
            LoginRegistrationViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginRegistrationViewController"];
            [self.navigationController pushViewController:newView animated:YES];
        }
    }
    else
    {
        NeedHelpWebViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"NeedHelpWebViewController"];
        newView.url = [arrayForUrl objectAtIndex:indexPath.row];
        [self.navigationController pushViewController:newView animated:YES];
    }
}
- (IBAction)btnBottomBackAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnBottomCartAction:(id)sender
{
    CartViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"CartViewController"];
    
    [self.navigationController pushViewController:newView animated:YES];
}

- (IBAction)btnBottomHomeAction:(id)sender
{
    ViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
    [self.navigationController pushViewController:newView animated:YES];
}
- (IBAction)btnCartAction:(id)sender
{
    CartViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"CartViewController"];
    [self.navigationController pushViewController:newView animated:YES];
}

- (IBAction)btnMenuAction:(id)sender
{
    [self.menuContainerViewController toggleLeftSideMenuCompletion:nil];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
