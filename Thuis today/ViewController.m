//
//  ViewController.m
//  Thuis today
//
//  Created by IMMANENT on 11/01/17.
//  Copyright © 2017 IMMANENT. All rights reserved.
//
#define ACCEPTABLE_CHARACTERS @"0123456789"
#import "ViewController.h"
#import "MFSideMenu.h"
@interface ViewController ()<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>
{
    NSMutableArray *arrayForSelectedIndexes,*arrayForCategories,*arrayForStoreCategoriesIds,*arrayForCities;
    NSString *categorieIds;
    BOOL checkViewForPopUpIsHidden,checkForCategorieTableView;
    NSArray *arrayCollectData;
}
@property (weak, nonatomic) IBOutlet UILabel *lbl_versionNumber;
@property (weak, nonatomic) IBOutlet UIView *navigationBackgroundView;
@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [CommonMethods configureNavigationBarForViewController:self withTitle:@"Welcome"];
    
    txtCity.autocorrectionType = UITextAutocorrectionTypeNo;
    viewForPopUp.hidden =YES;
 
    arrayForSelectedIndexes = [[NSMutableArray alloc]init];
    arrayForCategories = [[NSMutableArray alloc]init];
    arrayForStoreCategoriesIds= [[NSMutableArray alloc]init];
    arrayForCities = [[NSMutableArray alloc]init];
   
    self.lbl_versionNumber.text = [CommonMethods appVersionNumber];
    NSDate *now = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"EEE"];
    NSLog(@"%@",[dateFormatter stringFromDate:now]);
    
    if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"order_Status"] isEqualToString:@"Your order has been placed."])
    {
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:nil
                                      message:@"Your order has been placed."
                                      preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                   {
                                      [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"order_Status"];
                                   }];
        [alert addAction:okAction];
        [self presentViewController:alert animated:YES completion:nil];

    }
}

- (NSString *)hexStringForColor:(UIColor *)color {
    const CGFloat *components = CGColorGetComponents(color.CGColor);
    CGFloat r = components[0];
    CGFloat g = components[1];
    CGFloat b = components[2];
    NSString *hexString=[NSString stringWithFormat:@"%02X%02X%02X", (int)(r * 255), (int)(g * 255), (int)(b * 255)];
    return hexString;
}

-(void)viewWillAppear:(BOOL)animated
{
   
    [super viewWillAppear:YES];
    [self callWebServiceForGetAllCategories];
    lblCount.layer.cornerRadius = lblCount.frame.size.width/2;
    lblCount.layer.masksToBounds=YES;
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"cartCount"]!=nil) {
        lblCount.text = [[NSUserDefaults standardUserDefaults] valueForKey:@"cartCount"];
    }
    else
    {
        lblCount.text=@"";
    }
    
    checkForCategorieTableView =false;
        [[NSUserDefaults standardUserDefaults]setObject:@"no" forKey:@"isCartView"];
    [[NSUserDefaults standardUserDefaults]synchronize];

}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.view endEditing:YES];
}

- (IBAction)btnCartAction:(UIButton *)sender {
    [self performSegueWithIdentifier:@"NavigateToCartViewController" sender:nil];
}

- (void)callWebServiceForGetAllCategories
{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [[SingletonClass sharedManager] m_PostApiResponse:@"shopType" paramDiction:nil islogout:NO  onCompletion:^(NSDictionary *response)
         {
             NSDictionary *responseDic = (NSDictionary *)response;
             if ([[[responseDic objectForKey:@"payload"]valueForKey:@"status"]integerValue]==1)
             {
                 arrayForCategories = [[responseDic objectForKey:@"payload"] objectForKey:@"data"];
                 
                 [tblForCategories reloadData];
                 if (checkViewForPopUpIsHidden == true)
                 {
                     viewForPopUp.hidden=NO;
                     checkViewForPopUpIsHidden = false;
                 }
            }
            else
            {
                
            }
         }];
    });
}

#pragma -mark UITableView Delegates
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (checkForCategorieTableView == false)
    {
        return arrayForCategories.count;
    }
    else
    {
         return arrayForCities.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    UIImageView *imgForCheck = (UIImageView*)[cell viewWithTag:101];
    
    UILabel *lblTitle = (UILabel*)[cell viewWithTag:102];
    if(checkForCategorieTableView == false)
    {
        imgForCheck.hidden = NO;
        lblTitle.text =[[arrayForCategories objectAtIndex:indexPath.row] valueForKey:@"category_name"];
        if ([arrayForSelectedIndexes containsObject:[NSString stringWithFormat:@"%ld",(long)indexPath.row]])
        {
            imgForCheck.image=[UIImage imageNamed:@"green-checkbox@1x"];
        }
        else
        {
            imgForCheck.image=[UIImage imageNamed:@"check-box@1x"];
        }
    }
    else
    {
        imgForCheck.hidden = YES;
        lblTitle.text =[NSString stringWithFormat:@"%@,%@",[[arrayForCities objectAtIndex:indexPath.row] valueForKey:@"straatnaam_utf8_nen"],[[arrayForCities objectAtIndex:indexPath.row] valueForKey:@"plaatsnaam_utf8_nen"]];
        if(IDIOM==IPAD) {
            [lblTitle setFont:[UIFont fontWithName:@"HelveticaNeue" size:17]];
        }
        else {
            [lblTitle setFont:[UIFont fontWithName:@"HelveticaNeue" size:13]];
        }
        
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (checkForCategorieTableView ==false) {
        if ([arrayForSelectedIndexes containsObject:[NSString stringWithFormat:@"%ld",(long)indexPath.row]]) {
            [arrayForSelectedIndexes removeObject:[NSString stringWithFormat:@"%ld",(long)indexPath.row]];
            [arrayForStoreCategoriesIds removeObject:[[arrayForCategories objectAtIndex:indexPath.row] valueForKey:@"category_id"]];
        }
        else
        {
            [arrayForSelectedIndexes addObject:[NSString stringWithFormat:@"%ld",(long)indexPath.row]];
            [arrayForStoreCategoriesIds addObject:[[arrayForCategories objectAtIndex:indexPath.row] valueForKey:@"category_id"]];
        }
        [tableView reloadData];
    }
    else
    {
        ShopListViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"ShopListViewController"];
        newView.cityName =[NSString stringWithFormat:@"%@,%@",[[arrayForCities objectAtIndex:indexPath.row] valueForKey:@"straatnaam_utf8_nen"],[[arrayForCities objectAtIndex:indexPath.row] valueForKey:@"plaatsnaam_utf8_nen"]];
        if (![categorieIds isEqualToString:@""]) {
            newView.categorieIds= categorieIds;
        }
        [self.navigationController pushViewController:newView animated:YES];
    }
  

}

- (IBAction)btnCloseAction:(id)sender
{
    viewForPopUp.hidden =YES;
     checkForCategorieTableView =false;
    [tblForCategories reloadData];
}

- (IBAction)btnChooseAction:(id)sender
{
    lblChooseTheShopTpe.text=@"Choose The Shop Type";
    [txtCity resignFirstResponder ];
   checkForCategorieTableView = false;
    if (arrayForCategories.count)
    {
        viewForPopUp.hidden =NO;
        [tblForCategories reloadData];
    }
    else
    {
        checkViewForPopUpIsHidden=true;
        [self callWebServiceForGetAllCategories];
    }
}

- (IBAction)btnActionMenu:(id)sender
{
    [self.menuContainerViewController toggleLeftSideMenuCompletion:nil];
}

- (IBAction)btnChooseCategorieAction:(id)sender
{
    viewForPopUp.hidden =YES;
    if (arrayForStoreCategoriesIds.count)
    {
        categorieIds= @"";
        categorieIds= [arrayForStoreCategoriesIds componentsJoinedByString:@","];
    }
    else
    {
        categorieIds= @"";
    }
}

- (IBAction)btnSearchShop:(id)sender
{
    [txtCity resignFirstResponder];
    if([txtCity.text isEqualToString:@""])
    {
        [[AppDelegate shareInstance] showAlertWithErrorMessage:@"Please Enter Area Code"];
    }
    else
    {
        NSString *txtValue = [txtCity.text stringByTrimmingCharactersInSet:
                             [NSCharacterSet whitespaceCharacterSet]];
        
        NSString *areaCode = [txtValue substringWithRange:NSMakeRange(0,txtValue.length)];
        
        id<GAITracker> tracker = [GAI sharedInstance].defaultTracker;
        [tracker set:kGAIScreenName value:@"Find Shop Screen"];
        [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
        
        [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Find Shop"
                                                              action:[NSString stringWithFormat:@"%@", areaCode]
                                                               label:nil
                                                               value:nil] build]];
        
        categorieIds = [categorieIds stringByTrimmingCharactersInSet:
                       [NSCharacterSet whitespaceCharacterSet]];
        if (![txtValue isEqualToString:@""])
        {
            if ([self isNumeric:areaCode] == true)
            {
                ShopListViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"ShopListViewController"];
                if (![categorieIds isEqualToString:@""]) {
                    newView.categorieIds= categorieIds;
                }
                newView.zipCode = areaCode;
                [self.navigationController pushViewController:newView animated:YES];
            }
            else
            {
                
                checkForCategorieTableView =true;
                [self callWebServiceForGetAllCities:txtValue];
            }
            
        }
        if(checkForCategorieTableView == false)
        {
            lblChooseTheShopTpe.text=@"Choose The Shop Type";
        }
        else
        {
            lblChooseTheShopTpe.text=@"Choose The City";
        }
    }
    
}

- (IBAction)btnBottomCartAction:(id)sender
{
    CartViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"CartViewController"];
    [self.navigationController pushViewController:newView animated:YES];
}

- (IBAction)btnFBClick:(id)sender {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://www.facebook.com/thuistoday"]];
}

- (IBAction)btnResetPasswordAction:(id)sender {
}
-(BOOL)isNumeric:(NSString*)inputString{
    NSCharacterSet *alphaNumbersSet = [NSCharacterSet decimalDigitCharacterSet];
    NSCharacterSet *stringSet = [NSCharacterSet characterSetWithCharactersInString:inputString];
    return [alphaNumbersSet isSupersetOfSet:stringSet];
}
-(void)callWebServiceForGetAllCities:(NSString *)cityname
{
    
    [[AppDelegate shareInstance]showActivityIndicator];
    
    NSDictionary *params = @{
                            @"cityname":cityname
                            };
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [[SingletonClass sharedManager] m_PostApiResponse:@"findStreetCity" paramDiction:params  islogout:NO onCompletion:^(NSDictionary *response)
         {
             [[AppDelegate shareInstance]hideActivityIndicator];

             NSDictionary *responseDic = (NSDictionary *)response;
             if ([[[responseDic objectForKey:@"payload"]valueForKey:@"status"]integerValue]==1)
             {
                 arrayForCities = [[responseDic objectForKey:@"payload"] objectForKey:@"data"];
                 
                 [tblForCategories reloadData];
                 viewForPopUp.hidden=NO;
             }
             else
             {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     [[AppDelegate shareInstance]hideActivityIndicator];
                     [[AppDelegate shareInstance] showAlertWithErrorMessage:[[responseDic objectForKey:@"payload"]valueForKey:@"message"]];
                 });
             }
         }];
    });    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [txtCity resignFirstResponder];
}
-(void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(BOOL)checkForValidCharacter:(NSString*)str
{
    NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:ACCEPTABLE_CHARACTERS] invertedSet];
    NSString *filtered = [[str componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
    if ([str isEqualToString:filtered])
    {
        return true;
    }
    else
    {
        return false;
    }
}
//- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
//{
//    if ([self checkForValidCharacter:string])
//    {
//        return true;
//    }
//    else
//    {
//        return  false;
//    }
//}
@end
