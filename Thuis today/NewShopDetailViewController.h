//
//  NewShopDetailViewController.h
//  Thuis today
//
//  Created by offshore_mac_1 on 05/01/18.
//  Copyright © 2018 IMMANENT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewShopDetailViewController : UIViewController<UITextFieldDelegate,UISearchBarDelegate>
{
    IBOutlet UIView *viewForCategory;
    
    IBOutlet UITableView *tableViewForShopsList;
    IBOutlet UITableView *tableViewForCategory;
    
    __weak IBOutlet UITextView *lblproductDesc;
    __weak IBOutlet UIView *viewSearchBar;
    IBOutlet UILabel *lblCount;
        
    IBOutlet UILabel *lblShopAddress;
    
    IBOutlet UIImageView *imgShopLogo;
    
    IBOutlet UILabel *lblShopDescription;
    
    
    __weak IBOutlet UIImageView *imgViewProdBackground;
    __weak IBOutlet UILabel *lblPrice;
    __weak IBOutlet UIImageView *imgProductImage;
    
    __weak IBOutlet UILabel *lblProductName;
    __weak IBOutlet UIView *viewPopProductDetail;
    __weak IBOutlet UIImageView *imgTableviewSep;
    IBOutlet UILabel *lblOpenCLose;
    __weak IBOutlet UISearchBar *searchBar;
    __weak IBOutlet UIView *viewTop;
    __weak IBOutlet UIView *viewbottom;
}
- (IBAction)btnCloseSearchBar:(id)sender;
- (IBAction)btnCategoryAction:(id)sender;
- (IBAction)BtnOkAction:(id)sender;
- (IBAction)btnAddToCartAction:(id)sender;
- (IBAction)btnMenuAction:(id)sender;
- (IBAction)btnActionClosePopUp:(id)sender;


- (IBAction)btnBottomBackAction:(id)sender;
- (IBAction)btnBottomCartAction:(id)sender;
- (IBAction)btnBottomHomeAction:(id)sender;




@property NSString *stringForShopId;
@property NSString *strForShopName;

@end
