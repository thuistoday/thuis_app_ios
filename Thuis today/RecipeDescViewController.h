//
//  RecipeDescViewController.h
//  RecipeTinder8
//
//  Created by offshore_mac_1 on 16/10/17.
//  Copyright © 2017 offshore_mac_1. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RecipeDescViewController : UIViewController<UITextFieldDelegate>
{

    __weak IBOutlet UITableView *tableViewDesc;
    IBOutlet UILabel *lblCount;
    NSArray *myArray;
}
- (IBAction)btnMenuAction:(id)sender;
- (IBAction)btnActionBack:(id)sender;
@property (retain, nonatomic) NSMutableArray *recipeData;
@property (nonatomic) NSInteger index;
@end
