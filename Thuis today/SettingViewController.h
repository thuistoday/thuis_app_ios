//
//  SettingViewController.h
//  Thuis today
//
//  Created by Apple on 04/04/17.
//  Copyright © 2017 IMMANENT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingViewController : UIViewController
{
     IBOutlet UILabel *lblCount;
    
}

- (IBAction)btnCartAction:(id)sender;
- (IBAction)btnMenuAction:(id)sender;
- (IBAction)btnLanguage:(id)sender;

- (IBAction)btnBottomBackAction:(id)sender;
- (IBAction)btnBottomCartAction:(id)sender;
- (IBAction)btnBottomHomeAction:(id)sender;
@end
