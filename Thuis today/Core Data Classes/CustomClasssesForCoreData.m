//
//  CustomClasssesForCoreData.m
//  Thuis today
//
//  Created by IMMANENT on 20/02/17.
//  Copyright © 2017 IMMANENT. All rights reserved.
//

#import "CustomClasssesForCoreData.h"
#import <CoreData/CoreData.h>
@implementation CustomClasssesForCoreData
@synthesize strVariable;

+(id)sharedManagerForCoreData
{
    static CustomClasssesForCoreData *sharedVariable = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken,^{
        sharedVariable = [[self alloc] init];
    });
    return sharedVariable;
}
- (id)init
{
    if (self = [super init])
    {
        strVariable =@"String Variable";
    }
    return self;
}

#pragma  coredata Methods
- (NSManagedObjectContext *)managedObjectContext {
    NSManagedObjectContext *context = nil;
    id delegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    // call "persistentContainer" not "managedObjectContext"
    if( [delegate performSelector:@selector(persistentContainer)] ){
        // call viewContext from persistentContainer not "managedObjectContext"
        context = [[delegate persistentContainer] viewContext];
    }
    return context;
}

-(void)methodForSaveValues:(NSArray *)Param price:(NSString*)price quantity:(NSString *)quantity singlePrice:(NSString *)singlePrice type:(NSString *)type onCompletion:(ResponseBlock)completionBlock{
  //  http://deliveryapp.demodemo.ga/uploads/
    
    NSLog(@"param %@",Param);
    
    [self methodForFetchValuesForUpdate:Param onCompletion:^(BOOL response)
     {
         if (response==false)
         {
             NSManagedObjectContext *context = [self managedObjectContext];
             NSManagedObject *newDevice = [NSEntityDescription insertNewObjectForEntityForName:@"Product"inManagedObjectContext:context];
             float Quantity;
             NSString *strquan=[[[NSString stringWithFormat:@"%@",quantity] stringByReplacingOccurrencesOfString:@"," withString:@"."]
                                stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
              Quantity = [strquan floatValue];
             NSString *strQuan=[[[NSString stringWithFormat:@"%.1f",Quantity] stringByReplacingOccurrencesOfString:@"." withString:@","]
                                stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
             [newDevice setValue:[NSString stringWithFormat:@"%@",[Param valueForKey:@"category_id"]] forKey:@"category_id"];
             [newDevice setValue:[NSString stringWithFormat:@"%@",[Param valueForKey:kPreparation_time]] forKey:kPreparation_time];
             [newDevice setValue:[NSString stringWithFormat:@"%@",[Param valueForKey:@"meal_id"]] forKey:@"product_id"];
             [newDevice setValue:[NSString stringWithFormat:@"%@",[Param  valueForKey:@"meal_name_ar"]] forKey:@"product_name"];
             [newDevice setValue:[NSString stringWithFormat:@"%@",[Param  valueForKey:@"actual_unit"]]  forKey:@"product_unit"];
             [newDevice setValue:[NSString stringWithFormat:@"%@",price] forKey:@"product_price"];
             [newDevice setValue:[NSString stringWithFormat:@"%@",[Param valueForKey:@"meal_description_ar"] ]forKey:@"product_description"];
             [newDevice setValue:[NSString stringWithFormat:@"%@",[Param  valueForKey:@"restaurant_id"]] forKey:@"shop_id"];
             [newDevice setValue:[NSString stringWithFormat:@"%@",[Param  valueForKey:@"vat_name"]] forKey:@"vat"];
             [newDevice setValue:[NSString stringWithFormat:@"%@",[Param  valueForKey:@"vat"]] forKey:@"vat_id"];
             [newDevice setValue:[NSString stringWithFormat:@"%@",strQuan] forKey:@"product_quantity"];
             [newDevice setValue:[NSString stringWithFormat:@"%@",singlePrice] forKey:@"price_vat"];
             [newDevice setValue:[NSString stringWithFormat:@"%@",type] forKey:@"type"];
             [newDevice setValue:[NSString stringWithFormat:@"%@",singlePrice] forKey:@"single_price"];
             //[newDevice setValue:[NSString stringWithFormat:@"%@",[Param  valueForKey:@"meal_price"]] forKey:@"single_price"];
             [newDevice setValue:[NSString stringWithFormat:@"%@",[Param  valueForKey:@"meal_image"]] forKey:@"image_name"];
             //[newDevice setValue:[NSString stringWithFormat:@"%@",[Param  valueForKey:@"type"]] forKey:@"type"];
             NSError *error = nil;
             // Save the object to persistent store
             if (![context save:&error]) {
                 NSLog(@"Can't Save! %@ %@", error, [error localizedDescription]);
             }

         }
         else
         {
             NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
             
             NSEntityDescription *productEntity=[NSEntityDescription entityForName:@"Product" inManagedObjectContext:managedObjectContext];
             NSFetchRequest *fetch=[[NSFetchRequest alloc] init];
             [fetch setEntity:productEntity];
             NSPredicate *pred= [NSPredicate predicateWithFormat:@"(product_id = %@)", [NSString stringWithFormat:@"%@",[Param valueForKey:@"meal_id"]]];
             
             [fetch setPredicate:pred];
             //... add sorts if you want them
             NSError *fetchError;
             NSError *error;
             NSArray *fetchedProducts=[managedObjectContext executeFetchRequest:fetch error:&fetchError];
              NSManagedObject *device = [fetchedProducts objectAtIndex:0];
             NSLog(@"%@",[device valueForKey:@"product_price"]);
             NSLog(@"%@",[device valueForKey:@"product_quantity"]);
             NSString* oldPrice = [[[device valueForKey:@"product_price"] stringByReplacingOccurrencesOfString:@"," withString:@"."]
                                        stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
             NSLog(@"%@",oldPrice);
             NSString* updatedPrice = [[price stringByReplacingOccurrencesOfString:@"," withString:@"."]
                                   stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
              NSString* strUpdatedQuantity = [[quantity stringByReplacingOccurrencesOfString:@"," withString:@"."]
                                       stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
             
             NSLog(@"%@",updatedPrice);
             float updatePrice;
             float updateQuantity;
             updatePrice = 0.00;
             updateQuantity = 0.0;
             NSLog(@"%@",[device valueForKey:@"product_price"]);
             updatePrice = [updatedPrice floatValue];
           //  updateQuantity = [[device valueForKey:@"product_quantity"] floatValue]+[strUpdatedQuantity floatValue];
             
             updateQuantity = [strUpdatedQuantity floatValue];
             
             
             
             for (NSManagedObject *product in fetchedProducts) {
                 NSString *str=[[[NSString stringWithFormat:@"%.2f",updatePrice] stringByReplacingOccurrencesOfString:@"." withString:@","]
                                stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
                 NSString *strQuan=[[[NSString stringWithFormat:@"%.1f",updateQuantity] stringByReplacingOccurrencesOfString:@"." withString:@","]
                                stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
                 // [managedObjectContext deleteObject:product];
                 [product setValue:[NSString stringWithFormat:@"€ %@",str] forKey:@"product_price"];
                 [product setValue:[NSString stringWithFormat:@"%@",strQuan] forKey:@"product_quantity"];
             }
             [managedObjectContext save:&error];
                      // Save the object to persistent store
             if (![managedObjectContext save:&error]) {
                 NSLog(@"Can't Save! %@ %@", error, [error localizedDescription]);
             }
        }
     }];
}

-(void)methodForFetchValues:(NSArray *)Param  onCompletion:(ResponseBlock)completionBlock
{
    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Product"];
    NSArray *result = [managedObjectContext executeFetchRequest:fetchRequest error:nil] ;
    NSDictionary *dictionary;
    NSMutableArray *newArray=[[NSMutableArray alloc] init];
    for (NSManagedObject *obj in result) {
        
        
        NSArray *keys = [[[obj entity] attributesByName] allKeys];
      dictionary = [obj dictionaryWithValuesForKeys:keys];
        [newArray addObject:dictionary];
    }
    NSLog(@"%@",newArray);
    NSString *count = [NSString stringWithFormat:@"%lu",(unsigned long)newArray.count];
    [[NSUserDefaults standardUserDefaults]setObject:count forKey:@"cartCount"];
    [[NSUserDefaults standardUserDefaults] synchronize];
        completionBlock(result);
}

-(void)methodForFetchValuesForUpdate:(NSArray *)Param  onCompletion:(ResponseBlockA)completionBlock{
    BOOL flag = false;
    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Product"];
    NSArray *result = [managedObjectContext executeFetchRequest:fetchRequest error:nil] ;
    NSDictionary *dictionary;
    NSMutableArray *newArray=[[NSMutableArray alloc]init];
    for (NSManagedObject *obj in result) {
        NSArray *keys = [[[obj entity] attributesByName] allKeys];
        dictionary = [obj dictionaryWithValuesForKeys:keys];
        [newArray addObject:dictionary];
    }
    NSLog(@"%@",dictionary);
    for(int i=0; i<newArray.count; i++)
    {
        if ([[[newArray objectAtIndex:i] valueForKey:@"product_id"] isEqualToString:[NSString stringWithFormat:@"%@",[Param valueForKey:@"meal_id"]]])
        {
            flag = true;
        }
        
    }
    if (flag==true)
    {
        completionBlock(true);
    }
    else
    {
        completionBlock(false);
    }
}

-(void)methodForDeleteValues:(NSString *)product_id  onCompletion:(ResponseBlock)completionBlock
{
    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];

//    NSEntityDescription *productEntity=[NSEntityDescription entityForName:@"Product" inManagedObjectContext:managedObjectContext];
//    NSFetchRequest *fetch=[[NSFetchRequest alloc] init];
//    [fetch setEntity:productEntity];
//    NSPredicate *p=[NSPredicate predicateWithFormat:@"product_id == %@", product_id];
//    [fetch setPredicate:p];
//    //... add sorts if you want them
//    NSError *fetchError;
//    NSError *error;
//    NSArray *fetchedProducts=[managedObjectContext executeFetchRequest:fetch error:&fetchError];
//    for (NSManagedObject *product in fetchedProducts) {
//        [managedObjectContext deleteObject:product];
//    }
//    [managedObjectContext save:&error];
//    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
    
    
    
    
    NSEntityDescription *entityDesc = [NSEntityDescription entityForName:@"Product" inManagedObjectContext:managedObjectContext];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    [request setEntity:entityDesc];
    
    NSPredicate *pred= [NSPredicate predicateWithFormat:@"(product_id = %@)", product_id];
    
    [request setPredicate:pred];
    NSManagedObject *matches = nil;
    
    NSError *error;
    NSArray *objects = [managedObjectContext executeFetchRequest:request error:&error];
    
    if([objects count] == 0)
    {
        NSLog(@"No Match found");
    }
    else{
        matches = objects[0];
        
        [managedObjectContext deleteObject:matches];
        
    }
     completionBlock(objects);
}
-(void)methodForDeleteAllValues:(NSString *)product_id  onCompletion:(ResponseBlock)completionBlock
{
        NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
    
        NSEntityDescription *productEntity=[NSEntityDescription entityForName:@"Product" inManagedObjectContext:managedObjectContext];
    
        NSFetchRequest *fetch=[[NSFetchRequest alloc] init];

        [fetch setEntity:productEntity];
               //... add sorts if you want them
        NSError *fetchError;
        NSError *error;
        NSArray *fetchedProducts=[managedObjectContext executeFetchRequest:fetch error:&fetchError];
        for (NSManagedObject *product in fetchedProducts) {
            [managedObjectContext deleteObject:product];
        }
        [managedObjectContext save:&error];
       completionBlock(fetchedProducts);
}

//methodForUpdateValues
-(void)methodForUpdateValues:(NSString *)product_id price:(NSString*)price  quantity:(NSString*)quantity onCompletion:(ResponseBlockA)completionBlock
{
    NSManagedObjectContext *managedObjectContext = [self managedObjectContext];
    
    NSEntityDescription *productEntity=[NSEntityDescription entityForName:@"Product" inManagedObjectContext:managedObjectContext];
    NSFetchRequest *fetch=[[NSFetchRequest alloc] init];
    [fetch setEntity:productEntity];
    NSPredicate *pred= [NSPredicate predicateWithFormat:@"(product_id = %@)", product_id];
    
    [fetch setPredicate:pred];
    //... add sorts if you want them
    NSError *fetchError;
    NSError *error;
    NSArray *fetchedProducts=[managedObjectContext executeFetchRequest:fetch error:&fetchError];
    for (NSManagedObject *product in fetchedProducts) {
        float Quantity;
        NSString *strquan=[[[NSString stringWithFormat:@"%@",quantity] stringByReplacingOccurrencesOfString:@"," withString:@"."]
                           stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
        Quantity = [strquan floatValue];
        NSString *strQuan=[[[NSString stringWithFormat:@"%.1f",Quantity] stringByReplacingOccurrencesOfString:@"." withString:@","]
                           stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
       // [managedObjectContext deleteObject:product];
        [product setValue:price forKey:@"product_price"];
         [product setValue:strQuan forKey:@"product_quantity"];
    }
    [managedObjectContext save:&error];
    completionBlock(true);
}
@end
