//
//  CustomClasssesForCoreData.h
//  Thuis today
//
//  Created by IMMANENT on 20/02/17.
//  Copyright © 2017 IMMANENT. All rights reserved.
//

#import <Foundation/Foundation.h>
typedef void (^ResponseBlock)(NSArray* json);
typedef void (^ResponseBlockA)(BOOL Check);
@interface CustomClasssesForCoreData : NSObject
{
    NSOperationQueue *_queue;

}
@property(strong,nonatomic) NSString *strVariable;
+(id)sharedManagerForCoreData;
-(void)methodForSaveValues:(NSArray *)Param price:(NSString*)price quantity:(NSString *)quantity singlePrice:(NSString *)singlePrice type:(NSString *)type onCompletion:(ResponseBlock)completionBlock;
-(void)methodForFetchValues:(NSArray *)Param  onCompletion:(ResponseBlock)completionBlock;
-(void)methodForDeleteValues:(NSString *)product_id  onCompletion:(ResponseBlock)completionBlock;
-(void)methodForDeleteAllValues:(NSString *)product_id  onCompletion:(ResponseBlock)completionBlock;
-(void)methodForUpdateValues:(NSString *)product_id price:(NSString*)price quantity:(NSString *)quantity onCompletion:(ResponseBlockA)completionBlock;
-(void)methodForFetchValuesForUpdate:(NSArray *)Param  onCompletion:(ResponseBlockA)completionBlock;
@end
