//
//  AppDelegate.m
//  Thuis today
//
//  Created by IMMANENT on 11/01/17.
//  Copyright © 2017 IMMANENT. All rights reserved.
//

#import "AppDelegate.h"
#import "MFSideMenuContainerViewController.h"
#import <UserNotifications/UserNotifications.h>
#import <Crashlytics/Crashlytics.h>
#import "AppVersionUpdateManager.h"

@import Firebase;
@interface AppDelegate ()<AppVersionUpdateManagerDelegate>
{
    UIActivityIndicatorView *spinner;
    UIView  *mainBackBlurView;
    UIView *spinnerView;
    UIView *pushView,*viewForText;
    UILabel *pushAlertLabel;
    UIButton *pushButton;
    UINavigationController *navObj;
}
@end

@implementation AppDelegate

NSString *const kGCMMessageIDKey = @"gcm.message_id";
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Firebase configuration
    [FIRApp configure];
    [self addGoogleAnalytics];
    [self createActivityIndicator];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSLog(@"%@",[paths objectAtIndex:0]);
    if ( IDIOM == IPAD ) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Storyboard" bundle:[NSBundle mainBundle]];
        MFSideMenuContainerViewController *container = (MFSideMenuContainerViewController *)self.window.rootViewController;
        UINavigationController *navigationController = [storyboard instantiateViewControllerWithIdentifier:@"navigationController"];
        UIViewController *leftSideMenuViewController = [storyboard instantiateViewControllerWithIdentifier:@"leftSideMenuViewController"];
        
        [navigationController.navigationController setNavigationBarHidden:YES animated:YES];
        //[self.navigationController setNavigationBarHidden:YES animated:YES];
        //    UIViewController *rightSideMenuViewController = [storyboard instantiateViewControllerWithIdentifier:@"rightSideMenuViewController"];
        
        [container setLeftMenuViewController:leftSideMenuViewController];
        //  [container setRightMenuViewController:rightSideMenuViewController];
        [container setCenterViewController:navigationController];
        
    } else {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
        MFSideMenuContainerViewController *container = (MFSideMenuContainerViewController *)self.window.rootViewController;
        UINavigationController *navigationController = [storyboard instantiateViewControllerWithIdentifier:@"navigationController"];
        UIViewController *leftSideMenuViewController = [storyboard instantiateViewControllerWithIdentifier:@"leftSideMenuViewController"];
        //    UIViewController *rightSideMenuViewController = [storyboard instantiateViewControllerWithIdentifier:@"rightSideMenuViewController"];
        
        [container setLeftMenuViewController:leftSideMenuViewController];
        //  [container setRightMenuViewController:rightSideMenuViewController];
        [container setCenterViewController:navigationController];
    }
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];
    NSString *language = [[[NSBundle mainBundle] preferredLocalizations] objectAtIndex:0];
    NSLog(@"%@",language);
    
    UIView *view=[[UIView alloc] initWithFrame:CGRectMake(0, 0,[UIScreen mainScreen].bounds.size.width, 20)];
    view.backgroundColor=[UIColor colorWithRed:252/255.0f green:103/255.0f blue:1/255.0f alpha:1];
    [self.window.rootViewController.view addSubview:view];
    
    //[[UIApplication sharedApplication] registerForRemoteNotifications];
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_9_x_Max) {
        UIUserNotificationType allNotificationTypes =
        (UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge);
        UIUserNotificationSettings *settings =
        [UIUserNotificationSettings settingsForTypes:allNotificationTypes categories:nil];
        [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
    } else {
        // iOS 10 or later
#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
        // For iOS 10 display notification (sent via APNS)
        [UNUserNotificationCenter currentNotificationCenter].delegate = self;
        UNAuthorizationOptions authOptions =
        UNAuthorizationOptionAlert
        | UNAuthorizationOptionSound
        | UNAuthorizationOptionBadge;
        [[UNUserNotificationCenter currentNotificationCenter] requestAuthorizationWithOptions:authOptions completionHandler:^(BOOL granted, NSError * _Nullable error) {
        }];
#endif
    }
    
    [[UIApplication sharedApplication] registerForRemoteNotifications];
    
    
    //Show appversion update alert
    [self showAppVersionUpdateAlert];
    return YES;
}
//                                        -(void)alertView:(UIAlertView *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
//        {
//            NSString *iTunesLink = @"itms-apps://phobos.apple.com/WebObjects/MZStore.woa/wa/viewSoftwareUpdate?id=<appid>&mt=8";
//            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:iTunesLink]];
//        }



- (void)addGoogleAnalytics {
    GAI *gai = [GAI sharedInstance];
    [gai trackerWithTrackingId:kGoogleAnalyticsTrackingID];
    
    // Optional: automatically report uncaught exceptions.
    gai.trackUncaughtExceptions = NO;
    
    // Optional: set Logger to VERBOSE for debug information.
    // Remove before app release.
    gai.logger.logLevel = kGAILogLevelVerbose;
}


-(void)showAlert
{
    UIAlertController *alertController = [UIAlertController  alertControllerWithTitle:@"please update app"  message:nil  preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Okay!" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                {
                                    @try
                                    {
                                        NSLog(@"tapped ok");
                                        BOOL canOpenSettings = (UIApplicationOpenSettingsURLString != NULL);
                                        if (canOpenSettings)
                                        {
                                            NSURL *url = [NSURL URLWithString:@"https://itunes.apple.com/nl/app/thuis-today/id1278891670?mt=8"];
                                            //[[UIApplication sharedApplication] openURL:url options:@{} completionHandler:nil];
                                            [[UIApplication sharedApplication] openURL:url];
                                        }
                                    }
                                    @catch (NSException *exception)
                                    {
                                        
                                    }
                                }]];
    UIWindow* topWindow = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    topWindow.rootViewController = [UIViewController new];
    topWindow.windowLevel = UIWindowLevelAlert + 1;
    [topWindow makeKeyAndVisible];
    [topWindow.rootViewController presentViewController:alertController animated:YES completion:nil];
}
-(void)reloadSideMenu
{
    if ( IDIOM == IPAD ) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Storyboard" bundle:[NSBundle mainBundle]];
        MFSideMenuContainerViewController *container = (MFSideMenuContainerViewController *)self.window.rootViewController;
        UINavigationController *navigationController = [storyboard instantiateViewControllerWithIdentifier:@"navigationController"];
        UIViewController *leftSideMenuViewController = [storyboard instantiateViewControllerWithIdentifier:@"leftSideMenuViewController"];
        //    UIViewController *rightSideMenuViewController = [storyboard instantiateViewControllerWithIdentifier:@"rightSideMenuViewController"];
        
        [container setLeftMenuViewController:leftSideMenuViewController];
        //  [container setRightMenuViewController:rightSideMenuViewController];
        [container setCenterViewController:navigationController];
        
    }else{
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
        MFSideMenuContainerViewController *container = (MFSideMenuContainerViewController *)self.window.rootViewController;
        UINavigationController *navigationController = [storyboard instantiateViewControllerWithIdentifier:@"navigationController"];
        UIViewController *leftSideMenuViewController = [storyboard instantiateViewControllerWithIdentifier:@"leftSideMenuViewController"];
        //    UIViewController *rightSideMenuViewController = [storyboard instantiateViewControllerWithIdentifier:@"rightSideMenuViewController"];
        
        [container setLeftMenuViewController:leftSideMenuViewController];
        //  [container setRightMenuViewController:rightSideMenuViewController];
        [container setCenterViewController:navigationController];
    }

}


#pragma mark -
#pragma mark - AppDelegate Shared Method
+(AppDelegate*)shareInstance
{
    return (AppDelegate*)[[UIApplication sharedApplication] delegate];
}
#pragma mark- 
#pragma mark- Change status Bar Color
-(void)methodForChangeStatusBarBackGroundColor
{
//    UIApplication *app = [UIApplication sharedApplication];
//    CGFloat statusBarHeight = app.statusBarFrame.size.height;
//    
//    UIView *statusBarView =  [[UIView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, statusBarHeight)];
//    statusBarView.backgroundColor  =  [UIColor yellowColor];
//    [self.view addSubview:statusBarView];
}

#pragma mark -
#pragma mark - AppVersionUpdateAlert
- (void)showAppVersionUpdateAlert {
    AppVersionUpdateManager *appVersionUpdater = [AppVersionUpdateManager sharedManager];
    [appVersionUpdater setAlertTitle:NSLocalizedString(@"Waarschuwingstitel", @"Alert Title")];
    [appVersionUpdater setAlertMessage:NSLocalizedString(@"Versie %@ is beschikbaar in de App Store.", @"Alert Message")];
    [appVersionUpdater setAlertUpdateButtonTitle:@"Bijwerken"];
    [appVersionUpdater setAlertCancelButtonTitle:@"Annuleer"];
    [appVersionUpdater setDelegate:self]; // Optional
    [appVersionUpdater showUpdateWithConfirmation];
}

#pragma mark - AppVersionUpdateManagerDelegate Methods
- (void)appVersionUpdateManagerDidShowUpdateDialog {
    // when update alert is displayed
}

- (void)appVersionUpdateManagerUserDidCancel {
    // not required as we have force option
}

- (void)appVersionUpdateManagerUserDidLaunchAppStore {
    // when control moves to appstore
}


#pragma mark -
#pragma mark - AlertView Methods
- (void)showAlertWithErrorMessage:(NSString *)message
{
    if(message.length != 0) {
        [[[UIAlertView alloc] initWithTitle:@"" message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
    }
}


-(void)createActivityIndicator
{
    
    mainBackBlurView=[[UIView  alloc] initWithFrame:self.window.frame];
    mainBackBlurView.backgroundColor=[UIColor colorWithRed:127.0/255.0 green:127.0/255 blue:127.0/255 alpha:1.0];
    mainBackBlurView.alpha=0.3f;
    
    spinnerView = [[UIView  alloc] initWithFrame:self.window.frame];
    spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    spinner.frame=CGRectMake(self.window.frame.size.width/2-20, self.window.frame.size.height/2, 40, 40);
    spinner.color=[UIColor colorWithRed:127.0/255.0 green:127.0/255 blue:127.0/255 alpha:1.0];
    //    [self.window addSubview:spinner];
    
}

-(void)hideActivityIndicator
{
    dispatch_async(dispatch_get_main_queue(), ^{
        self.window.userInteractionEnabled = YES;
        
        spinner.hidden = YES;
        [spinner stopAnimating];
        [spinnerView removeFromSuperview];
        [mainBackBlurView removeFromSuperview];
    });
    
}

-(void)showActivityIndicator
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [spinnerView addSubview:spinner];
        [self.window addSubview:spinnerView];
        [self.window addSubview:mainBackBlurView];
        
        [self.window bringSubviewToFront:spinner];
        spinner.hidden = NO;
        self.window.userInteractionEnabled = NO;
        
        [spinner startAnimating];
    });
}

- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    return [[FBSDKApplicationDelegate sharedInstance] application:application
                                                          openURL:url
                                                sourceApplication:sourceApplication
                                                       annotation:annotation];
}

- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url {
    NSLog(@"url recieved: %@", url);
    NSLog(@"query string: %@", [url query]);
    NSLog(@"host: %@", [url host]);
    NSLog(@"url path: %@", [url path]);
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}
- (BOOL)application:(UIApplication *)app
            openURL:(NSURL *)url
            options:(NSDictionary<NSString *,id> *)options {
    
    // Check the calling application Bundle ID
    if ([[url scheme] isEqualToString:@"thuistoday"])
    {
        NSLog(@"URL scheme:%@", [url scheme]);
        [[NSNotificationCenter defaultCenter] postNotificationName:@"pushNotification" object:nil userInfo:nil];
        
        return YES;
    }
    else
        return NO;
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
    [self saveContext];
}


#pragma mark - Core Data stack

@synthesize persistentContainer = _persistentContainer;

- (NSPersistentContainer *)persistentContainer {
    // The persistent container for the application. This implementation creates and returns a container, having loaded the store for the application to it.
    @synchronized (self) {
        if (_persistentContainer == nil) {
            _persistentContainer = [[NSPersistentContainer alloc] initWithName:@"Thuis_today"];
            [_persistentContainer loadPersistentStoresWithCompletionHandler:^(NSPersistentStoreDescription *storeDescription, NSError *error) {
                if (error != nil) {
                    // Replace this implementation with code to handle the error appropriately.
                    // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                    
                    /*
                     Typical reasons for an error here include:
                     * The parent directory does not exist, cannot be created, or disallows writing.
                     * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                     * The device is out of space.
                     * The store could not be migrated to the current model version.
                     Check the error message to determine what the actual problem was.
                    */
                    NSLog(@"Unresolved error %@, %@", error, error.userInfo);
                    abort();
                }
            }];
        }
    }
    
    return _persistentContainer;
}

#pragma mark - Core Data Saving support

- (void)saveContext {
    NSManagedObjectContext *context = self.persistentContainer.viewContext;
    NSError *error = nil;
    if ([context hasChanges] && ![context save:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, error.userInfo);
        abort();
    }
}
//#pragma mark - Remote Notifications
//// [START receive_message]
//- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
//    // If you are receiving a notification message while your app is in the background,
//    // this callback will not be fired till the user taps on the notification launching the application.
//    // TODO: Handle data of notification
//
//    // With swizzling disabled you must let Messaging know about the message, for Analytics
//    // [[FIRMessaging messaging] appDidReceiveMessage:userInfo];
//
//    // Print message ID.
//    if (userInfo[kGCMMessageIDKey]) {
//        NSLog(@"Message ID: %@", userInfo[kGCMMessageIDKey]);
//    }
//
//    // Print full message.
//    NSLog(@"%@", userInfo);
//}
//
//- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
//fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
//    // If you are receiving a notification message while your app is in the background,
//    // this callback will not be fired till the user taps on the notification launching the application.
//    // TODO: Handle data of notification
//
//    // With swizzling disabled you must let Messaging know about the message, for Analytics
//    // [[FIRMessaging messaging] appDidReceiveMessage:userInfo];
//
//    // Print message ID.
//    if (userInfo[kGCMMessageIDKey]) {
//        NSLog(@"Message ID: %@", userInfo[kGCMMessageIDKey]);
//    }
//
//    // Print full message.
//    NSLog(@"%@", userInfo);
//
//    completionHandler(UIBackgroundFetchResultNewData);
//}
//// [END receive_message]
//
//// [START ios_10_message_handling]
//// Receive displayed notifications for iOS 10 devices.
//#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
//// Handle incoming notification messages while app is in the foreground.
//- (void)userNotificationCenter:(UNUserNotificationCenter *)center
//       willPresentNotification:(UNNotification *)notification
//         withCompletionHandler:(void (^)(UNNotificationPresentationOptions))completionHandler {
//    NSDictionary *userInfo = notification.request.content.userInfo;
//
//    // With swizzling disabled you must let Messaging know about the message, for Analytics
//    // [[FIRMessaging messaging] appDidReceiveMessage:userInfo];
//
//    // Print message ID.
//    if (userInfo[kGCMMessageIDKey]) {
//        NSLog(@"Message ID: %@", userInfo[kGCMMessageIDKey]);
//    }
//
//    // Print full message.
//    NSLog(@"%@", userInfo);
//
//    // Change this to your preferred presentation option
//    completionHandler(UNNotificationPresentationOptionNone);
//}
//
//// Handle notification messages after display notification is tapped by the user.
//- (void)userNotificationCenter:(UNUserNotificationCenter *)center
//didReceiveNotificationResponse:(UNNotificationResponse *)response
//         withCompletionHandler:(void(^)())completionHandler
//{
//    NSDictionary *userInfo = response.notification.request.content.userInfo;
//    if (userInfo[kGCMMessageIDKey]) {
//        NSLog(@"Message ID: %@", userInfo[kGCMMessageIDKey]);
//    }
//
//    // Print full message.
//    NSLog(@"%@", userInfo);
//
//    completionHandler();
//}
//#endif
//// [END ios_10_message_handling]
//
//// [START refresh_token]
//- (void)messaging:(nonnull FIRMessaging *)messaging didRefreshRegistrationToken:(nonnull NSString *)fcmToken {
//    // Note that this callback will be fired everytime a new token is generated, including the first
//    // time. So if you need to retrieve the token as soon as it is available this is where that
//    // should be done.
//    NSLog(@"FCM registration token: %@", fcmToken);
//
//    // TODO: If necessary send token to application server.
//}
//// [END refresh_token]
//
//// [START ios_10_data_message]
//// Receive data messages on iOS 10+ directly from FCM (bypassing APNs) when the app is in the foreground.
//// To enable direct data messages, you can set [Messaging messaging].shouldEstablishDirectChannel to YES.
//- (void)messaging:(FIRMessaging *)messaging didReceiveMessage:(FIRMessagingRemoteMessage *)remoteMessage {
//    NSLog(@"Received data message: %@", remoteMessage.appData);
//}
//// [END ios_10_data_message]
//
//- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
//    NSLog(@"Unable to register for remote notifications: %@", error);
//}
//
//// This function is added here only for debugging purposes, and can be removed if swizzling is enabled.
//// If swizzling is disabled then this function must be implemented so that the APNs device token can be paired to
//// the FCM registration token.
//- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
//    NSLog(@"APNs device token retrieved: %@", deviceToken);
//    NSString * deviceTokenString = [[[[deviceToken description]
//                                      stringByReplacingOccurrencesOfString: @"<" withString: @""]
//                                     stringByReplacingOccurrencesOfString: @">" withString: @""]
//                                    stringByReplacingOccurrencesOfString: @" " withString: @""];
//    [[NSUserDefaults standardUserDefaults] setValue:deviceTokenString forKey:@"Device_Token"];
//    // With swizzling disabled you must set the APNs device token here.
//    // [FIRMessaging messaging].APNSToken = deviceToken;
//}




#pragma mark - Remote Notifications
#pragma mark - Push Notifications

- (void)application:(UIApplication* )application didRegisterForRemoteNotificationsWithDeviceToken:(NSData* )deviceToken {
    NSLog(@"Device token: %@", deviceToken.description);
    NSString * deviceTokenString = [[[[deviceToken description]
                                      stringByReplacingOccurrencesOfString: @"<" withString: @""]
                                     stringByReplacingOccurrencesOfString: @">" withString: @""]
                                    stringByReplacingOccurrencesOfString: @" " withString: @""];
    [[NSUserDefaults standardUserDefaults] setValue:deviceTokenString forKey:@"Device_Token"];
}

- (void)application:(UIApplication* )application didFailToRegisterForRemoteNotificationsWithError:(NSError* )error {
    NSLog(@"Failed to get token, error: %@", error);
}

- (void)application:(UIApplication* )application didReceiveRemoteNotification:(NSDictionary* )userInfo {

    if (userInfo[kGCMMessageIDKey]) {
        NSLog(@"Message ID: %@", userInfo[kGCMMessageIDKey]);
    }
    // Print full message.
    NSLog(@"%@", userInfo);
    
    //completionHandler(UIBackgroundFetchResultNewData);
}
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler {
    // If you are receiving a notification message while your app is in the background,
    // this callback will not be fired till the user taps on the notification launching the application.
    // TODO: Handle data of notification
    
    // With swizzling disabled you must let Messaging know about the message, for Analytics
    // [[FIRMessaging messaging] appDidReceiveMessage:userInfo];
    
    // Print message ID.
//    UserProfileViewController *profileObj = [storyboard instantiateViewControllerWithIdentifier:@"UserProfileViewController"];
    
    if (userInfo[kGCMMessageIDKey]) {
        NSLog(@"Message ID: %@", userInfo[kGCMMessageIDKey]);
    }
    
    // Print full message.
    NSLog(@"%@", userInfo);
    
    completionHandler(UIBackgroundFetchResultNewData);
}
- (void)application:(UIApplication* )application handleEventsForBackgroundURLSession:(NSString* )identifier completionHandler:(void (^)(void))completionHandler {
    NSLog(@"method for handling events for background url session is waiting to be process. background session id: %@", identifier);
    if (completionHandler != nil) {
        completionHandler();
    }
}

#if defined(__IPHONE_10_0) && _IPHONE_OS_VERSION_MAX_ALLOWED >= _IPHONE_10_0
// Handle incoming notification messages while app is in the foreground.
- (void)userNotificationCenter:(UNUserNotificationCenter *)center
       willPresentNotification:(UNNotification *)notification
         withCompletionHandler:(void (^)(UNNotificationPresentationOptions))completionHandler {
    NSDictionary *userInfo = notification.request.content.userInfo;
    
    // With swizzling disabled you must let Messaging know about the message, for Analytics
    // [[FIRMessaging messaging] appDidReceiveMessage:userInfo];
    
    
    // Print full message.
    NSLog(@"%@", userInfo);
    UNNotificationPresentationOptions authOptions =
    UNNotificationPresentationOptionAlert
    | UNNotificationPresentationOptionSound
    | UNNotificationPresentationOptionBadge;
    
    // Change this to your preferred presentation option
    completionHandler(authOptions);
}
- (void)userNotificationCenter:(UNUserNotificationCenter *)center
didReceiveNotificationResponse:(UNNotificationResponse *)response
         withCompletionHandler:(void(^)())completionHandler {
    NSDictionary *userInfo = response.notification.request.content.userInfo;
    if (userInfo[kGCMMessageIDKey]) {
        NSLog(@"Message ID: %@", userInfo[kGCMMessageIDKey]);
    }
    
    // Print full message.
    NSLog(@"%@", userInfo);
    if ( IDIOM == IPAD ) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Storyboard" bundle:[NSBundle mainBundle]];
        MFSideMenuContainerViewController *container = (MFSideMenuContainerViewController *)self.window.rootViewController;
        UINavigationController *navigationController = [storyboard instantiateViewControllerWithIdentifier:@"navigationController"];
        NSLog(@"navigationController = %@",navigationController);
        UserProfileViewController *homeView = [storyboard instantiateViewControllerWithIdentifier:@"UserProfileViewController"];
        [navigationController initWithRootViewController:homeView];
        [container setCenterViewController:navigationController];
        //[navigationController pushViewController:homeView animated:YES];
    }
    else{
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
        MFSideMenuContainerViewController *container = (MFSideMenuContainerViewController *)self.window.rootViewController;
        UINavigationController *navigationController = [storyboard instantiateViewControllerWithIdentifier:@"navigationController"];
        NSLog(@"navigationController = %@",navigationController);
        UserProfileViewController *homeView = [storyboard instantiateViewControllerWithIdentifier:@"UserProfileViewController"];
        [navigationController initWithRootViewController:homeView];
        [container setCenterViewController:navigationController];
        //[navigationController pushViewController:homeView animated:YES];
        
    }
    completionHandler(userInfo);
}
#endif
@end
