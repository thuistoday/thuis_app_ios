//
//  LoginRegistrationViewController.h
//  Thuis today
//
//  Created by IMMANENT on 12/01/17.
//  Copyright © 2017 IMMANENT. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    TextTypeNumeric =1,
    TextTypeAlphates ,
} TextType;

@interface LoginRegistrationViewController : UIViewController
{
    
     IBOutlet UIButton *btnSwitchOutlet;
     IBOutlet UIView *viewForRegistration;
     IBOutlet UIView *viewForLogin;
    
     IBOutlet UITextField *txtFldName;
    
    
     IBOutlet UITextField *txtFldAddress;
    
     IBOutlet UITextField *txtFldPostCode;
    
     IBOutlet UITextField *txtFldCity;
    
    
     IBOutlet UITextField *txtFldPhoneNumber;
    
    
     IBOutlet UITextField *txtFldEmail;
    
    
     IBOutlet UITextField *txtFldPassword;
    
     IBOutlet UITextField *txtFldConfirmPassword;
    
     IBOutlet UITextField *txtFldUsernameLogin;
    
     IBOutlet UITextField *txtFldPasswordLogin;
    
     IBOutlet UILabel *lblCount;
    
    IBOutlet UIView *viewForForgotPassword;
    
    IBOutlet UITextField *txtForgotPassword;
    
     IBOutlet UIButton *btnResetPassword;
    
    
     IBOutlet UIButton *btnFacebookOutlet;
    
    __weak IBOutlet UITableView *tblSearch;
    
}
- (IBAction)btnSwitchAction:(id)sender;
- (IBAction)btnMenuAction:(id)sender;
- (IBAction)btnRegisterAction:(id)sender;
- (IBAction)btnLoginAction:(id)sender;
- (IBAction)btnCloseForgotViewAction:(id)sender;
- (IBAction)btnResetPasswordAction:(id)sender;
- (IBAction)btnForgotPasswordAction:(id)sender;
- (IBAction)btnFacebookAction:(id)sender;
@property NSString *checkForDisplayView;
-(void) textFieldDidChange:(UITextField *) theTextField;

@end
