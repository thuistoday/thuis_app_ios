 //
//  LoginRegistrationViewController.m
//  Thuis today
//
//  Created by IMMANENT on 12/01/17.
//  Copyright © 2017 IMMANENT. All rights reserved.
//

#import "LoginRegistrationViewController.h"
#import "MFSideMenu.h"
#import "Constants.h"

@interface LoginRegistrationViewController ()
{
    NSMutableArray *arrayForUserData, *arraySearchData;
    NSString *otp,*userId;
    NSString *Social;
    NSString *userID;
    NSString *userName;
    NSString *ProfilePic;
    NSString *loginType;
    BOOL checkForButtonClick;
}

@property (nonatomic,strong) NSMutableDictionary * dict_postalCodeDetails;
@property (weak, nonatomic) IBOutlet UITextField *txtFld_postCode;
@property (weak, nonatomic) IBOutlet UITextField *txtFld_StreetCode;
@property (weak, nonatomic) IBOutlet UITextField *txtFld_HouseCode;
@property (weak, nonatomic) IBOutlet UILabel *lbl_hiphenHouseCode;

@end

@implementation LoginRegistrationViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [CommonMethods configureNavigationBarForViewController:self withTitle:@"Welcome"];
    // Do any additional setup after loading the view.
    [btnSwitchOutlet setImage:[UIImage imageNamed:@"push-btn-green@1x"] forState:UIControlStateNormal];
    if ([_checkForDisplayView isEqualToString:@"Register"]) {
        viewForLogin.hidden =YES;
        viewForRegistration.hidden =NO;
       [btnSwitchOutlet setImage:[UIImage imageNamed:@"push-btn-green1@1x"] forState:UIControlStateNormal];

    }
    else {
        viewForLogin.hidden =NO;
        viewForRegistration.hidden =YES;
         [btnSwitchOutlet setImage:[UIImage imageNamed:@"push-btn-green@1x"] forState:UIControlStateNormal];
    }
   
    tblSearch.layer.cornerRadius = 3.0;
    tblSearch.layer.borderColor = [UIColor blackColor].CGColor;
    tblSearch.layer.borderWidth = 2.0;
    txtFldPassword.delegate=self;
    txtFldPhoneNumber.delegate=self;
    arrayForUserData = [[NSMutableArray alloc]init];
    arraySearchData = [NSMutableArray new];
    viewForForgotPassword.hidden=YES;
    [txtFldPostCode addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
    self.dict_postalCodeDetails = [[NSMutableDictionary alloc] init];
    lblCount.layer.cornerRadius = lblCount.frame.size.width/2;
    lblCount.layer.masksToBounds=YES;
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"cartCount"]!=nil) {
        lblCount.text = [[NSUserDefaults standardUserDefaults] valueForKey:@"cartCount"];
    }
    else
    {
        lblCount.text = @"";
    }
    
   // UIColor *color = [UIColor colorWithRed:210/255.0f green:45/255.0f blue:62/255.0f alpha:1];
    UIColor *color = [UIColor lightGrayColor];

    [txtForgotPassword setValue:color forKeyPath:@"_placeholderLabel.textColor"];
    txtForgotPassword.placeholder =NSLocalizedString(@"Enter your email address", @"Cancel");
    checkForButtonClick =false;
    
}

- (IBAction)btnCartAction:(id)sender
{
    [self performSegueWithIdentifier:@"NavigateToCartViewController" sender:nil];
}

- (IBAction)btnSwitchAction:(id)sender
{
    if ([btnSwitchOutlet.currentImage isEqual:[UIImage imageNamed:@"push-btn-green1@1x"]])
    {
         [btnSwitchOutlet setImage:[UIImage imageNamed:@"push-btn-green@1x"] forState:UIControlStateNormal];
        viewForLogin.hidden =NO;
        viewForRegistration.hidden =YES;
    }
    else
    {
        [btnSwitchOutlet setImage:[UIImage imageNamed:@"push-btn-green1@1x"] forState:UIControlStateNormal];
        viewForLogin.hidden =YES;
        viewForRegistration.hidden =NO;
    }
}

- (IBAction)btnMenuAction:(id)sender
{
    [self.menuContainerViewController toggleLeftSideMenuCompletion:nil];
}

- (IBAction)btnRegisterAction:(id)sender
{
    if([self validateAllValues]==YES)
    {
        if ([txtFldPassword.text isEqualToString:txtFldConfirmPassword.text])
        {
            NSString *token;
            if([[NSUserDefaults standardUserDefaults] valueForKey:@"Device_Token"])
            {
                token = [[NSUserDefaults standardUserDefaults] valueForKey:@"Device_Token"];
            }
            else
            {
                token = @"123456";
            }
            [[AppDelegate shareInstance]showActivityIndicator];
            NSString *postalCode = [NSString stringWithFormat:@"%@-%@",[self.dict_postalCodeDetails valueForKey:kPostalCode],[self.dict_postalCodeDetails valueForKey:kStreetCode]];
            NSDictionary *params = @{
                                     @"name":txtFldName.text,
                                      @"email":txtFldEmail.text,
                                      @"password":txtFldPassword.text,
                                      @"phone":txtFldPhoneNumber.text,
                                      @"zipcode":postalCode,
                                      @"address":txtFldAddress.text,
                                      @"city":txtFldCity.text,
                                      @"device_type":@"ios",
                                      @"token":token
                                    };
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [[SingletonClass sharedManager] m_PostApiResponse:@"register" paramDiction:params islogout:NO  onCompletion:^(NSDictionary *response)
                 {
                     NSDictionary *responseDic = (NSDictionary *)response;
                     if ([[[responseDic objectForKey:@"payload"]valueForKey:@"status"]integerValue]==1)
                     {
                         txtFldName.text=@"";
                         txtFldEmail.text=@"";
                         txtFldPassword.text=@"";
                         txtFldPhoneNumber.text=@"";
                         self.txtFld_postCode.text=@"";
                         self.txtFld_StreetCode.text = @"";
                         self.txtFld_HouseCode.text = @"";
                         txtFldAddress.text=@"";
                         txtFldCity.text=@"";
                         txtFldConfirmPassword.text=@"";
                         [[NSUserDefaults standardUserDefaults] setValue:@"user_login" forKey:@"Login_Type"];
                         dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.6 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                             [self.view endEditing:YES];
                            [self methodForAlert:[[responseDic objectForKey:@"payload"] valueForKey:@"message"]];
                         });
                      
                         
                         [[AppDelegate shareInstance]hideActivityIndicator];

                     }
                     else
                     {
                         dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.6 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                             [self.view endEditing:YES];
                              [[AppDelegate shareInstance] showAlertWithErrorMessage:[[responseDic objectForKey:@"payload"] valueForKey:@"message"]];
                         });
                         [[AppDelegate shareInstance]hideActivityIndicator];
                     }
                 }];
            });

        }
        else{
            [[AppDelegate shareInstance] showAlertWithErrorMessage:NSLocalizedString(@"Password is not matched", @"Cancel")];
            [[AppDelegate shareInstance]hideActivityIndicator];
        }
    }
}

- (IBAction)btnLoginAction:(id)sender
{
    NSLog(@"%@", [[NSUserDefaults standardUserDefaults] objectForKey:@"Device_Token"]);
    if([self validateAllValuesForLogin]==YES)
    {
        NSString *token;
        if([[NSUserDefaults standardUserDefaults] valueForKey:@"Device_Token"])
        {
            token = [[NSUserDefaults standardUserDefaults] valueForKey:@"Device_Token"];
        }
        else
        {
            token = @"123456";
        }
        [[AppDelegate shareInstance]showActivityIndicator];
        [self.view endEditing:YES];
            NSDictionary *params = @{
                                     @"email":txtFldUsernameLogin.text,
                                     @"password":txtFldPasswordLogin.text,
                                     @"device_type":@"ios",
                                     @"token":token
                                     };
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [[SingletonClass sharedManager] m_PostApiResponse:@"login" paramDiction:params islogout:NO  onCompletion:^(NSDictionary *response)
                 {
                     NSDictionary *responseDic = (NSDictionary *)response;
                     if ([[[responseDic objectForKey:@"payload"]valueForKey:@"status"]integerValue]==1)
                     {
                        
                         arrayForUserData = [[responseDic objectForKey:@"payload"] objectForKey:@"data"];
                         [[NSUserDefaults standardUserDefaults] setObject:arrayForUserData forKey:@"userData"];
                         [[NSUserDefaults standardUserDefaults] setValue:@"user_login" forKey:@"Login_Type"];
                         [[NSUserDefaults standardUserDefaults] synchronize];
                         
                         id<GAITracker> tracker = [GAI sharedInstance].defaultTracker;
                         [tracker set:kGAIScreenName value:@"User Login Screen"];
                         [tracker send:[[GAIDictionaryBuilder createScreenView] build]];

                         [tracker set:kGAIUserId value:[NSString stringWithFormat:@"%@", [arrayForUserData valueForKey:@"id"]]];

                         
                         [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"User id"
                                                                               action:[NSString stringWithFormat:@"%@", [arrayForUserData valueForKey:@"id"]]
                                                                                label:nil
                                                                                value:nil] build]];
                         NSLog(@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"userData"]);
                         dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.6 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                             [self.view endEditing:YES];
                             [[NSNotificationCenter defaultCenter] postNotificationName:@"MyCacheUpdatedNotification" object:self];

                         });
                         [[AppDelegate shareInstance] hideActivityIndicator];
                       //  [[AppDelegate shareInstance]reloadSideMenu];
                         if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"isCartView"]isEqualToString:@"yes"])
                         {
                             CheckoutViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"CheckoutViewController"];
                             [self.navigationController pushViewController:newView animated:YES];
                         }
                         else
                         {
                            // UserProfileViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"UserProfileViewController"];
                             
                             ViewController *hoveViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
                             
                             [self.navigationController pushViewController:hoveViewController animated:YES];
                         }
                         
                        
                     }
                     else
                     {
                         [[AppDelegate shareInstance]hideActivityIndicator];
                         dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.6 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                             [self.view endEditing:YES];
                             [[AppDelegate shareInstance] showAlertWithErrorMessage:[[responseDic objectForKey:@"payload"] valueForKey:@"message"]];
                         });
                      
                     }
                 }];
            });
    }
}
#pragma -mark method For alert
-(void)methodForAlert:(NSString *)message
{
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:nil
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        [btnSwitchOutlet setImage:[UIImage imageNamed:@"push-btn-green@1x"] forState:UIControlStateNormal];
        viewForLogin.hidden =NO;
        viewForRegistration.hidden =YES;
       
    }];
    [alert addAction:okAction];
  
    [self presentViewController:alert animated:YES completion:nil];
}

#pragma mark -
#pragma mark - Postal Code
- (IBAction)txtFld_PostCode_action:(UITextField *)sender {
    [self validatePostCode];
}

- (void)validatePostCode {
    if (self.txtFld_postCode.text.length >= 4) {
        if ([self checktext:self.txtFld_postCode.text forType:TextTypeNumeric]) {
            [self enableStreetCode:YES];
            [self.txtFld_StreetCode becomeFirstResponder];
        }
        else {
            [self enableStreetCode:NO];
            [self showAlertWithTitle:@"Wrong Input" message:@"Enter valid Post code. post code accepts only numbers"];
        }
    }
}

- (IBAction)txtFld_StreetCode_action:(UITextField *)sender {
    [self validateStreetCode];
}

- (void)validateStreetCode {
    if (self.txtFld_StreetCode.text.length >= 2) {
        if ([self checktext:self.txtFld_StreetCode.text forType:TextTypeAlphates]) {
            [self enableCityCode:YES];
            [self.txtFld_HouseCode becomeFirstResponder];
        }
        else {
            [self enableCityCode:NO];
            [self showAlertWithTitle:@"Wrong Input" message:@"Enter Valid street code"];
        }
    }
    else if([self.txtFld_StreetCode.text isEqualToString:@""]) {
        [self.txtFld_postCode becomeFirstResponder];
    }
}
- (IBAction)txtFld_HouseCode_action:(UITextField *)sender {
    [self validateHouseCode];
}

- (void)validateHouseCode {
    if (self.txtFld_HouseCode.text.length >= 1) {
        if ([self checktext:self.txtFld_HouseCode.text forType:TextTypeNumeric]) {
            [self.txtFld_HouseCode resignFirstResponder];
            NSDictionary *postalCodeDetails = @{kPostalCode : self.txtFld_postCode.text, kStreetCode : self.txtFld_StreetCode.text, kHouseNumber : self.txtFld_HouseCode.text};
            [self searchAddressWithPostCodeDetails:postalCodeDetails];
            self.dict_postalCodeDetails = [NSMutableDictionary dictionaryWithDictionary:postalCodeDetails];
        }
        else {
            [self showAlertWithTitle:@"Wrong Input" message:@"Enter valid house number"];
        }
    }
    else if([self.txtFld_HouseCode.text isEqualToString:@""]) {
        [self.txtFld_HouseCode becomeFirstResponder];
    }
}

- (void)enableCityCode:(BOOL)value {
    self.txtFld_HouseCode.userInteractionEnabled = value;
    [self hideHouseCodePart:NO];
}

- (void)enableStreetCode:(BOOL)value {
    self.txtFld_StreetCode.userInteractionEnabled = value;
}

- (BOOL)checktext:(NSString *)txtFld_string forType:(TextType)textType{
    NSCharacterSet *textCharacterSet;
    if (textType == TextTypeNumeric) {
        textCharacterSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
    }
    else if (textType == TextTypeAlphates) {
        textCharacterSet = NSCharacterSet.letterCharacterSet;
    }
    NSCharacterSet *characterSetFromTextField = [NSCharacterSet characterSetWithCharactersInString:txtFld_string];
    
    BOOL stringIsValid = [textCharacterSet isSupersetOfSet:characterSetFromTextField];
    return stringIsValid;
}


- (void)searchAddressWithPostCodeDetails:(NSDictionary *)postCodeDetails {
    NSDictionary *params = @{
                             @"wijkcode":[postCodeDetails valueForKey:kPostalCode],
                             @"lettercombinatie":[postCodeDetails valueForKey:kStreetCode],
                             @"huisnr":[postCodeDetails valueForKey:kHouseNumber],
                             };
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [[SingletonClass sharedManager] m_PostAddressSuggestionApiResponse:@"delivery-agent-get-address" paramDiction:params onCompletion:^(NSDictionary *response)
         {
             NSDictionary *responseDic = (NSDictionary *)response;
             if ([[[responseDic objectForKey:@"payload"]valueForKey:@"status"]integerValue]==1)
             {
                 arraySearchData = [[responseDic objectForKey:@"payload"] objectForKey:@"data"];
                 tblSearch.hidden = NO;
                 [tblSearch reloadData];
             }
             else
             {
                 dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.6 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                     [self.view endEditing:YES];
                     [[AppDelegate shareInstance] showAlertWithErrorMessage:[[responseDic objectForKey:@"payload"] valueForKey:@"message"]];
                     tblSearch.hidden = YES;
                 });
             }
         }];
    });
}


- (void)showAlertWithTitle:(NSString *)alertTitle message:(NSString *)alertMessage {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:alertTitle message:alertMessage preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *OKBtn = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
    [alert addAction:OKBtn];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)hideHouseCodePart:(BOOL)value {
    
    self.lbl_hiphenHouseCode.hidden = value;
    self.txtFld_HouseCode.text = @"";
    self.txtFld_HouseCode.hidden = value;
    if ([[self.dict_postalCodeDetails allKeys] containsObject:kHouseNumber]) {
        [self.dict_postalCodeDetails removeObjectForKey:kHouseNumber];
    }
}

#pragma mark -
#pragma mark - Check Validation Methods
-(BOOL)checkValidations
{
    NSString    *emailid            = txtFldEmail.text;
    NSString    *emailRegex         = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,20}";
    NSPredicate *emailTest          =[NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    return[emailTest evaluateWithObject:emailid];
}
//Method for validation for first name
-(BOOL)validFirstName
{
    NSString *emailid = txtFldName.text;
    NSString *emailRegex = @"[a-zA-z]+([ '-][a-zA-Z]+)*$";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:emailid];
}
//Method for validation for first name
-(BOOL)validPostcode
{
    NSString *emailid = txtFldPostCode.text;
    NSString *emailRegex = @"([0-9]{4})-[a-zA-Z]{2}-[0-9]{1,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:emailid];
}
//Method for validation for first name
-(BOOL)validPostcode1
{
    NSString *emailid = txtFldPostCode.text;
    NSString *emailRegex = @"([0-9]{4})[a-zA-Z]{2}[0-9]{1,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:emailid];
}

//Method for password validation
-(BOOL)isValidPassword:(NSString *)passwordString
{
    NSString *stricterFilterString = @"^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[$@$!%*?&])[A-Za-z\\d$@$!%*?&]{10,}";
    NSPredicate *passwordTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", stricterFilterString];
    return [passwordTest evaluateWithObject:passwordString];
}
#pragma -mark Custom Methods
//Method For Check Empty Fields
-(BOOL)emptyFieldsValidation
{
    txtFldName.text =[txtFldName.text stringByTrimmingCharactersInSet:
                         [NSCharacterSet whitespaceCharacterSet]];
    txtFldAddress.text =[txtFldAddress.text stringByTrimmingCharactersInSet:
                      [NSCharacterSet whitespaceCharacterSet]];
    self.txtFld_postCode.text =[self.txtFld_postCode.text stringByTrimmingCharactersInSet:
                         [NSCharacterSet whitespaceCharacterSet]];
    self.txtFld_StreetCode.text =[self.txtFld_StreetCode.text stringByTrimmingCharactersInSet:
                                [NSCharacterSet whitespaceCharacterSet]];
    self.txtFld_HouseCode.text =[self.txtFld_HouseCode.text stringByTrimmingCharactersInSet:
                                  [NSCharacterSet whitespaceCharacterSet]];
    txtFldCity.text =[txtFldCity.text stringByTrimmingCharactersInSet:
                          [NSCharacterSet whitespaceCharacterSet]];
    txtFldPhoneNumber.text =[txtFldPhoneNumber.text stringByTrimmingCharactersInSet:
                          [NSCharacterSet whitespaceCharacterSet]];
    txtFldEmail.text =[txtFldEmail.text stringByTrimmingCharactersInSet:
                          [NSCharacterSet whitespaceCharacterSet]];
    txtFldPassword.text =[txtFldPassword.text stringByTrimmingCharactersInSet:
                       [NSCharacterSet whitespaceCharacterSet]];
    txtFldConfirmPassword.text =[txtFldConfirmPassword.text stringByTrimmingCharactersInSet:
                       [NSCharacterSet whitespaceCharacterSet]];
   
    if (txtFldName.text.length==0||txtFldAddress.text.length==0 ||(self.txtFld_postCode.text.length==0 || self.txtFld_StreetCode.text.length == 0)||txtFldCity.text.length==0 || txtFldPhoneNumber.text.length==0 || txtFldEmail.text.length==0|| txtFldPassword.text.length==0|| txtFldConfirmPassword.text.length==0)
    {
        return NO;
    }
    return YES;
    
}

// Validations on Textfields
-(BOOL) validateAllValues
{
    if (![self emptyFieldsValidation] )
    {
        [[AppDelegate shareInstance] showAlertWithErrorMessage:NSLocalizedString(@"Please fill all fields.", @"Cancel")];
        return NO;
    }
    else if (![self validFirstName])
    {
          [[AppDelegate shareInstance] showAlertWithErrorMessage:NSLocalizedString(@"Enter Valid  Name.", @"Cancel")];
        return NO;
    }
    else if (![self checkValidations])
    {
         [[AppDelegate shareInstance] showAlertWithErrorMessage:NSLocalizedString(@"Enter Valid Email Id", @"Cancel")];
        return NO;
    }
    return YES;
}

#pragma mark - Validations For Login
-(BOOL)emptyFieldsValidationForLogin
{
    txtFldUsernameLogin.text =[txtFldUsernameLogin.text stringByTrimmingCharactersInSet:
                      [NSCharacterSet whitespaceCharacterSet]];
    txtFldPasswordLogin.text =[txtFldPasswordLogin.text stringByTrimmingCharactersInSet:
                         [NSCharacterSet whitespaceCharacterSet]];
    
    
    if (txtFldPasswordLogin.text.length==0||txtFldUsernameLogin.text.length==0 )
    {
        return NO;
    }
    return YES;
    
}

-(BOOL)checkEmailValidationForLogin
{
    NSString    *emailid            = txtFldUsernameLogin.text;
    NSString    *emailRegex         = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,20}";
    NSPredicate *emailTest          =[NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    return[emailTest evaluateWithObject:emailid];
}

// Validations on Textfields
-(BOOL) validateAllValuesForLogin
{
    if (![self emptyFieldsValidationForLogin] )
    {
         [[AppDelegate shareInstance] showAlertWithErrorMessage:NSLocalizedString(@"Please fill all fields.", @"Cancel")];
        return NO;
    }
    else if (![self checkEmailValidationForLogin])
    {
        [[AppDelegate shareInstance] showAlertWithErrorMessage:NSLocalizedString(@"Enter Valid Email Id", @"Cancel")];
        return NO;
    }
    return YES;
}


-(BOOL)checkEmailValidationForForgot
{
    NSString    *emailid            = txtForgotPassword.text;
    NSString    *emailRegex         = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,20}";
    NSPredicate *emailTest          =[NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    return[emailTest evaluateWithObject:emailid];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)btnForgotPasswordAction:(id)sender
{
    viewForForgotPassword.hidden=NO;
}

- (IBAction)btnFacebookAction:(id)sender
{
    if (checkForButtonClick == false)
    {
        checkForButtonClick = true;
        FBSDKLoginManager *FBLogin = [[FBSDKLoginManager alloc] init];
        FBLogin.loginBehavior = FBSDKLoginBehaviorWeb;
        [FBLogin logOut];
        
        [FBLogin logInWithReadPermissions:@[@"public_profile",@"email"] fromViewController:nil handler:^(FBSDKLoginManagerLoginResult *result, NSError *error)
         {
             //btnFacebookOutlet.enabled = false;
             [[AppDelegate shareInstance]hideActivityIndicator];
             if (error)
             {
                 //show error message
                 // btnFacebookOutlet.enabled = true;
                 [[AppDelegate shareInstance]showAlertWithErrorMessage:error.description];
                 [[AppDelegate shareInstance]hideActivityIndicator];
                 return ;
             }
             else if (result.isCancelled)
             {
                   // btnFacebookOutlet.enabled = true;
                 checkForButtonClick = false;
                 return;
             }
             if ([FBSDKAccessToken currentAccessToken])
             {
                 [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields":@"picture, email, first_name, last_name ,id,cover"}]
                  startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error)
                  {
                      if (!error)
                      {
                          NSLog(@"%@",result);
                          userID = [result  valueForKey:@"email"];
                          userName = [result  valueForKey:@"first_name"];
                          ProfilePic=[[[result  objectForKey:@"picture"]valueForKey:@"data"]valueForKey:@"url"];
                          loginType =@"facebook";
                          [[NSUserDefaults standardUserDefaults] setValue:loginType forKey:@"Login_Type"];
                          
                          /*Saving facebook data to defaults*/
                          [[NSUserDefaults standardUserDefaults] setValue:userID forKey:@"fb_email"];
                          [[NSUserDefaults standardUserDefaults] setValue:userName forKey:@"fb_username"];
                          [[NSUserDefaults standardUserDefaults] setValue:ProfilePic forKey:@"fb_pic"];
                          
                          NSString *token;
                          if([[NSUserDefaults standardUserDefaults] valueForKey:@"Device_Token"])
                          {
                              token = [[NSUserDefaults standardUserDefaults] valueForKey:@"Device_Token"];
                          }
                          else
                          {
                              token = @"123456";
                          }
                          dispatch_async(dispatch_get_main_queue(), ^{
                              [[AppDelegate shareInstance]showActivityIndicator];
                              NSDictionary *params = @{//@"email":[result  valueForKey:@"id"],
                                                       @"fb_id":[result  valueForKey:@"id"],
                                                       @"device_id":@"13245664544",
                                                       @"device_type":@"IOS",
                                                       @"token":token
                                                       };
                              dispatch_async(dispatch_get_main_queue(), ^{
                                  [[SingletonClass sharedManager] m_PostApiResponse:@"is_login" paramDiction:params  islogout:NO onCompletion:^(NSDictionary *response)
                                   {
                                       dispatch_async(dispatch_get_main_queue(), ^{
                                           [[AppDelegate shareInstance]hideActivityIndicator];
                                       });
                                       NSDictionary *responseDic = (NSDictionary *)response;
                                       if ([[[responseDic objectForKey:@"payload"]valueForKey:@"status"]integerValue]==1)
                                       {
                                           arrayForUserData = [[responseDic objectForKey:@"payload"] objectForKey:@"data"];
                                           [[NSUserDefaults standardUserDefaults] setObject:arrayForUserData forKey:@"userData"];
                                           [[NSUserDefaults standardUserDefaults] synchronize];
                                           NSLog(@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"userData"]);
                                           [[AppDelegate shareInstance]hideActivityIndicator];
                                            [[NSNotificationCenter defaultCenter] postNotificationName:@"MyCacheUpdatedNotification" object:self];
                                           if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"isCartView"]isEqualToString:@"yes"])
                                           {
                                               CheckoutViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"CheckoutViewController"];
                                               [self.navigationController pushViewController:newView animated:YES];
                                           }
                                           else
                                           {
                                               ViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
                                               [self.navigationController pushViewController:newView animated:YES];
                                           }
                                       }
                                       else if([[[responseDic objectForKey:@"payload"]valueForKey:@"status"]integerValue]==2)
                                       {
                                           dispatch_async(dispatch_get_main_queue(), ^{
                                               FacebookRegistrationViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"FacebookRegistrationViewController"];
                                               newView.userId =[result  valueForKey:@"id"];
                                               [self.navigationController pushViewController:newView animated:YES];
                                           });
                                       }
                                   }];
                              });
                              
                          });
                          
                      }
                  }];
             }
             else
             {
                  [[AppDelegate shareInstance]hideActivityIndicator];
             }
         }];
    }
}
- (IBAction)btnCloseForgotViewAction:(id)sender
{
     viewForForgotPassword.hidden=YES;
}
- (IBAction)btnResetPasswordAction:(id)sender
{
    [self.view endEditing:YES];
    UIButton *button = (UIButton *)sender;
    NSString *buttonTitle = button.currentTitle;
    if ([buttonTitle isEqualToString:@"Submit"])
    {
        if (![txtForgotPassword.text isEqualToString:@""])
        {
            if ([otp isEqualToString:txtForgotPassword.text])
            {
                ChangePasswordViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"ChangePasswordViewController"];
                newView.textUserId =userId;
                newView.checkForFields=true;
                [self.navigationController pushViewController:newView animated:YES];
            }
            else
            {
                [[AppDelegate shareInstance] showAlertWithErrorMessage:NSLocalizedString(@"OTP is not correct.", @"Cancel")];
            }
        }
        else
        {
           
             [[AppDelegate shareInstance] showAlertWithErrorMessage:NSLocalizedString(@"Please enter your OTP.", @"Cancel")];
        }
        
    }
    else
    {
        txtForgotPassword.text =[txtForgotPassword.text stringByTrimmingCharactersInSet:
                                 [NSCharacterSet whitespaceCharacterSet]];
        if (![txtForgotPassword.text isEqualToString:@""])
        {
            if ([self checkEmailValidationForForgot])
            {
                [self callWebserviceForForgotPassword];
            }
            else
            {
                [[AppDelegate shareInstance] showAlertWithErrorMessage:NSLocalizedString(@"Enter Valid Email Id", @"Cancel")];

            }
        }
        else
        {
             [[AppDelegate shareInstance] showAlertWithErrorMessage:NSLocalizedString(@"Enter your email address", @"Cancel")];
        }
  
    }
}

-(void)callWebserviceForForgotPassword
{
    [[AppDelegate shareInstance]showActivityIndicator];
    NSDictionary *params = @{
                             @"email":txtForgotPassword.text,
                             };
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [[SingletonClass sharedManager] m_PostApiResponse:@"forget_password" paramDiction:params islogout:NO  onCompletion:^(NSDictionary *response)
         {
             NSDictionary *responseDic = (NSDictionary *)response;
             if ([[[responseDic objectForKey:@"payload"]valueForKey:@"status"]integerValue]==1)
             {
                 NSLog(@"%@", [[responseDic objectForKey:@"payload"] objectForKey:@"data"]);
                 otp =[[responseDic objectForKey:@"payload"] objectForKey:@"otp"];
                 UIAlertController * alert=   [UIAlertController
                                               alertControllerWithTitle:nil
                                               message:[[responseDic objectForKey:@"payload"] objectForKey:@"message"]
                                               preferredStyle:UIAlertControllerStyleAlert];
                 
                 UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                 {
                 
                     //UIColor *color = [UIColor colorWithRed:210/255.0f green:45/255.0f blue:62/255.0f alpha:1];
                     UIColor *color = [UIColor lightGrayColor];

                       [txtForgotPassword setValue:color forKeyPath:@"_placeholderLabel.textColor"];
                     txtForgotPassword.placeholder =@"Enter your OTP";
                     [btnResetPassword setTitle:@"Submit" forState:UIControlStateNormal];
                     txtForgotPassword.text =@"";
                     userId =[[responseDic objectForKey:@"payload"] objectForKey:@"user_id"];
                 }];
                 [alert addAction:okAction];
                 [self presentViewController:alert animated:YES completion:nil];
                 [[AppDelegate shareInstance]hideActivityIndicator];
                 
             }
             else
             {
                 [[AppDelegate shareInstance]hideActivityIndicator];
                 dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.6 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                     [self.view endEditing:YES];
                     [[AppDelegate shareInstance] showAlertWithErrorMessage:[[responseDic objectForKey:@"payload"] valueForKey:@"message"]];
                 });
             }
         }];
    });
}
#pragma -mark UITextField delegates

-(void)textFieldDidEndEditing:(UITextField *)textField{
    int numberofCharacters = 0;
    BOOL lowerCaseLetter=0,upperCaseLetter=0,digit=0,specialCharacter = 0;
//    if(textField==txtFldPassword)
//    {
//        if([txtFldPassword.text length] >= 8)
//        {
//            for (int i = 0; i < [txtFldPassword.text length]; i++)
//            {
//                unichar c = [txtFldPassword.text characterAtIndex:i];
//                if(!lowerCaseLetter)
//                {
//                    lowerCaseLetter = [[NSCharacterSet lowercaseLetterCharacterSet] characterIsMember:c];
//                }
////                if(!upperCaseLetter)
////                {
////                    upperCaseLetter = [[NSCharacterSet uppercaseLetterCharacterSet] characterIsMember:c];
////                }
//                if(!digit)
//                {
//                    digit = [[NSCharacterSet decimalDigitCharacterSet] characterIsMember:c];
//                }
//                if(!specialCharacter)
//                {
//                    NSRange range;
//                    NSString* s = [NSString stringWithCharacters:&c length:1];
//                    NSCharacterSet *lowerCaseChars = [NSCharacterSet characterSetWithCharactersInString:@"!@#$%^&*()_+"];
//                    range = [s rangeOfCharacterFromSet:lowerCaseChars];
//                    if ( !range.length )
//                    {
//                        specialCharacter=NO;
//                    }
//                    else{
//                        specialCharacter= YES;
//                    }
//                    //specialCharacter = [[NSCharacterSet symbolCharacterSet] characterIsMember:c];
//                }
//            }
//            
//            if(specialCharacter && digit && lowerCaseLetter )
//            {
//                //do what u want
//            }
//            else
//            {
//                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
//                                                                message:@"Please Ensure that you have at least one lower case letter,  one digit and one special character"
//                                                               delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//                [alert show];
//                txtFldPassword.text=@"";
//            }
//            
//        }
//        else
//        {
//            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
//                                                            message:@"Please Enter at least 8 password"
//                                                           delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//            [alert show];
//            txtFldPassword.text=@"";
//        }
//    }
//    else if(textField==txtForgotPassword)
//    {
//        if([txtForgotPassword.text length] >= 8)
//        {
//            for (int i = 0; i < [txtForgotPassword.text length]; i++)
//            {
//                unichar c = [txtForgotPassword.text characterAtIndex:i];
//                if(!lowerCaseLetter)
//                {
//                    lowerCaseLetter = [[NSCharacterSet lowercaseLetterCharacterSet] characterIsMember:c];
//                }
////                if(!upperCaseLetter)
////                {
////                    upperCaseLetter = [[NSCharacterSet uppercaseLetterCharacterSet] characterIsMember:c];
////                }
//                if(!digit)
//                {
//                    digit = [[NSCharacterSet decimalDigitCharacterSet] characterIsMember:c];
//                }
//                if(!specialCharacter)
//                {
//                    NSRange range;
//                    NSString* s = [NSString stringWithCharacters:&c length:1];
//                    NSCharacterSet *lowerCaseChars = [NSCharacterSet characterSetWithCharactersInString:@"!@#$%^&*()_+"];
//                    range = [s rangeOfCharacterFromSet:lowerCaseChars];
//                    if ( !range.length )
//                    {
//                        specialCharacter=NO;
//                    }
//                    else{
//                        specialCharacter= YES;
//                    }
//                    //specialCharacter = [[NSCharacterSet symbolCharacterSet] characterIsMember:c];
//                }
//            }
//
//            if(specialCharacter && digit && lowerCaseLetter)
//            {
//                //do what u want
//            }
//            else
//            {
//                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
//                                                                message:@"Please Ensure that you have at least one lower case letter, one digit and one special character"
//                                                               delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//                [alert show];
//                txtForgotPassword.text=@"";
//            }
//
//        }
//        else
//        {
//            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
//                                                            message:@"Please Enter at least 8 password"
//                                                           delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//            [alert show];
//            txtForgotPassword.text=@"";
//        }
//    }
}

-(void) textFieldDidChange:(UITextField *) theTextField
{
    if(txtFldPostCode.text.length < 6)
    {
        tblSearch.hidden = YES;
    }
    
    if ([txtFldPostCode.text rangeOfString:@"-"].location == NSNotFound)
    {
        if (txtFldPostCode.text.length > 6)
        {
            if (![self validPostcode1])
            {
                [[AppDelegate shareInstance] showAlertWithErrorMessage:NSLocalizedString(@"Enter Valid  Postcode.", @"Cancel")];
            }
            else
            {
                [self searchText:txtFldPostCode replacementString:txtFldPostCode.text];
            }
        }
    }
    else
    {
        if (txtFldPostCode.text.length > 8)
        {
            if (![self validPostcode])
            {
                [[AppDelegate shareInstance] showAlertWithErrorMessage:NSLocalizedString(@"Enter Valid  Postcode.", @"Cancel")];
            }
            else
            {
                [self searchText:txtFldPostCode replacementString:txtFldPostCode.text];
            }
        }
    }
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [txtFldPostCode resignFirstResponder];
    [txtFldCity resignFirstResponder];
    [txtFldName resignFirstResponder];
    [txtFldEmail resignFirstResponder];
    [txtFldAddress resignFirstResponder];
    [txtFldPassword resignFirstResponder];
    [txtFldPhoneNumber resignFirstResponder];
    [txtForgotPassword resignFirstResponder];
    [txtFldPasswordLogin resignFirstResponder];
    [txtFldUsernameLogin resignFirstResponder];
    [txtFldConfirmPassword resignFirstResponder];
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    if(textField == self.txtFld_StreetCode) {
        [self hideHouseCodePart:NO];
        if (txtFldAddress.text.length != 0) {
            txtFldAddress.text = @"";
        }
        if (txtFldCity.text.length != 0) {
            txtFldCity.text = @"";
        }
    }
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    BOOL isValidate = NO;
    if(range.length + range.location > textField.text.length)
    {
        isValidate = NO;
    }
    
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    if(textField == self.txtFld_postCode) {
        isValidate =  (newLength <=4)?YES:NO;
        if (newLength == 5) {
            [self validatePostCode];
        }
    }
    else if (textField == self.txtFld_StreetCode) {
        isValidate =  (newLength <=2)?YES:NO;
        if (newLength == 3) {
            [self validateStreetCode];
        }
        
    }
    else if (textField == self.txtFld_HouseCode) {
        isValidate =  (newLength <=3)?YES:NO;
    }
    else if(textField == txtFldPhoneNumber)
    {
        if ([string rangeOfCharacterFromSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].location != NSNotFound)
        {
            return NO;
        }
        if(range.length + range.location > txtFldPhoneNumber.text.length)
        {
            return NO;
        }
        
        NSUInteger newLength = [txtFldPhoneNumber.text length] + [string length] - range.length;
        isValidate =  (newLength <=10)?YES:NO;
    }
    else {
        isValidate = YES;
    }
    
    //    if(textField == txtFldPostCode)
    //    {
    //        if ([string rangeOfCharacterFromSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].location != NSNotFound)
    //        {
    //            return NO;
    //        }
    //        if(range.length + range.location > txtFldPostCode.text.length)
    //        {
    //            return NO;
    //        }
    //
    //        NSUInteger newLength = [txtFldPostCode.text length] + [string length] - range.length;
    //        isValidate =  (newLength <=20)?YES:NO;
    //    }
    
    return isValidate;
}
-(void) searchText:(UITextField *)textField replacementString:(NSString *)string
{
    NSString *tempStr = textField.text;
    NSString *postcode;
    NSString *street;
    NSString *houseNumber;
    if ([string rangeOfString:@"-"].location == NSNotFound)
    {
        if(tempStr.length == 7)
        {
            postcode = [tempStr substringWithRange:NSMakeRange(0,4)];
            street = [tempStr substringWithRange:NSMakeRange(4,2)];
            houseNumber = [tempStr substringWithRange:NSMakeRange(6,1)];
        }
        else if(tempStr.length == 8)
        {
            postcode = [tempStr substringWithRange:NSMakeRange(0,4)];
            street = [tempStr substringWithRange:NSMakeRange(4,2)];
            houseNumber = [tempStr substringWithRange:NSMakeRange(6,2)];
        }
        else if(tempStr.length == 9)
        {
            postcode = [tempStr substringWithRange:NSMakeRange(0,4)];
            street = [tempStr substringWithRange:NSMakeRange(4,2)];
            houseNumber = [tempStr substringWithRange:NSMakeRange(6,3)];
        }
        if(tempStr.length == 10)
        {
            postcode = [tempStr substringWithRange:NSMakeRange(0,4)];
            street = [tempStr substringWithRange:NSMakeRange(4,2)];
            houseNumber = [tempStr substringWithRange:NSMakeRange(6,4)];
        }
        
    }
    else
    {
        NSArray *arrTemp = [tempStr componentsSeparatedByString:@"-"];
        postcode = [arrTemp objectAtIndex:0];
        street = [arrTemp objectAtIndex:1];
        houseNumber = [arrTemp objectAtIndex:2];
    }
    
    
    NSDictionary *params = @{
                             @"wijkcode":postcode,
                             @"lettercombinatie":street,
                             @"huisnr":houseNumber,
                             };
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [[SingletonClass sharedManager] m_PostAddressSuggestionApiResponse:@"delivery-agent-get-address" paramDiction:params onCompletion:^(NSDictionary *response)
         {
             NSDictionary *responseDic = (NSDictionary *)response;
             if ([[[responseDic objectForKey:@"payload"]valueForKey:@"status"]integerValue]==1)
             {
                 arraySearchData = [[responseDic objectForKey:@"payload"] objectForKey:@"data"];
                 [txtFldPostCode resignFirstResponder];
                 tblSearch.hidden = NO;
                 [tblSearch reloadData];
             }
             else
             {
                 dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.6 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                     [self.view endEditing:YES];
                     [[AppDelegate shareInstance] showAlertWithErrorMessage:[[responseDic objectForKey:@"payload"] valueForKey:@"message"]];
                     tblSearch.hidden = YES;
                 });
             }
         }];
    });
}
#pragma mark - TableView DataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [arraySearchData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"Cell"];
    }
    NSMutableDictionary *dict = [NSMutableDictionary new];
    dict = [arraySearchData objectAtIndex:indexPath.row];
    cell.textLabel.text =[NSString stringWithFormat:@"%@,%@",[dict objectForKey:@"straatnaam"],[dict objectForKey:@"plaatsnaam"]];
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSMutableDictionary *dict = [NSMutableDictionary new];
    dict = [arraySearchData objectAtIndex:indexPath.row];
    [self hideHouseCodePart:YES];
    tblSearch.hidden = YES;
    txtFldPostCode.text=@"";
    txtFldPostCode.text=[NSString stringWithFormat:@"%@-%@-%@",[dict objectForKey:@"wijkcode"],[dict objectForKey:@"lettercombinatie"],[dict objectForKey:@"huisnr_van"]];
    txtFldCity.text = [dict objectForKey:@"plaatsnaam"];
    txtFldAddress.text = [NSString stringWithFormat:@"%@",[dict objectForKey:@"straatnaam"]];
}
@end
