//
//  PostalCodeTableViewCell.h
//  Thuis today
//
//  Created by Rajeev Lochan Ranga on 26/02/18.
//  Copyright © 2018 Offshore Software Solutions. All rights reserved.
//
@class PostalCodeTableViewCell;
@protocol PostalCodeTableViewCellDelegate <NSObject>
@optional
- (void)txtFld_PostCode_action:(UITextField *)textField forPostalCodeTableViewCell:(PostalCodeTableViewCell *)PostalCodeTableViewCell;
- (void)
txtFld_streetCode_action:(UITextField *)textField forPostalCodeTableViewCell:(PostalCodeTableViewCell *)PostalCodeTableViewCell;
- (void)txtFld_HouseCode_action:(UITextField *)textField forPostalCodeTableViewCell:(PostalCodeTableViewCell *)PostalCodeTableViewCell;

- (void)buttonEdit_action:(UIButton *)sender forPostalCodeTableViewCell:(PostalCodeTableViewCell *)PostalCodeTableViewCell;
@end

#import <UIKit/UIKit.h>


@interface PostalCodeTableViewCell : UITableViewCell

@property (nonatomic) id<PostalCodeTableViewCellDelegate>delegate;
@property (weak, nonatomic) IBOutlet UILabel *lbl_hiphenHouseCode;

@property (weak, nonatomic) IBOutlet UIButton *btnEdit;
@property (weak, nonatomic) IBOutlet UITextField *txtFld_postCode;
@property (weak, nonatomic) IBOutlet UITextField *txtFld_StreetCode;
@property (weak, nonatomic) IBOutlet UITextField *txtFld_HouseCode;
@end
