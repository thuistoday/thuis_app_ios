//
//  ContactUsViewController.h
//  Thuis today
//
//  Created by offshore_mac_1 on 18/08/17.
//  Copyright © 2017 IMMANENT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ContactUsViewController : UIViewController
{
    IBOutlet UILabel *lblCount;
    IBOutlet UITextField *txtSubject;
    
    IBOutlet UITextField *txtMessage;
    IBOutlet UITextField *txtContact;
    IBOutlet UITextField *txtEmail;
    IBOutlet UITextField *txtName;
}

- (IBAction)btnCartAction:(id)sender;
- (IBAction)btnMenuAction:(id)sender;
- (IBAction)btnProceedAction:(id)sender;


- (IBAction)btnBottomBackAction:(id)sender;
- (IBAction)btnBottomCartAction:(id)sender;
- (IBAction)btnBottomHomeAction:(id)sender;
@end
