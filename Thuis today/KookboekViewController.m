//
//  KookboekViewController.m
//  RecipeTinder8
//
//  Created by offshore_mac_1 on 11/10/17.
//  Copyright © 2017 offshore_mac_1. All rights reserved.
//

#import "KookboekViewController.h"

@interface KookboekViewController ()
{
    NSMutableArray *arrData , *arrCategoryId ,*arrTags;
    NSInteger minValue ,maxValue;
    NSInteger tag;
}
@property (strong, nonatomic) IBOutlet BSStackView *stackView;
@end

@implementation KookboekViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    arrData = [NSMutableArray new];
    arrCategoryId = [NSMutableArray new];
    arrTags = [NSMutableArray new];
    [self callWebserviceForFetchRecipe];
    
    [self.rangeSlider setMinValue:5 maxValue:60];
    [self.rangeSlider setLeftValue:5 rightValue:60];
    
    minValue = 5;
    maxValue = 60;
    
    self.rangeSlider.minimumDistance = 1;
    
    _btnFilter.layer.cornerRadius = 6;
    _btnFilterView.layer.cornerRadius = 6;
    _textFieldView.layer.borderColor =(__bridge CGColorRef _Nullable)([UIColor blueColor]);

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (UIView *)viewWithLabel:(NSInteger)index
{
    RecipeSwipeViewController *swipeView = [self.storyboard instantiateViewControllerWithIdentifier:@"RecipeSwipeViewController"];
    swipeView.recipeData = arrData;
    swipeView.index = index;
    UIView *view = swipeView.view;
    view.layer.borderWidth = 1.0;
    view.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
     return view;
   
}

#pragma mark - Private Methods
-(void) callWebserviceForFetchRecipe
{
    //[[AppDelegate shareInstance]showActivityIndicator];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        NSString *_cityName =  [[NSUserDefaults standardUserDefaults] valueForKey:@"Cityname"];
    
        NSString *_zipCode =  [[NSUserDefaults standardUserDefaults] valueForKey:@"Zipcode"];
        
        if (_zipCode == nil) {
            _zipCode = @"";
        }
        NSDictionary *params;
        if (![_zipCode isEqualToString:@""]) {
            params = @{
                       @"zipcode":[NSString stringWithFormat:@"%@",_zipCode],
                       @"cityname":@""
                       };
        }
        else if (![_cityName isEqualToString:@""]) {
            params = @{
                       @"zipcode":@"",
                       @"cityname":[NSString stringWithFormat:@"%@",_cityName]
                       };
        }
        
        [[SingletonClass sharedManager] m_PostApiResponseRecipe:@"recipes-get" paramDiction:params   onCompletion:^(NSDictionary *response)
         {
             NSDictionary *responseDic = (NSDictionary *)response;
             if ([[[responseDic objectForKey:@"payload"] objectForKey:@"status"] integerValue]==1)
             {
                 [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"No_Recipes"];
                 arrData = [[responseDic objectForKey:@"payload"] objectForKey:@"data"];
                 
                 //[[AppDelegate shareInstance]hideActivityIndicator];
                 NSMutableArray *views = [NSMutableArray array];
                 for (NSInteger i = 0; i < [arrData count]; i++)
                 {
                     [views addObject:[self viewWithLabel:i]];
                 }
                 
                 self.stackView.swipeDirections = UISwipeGestureRecognizerDirectionLeft | UISwipeGestureRecognizerDirectionRight;
                 self.stackView.forwardDirections = UISwipeGestureRecognizerDirectionRight | UISwipeGestureRecognizerDirectionLeft;
                 self.stackView.changeAlphaOnSendAnimation = YES;
                 [self.stackView configureWithViews:views];
                 self.stackView.delegate = self;
             }
             else
             {
                // [[AppDelegate shareInstance]hideActivityIndicator];
                 dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.6 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                     [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"No_Recipes"];
                     [self.view endEditing:YES];
                     //[[AppDelegate shareInstance] showAlertWithErrorMessage:[[responseDic objectForKey:@"payload"] valueForKey:@"message"]];
                 });
                 
             }
         }];
    });
}
-(void) callWebserviceForFetchFilterRecipe
{
    arrData = [NSMutableArray new];
    [[AppDelegate shareInstance]showActivityIndicator];
    
    NSError* error = nil;
    NSData* data = [NSJSONSerialization dataWithJSONObject:arrCategoryId
                                                   options:0 error:&error];
    
    NSString* catIds;
    catIds = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    catIds = [catIds stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    catIds = [catIds stringByReplacingOccurrencesOfString:@"\\" withString:@""];
    catIds = [catIds stringByReplacingOccurrencesOfString:@"[" withString:@"{"];
    catIds = [catIds stringByReplacingOccurrencesOfString:@"]" withString:@"}"];
    
    NSData* data1 = [NSJSONSerialization dataWithJSONObject:arrTags
                                                   options:0 error:&error];
    
    NSString* tags;
    tags = [[NSString alloc] initWithData:data1 encoding:NSUTF8StringEncoding];
    tags = [tags stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    tags = [tags stringByReplacingOccurrencesOfString:@"\\" withString:@""];
    tags = [tags stringByReplacingOccurrencesOfString:@"[" withString:@"{"];
    tags = [tags stringByReplacingOccurrencesOfString:@"]" withString:@"}"];
    
    NSString *_cityName =  [[NSUserDefaults standardUserDefaults] valueForKey:@"Cityname"];
    
    NSString *_zipCode =  [[NSUserDefaults standardUserDefaults] valueForKey:@"Zipcode"];
    
    if (_zipCode == nil) {
        _zipCode = @"";
    }
    NSDictionary *params;
    if (![_zipCode isEqualToString:@""]) {
        params = @{
                   @"zipcode":[NSString stringWithFormat:@"%@",_zipCode],
                   @"cityname":@"",
                   @"maxtime":[NSString stringWithFormat:@"%ld",maxValue],
                   @"mintime":[NSString stringWithFormat:@"%ld",minValue],
                   @"tag":tags,
                   @"cat":catIds
                   };
    }
    else if (![_cityName isEqualToString:@""]) {
        params = @{
                   @"zipcode":@"",
                   @"cityname":[NSString stringWithFormat:@"%@",_cityName],
                   @"maxtime":[NSString stringWithFormat:@"%ld",maxValue],
                   @"mintime":[NSString stringWithFormat:@"%ld",minValue],
                   @"tag":tags,
                   @"cat":catIds
                   };
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [[SingletonClass sharedManager] m_PostApiResponseRecipe:@"recipes-get" paramDiction:params   onCompletion:^(NSDictionary *response)
         {
             NSDictionary *responseDic = (NSDictionary *)response;
             if ([[[responseDic objectForKey:@"payload"]valueForKey:@"status"]integerValue]==1)
             {
                 _FilterView.hidden = YES;
                 arrData = [[responseDic objectForKey:@"payload"] objectForKey:@"data"];
                 
                 [[AppDelegate shareInstance]hideActivityIndicator];
                 NSMutableArray *views = [NSMutableArray array];
                 for (NSInteger i = 0; i < [arrData count]; i++)
                 {
                     [views addObject:[self viewWithLabel:i]];
                 }
                 
                 self.stackView.swipeDirections = UISwipeGestureRecognizerDirectionLeft | UISwipeGestureRecognizerDirectionRight;
                 self.stackView.forwardDirections = UISwipeGestureRecognizerDirectionRight | UISwipeGestureRecognizerDirectionLeft;
                 self.stackView.changeAlphaOnSendAnimation = YES;
                 [self.stackView configureWithViews:views];
                 self.stackView.delegate = self;
             }
             else
             {
                 [[AppDelegate shareInstance]hideActivityIndicator];
                 dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.6 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                     [self.view endEditing:YES];
                     //[[AppDelegate shareInstance] showAlertWithErrorMessage:[[responseDic objectForKey:@"payload"] valueForKey:@"message"]];
                 });
                 
             }
         }];
    });
}
- (void)rangeSliderValueDidChange:(MARKRangeSlider *)slider
{
    NSLog(@"%f - %f", slider.leftValue, slider.rightValue);
    minValue = slider.leftValue;
    maxValue = slider.rightValue;
}

#pragma mark - UICollectionViewDelegate
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [arrTags count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    
    UILabel *lblTag = (UILabel*)[cell viewWithTag:201];
    UIButton *btnCross = (UIButton*)[cell viewWithTag:202];
    
    lblTag.text = [arrTags objectAtIndex:indexPath.row];
    
    [btnCross addTarget:self action:@selector(btnActionCross:) forControlEvents:UIControlEventTouchUpInside];
    return cell;
}
-(void)btnActionCross:(id)sender
{
    CGPoint buttonPosition = [sender convertPoint:CGPointZero
                                           toView:_collectionView];
    NSIndexPath *tappedIP = [_collectionView indexPathForItemAtPoint:buttonPosition];
    [arrTags removeObjectAtIndex:tappedIP.row];
    [_collectionView reloadData];
}
#pragma mark - BSStackViewProtocol
- (NSString *)translateDirection:(BSStackViewItemDirection)direction {
    switch (direction) {
        case BSStackViewItemDirectionFront:
            return @"front";
            
        case BSStackViewItemDirectionBack:
            return @"back";
    }
}



- (void)stackView:(BSStackView *)stackView willSendItem:(UIView *)item direction:(BSStackViewItemDirection)direction {
    NSLog(@"stackView willSendItem %ld direction %@", item.tag, [self translateDirection:direction]);
    if (item.tag == [arrData count]-1)
    {
        tag = 0;
    }
    else
    {
        tag = item.tag+1;
    }
}

- (void)stackView:(BSStackView *)stackView didSendItem:(UIView *)item direction:(BSStackViewItemDirection)direction {
    NSLog(@"stackView didSendItem %ld direction %@", item.tag, [self translateDirection:direction]);
    if (item.tag == [arrData count]-1)
    {
        tag = 0;
    }
    else
    {
        tag = item.tag+1;
    }
}

#pragma mark - Button Actions
-(IBAction)btnFilterViewTouchupInside:(id)sender
{
    _FilterView.hidden= NO;
    _subView.hidden=YES;
}

-(IBAction)btnFilterTouchupInside:(id)sender
{
    [self callWebserviceForFetchFilterRecipe];
}

- (IBAction)btnActionLeftSwipe:(id)sender
{
    UISwipeGestureRecognizer *swipeLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self.stackView action:nil];
    swipeLeft.direction = UISwipeGestureRecognizerDirectionLeft;
    [self.stackView swipe:swipeLeft];
}

- (IBAction)btnActionLike:(id)sender
{
    [self performSelector:@selector(callOnDelay) withObject:nil afterDelay:1.20];
}
-(void) callOnDelay
{
    RecipeDescViewController *recipeDesc = [self.storyboard instantiateViewControllerWithIdentifier:@"RecipeDescViewController"];
    recipeDesc.recipeData = arrData;
    recipeDesc.index = tag;
    [self.navigationController pushViewController:recipeDesc animated:YES];
    _btnSpark.selected = NO;
}

- (IBAction)btnActionRightSwipe:(id)sender
{
    UISwipeGestureRecognizer *swipeRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self.stackView action:nil];
    swipeRight.direction = UISwipeGestureRecognizerDirectionRight;
    [self.stackView swipe:swipeRight];
}

-(IBAction)BacktoKookboekTouchupInside:(id)sender
{
    _FilterView.hidden= YES;
    _subView.hidden=NO;
}
- (IBAction)btnActionFilterCategory:(id)sender
{
    UIButton *btnFilter = (UIButton *)[_FilterView viewWithTag:[sender tag]];
    if (btnFilter.selected == YES)
    {
        btnFilter.selected = NO;
        [arrCategoryId removeObject:[NSString stringWithFormat:@"%ld",[sender tag]]];
    }
    else
    {
        btnFilter.selected = YES;
        [arrCategoryId addObject:[NSString stringWithFormat:@"%ld",[sender tag]]];
    }
}
#pragma mark - UITextField Methods

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if(_txtTag.text.length > 0)
    {
        [arrTags addObject:_txtTag.text];
        [_collectionView reloadData];
    }
    textField.text = @"";
    [textField resignFirstResponder];
    return YES;
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    if(_txtTag.text.length > 0)
    {
        [arrTags addObject:_txtTag.text];
        [_collectionView reloadData];
    }
    _txtTag.text = @"";
    [_txtTag resignFirstResponder];
}

@end
