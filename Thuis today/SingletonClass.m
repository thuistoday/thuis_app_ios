 //
//  SingletonClass.m
//  ScottApp
//  Created by NS on 03/06/15.
//  Copyright (c) 2015 Deftsoft. All rights reserved.
//

#import "SingletonClass.h"
#import "Reachability.h"
@implementation SingletonClass
@synthesize strVariable;

+(id)sharedManager
{
    static SingletonClass *sharedVariable = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken,^{
        sharedVariable = [[self alloc] init];
    });
    return sharedVariable;
}

- (id)init
{
    if (self = [super init])
    {
        strVariable =@"String Variable";
    }
    return self;
}
#pragma mark  -
#pragma mark  - method for check reachebility
-(BOOL)checkInternetAvailability {
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus remoteHostStatus = [reachability currentReachabilityStatus];
    
    if(remoteHostStatus == NotReachable) {
        NSLog(@"No Internet Connection Avilable");
        [[AppDelegate shareInstance]hideActivityIndicator];
        return false;
    }
    else{
        return true;
    }
}



#pragma mark -
#pragma mark - GetApi Method
-(void)m_PostApiResponse:(NSString *)methodName paramDiction:(NSDictionary *)paramDictionary profilePicData:(NSData *)profilePicData coverPicData:(NSData *)coverPicData :(NSString *)stringName onCompletion:(JSONResponseBlock)completionBlock
{
    
       
    
    if ([self checkInternetAvailability]) {
    // the boundary string : a random string, that will not repeat in post data, to separate post data fields.
    NSString *BoundaryConstant = @"----------V2ymHFg03ehbqgZCaKO6jy";
    
    NSString *string=[NSString stringWithFormat:@"%@%@",baseUrl,methodName];
    // the server url to which the image (or the media) is uploaded. Use your server url here
    NSURL* requestURL = [NSURL URLWithString:string];
    
    // create request
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setHTTPShouldHandleCookies:NO];
    [request setTimeoutInterval:180];
    [request setHTTPMethod:@"POST"];
    
    // set Content-Type in HTTP header
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", BoundaryConstant];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    if(!coverPicData){
        coverPicData=UIImageJPEGRepresentation([UIImage imageNamed:@"dummyCoverImage"], 0.9);
    }
    
    // post body
    NSMutableData *body = [NSMutableData data];

    for (NSString *param in paramDictionary)
    {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@\r\n", [paramDictionary objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    if (profilePicData)
    {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"thumbnail.jpg\"\r\n", stringName] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"Content-Type: image/jpeg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:profilePicData];
        [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    if (coverPicData)
    {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"coverImage.jpg\"\r\n", @"coverImage"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"Content-Type: image/jpeg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:coverPicData];
        [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // setting the body of the post to the reqeust
    [request setHTTPBody:body];
    
    // set the content-length
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
    // set URL
    [request setURL:requestURL];
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if ([data length] > 0 && error == nil)
         {
             NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
             //NSLog(@"post jsondict %@",jsonDict);
             completionBlock(jsonDict);
         }
         else if (error != nil )
         {
             
             completionBlock([NSDictionary dictionaryWithObject:[error localizedDescription] forKey:@"error"]);
             // [[AppDelegate shareInstance]hideActivityIndicator];
         }
     }];
    }
    else
    {
         //[[AppDelegate shareInstance]hideActivityIndicator];
        [[AppDelegate shareInstance]showAlertWithErrorMessage:@"No Internet Connection Avilable"];
    }
}

-(void)m_PostApiResponse:(NSString *)methodName paramDiction:(NSDictionary *)paramDictionary islogout:(BOOL)Is_logout onCompletion:(JSONResponseBlock)completionBlock
{
    
    if(Is_logout==YES)
    {
        if ([self checkInternetAvailability])
        {
            NSString *string=[NSString stringWithFormat:@"%@%@",@"http://52.7.27.60/maintoday/api/user/",methodName];
            
            NSURL *url = [NSURL URLWithString:string];
            NSData *postData = [self encodeDictionary:paramDictionary];
            NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:url];
            [theRequest addValue: @"application/x-www-form-urlencoded; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
            [theRequest setHTTPMethod:@"POST"];
            [theRequest setHTTPBody:postData];
            NSOperationQueue *queue = [[NSOperationQueue alloc] init];
            
            [NSURLConnection sendAsynchronousRequest:theRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
             {
                 if ([data length] > 0 && error == nil)
                 {
                     NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
                     NSLog(@"response %@",jsonDict);
                     
                     NSString *UserRole=[jsonDict objectForKey:@"jwt"];
                     
                     
                     
                     //Decode
                     NSString *jwtToken = UserRole;
                     NSData *secretData = [@"try@!tfgh5673" dataUsingEncoding:NSUTF8StringEncoding];
                     NSString *algorithmName = @"HS256"; //Must specify an algorithm to use
                     
                     NSDictionary *payload = [JWTBuilder decodeMessage:jwtToken].secretData(secretData).algorithmName(algorithmName).decode;
                     
                     NSLog(@"post jsondict %@",payload);
                     dispatch_async(dispatch_get_main_queue(), ^{
                         completionBlock(payload);
                     });
                 }
                 else if (error != nil )
                 {
                     //  [[AppDelegate shareInstance]hideActivityIndicator];
                     dispatch_async(dispatch_get_main_queue(), ^{
                         completionBlock([NSDictionary dictionaryWithObject:[error localizedDescription] forKey:@"error"]);
                     });
                     
                 }
             }];
        }
        else
        {
            //    [[AppDelegate shareInstance]hideActivityIndicator];
            [[AppDelegate shareInstance]showAlertWithErrorMessage:@"No Internet Connection Avilable"];
        }
    }
    else{
        if ([self checkInternetAvailability])
        {
            NSString *string=[NSString stringWithFormat:@"%@%@",baseUrl,methodName];
            NSLog(@"Method name - %@",methodName);
            NSLog(@"Path - \n %@",string);
            NSLog(@"Request - \n %@",paramDictionary);
            NSURL *url = [NSURL URLWithString:string];
            NSData *postData = [self encodeDictionary:paramDictionary];
            NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:url];
            [theRequest addValue: @"application/x-www-form-urlencoded; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
            [theRequest setHTTPMethod:@"POST"];
            [theRequest setHTTPBody:postData];
            NSOperationQueue *queue = [[NSOperationQueue alloc] init];
            
            [NSURLConnection sendAsynchronousRequest:theRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
             {
    
                if ([data length] > 0 && error == nil)
                 {
                    
                     NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
                     NSLog(@"%@",jsonDict);
                     
                     NSString *UserRole=[jsonDict objectForKey:@"jwt"];
                     
                     //Decode
                     NSString *jwtToken = UserRole;
                     NSData *secretData = [@"try@!tfgh5673" dataUsingEncoding:NSUTF8StringEncoding];
                     NSString *algorithmName = @"HS256"; //Must specify an algorithm to use
                     
                     NSDictionary *payload = [JWTBuilder decodeMessage:jwtToken].secretData(secretData).algorithmName(algorithmName).decode;
                     
                     NSLog(@"Response - \n %@",payload);
                     dispatch_async(dispatch_get_main_queue(), ^{
                         completionBlock(payload);
                     });
                 }
                 else if (error != nil )
                 {
                     //  [[AppDelegate shareInstance]hideActivityIndicator];
                     dispatch_async(dispatch_get_main_queue(), ^{
                         completionBlock([NSDictionary dictionaryWithObject:[error localizedDescription] forKey:@"error"]);
                     });
                     
                 }
             }];
        }
        else
        {
            //    [[AppDelegate shareInstance]hideActivityIndicator];
            [[AppDelegate shareInstance]showAlertWithErrorMessage:@"No Internet Connection Avilable"];
        }
    }
}
-(void)m_PostApiResponseRecipe:(NSString *)methodName paramDiction:(NSDictionary *)paramDictionary  onCompletion:(JSONResponseBlock)completionBlock
{
    if ([self checkInternetAvailability])
    {
        NSString *string=[NSString stringWithFormat:@"%@%@",baseUrlRecipe,methodName];
        
        NSURL *url = [NSURL URLWithString:string];
        NSData *postData = [self encodeDictionary:paramDictionary];
        NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:url];
        [theRequest addValue: @"application/x-www-form-urlencoded; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
        [theRequest setHTTPMethod:@"POST"];
        [theRequest setHTTPBody:postData];
        NSOperationQueue *queue = [[NSOperationQueue alloc] init];
        
        [NSURLConnection sendAsynchronousRequest:theRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
         {
             if ([data length] > 0 && error == nil)
             {
                 NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
                 NSLog(@"%@",jsonDict);
                 
                 NSString *UserRole=[jsonDict objectForKey:@"jwt"];
                 
                 
                 
                 //Decode
                 NSString *jwtToken = UserRole;
                 NSData *secretData = [@"try@!tfgh5673" dataUsingEncoding:NSUTF8StringEncoding];
                 NSString *algorithmName = @"HS256"; //Must specify an algorithm to use
                 
                 NSDictionary *payload = [JWTBuilder decodeMessage:jwtToken].secretData(secretData).algorithmName(algorithmName).decode;
                 
                 NSLog(@"post jsondict %@",payload);
                 dispatch_async(dispatch_get_main_queue(), ^{
                     completionBlock(payload);
                 });
             }
             else if (error != nil )
             {
                 //  [[AppDelegate shareInstance]hideActivityIndicator];
                 dispatch_async(dispatch_get_main_queue(), ^{
                     completionBlock([NSDictionary dictionaryWithObject:[error localizedDescription] forKey:@"error"]);
                 });
                 
             }
         }];
    }
    else
    {
        //    [[AppDelegate shareInstance]hideActivityIndicator];
        [[AppDelegate shareInstance]showAlertWithErrorMessage:@"No Internet Connection Avilable"];
    }
}

-(void)m_PostApiResponseRecipe:(NSString *)methodName onCompletion:(JSONResponseBlock)completionBlock
{
    if ([self checkInternetAvailability]) {
        NSString *string=[NSString stringWithFormat:@"%@%@",baseUrlRecipe,methodName];
        
        NSURL *url = [NSURL URLWithString:string];
        NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:url];
        [theRequest addValue: @"application/x-www-form-urlencoded; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
        [theRequest setHTTPMethod:@"POST"];
        NSOperationQueue *queue = [[NSOperationQueue alloc] init];
        
        [NSURLConnection sendAsynchronousRequest:theRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
         {
             if ([data length] > 0 && error == nil)
             {
                 NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
                 NSLog(@"%@",jsonDict);
                 
                 NSString *UserRole=[jsonDict objectForKey:@"jwt"];
                 
                 
                 
                 //Decode
                 NSString *jwtToken = UserRole;
                 NSData *secretData = [@"try@!tfgh5673" dataUsingEncoding:NSUTF8StringEncoding];
                 NSString *algorithmName = @"HS256"; //Must specify an algorithm to use
                 
                 NSDictionary *payload = [JWTBuilder decodeMessage:jwtToken].secretData(secretData).algorithmName(algorithmName).decode;
                 
                 NSLog(@"post jsondict %@",payload);
                 dispatch_async(dispatch_get_main_queue(), ^{
                     completionBlock(payload);
                 });
             }
             else if (error != nil )
             {
                 //  [[AppDelegate shareInstance]hideActivityIndicator];
                 dispatch_async(dispatch_get_main_queue(), ^{
                     completionBlock([NSDictionary dictionaryWithObject:[error localizedDescription] forKey:@"error"]);
                 });
                 
             }
         }];
    }
    else
    {
        //    [[AppDelegate shareInstance]hideActivityIndicator];
        [[AppDelegate shareInstance]showAlertWithErrorMessage:@"No Internet Connection Avilable"];
    }
}

-(void)m_PostAddressSuggestionApiResponse:(NSString *)methodName paramDiction:(NSDictionary *)paramDictionary onCompletion:(JSONResponseBlock)completionBlock
{
    if ([self checkInternetAvailability]) {
        NSString *string=[NSString stringWithFormat:@"%@%@",@"http://52.7.27.60/accept/thuis_today/",methodName];
        
        NSURL *url = [NSURL URLWithString:string];
        NSData *postData = [self encodeDictionary:paramDictionary];
        NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:url];
        [theRequest addValue: @"application/x-www-form-urlencoded; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
        [theRequest setHTTPMethod:@"POST"];
        [theRequest setHTTPBody:postData];
        NSOperationQueue *queue = [[NSOperationQueue alloc] init];
        
        [NSURLConnection sendAsynchronousRequest:theRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
         {
             if ([data length] > 0 && error == nil)
             {
                 NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
                 NSLog(@"%@",jsonDict);
                 
                 NSString *UserRole=[jsonDict objectForKey:@"AUTH"];
                 
                 
                 
                 //Decode
                 NSString *jwtToken = UserRole;
                 NSData *secretData = [@"try@!tfgh5673" dataUsingEncoding:NSUTF8StringEncoding];
                 NSString *algorithmName = @"HS256"; //Must specify an algorithm to use
                 
                 NSDictionary *payload = [JWTBuilder decodeMessage:jwtToken].secretData(secretData).algorithmName(algorithmName).decode;
                 
                 NSLog(@"post jsondict %@",payload);
                 dispatch_async(dispatch_get_main_queue(), ^{
                     completionBlock(payload);
                 });
             }
             else if (error != nil )
             {
                 //  [[AppDelegate shareInstance]hideActivityIndicator];
                 dispatch_async(dispatch_get_main_queue(), ^{
                     completionBlock([NSDictionary dictionaryWithObject:[error localizedDescription] forKey:@"error"]);
                 });
                 
             }
         }];
    }
    else
    {
        //    [[AppDelegate shareInstance]hideActivityIndicator];
        [[AppDelegate shareInstance]showAlertWithErrorMessage:@"No Internet Connection Avilable"];
    }
}

- (NSData*)encodeDictionary:(NSDictionary*)dictionary
{
    NSMutableArray *parts = [[NSMutableArray alloc] init];
    NSLog(@"%@",dictionary);
    for (NSString *key in dictionary)
    {
        //NSLog(@"%@",key);
        
        NSString *encodedValue = [[dictionary objectForKey:key] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        //NSLog(@"%@",encodedValue);
        
        
        NSString *encodedKey = [key stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        //NSLog(@"%@",encodedKey);
        
        NSString *part = [NSString stringWithFormat: @"%@=%@", encodedKey, encodedValue];
        //NSLog(@"%@",part);
        
        [parts addObject:part];
        //NSLog(@"%@",parts);
    }
    NSString *encodedDictionary = [parts componentsJoinedByString:@"&"];
    //NSLog(@"%@",encodedDictionary);
    return [encodedDictionary dataUsingEncoding:NSUTF8StringEncoding];
}


#pragma mark -
#pragma mark - GetApi Method
-(void)m_UpdateGallery:(NSString *)methodName paramDiction:(NSDictionary *)paramDictionary uploadPicData:(NSData *)profilePicData coverPicData:(NSData *)coverPicData  imageName:(NSString *)imageName  extension:(NSString *)extension  onCompletion:(JSONResponseBlock)completionBlock
{
     if ([self checkInternetAvailability]) {
    // the boundary string : a random string, that will not repeat in post data, to separate post data fields.
    NSString *BoundaryConstant = @"----------V2ymHFg03ehbqgZCaKO6jy";
    
    NSString *string=[NSString stringWithFormat:@"%@%@",baseUrl,methodName];
    // the server url to which the image (or the media) is uploaded. Use your server url here
    NSURL* requestURL = [NSURL URLWithString:string];
    
    // create request
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setHTTPShouldHandleCookies:NO];
    [request setTimeoutInterval:180];
    [request setHTTPMethod:@"POST"];
    
    // set Content-Type in HTTP header
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", BoundaryConstant];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
   
    // post body
    NSMutableData *body = [NSMutableData data];
    
    for (NSString *param in paramDictionary)
    {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@\r\n", [paramDictionary objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    if (profilePicData)
    {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"UploadedImage.%@\"\r\n",imageName,extension] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"Content-Type: image/jpeg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:profilePicData];
        [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
     }
    
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
    //NSLog(@"fefewf%@",body);

    // setting the body of the post to the reqeust
    [request setHTTPBody:body];
    //NSLog(@"fefewf%@",request);

    // set the content-length
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    //NSLog(@"fefewf%@",postLength);

    // set URL
    [request setURL:requestURL];
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if ([data length] > 0 && error == nil)
         {
             NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
             NSLog(@"post jsondict %@",jsonDict);
             NSString *UserRole=[jsonDict objectForKey:@"jwt"];
             
             NSLog(@"%@",UserRole);
             
             NSString *jwtToken = UserRole;
             NSString *secret = @"try@!tfgh5673";
             NSString *algorithmName = @"HS256"; //Must specify an algorithm to use
             
             NSDictionary *payload = [JWTBuilder decodeMessage:jwtToken].secret(secret).algorithmName(algorithmName).decode;
             
             NSLog(@"post jsondict %@",payload);
             dispatch_async(dispatch_get_main_queue(), ^{

             completionBlock(payload);
             });
             //completionBlock(jsonDict);
         }
         else if (error != nil )
         {
             
             completionBlock([NSDictionary dictionaryWithObject:[error localizedDescription] forKey:@"error"]);
             NSLog(@"%@",[NSDictionary dictionaryWithObject:[error localizedDescription] forKey:@"error"]);
             [[AppDelegate shareInstance]hideActivityIndicator];
         }
     }];
     }
     else
     {
          dispatch_async(dispatch_get_main_queue(), ^{
          [[AppDelegate shareInstance]hideActivityIndicator];
         [[AppDelegate shareInstance]showAlertWithErrorMessage:@"No Internet Connection Avilable"];
          });
     }
}


@end
