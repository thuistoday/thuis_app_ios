//
//  RecipeSwipeViewController.m
//  RecipeTinder8
//
//  Created by offshore_mac_1 on 17/10/17.
//  Copyright © 2017 offshore_mac_1. All rights reserved.
//

#import "RecipeSwipeViewController.h"

@interface RecipeSwipeViewController ()

@end

@implementation RecipeSwipeViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    _lblRecipeName.text = [[_recipeData objectAtIndex:_index] objectForKey:@"recipe_name"];
    _lblPrepTime.text = [NSString stringWithFormat:@"%@ min",[[_recipeData objectAtIndex:_index] objectForKey:@"preptime"]];
    _lblLikes.text = [[_recipeData objectAtIndex:_index] objectForKey:@"like"];
    _lblAddedBy.text = [NSString stringWithFormat:@"By: %@",[[_recipeData objectAtIndex:_index] objectForKey:@"addedby"]];
    [_imgRecipe sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",baseUrlForImageRecipe,[[_recipeData objectAtIndex:_index] objectForKey:@"image"]]]
                           placeholderImage:[UIImage imageNamed:@""]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


@end
