//
//  SingletonClass.h
//  ScottApp
//
//  Created by NS on 03/06/15.
//  Copyright (c) 2015 Deftsoft. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^JSONResponseBlock)(NSDictionary* json);
@interface SingletonClass : NSObject
{
    NSString *strVariable;
    NSOperationQueue *_queue;
}
@property(strong,nonatomic) NSString *strVariable;
+(id)sharedManager;

#pragma mark  - Get WebService Method
//-(void)m_GetApiResponse:(NSString*)methodNameAndParam  onCompletion:(JSONResponseBlock)completionBlock;

#pragma mark -
#pragma mark - GetApi Method

-(void)m_PostApiResponse:(NSString *)methodName paramDiction:(NSDictionary *)paramDictionary islogout:(BOOL)Is_logout onCompletion:(JSONResponseBlock)completionBlock;

-(void)m_PostAddressSuggestionApiResponse:(NSString *)methodName paramDiction:(NSDictionary *)paramDictionary onCompletion:(JSONResponseBlock)completionBlock;

-(void)m_PostApiResponse:(NSString *)methodName paramDiction:(NSDictionary *)paramDictionary profilePicData:(NSData *)profilePicData coverPicData:(NSData *)coverPicData :(NSString *)stringName onCompletion:(JSONResponseBlock)completionBlock;


-(void)m_UpdateGallery:(NSString *)methodName paramDiction:(NSDictionary *)paramDictionary uploadPicData:(NSData *)profilePicData coverPicData:(NSData *)coverPicData imageName:(NSString *)imageName extension:(NSString *)extension onCompletion:(JSONResponseBlock)completionBlockl;

-(void)m_PostApiResponseRecipe:(NSString *)methodName onCompletion:(JSONResponseBlock)completionBlock;
-(void)m_PostApiResponseRecipe:(NSString *)methodName paramDiction:(NSDictionary *)paramDictionary onCompletion:(JSONResponseBlock)completionBlock;

@end
