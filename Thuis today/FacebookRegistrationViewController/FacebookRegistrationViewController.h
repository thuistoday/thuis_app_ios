//
//  FacebookRegistrationViewController.h
//  Thuis today
//
//  Created by IMMANENT on 27/03/17.
//  Copyright © 2017 IMMANENT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FacebookRegistrationViewController : UIViewController
{
     IBOutlet UITableView *tblForFields;
     IBOutlet UILabel *lblCountAction;
    
    __weak IBOutlet UITableView *tblSearch;
}
- (IBAction)btnCartAction:(id)sender;
- (IBAction)btnMenuAction:(id)sender;
- (IBAction)btnActionProceed:(id)sender;

@property NSString *userId;
@end
