//
//  FacebookRegistrationViewController.m
//  Thuis today
//
//  Created by IMMANENT on 27/03/17.
//  Copyright © 2017 IMMANENT. All rights reserved.
//

#import "FacebookRegistrationViewController.h"
#import "PostalCodeTableViewCell.h"
#import "Constants.h"

@interface FacebookRegistrationViewController ()<PostalCodeTableViewCellDelegate>
{
    NSMutableArray *arrayForPlaceholder,*arrayForuserData, *arraySearchData;
    NSMutableDictionary *dicForTextFieldvalues;
    NSIndexPath *newIndexPath;
    BOOL checkForEmptyField;
    NSString *txtPostCodeValue;
}

@property (nonatomic,strong) NSMutableDictionary * dict_postalCodeDetails;

@end

@implementation FacebookRegistrationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    arrayForPlaceholder = [[NSMutableArray alloc]initWithObjects:NSLocalizedString(@"Name", @"Cancel"),NSLocalizedString(@"Postcode", @"Cancel"),NSLocalizedString(@"Address", @"Cancel"),NSLocalizedString(@"city", @"Cancel"),NSLocalizedString(@"Phone number", @"Cancel"),NSLocalizedString(@"Enter Valid Email Id", @"Cancel") ,nil];
    arrayForuserData = [[NSMutableArray alloc]init];
    arraySearchData = [NSMutableArray new];
    dicForTextFieldvalues= [[NSMutableDictionary alloc]init];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    self.dict_postalCodeDetails = [[NSMutableDictionary alloc] init];
    [dicForTextFieldvalues setObject:@"" forKey:@"address"];
    [dicForTextFieldvalues setObject:@"" forKey:@"city"];
    [dicForTextFieldvalues setObject:[[NSUserDefaults standardUserDefaults] valueForKey:@"fb_username"] forKey:@"name"];
    [dicForTextFieldvalues setObject:@"" forKey:@"zipcode"];
    [dicForTextFieldvalues setObject:@"" forKey:@"phone"];
    [dicForTextFieldvalues setObject:[[NSUserDefaults standardUserDefaults] valueForKey:@"fb_email"] forKey:@"email"];
    lblCountAction.layer.cornerRadius = lblCountAction.frame.size.width/2;
    lblCountAction.layer.masksToBounds=YES;
    
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"cartCount"]!=nil) {
        lblCountAction.text=[[NSUserDefaults standardUserDefaults] valueForKey:@"cartCount"];
    }
    else
    {
        lblCountAction.text=@"";
    }
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark - Postal Code

- (void)validatePostCode:(UITextField *)textField forCell:(PostalCodeTableViewCell *)Cell {
    if (textField.text.length >= 4) {
        if ([self checktext:Cell.txtFld_postCode.text forType:TextTypeNumeric]) {
            [self enableStreetCode:YES forCell:Cell];
            [Cell.txtFld_StreetCode becomeFirstResponder];
        }
        else {
            [self enableStreetCode:NO forCell:Cell];
            [self showAlertWithTitle:@"Wrong Input" message:@"Enter valid Post code. post code accepts only numbers"];
        }
    }
}


- (void)validateStreetCode:(UITextField *)textField forCell:(PostalCodeTableViewCell *)Cell {
    if (textField.text.length >= 2) {
        if ([self checktext:Cell.txtFld_StreetCode.text forType:TextTypeAlphates]) {
            [self enableCityCode:YES forCell:Cell];
            [Cell.txtFld_HouseCode becomeFirstResponder];
        }
        else {
            [self enableCityCode:NO forCell:Cell];
            [self showAlertWithTitle:@"Wrong Input" message:@"Enter Valid street code"];
        }
    }
    else if([Cell.txtFld_StreetCode.text isEqualToString:@""]) {
        [Cell.txtFld_postCode becomeFirstResponder];
    }
}


- (void)validateHouseCode:(UITextField *)textField forCell:(PostalCodeTableViewCell *)Cell{
    if (Cell.txtFld_HouseCode.text.length >= 1) {
        if ([self checktext:Cell.txtFld_HouseCode.text forType:TextTypeNumeric]) {
            [Cell.txtFld_HouseCode resignFirstResponder];
            NSDictionary *postalCodeDetails = @{kPostalCode : Cell.txtFld_postCode.text, kStreetCode :Cell.txtFld_StreetCode.text, kHouseNumber : Cell.txtFld_HouseCode.text};
            [self searchAddressWithPostCodeDetails:postalCodeDetails];
            self.dict_postalCodeDetails = [NSMutableDictionary dictionaryWithDictionary:postalCodeDetails];
        }
        else {
            [self showAlertWithTitle:@"Wrong Input" message:@"Enter valid house number"];
        }
    }
    else if([Cell.txtFld_HouseCode.text isEqualToString:@""]) {
        [Cell.txtFld_HouseCode becomeFirstResponder];
    }
}

- (void)enableCityCode:(BOOL)value forCell:(PostalCodeTableViewCell *)Cell{
    Cell.txtFld_HouseCode.userInteractionEnabled = value;
    [self hideHouseCodePart:NO forCell:Cell];
}

- (void)enableStreetCode:(BOOL)value forCell:(PostalCodeTableViewCell *)Cell{
    Cell.txtFld_StreetCode.userInteractionEnabled = value;
}

- (BOOL)checktext:(NSString *)txtFld_string forType:(TextType)textType{
    NSCharacterSet *textCharacterSet;
    if (textType == TextTypeNumeric) {
        textCharacterSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
    }
    else if (textType == TextTypeAlphates) {
        textCharacterSet = NSCharacterSet.letterCharacterSet;
    }
    NSCharacterSet *characterSetFromTextField = [NSCharacterSet characterSetWithCharactersInString:txtFld_string];
    
    BOOL stringIsValid = [textCharacterSet isSupersetOfSet:characterSetFromTextField];
    return stringIsValid;
}

- (void)showAlertWithTitle:(NSString *)alertTitle message:(NSString *)alertMessage {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:alertTitle message:alertMessage preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *OKBtn = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
    [alert addAction:OKBtn];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)searchAddressWithPostCodeDetails:(NSDictionary *)postCodeDetails {
    NSDictionary *params = @{
                             @"wijkcode":[postCodeDetails valueForKey:kPostalCode],
                             @"lettercombinatie":[postCodeDetails valueForKey:kStreetCode],
                             @"huisnr":[postCodeDetails valueForKey:kHouseNumber],
                             };
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [[SingletonClass sharedManager] m_PostAddressSuggestionApiResponse:@"delivery-agent-get-address" paramDiction:params onCompletion:^(NSDictionary *response)
         {
             NSDictionary *responseDic = (NSDictionary *)response;
             if ([[[responseDic objectForKey:@"payload"]valueForKey:@"status"]integerValue]==1)
             {
                 arraySearchData = [[responseDic objectForKey:@"payload"] objectForKey:@"data"];
                 tblSearch.hidden = NO;
                 [tblSearch reloadData];
             }
             else
             {
                 dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.6 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                     [self.view endEditing:YES];
                     [[AppDelegate shareInstance] showAlertWithErrorMessage:[[responseDic objectForKey:@"payload"] valueForKey:@"message"]];
                     tblSearch.hidden = YES;
                 });
             }
         }];
    });
}

- (void)hideHouseCodePart:(BOOL)value forCell:(PostalCodeTableViewCell *)Cell {
    
    Cell.lbl_hiphenHouseCode.hidden = value;
    Cell.txtFld_HouseCode.text = @"";
    Cell.txtFld_HouseCode.hidden = value;
    if ([[self.dict_postalCodeDetails allKeys] containsObject:kHouseNumber]) {
        [self.dict_postalCodeDetails removeObjectForKey:kHouseNumber];
    }
}
#pragma mark - PostalCodeTableViewCellDelegate
- (void)txtFld_PostCode_action:(UITextField *)textField forPostalCodeTableViewCell:(PostalCodeTableViewCell *)PostalCodeTableViewCell {
    [self validatePostCode:textField forCell:PostalCodeTableViewCell];
}

- (void)txtFld_streetCode_action:(UITextField *)textField forPostalCodeTableViewCell:(PostalCodeTableViewCell *)PostalCodeTableViewCell {
    [self validateStreetCode:textField forCell:PostalCodeTableViewCell];
}

- (void)txtFld_HouseCode_action:(UITextField *)textField forPostalCodeTableViewCell:(PostalCodeTableViewCell *)PostalCodeTableViewCell {
    [self validateHouseCode:textField forCell:PostalCodeTableViewCell];
}

#pragma mark -
#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableView.tag == 0) {
        return 7;
    }
    else
    {
        return [arraySearchData count];
    }
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView.tag == 0) {
        static NSString *CellIdentifier;
        UITableViewCell *cell ;
        if (indexPath.row ==6) {
            CellIdentifier = @"cell1";
            cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            return cell;
        }
        else
        {
            CellIdentifier = @"cell";
            
            if (indexPath.row == 1) {
                PostalCodeTableViewCell *postCodeCell = (PostalCodeTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"PostalCodeTableViewCell"];
                postCodeCell.delegate = self;
                return postCodeCell;
            }
            else {
                cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
                UITextField *txtField=(UITextField *)[cell viewWithTag:101];
                
                txtField.placeholder =[arrayForPlaceholder objectAtIndex:indexPath.row];
                
                // UIColor *color = [UIColor colorWithRed:210/255.0f green:45/255.0f blue:62/255.0f alpha:1];
                UIColor *color = [UIColor lightGrayColor];
                
                [txtField setValue:color forKeyPath:@"_placeholderLabel.textColor"];
                txtField.placeholder = [arrayForPlaceholder objectAtIndex:indexPath.row];
                
                
                if (indexPath.row ==0) {
                    txtField.text =[dicForTextFieldvalues valueForKey:@"name"];
                    [txtField setKeyboardType:UIKeyboardTypeASCIICapable];
                }
                //        else if(indexPath.row ==1)
                //        {
                //            txtField.placeholder=@"Postcode(1234-aa-12 or 1234aa12)";
                //            [txtField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
                //            txtField.text = [dicForTextFieldvalues valueForKey:@"zipcode"];
                //            [txtField setKeyboardType:UIKeyboardTypeASCIICapable];
                //        }
                else if(indexPath.row ==2)
                {
                    txtField.text = [dicForTextFieldvalues valueForKey:@"address"];
                    txtField.userInteractionEnabled = NO;
                    [txtField setKeyboardType:UIKeyboardTypeASCIICapable];
                }
                else if(indexPath.row ==3)
                {
                    txtField.text = [dicForTextFieldvalues valueForKey:@"city"];
                    txtField.userInteractionEnabled = NO;
                    [txtField setKeyboardType:UIKeyboardTypeASCIICapable];
                }
                else if(indexPath.row ==4)
                {
                    txtField.text = [dicForTextFieldvalues valueForKey:@"phone"];
                    [txtField setKeyboardType:UIKeyboardTypeNumberPad];
                }
                else if(indexPath.row ==5)
                {
                    txtField.text = [dicForTextFieldvalues valueForKey:@"email"];
                    [txtField setKeyboardType:UIKeyboardTypeEmailAddress];
                }
            }
            
            
        }
        return cell;
    }
    else
    {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
        if (cell == nil)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"Cell"];
        }
        NSMutableDictionary *dict = [NSMutableDictionary new];
        dict = [arraySearchData objectAtIndex:indexPath.row];
        cell.textLabel.text =[NSString stringWithFormat:@"%@,%@",[dict objectForKey:@"straatnaam"],[dict objectForKey:@"plaatsnaam"]];
        return cell;
    }
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView.tag == 1)
    {
        NSMutableDictionary *dict = [NSMutableDictionary new];
        dict = [arraySearchData objectAtIndex:indexPath.row];
        NSString *postCode=[NSString stringWithFormat:@"%@-%@-%@",[dict objectForKey:@"wijkcode"],[dict objectForKey:@"lettercombinatie"],[dict objectForKey:@"huisnr_van"]];
        tblSearch.hidden = YES;
        PostalCodeTableViewCell *Cell = [tblForFields cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
        [self hideHouseCodePart:YES forCell:Cell];
        [dicForTextFieldvalues setObject:[NSString stringWithFormat:@"%@",[dict objectForKey:@"straatnaam"]] forKey:@"address"];
        [dicForTextFieldvalues setObject:[dict objectForKey:@"plaatsnaam"] forKey:@"city"];
        //[dicForTextFieldvalues setObject:txtPostCodeValue forKey:@"zipcode"];
        [dicForTextFieldvalues setObject:postCode forKey:@"zipcode"];
        [tblForFields reloadData];
    }
}
#pragma mark - UITextField delegates
//Method for validation for first name
-(BOOL)validPostcode: (NSString *) textFieldString
{
    NSString *emailid = textFieldString;
    NSString *emailRegex = @"([0-9]{4})-[a-zA-Z]{2}-[0-9]{1,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:emailid];
}
-(BOOL)validPostcode1: (NSString *) textFieldString
{
    NSString *emailid = textFieldString;
    NSString *emailRegex = @"([0-9]{4})[a-zA-Z]{2}[0-9]{1,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:emailid];
}

-(void) textFieldDidChange:(UITextField *) theTextField
{
    if(theTextField.text.length < 6)
    {
        tblSearch.hidden = YES;
    }
    
    if ([theTextField.text rangeOfString:@"-"].location == NSNotFound)
    {
        if (theTextField.text.length > 6)
        {
            if (![self validPostcode1:theTextField.text])
            {
                [[AppDelegate shareInstance] showAlertWithErrorMessage:NSLocalizedString(@"Enter Valid  Postcode.", @"Cancel")];
            }
            else
            {
                [self searchText:theTextField replacementString:theTextField.text];
            }
        }
    }
    else
    {
        if (theTextField.text.length > 8)
        {
            if (![self validPostcode:theTextField.text])
            {
                [[AppDelegate shareInstance] showAlertWithErrorMessage:NSLocalizedString(@"Enter Valid  Postcode.", @"Cancel")];
            }
            else
            {
                [self searchText:theTextField replacementString:theTextField.text];
            }
        }
    }
}
-(void) searchText:(UITextField *)textField replacementString:(NSString *)string
{
    txtPostCodeValue = textField.text;
    NSString *postcode;
    NSString *street;
    NSString *houseNumber;
    if ([string rangeOfString:@"-"].location == NSNotFound)
    {
        if(txtPostCodeValue.length == 7)
        {
            postcode = [txtPostCodeValue substringWithRange:NSMakeRange(0,4)];
            street = [txtPostCodeValue substringWithRange:NSMakeRange(4,2)];
            houseNumber = [txtPostCodeValue substringWithRange:NSMakeRange(6,1)];
        }
        else if(txtPostCodeValue.length == 8)
        {
            postcode = [txtPostCodeValue substringWithRange:NSMakeRange(0,4)];
            street = [txtPostCodeValue substringWithRange:NSMakeRange(4,2)];
            houseNumber = [txtPostCodeValue substringWithRange:NSMakeRange(6,2)];
        }
        else if(txtPostCodeValue.length == 9)
        {
            postcode = [txtPostCodeValue substringWithRange:NSMakeRange(0,4)];
            street = [txtPostCodeValue substringWithRange:NSMakeRange(4,2)];
            houseNumber = [txtPostCodeValue substringWithRange:NSMakeRange(6,3)];
        }
        if(txtPostCodeValue.length == 10)
        {
            postcode = [txtPostCodeValue substringWithRange:NSMakeRange(0,4)];
            street = [txtPostCodeValue substringWithRange:NSMakeRange(4,2)];
            houseNumber = [txtPostCodeValue substringWithRange:NSMakeRange(6,4)];
        }
        
    }
    else
    {
        NSArray *arrTemp = [txtPostCodeValue componentsSeparatedByString:@"-"];
        postcode = [arrTemp objectAtIndex:0];
        street = [arrTemp objectAtIndex:1];
        houseNumber = [arrTemp objectAtIndex:2];
    }
    
    NSDictionary *params = @{
                             @"wijkcode":postcode,
                             @"lettercombinatie":street,
                             @"huisnr":houseNumber,
                             };
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [[SingletonClass sharedManager] m_PostAddressSuggestionApiResponse:@"delivery-agent-get-address" paramDiction:params onCompletion:^(NSDictionary *response)
         {
             NSDictionary *responseDic = (NSDictionary *)response;
             if ([[[responseDic objectForKey:@"payload"]valueForKey:@"status"]integerValue]==1)
             {
                 arraySearchData = [[responseDic objectForKey:@"payload"] objectForKey:@"data"];
                 tblSearch.hidden = NO;
                 [tblSearch reloadData];
             }
             else
             {
                 dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.6 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                     [self.view endEditing:YES];
                     [[AppDelegate shareInstance] showAlertWithErrorMessage:[[responseDic objectForKey:@"payload"] valueForKey:@"message"]];
                     tblSearch.hidden = YES;
                     
                     
                 });
             }
         }];
    });
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    CGPoint origin = textField.frame.origin;
    CGPoint point = [textField.superview convertPoint:origin toView:tblForFields];
    NSIndexPath *postalCodeCell_IndexPath = [NSIndexPath indexPathForRow:1 inSection:0];
    newIndexPath = [tblForFields indexPathForRowAtPoint:point];
    if (newIndexPath  == postalCodeCell_IndexPath) {
    PostalCodeTableViewCell *postCodeCell = [tblForFields cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
    
   
    
    if(textField == postCodeCell.txtFld_StreetCode) {
        UITableViewCell *addressCell = [tblForFields cellForRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:0]];
        UITableViewCell *cityCell = [tblForFields cellForRowAtIndexPath:[NSIndexPath indexPathForRow:3 inSection:0]];
        
        UITextField *address_txtField=(UITextField *)[addressCell viewWithTag:101];
        address_txtField.text = @"";
        
        UITextField *city_txtField=(UITextField *)[cityCell viewWithTag:101];
        city_txtField.text = @"";
        [self hideHouseCodePart:NO forCell:postCodeCell];
        
    }
    }
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    CGPoint origin = textField.frame.origin;
    CGPoint point = [textField.superview convertPoint:origin toView:tblForFields];
    newIndexPath = [tblForFields indexPathForRowAtPoint:point];
    
    NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    newString =     [newString stringByTrimmingCharactersInSet:
                     [NSCharacterSet whitespaceCharacterSet]];
    if (newIndexPath.row ==0)
    {
        [dicForTextFieldvalues setObject:newString forKey:@"name"];
    }
    else if(newIndexPath.row ==2)
    {
        [dicForTextFieldvalues setObject:newString forKey:@"address"];
    }
    else if(newIndexPath.row ==1)
    {
        [dicForTextFieldvalues setObject:newString forKey:@"zipcode"];
        //        if(range.length + range.location > textField.text.length)
        //        {
        //            return NO;
        //        }
        //
        //        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        //         return newLength <= 20;
        BOOL isValidate = NO;
        if(range.length + range.location > textField.text.length)
        {
            isValidate = NO;
        }
        
        PostalCodeTableViewCell *Cell = [tblForFields cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        if(textField == Cell.txtFld_postCode) {
            isValidate =  (newLength <=4)?YES:NO;
            if (newLength == 5) {
                [self validatePostCode:textField forCell:Cell];
            }
        }
        else if (textField == Cell.txtFld_StreetCode) {
            isValidate =  (newLength <=2)?YES:NO;
            if (newLength == 3) {
                [self validateStreetCode:textField forCell:Cell];
            }
            
        }
        else if (textField == Cell.txtFld_HouseCode) {
            isValidate =  (newLength <=3)?YES:NO;
            
        }
        return isValidate;
        
    }
    else if(newIndexPath.row ==3)
    {
        [dicForTextFieldvalues setObject:newString forKey:@"city"];
    }
    else if(newIndexPath.row ==4)
    {
        //[dicForTextFieldvalues setObject:newString forKey:@"phone"];
        if ([newString length] >= 10)
            newString = [newString substringToIndex:10];
        else
            newString = newString;
        [dicForTextFieldvalues setObject:newString forKey:@"phone"];
        if ([string rangeOfCharacterFromSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].location != NSNotFound)
        {
            return NO;
        }
        if(range.length + range.location > textField.text.length)
        {
            return NO;
        }
        
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return newLength <=10;
    }
    else if(newIndexPath.row == 5)
    {
        [dicForTextFieldvalues setObject:newString forKey:@"email"];
    }
    [tblForFields beginUpdates];
    [tblForFields endUpdates];
    return true;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITableViewCell* cell = (UITableViewCell*)(tblForFields);
    UITextField *txtFld = (UITextField *)[cell viewWithTag:101];
    [txtFld resignFirstResponder];
}

-(void)methodForUpdatevalues
{
    NSString *token;
    if([[NSUserDefaults standardUserDefaults] valueForKey:@"Device_Token"])
    {
        token = [[NSUserDefaults standardUserDefaults] valueForKey:@"Device_Token"];
        
    }
    else
    {
        token = @"123456";
        
    }
    
    [[AppDelegate shareInstance]showActivityIndicator];
    NSDictionary *params = @{
                             @"email":[dicForTextFieldvalues valueForKey:@"email"],
                             @"fb_id":_userId,
                             @"name":[dicForTextFieldvalues valueForKey:@"name"],
                             @"phone":[dicForTextFieldvalues valueForKey:@"phone"],
                             @"zipcode":[dicForTextFieldvalues valueForKey:@"zipcode"],
                             @"address":[dicForTextFieldvalues valueForKey:@"address"],
                             @"city":[dicForTextFieldvalues valueForKey:@"city"],
                             @"device_type":@"ios",
                             @"token":token
                             };
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [[SingletonClass sharedManager] m_PostApiResponse:@"register" paramDiction:params islogout:NO  onCompletion:^(NSDictionary *response)
         {
             NSDictionary *responseDic = (NSDictionary *)response;
             if ([[[responseDic objectForKey:@"payload"]valueForKey:@"status"]integerValue]==1)
             {
                 
                 
                 [[NSUserDefaults standardUserDefaults] setObject:[[responseDic objectForKey:@"payload"] objectForKey:@"data"] forKey:@"userData"];
                 [[NSUserDefaults standardUserDefaults] synchronize];
                 NSLog(@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"userData"]);
                 [[AppDelegate shareInstance]hideActivityIndicator];
                 [[AppDelegate shareInstance] hideActivityIndicator];
                 UIAlertController * alert=   [UIAlertController
                                               alertControllerWithTitle:nil
                                               message:[[responseDic objectForKey:@"payload"] valueForKey:@"message"]
                                               preferredStyle:UIAlertControllerStyleAlert];
                 UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                     [[AppDelegate shareInstance]hideActivityIndicator];
                     [[NSNotificationCenter defaultCenter] postNotificationName:@"MyCacheUpdatedNotification" object:self];
                     if ([[[NSUserDefaults standardUserDefaults] valueForKey:@"isCartView"]isEqualToString:@"yes"])
                     {
                         CheckoutViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"CheckoutViewController"];
                         [self.navigationController pushViewController:newView animated:YES];
                     }
                     else
                     {
                         UserProfileViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"UserProfileViewController"];
                         [self.navigationController pushViewController:newView animated:YES];
                     }
                     
                 }];
                 [alert addAction:okAction];
                 [self presentViewController:alert animated:YES completion:nil];
             }
             else
             {
                 [[AppDelegate shareInstance] hideActivityIndicator];
                 UIAlertController * alert=   [UIAlertController
                                               alertControllerWithTitle:nil
                                               message:[[responseDic objectForKey:@"payload"] valueForKey:@"message"]
                                               preferredStyle:UIAlertControllerStyleAlert];
                 UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                     // [self methodForUpdatevalues];
                     
                 }];
                 [alert addAction:okAction];
                 [self presentViewController:alert animated:YES completion:nil];
             }
         }];
    });
}

- (IBAction)btnCartAction:(id)sender
{
    CartViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"CartViewController"];
    [self.navigationController pushViewController:newView animated:YES];
}

- (IBAction)btnMenuAction:(id)sender
{
    [self.menuContainerViewController toggleLeftSideMenuCompletion:nil];
}

- (IBAction)btnActionProceed:(id)sender
{
    for (id currentValue in [dicForTextFieldvalues allValues])
    {
        if (![currentValue isEqualToString:@""])
        {
            checkForEmptyField = true;
        }
        else
        {
            checkForEmptyField = false;
            [[AppDelegate shareInstance]showAlertWithErrorMessage:NSLocalizedString(@"Please fill all fields.", @"Cancel")];
            return;
        }
    }
    if (checkForEmptyField == true)
    {
        [self methodForUpdatevalues];
    }
    else
    {
    }
}
@end

