//
//  ShopListViewController.h
//  Thuis today
//
//  Created by IMMANENT on 13/01/17.
//  Copyright © 2017 IMMANENT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShopListViewController : UIViewController
{
    
     IBOutlet UITableView *tableViewForShopsList;
     IBOutlet UIView *viewForPopUP;
    
     IBOutlet UITableView *tableViewForCategories;
     IBOutlet UILabel *lblCount;
    
     IBOutlet UILabel *lblShopName;
    
    __weak IBOutlet UIImageView *imgOrange;
    __weak IBOutlet UIView *kookboekView;
    
    __weak IBOutlet UIButton *btnStore;
    __weak IBOutlet UIButton *btnKookboek;
    
}
- (IBAction)btnCloseAction:(id)sender;
- (IBAction)btnChooseCategoriePop:(id)sender;
- (IBAction)btnOpenCategorieList:(id)sender;
- (IBAction)btnMenuAction:(id)sender;
- (IBAction)btnBottomBackAction:(id)sender;
- (IBAction)btnBottomCartAction:(id)sender;
- (IBAction)btnBottomHomeAction:(id)sender;
- (IBAction)btnActionStore:(id)sender;
- (IBAction)btnActionKookboek:(id)sender;



@property NSString *cityName;
@property NSString *zipCode;
@property NSString *categorieIds;
@end
