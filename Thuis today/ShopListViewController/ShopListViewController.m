//
//  ShopListViewController.m
//  Thuis today
//  Created by IMMANENT on 13/01/17.
//  Copyright © 2017 IMMANENT. All rights reserved.
//

#import "ShopListViewController.h"
#import "MFSideMenu.h"
#import "ShopDetailViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
@interface ShopListViewController ()
{
    NSMutableArray *arrayForSelectedIndexes,*arrayForCategories,*arrayForStoreCategoriesIds,*arrayForShops;
    NSString *categorieIds;
    BOOL checkViewForPopUpIsHidden;
    
}
@end

@implementation ShopListViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    viewForPopUP.hidden=YES;
    arrayForSelectedIndexes=[[NSMutableArray alloc]init];
    arrayForCategories = [[NSMutableArray alloc]init];
    arrayForStoreCategoriesIds = [[NSMutableArray alloc]init];
    arrayForShops = [[NSMutableArray alloc]init];
    
    [self callWebServiceForGetAllCategories];
    
    categorieIds = [_categorieIds stringByTrimmingCharactersInSet:
                    [NSCharacterSet whitespaceCharacterSet]];
    if (categorieIds == nil) {
        categorieIds = @"";
    }
    
    
    [self callWebServiceForGetAllShopes:categorieIds];
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    //[self callWebServiceForGetAllCategories];
//    categorieIds = [_categorieIds stringByTrimmingCharactersInSet:
//                   [NSCharacterSet whitespaceCharacterSet]];
//    if (categorieIds == nil) {
//        categorieIds = @"";
//    }
    lblCount.layer.cornerRadius = lblCount.frame.size.width/2;
    lblCount.layer.masksToBounds=YES;
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"cartCount"]!=nil) {
        lblCount.text=[[NSUserDefaults standardUserDefaults] valueForKey:@"cartCount"];
    }
    else
    {
        lblCount.text=@"";
    }
    [[NSUserDefaults standardUserDefaults]setObject:@"no" forKey:@"isCartView"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    //[self callWebServiceForGetAllShopes:categorieIds];
    
}
-(void)callWebServiceForGetAllCategories
{
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [[SingletonClass sharedManager] m_PostApiResponse:@"shopType" paramDiction:nil islogout:NO  onCompletion:^(NSDictionary *response)
         {
             NSDictionary *responseDic = (NSDictionary *)response;
             if ([[[responseDic objectForKey:@"payload"]valueForKey:@"status"]integerValue]==1)
             {
                 arrayForCategories = [[responseDic objectForKey:@"payload"] objectForKey:@"data"];
                 
                 [tableViewForCategories reloadData];
                 if (checkViewForPopUpIsHidden == true)
                 {
                     viewForPopUP.hidden=NO;
                     checkViewForPopUpIsHidden = false;
                 }
             }
             else
             {
                 
             }
         }];
    });
}
-(void)callWebServiceForGetAllShopes:(NSString *)categorieId{
    
    [[AppDelegate shareInstance]showActivityIndicator];
    
    _cityName =  [_cityName stringByTrimmingCharactersInSet:
     [NSCharacterSet whitespaceCharacterSet]];
    _zipCode =  [_zipCode stringByTrimmingCharactersInSet:
                  [NSCharacterSet whitespaceCharacterSet]];
    if (_zipCode == nil) {
        _zipCode = @"";
    }
    categorieId = [categorieId stringByTrimmingCharactersInSet:
                   [NSCharacterSet whitespaceCharacterSet]];
    NSDictionary *params;
    if (![_zipCode isEqualToString:@""] && ![categorieId isEqualToString:@""])
    {
        params = @{
                   @"zipcode":[NSString stringWithFormat:@"%@",_zipCode],
                   @"category":[NSString stringWithFormat:@"%@",categorieId]
                   };
    }
    else if (![_zipCode isEqualToString:@""]) {
        params = @{
                 @"zipcode":[NSString stringWithFormat:@"%@",_zipCode],
                };
    }
    else if (![_cityName isEqualToString:@""] && ![categorieId isEqualToString:@""])
    {
        params = @{
                   @"cityname":[NSString stringWithFormat:@"%@",_cityName],
                   @"category":[NSString stringWithFormat:@"%@",categorieId]
                   };
    }
    else if (![_cityName isEqualToString:@""]) {
        params = @{
                   @"cityname":[NSString stringWithFormat:@"%@",_cityName],
                   };
    }
    
    [[NSUserDefaults standardUserDefaults] setValue:_zipCode forKey:@"Zipcode"];
    [[NSUserDefaults standardUserDefaults] setValue:_cityName forKey:@"Cityname"];
    
    NSLog(@"%@",params);
    dispatch_async(dispatch_get_main_queue(), ^{
        [[SingletonClass sharedManager] m_PostApiResponse:@"findShop" paramDiction:params islogout:NO  onCompletion:^(NSDictionary *response)
         {
             NSDictionary *responseDic = (NSDictionary *)response;
             if ([[[responseDic objectForKey:@"payload"]valueForKey:@"status"]integerValue]==1)
             {
                 lblShopName.hidden = NO;
                 arrayForShops = [[responseDic objectForKey:@"payload"] objectForKey:@"data"];
                 lblShopName.text= [[responseDic objectForKey:@"payload"] objectForKey:@"town_name"];
                 [tableViewForShopsList reloadData];
                 viewForPopUP.hidden=YES;
                 [[AppDelegate shareInstance]hideActivityIndicator];
             }
             else
             {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     [[AppDelegate shareInstance]hideActivityIndicator];
                     [[AppDelegate shareInstance] showAlertWithErrorMessage:[[responseDic objectForKey:@"payload"]valueForKey:@"message"]];
                 });
             }
         }];
    });
}

#pragma mark -
#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
     if (tableView== tableViewForShopsList)
     {
          return arrayForShops.count;
     }
     else
     {
         return arrayForCategories.count;

     }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier;
    UITableViewCell *cell ;
    if (tableView== tableViewForShopsList) {
        
        CellIdentifier = @"cell";
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        UIImageView *imgForShopLogo = (UIImageView*)[cell viewWithTag:101];
        UIImageView *imgForShop = (UIImageView*)[cell viewWithTag:100];
         UILabel *lblShopName = (UILabel*)[cell viewWithTag:102];
         UILabel *lblShopTitle = (UILabel*)[cell viewWithTag:103];
        imgForShopLogo.layer.cornerRadius = imgForShopLogo.frame.size.width/2;
        imgForShopLogo.layer.masksToBounds =YES;
        
        
        imgForShop.layer.cornerRadius = imgForShop.frame.size.width/2;
        imgForShop.clipsToBounds = YES;
        
        lblShopName.text =[[arrayForShops objectAtIndex:indexPath.row] valueForKey:@"restaurant_name_ar"];
        lblShopTitle.text =[[arrayForShops objectAtIndex:indexPath.row] valueForKey:@"restaurant_description_ar"];
        [imgForShopLogo sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",baseUrlForImageProfile,[[arrayForShops objectAtIndex:indexPath.row] valueForKey:@"image"]]]
                        placeholderImage:[UIImage imageNamed:@"place-holder@1x"]];
        NSLog(@"%@%@",baseUrlForImageProfile,[[arrayForShops objectAtIndex:indexPath.row] valueForKey:@"image"]);
        [imgForShop sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",baseUrlForImageProfile,[[arrayForShops objectAtIndex:indexPath.row] valueForKey:@"logo"]]]
                          placeholderImage:[UIImage imageNamed:@"place-holder@1x"]];
        return cell;
    }
    else{
        CellIdentifier = @"Cell";
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        UIImageView *imgForCheck = (UIImageView*)[cell viewWithTag:101];
        
        UILabel *lblTitle = (UILabel*)[cell viewWithTag:102];
         lblTitle.text =[[arrayForCategories objectAtIndex:indexPath.row] valueForKey:@"category_name"];
        if ([arrayForSelectedIndexes containsObject:[NSString stringWithFormat:@"%ld",(long)indexPath.row]])
        {
            imgForCheck.image=[UIImage imageNamed:@"green-checkbox@1x"];
        }
        else
        {
            imgForCheck.image=[UIImage imageNamed:@"check-box@1x"];
            
        }
        return cell;
    }
    
}

#pragma mark -
#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (tableView ==tableViewForCategories)
    {
        if ([arrayForSelectedIndexes containsObject:[NSString stringWithFormat:@"%ld",(long)indexPath.row]]) {
            [arrayForSelectedIndexes removeObject:[NSString stringWithFormat:@"%ld",(long)indexPath.row]];
            [arrayForStoreCategoriesIds removeObject:[[arrayForCategories objectAtIndex:indexPath.row] valueForKey:@"category_id"]];
        }
        else
        {
            [arrayForSelectedIndexes addObject:[NSString stringWithFormat:@"%ld",(long)indexPath.row]];
            [arrayForStoreCategoriesIds addObject:[[arrayForCategories objectAtIndex:indexPath.row] valueForKey:@"category_id"]];
        }
        [tableView reloadData];
    }
    else{
        if (![[[NSUserDefaults standardUserDefaults]valueForKey:@"restaurant_id"]isEqualToString:[[arrayForShops objectAtIndex:indexPath.row] valueForKey:@"restaurant_id"]] && [[NSUserDefaults standardUserDefaults]valueForKey:@"restaurant_id"]!= nil)
        {
            UIAlertController * alert=   [UIAlertController
                                          alertControllerWithTitle:nil
                                          message:NSLocalizedString(@"Change Shop", @"Cancel")
                                          preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                [[CustomClasssesForCoreData sharedManagerForCoreData] methodForDeleteAllValues:nil onCompletion:^(NSArray *response)
                 {
                     [[NSUserDefaults standardUserDefaults]setObject:@"0" forKey:@"cartCount"];
                     [[NSUserDefaults standardUserDefaults]synchronize];
                     if ([[NSUserDefaults standardUserDefaults] valueForKey:@"cartCount"]!=nil) {
                         lblCount.text=[[NSUserDefaults standardUserDefaults] valueForKey:@"cartCount"];
                     }
                     else
                     {
                         lblCount.text=@"";
                     }

                 }];
                ShopDetailViewController*newView = [self.storyboard instantiateViewControllerWithIdentifier:@"ShopDetailViewController"];
                
                [[NSUserDefaults standardUserDefaults] setObject:[[arrayForShops objectAtIndex:indexPath.row] valueForKey:@"restaurant_id"] forKey:@"restaurant_id"];
                NSData *data = [NSKeyedArchiver archivedDataWithRootObject:[arrayForShops objectAtIndex:indexPath.row]];
                
                [[NSUserDefaults standardUserDefaults] setObject:data forKey:@"restaurantData"];
                
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                
                
                newView.stringForShopId =[[arrayForShops objectAtIndex:indexPath.row] valueForKey:@"restaurant_id"];
                newView.strForShopName =[[arrayForShops objectAtIndex:indexPath.row] valueForKey:@"restaurant_name_ar"];
                [self.navigationController pushViewController:newView animated:YES];

                
            }];
            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){
               
            }];
            [alert addAction:cancelAction];
            [alert addAction:okAction];
            [self presentViewController:alert animated:YES completion:nil];
            
        }
        else
        {
            ShopDetailViewController*newView = [self.storyboard instantiateViewControllerWithIdentifier:@"ShopDetailViewController"];
            
            [[NSUserDefaults standardUserDefaults] setObject:[[arrayForShops objectAtIndex:indexPath.row] valueForKey:@"restaurant_id"] forKey:@"restaurant_id"];
            NSData *data = [NSKeyedArchiver archivedDataWithRootObject:[arrayForShops objectAtIndex:indexPath.row]];
            
            [[NSUserDefaults standardUserDefaults] setObject:data forKey:@"restaurantData"];
            
            [[NSUserDefaults standardUserDefaults] synchronize];
            newView.stringForShopId =[[arrayForShops objectAtIndex:indexPath.row] valueForKey:@"restaurant_id"];
            newView.strForShopName =[[arrayForShops objectAtIndex:indexPath.row] valueForKey:@"restaurant_name_ar"];
            [self.navigationController pushViewController:newView animated:YES];
        }
    }
}

- (IBAction)btnCloseAction:(id)sender
{
    viewForPopUP.hidden=YES;
    tableViewForShopsList.hidden=NO;
}

- (IBAction)btnChooseCategoriePop:(id)sender
{
    viewForPopUP.hidden=YES;
    tableViewForShopsList.hidden=NO;
    if (arrayForStoreCategoriesIds.count)
    {
        categorieIds= @"";
        categorieIds= [arrayForStoreCategoriesIds componentsJoinedByString:@","];
    }
    else
    {
        categorieIds= @"";
    }
    [self callWebServiceForGetAllShopes:categorieIds];
}

- (IBAction)btnOpenCategorieList:(id)sender
{
    viewForPopUP.hidden=NO;
    tableViewForShopsList.hidden=YES;
    if (arrayForCategories.count)
    {
        viewForPopUP.hidden =NO;
        [tableViewForCategories reloadData];
    }
    else
    {
        checkViewForPopUpIsHidden=true;
        [self callWebServiceForGetAllCategories];
    }
}

- (IBAction)btnMenuAction:(id)sender
{
    [self.menuContainerViewController toggleLeftSideMenuCompletion:nil];

}

- (IBAction)btnBottomBackAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnBottomCartAction:(id)sender
{
    CartViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"CartViewController"];
    [self.navigationController pushViewController:newView animated:YES];
}

- (IBAction)btnBottomHomeAction:(id)sender
{
    ViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
    [self.navigationController pushViewController:newView animated:YES];
}

- (IBAction)btnActionStore:(id)sender
{
    kookboekView.hidden = YES;
    imgOrange.frame = CGRectMake(btnStore.frame.origin.x, imgOrange.frame.origin.y, btnStore.frame.size.width, imgOrange.frame.size.height);
}

- (IBAction)btnActionKookboek:(id)sender
{
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"No_Recipes"])
    {
        [[AppDelegate shareInstance] showAlertWithErrorMessage:@"No recipes available in this area"];
    }
    else
    {
        kookboekView.hidden = NO;
        imgOrange.frame = CGRectMake(btnKookboek.frame.origin.x, imgOrange.frame.origin.y, btnKookboek.frame.size.width, imgOrange.frame.size.height);
    }
}

- (IBAction)btnHomeAction:(id)sender {
    
        ViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
        [self.navigationController pushViewController:newView animated:YES];
}

- (IBAction)btnProfileAction:(id)sender
{
    if ( [[[NSUserDefaults standardUserDefaults] objectForKey:@"userData"] count])
    {
        UserProfileViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"UserProfileViewController"];
        [self.navigationController pushViewController:newView animated:YES];
    }
    else
    {
        LoginRegistrationViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginRegistrationViewController"];
        [self.navigationController pushViewController:newView animated:YES];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
