//
//  ContactUsViewController.m
//  Thuis today
//
//  Created by offshore_mac_1 on 18/08/17.
//  Copyright © 2017 IMMANENT. All rights reserved.
//

#import "ContactUsViewController.h"

@interface ContactUsViewController ()
{
    NSMutableArray *arrayForuserData;
}
@end

@implementation ContactUsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    arrayForuserData = [[NSMutableArray alloc]init];
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    arrayForuserData =[[NSUserDefaults standardUserDefaults] objectForKey:@"userData"];
    [[NSUserDefaults standardUserDefaults]setObject:@"no" forKey:@"isCartView"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    lblCount.layer.cornerRadius = lblCount.frame.size.width/2;
    lblCount.layer.masksToBounds=YES;
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"cartCount"]!=nil) {
        lblCount.text=[[NSUserDefaults standardUserDefaults] valueForKey:@"cartCount"];
    }
    else
    {
        lblCount.text=@"";
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Button Actions
- (IBAction)btnBottomBackAction:(id)sender
{
    ViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
    [self.navigationController pushViewController:newView animated:NO];
   // [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnBottomCartAction:(id)sender
{
    CartViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"CartViewController"];
    
    [self.navigationController pushViewController:newView animated:YES];
}

- (IBAction)btnBottomHomeAction:(id)sender
{
    ViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
    [self.navigationController pushViewController:newView animated:YES];
}
- (IBAction)btnCartAction:(id)sender
{
    CartViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"CartViewController"];
    [self.navigationController pushViewController:newView animated:YES];
}

- (IBAction)btnMenuAction:(id)sender
{
    [self.menuContainerViewController toggleLeftSideMenuCompletion:nil];
}

- (IBAction)btnProceedAction:(id)sender
{
    if ( [[[NSUserDefaults standardUserDefaults] objectForKey:@"userData"] count])
    {
        if([self validateAllValues]==YES)
        {
            [[AppDelegate shareInstance]showActivityIndicator];
            
            NSDictionary *params = @{
                                     @"subject":txtSubject.text,
                                     @"message":txtMessage.text,
                                     @"contact":txtContact.text,
                                     @"name":txtName.text,
                                     @"email":txtEmail.text,
                                     @"user_id":[arrayForuserData valueForKey:@"id"]
                                     };
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [[SingletonClass sharedManager] m_PostApiResponse:@"feedback_user" paramDiction:params  islogout:NO onCompletion:^(NSDictionary *response)
                 {
                     NSDictionary *responseDic = (NSDictionary *)response;
                     if ([[[responseDic objectForKey:@"payload"]valueForKey:@"status"]integerValue]==1)
                     {
                         txtSubject.text=@"";
                         txtMessage.text=@"";
                         txtContact.text=@"";
                         txtName.text=@"";
                         txtEmail.text=@"";
                         dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.6 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                             [self.view endEditing:YES];
                             [self methodForAlert:[[responseDic objectForKey:@"payload"] valueForKey:@"message"]];
                         });
                         
                         
                         [[AppDelegate shareInstance]hideActivityIndicator];
                         
                     }
                     else
                     {
                         dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.6 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                             [self.view endEditing:YES];
                             [[AppDelegate shareInstance] showAlertWithErrorMessage:[[responseDic objectForKey:@"payload"] valueForKey:@"message"]];
                         });
                         [[AppDelegate shareInstance]hideActivityIndicator];
                     }
                 }];
            });
        }
    }
    else
    {
        UIAlertController * alert=   [UIAlertController
                                      alertControllerWithTitle:nil
                                      message:NSLocalizedString(@"Contact Us Message", @"Cancel")
                                      preferredStyle:UIAlertControllerStyleAlert];
        //
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                   {
                                       [[NSUserDefaults standardUserDefaults]setObject:@"yes" forKey:@"isCartView"];
                                       [[NSUserDefaults standardUserDefaults]synchronize];
                                       LoginRegistrationViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginRegistrationViewController"];
                                       [self.navigationController pushViewController:newView animated:YES];
                                   }];
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){
            
        }];
        [alert addAction:cancelAction];
        [alert addAction:okAction];
        [self presentViewController:alert animated:YES completion:nil];
        
    }
}
#pragma mark - method For alert
-(void)methodForAlert:(NSString *)message
{
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:nil
                                  message:message
                                  preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        
    }];
    [alert addAction:okAction];
    
    [self presentViewController:alert animated:YES completion:nil];
}
#pragma mark -
#pragma mark - Check Validation Methods
-(BOOL)checkValidations
{
    NSString    *emailid            = txtEmail.text;
    NSString    *emailRegex         = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,20}";
    NSPredicate *emailTest          = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    return[emailTest evaluateWithObject:emailid];
}
//Method for validation for first name
-(BOOL)validFirstName
{
    NSString *emailid = txtName.text;
    NSString *emailRegex = @"[a-zA-z]+([ '-][a-zA-Z]+)*$";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:emailid];
}

#pragma -mark Custom Methods
//Method For Check Empty Fields
-(BOOL)emptyFieldsValidation
{
    txtSubject.text =[txtSubject.text stringByTrimmingCharactersInSet:
                      [NSCharacterSet whitespaceCharacterSet]];
    txtMessage.text =[txtMessage.text stringByTrimmingCharactersInSet:
                         [NSCharacterSet whitespaceCharacterSet]];
    txtContact.text =[txtContact.text stringByTrimmingCharactersInSet:
                          [NSCharacterSet whitespaceCharacterSet]];
    txtName.text =[txtName.text stringByTrimmingCharactersInSet:
                      [NSCharacterSet whitespaceCharacterSet]];
    txtEmail.text =[txtEmail.text stringByTrimmingCharactersInSet:
                       [NSCharacterSet whitespaceCharacterSet]];
    
    if (txtSubject.text.length==0||txtMessage.text.length==0 ||txtContact.text.length==0||txtName.text.length==0 || txtEmail.text.length==0)
    {
        return NO;
    }
    return YES;
    
}
// Validations on Textfields
-(BOOL) validateAllValues
{
    if (![self emptyFieldsValidation] )
    {
        [[AppDelegate shareInstance] showAlertWithErrorMessage:NSLocalizedString(@"Please fill all fields.", @"Cancel")];
        return NO;
    }
    else if (![self validFirstName])
    {
        [[AppDelegate shareInstance] showAlertWithErrorMessage:NSLocalizedString(@"Enter Valid  Name.", @"Cancel")];
        return NO;
    }
    else if (![self checkValidations])
    {
        [[AppDelegate shareInstance] showAlertWithErrorMessage:NSLocalizedString(@"Enter Valid Email Id", @"Cancel")];
        return NO;
    }
    return YES;
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [txtSubject resignFirstResponder];
    [txtMessage resignFirstResponder];
    [txtContact resignFirstResponder];
    [txtEmail resignFirstResponder];
    [txtName resignFirstResponder];
}
@end
