//
//  NewShopListViewController.h
//  Thuis today
//
//  Created by offshore_mac_1 on 28/12/17.
//  Copyright © 2017 IMMANENT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewShopListViewController : UIViewController
{
    
    IBOutlet UICollectionView *collectionViewForShopList;
    IBOutlet UIView *viewForPopUP;
    
    IBOutlet UITableView *tableViewForCategories;
    IBOutlet UILabel *lblCount;
    
    __weak IBOutlet UILabel *lblOpenCloseTime;
    __weak IBOutlet UIImageView *imgViewProdBorder;
    __weak IBOutlet UIImageView *imgCategory;
    __weak IBOutlet UIView *ViewForInfoPopUp;
    IBOutlet UILabel *lblShopName;
    __weak IBOutlet UILabel *lblShopOwnerName;
    
    __weak IBOutlet UIImageView *imgViewIcon;
    __weak IBOutlet UIImageView *imgViewCatgBorder;
    __weak IBOutlet UITextView *txtViewDescription;
    __weak IBOutlet UIImageView *imgShopImage;
    __weak IBOutlet UIImageView *imgOrange;
    __weak IBOutlet UIView *kookboekView;
    
    __weak IBOutlet UIButton *btnChooseCategory;
    __weak IBOutlet UILabel *lblTime;
   
    __weak IBOutlet UILabel *lblCategoryName;
    __weak IBOutlet UILabel *lbladdress;
    __weak IBOutlet UIButton *btnStore;
    __weak IBOutlet UIButton *btnKookboek;
    
}
- (IBAction)btnCloseInfoWndowPopUp:(id)sender;

- (IBAction)btnCloseAction:(id)sender;
- (IBAction)btnChooseCategoriePop:(id)sender;
- (IBAction)btnOpenCategorieList:(id)sender;
- (IBAction)btnMenuAction:(id)sender;
- (IBAction)btnBottomBackAction:(id)sender;
- (IBAction)btnBottomCartAction:(id)sender;
- (IBAction)btnBottomHomeAction:(id)sender;
- (IBAction)btnActionStore:(id)sender;
- (IBAction)btnActionKookboek:(id)sender;



@property NSString *cityName;
@property NSString *zipCode;
@property NSString *categorieIds;

@end
