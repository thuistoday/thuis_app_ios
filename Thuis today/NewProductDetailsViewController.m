//
//  NewProductDetailsViewController.m
//  Thuis today
//
//  Created by offshore_mac_1 on 13/01/18.
//  Copyright © 2018 IMMANENT. All rights reserved.
//
#define RGB(r, g, b) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:1]
#define RGBA(r, g, b, a) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:a]
#define ACCEPTABLE_CHARACTERS @"0123456789,"
#define ACCEPTABLE_CHARACTERSWITHOUT @"0123456789"
#import "NewProductDetailsViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
@interface NewProductDetailsViewController ()
{   NSMutableArray *arrayForShop,*arrayForCoreDatavalues,*arrayNutritionValueKeys;
    NSString *unit;
    NSMutableDictionary *dicInfo;
    
}
@property(nonatomic,weak) IBOutlet UILabel *lblTitle;
@end

@implementation NewProductDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    float sizeOfContent = 0;
    UIView *lLast = [scrollView.subviews lastObject];
    NSInteger wd = lLast.frame.origin.y;
    NSInteger ht = lLast.frame.size.height;
    
    sizeOfContent = wd+ht;
    [CommonMethods configureNavigationBarForViewController:self withTitle:@""];
    //scrollView.contentSize = CGSizeMake(scrollView.frame.size.width, 800);
    dicInfo = [NSMutableDictionary new];
    arrayForShop=[[NSMutableArray alloc]init];
    arrayNutritionValueKeys = [[NSMutableArray alloc]initWithObjects:@"Energy",@"which_saturated",@"single_unsaturated",@"multiply_unsaturated",@"Carbohydrates",@"sugars",@"Nutritional_fiber",@"Proteins",@"salt",@"vet", nil];
    
    NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:@"restaurantData"];
    arrayForShop = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    lblShopAddress.text =[arrayForShop valueForKey:@"address_ar"];
    
    if (IDIOM ==  IPAD) {
        self.lblTitle.text = [arrayForShop valueForKey:@"restaurant_name_ar"];
    }
    else {
    [CommonMethods addTitleLabelForNavigationBarForViewController:self withTitle:[arrayForShop valueForKey:@"restaurant_name_ar"]];
    }
    [imgShop sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",baseUrlForImage,[arrayForShop valueForKey:@"logo"]]]
               placeholderImage:[UIImage imageNamed:@"place-holder@1x"]];
    arrayForCoreDatavalues = [[NSMutableArray alloc]init];
    NSLog(@"%@",_arrayForProductInfo);
    lblDescription.text=[_arrayForProductInfo valueForKey:@"meal_description_ar"];
    lblDescription.numberOfLines = 0; //will wrap text in new line
    //lblDescription.textAlignment=NSTextAlignmentCenter;
    [lblDescription sizeToFit];
    lblCount.layer.cornerRadius = lblCount.frame.size.width/2;
    lblCount.layer.masksToBounds = YES;
    
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"cartCount"]!=nil) {
        lblCount.text=[[NSUserDefaults standardUserDefaults] valueForKey:@"cartCount"];
    }
    else
    {
        lblCount.text=@"";
    }
    //    if ([[_arrayForProductInfo  valueForKey:@"product_unit"] isEqualToString:@"weight"]) {
    //        lblUnit.text=@"Kg";
    //        unit =@"Kg";
    //
    //    }
    //    else{
    //        lblUnit.text=@"Pcs";
    //           unit =@"Pcs";
    //    }
    tbInfo.scrollEnabled = false;
    unit =[_arrayForProductInfo  valueForKey:@"actual_unit"];
    lblUnit.text = [_arrayForProductInfo  valueForKey:@"actual_unit"];
    [imgProduct sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",baseUrlForImage,[_arrayForProductInfo valueForKey:@"meal_image"]]]
                  placeholderImage:[UIImage imageNamed:@"place-holder@1x"]];
    
    NSString *str =@"%";
    lblVat.text=[NSString stringWithFormat:@"VAT:%@%@ %@",[_arrayForProductInfo valueForKey:@"vat_name"],str,[_arrayForProductInfo  valueForKey:@"type"]];
    
    lblProductName.text=[_arrayForProductInfo  valueForKey:@"meal_name_ar"];
    
    
    float sum=0.00 ;
    float vat=0.00;
    float orgPrice=0.00;
    NSString* cleanedString = [[[_arrayForProductInfo valueForKey:@"meal_price"] stringByReplacingOccurrencesOfString:@"," withString:@"."]
                               stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
    
    
    vat = ([cleanedString floatValue]*[[_arrayForProductInfo  valueForKey:@"vat_name"] floatValue])/100;
    
    if ([[_arrayForProductInfo valueForKey:@"type"] isEqualToString:@"Excl."]) {
        // sum = ([cleanedString floatValue]+vat)*[txtFld.text floatValue]  ;
        sum = [cleanedString floatValue]+vat;
        orgPrice =[cleanedString floatValue]+vat*1;
        NSString *roundOff = [NSString stringWithFormat:@"%.2f",sum];
        sum = [roundOff floatValue] *1;
    }
    else
    {
        sum = 1 * [cleanedString floatValue];
        orgPrice =[cleanedString floatValue]*1;
    }
    NSString* strForPriceWithComma = [[[NSString stringWithFormat:@"%.2f",sum] stringByReplacingOccurrencesOfString:@"." withString:@","]
                                      stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
    NSString* strForPriceWithComma2 = [[[NSString stringWithFormat:@"%.2f",orgPrice] stringByReplacingOccurrencesOfString:@"." withString:@","]
                                       stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
    
    lblOriginalPrice.text = [NSString stringWithFormat:@"€ %@/%@",strForPriceWithComma2,unit];
    txtQuantity.text = @"";
    lblTotalPrice.text =[NSString stringWithFormat:@"€ %@",strForPriceWithComma];
    
    NSArray *keys =  [NSArray array];
    NSDictionary *dict = [NSDictionary new];
    
    dict = [_arrayForProductInfo mutableCopy];
    keys = [dict allKeys];
    for (int i = 0; i < [arrayNutritionValueKeys count]; i++)
    {
        if ([keys containsObject:[arrayNutritionValueKeys objectAtIndex:i]])
        {
            [dicInfo setObject:[_arrayForProductInfo valueForKey:[arrayNutritionValueKeys objectAtIndex:i]] forKey:[arrayNutritionValueKeys objectAtIndex:i]];
        }
    }
    
    // dicInfo = [_arrayForProductInfo mutableCopy];
    
    if ([[_arrayForProductInfo valueForKey:@"additional_info"]intValue ]==0) {
        tbInfo.hidden = YES;
        lblProductInfo.hidden =YES;
    }
    else
        
    {
        tbInfo.hidden = NO;
        lblProductInfo.hidden =NO;
        if(IDIOM==IPAD) {
            scrollView.contentSize = CGSizeMake(scrollView.frame.size.width,scrollView.frame.size.height+150);
        }
        else{
            scrollView.contentSize = CGSizeMake(scrollView.frame.size.width,(scrollView.frame.size.height+arrayNutritionValueKeys.count*40));
        }
     
        
    }
    [tbInfo setTableFooterView:[[UIView alloc] initWithFrame:CGRectMake(0,0,0,0)]];
    
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self adjustViews];
}

- (void)adjustViews {
    CGRect productNameFrame = lblProductName.frame;
    productNameFrame.origin.y = imgProduct.frame.origin.y + imgProduct.frame.size.height + 8;
    lblProductName.frame = productNameFrame;
    
    CGRect originalPriceFrame = lblOriginalPrice.frame;
    originalPriceFrame.origin.y = lblProductName.frame.origin.y + lblProductName.frame.size.height + 8;
    lblOriginalPrice.frame = originalPriceFrame;
    
    lblDescription.frame = CGRectMake(lblDescription.frame.origin.x, (lblOriginalPrice.frame.origin.y+lblOriginalPrice.frame.size.height+8), lblDescription.frame.size.width, lblDescription.frame.size.height);
    
    viewData.frame=CGRectMake(viewData.frame.origin.x,lblDescription.frame.origin.y +lblDescription.frame.size.height,viewData.frame.size.width,viewData.frame.size.height);
    NSLog(@"%f",viewData.frame.origin.y);
    tbInfo.frame = CGRectMake(tbInfo.frame.origin.x,110, tbInfo.frame.size.width, tbInfo.frame.size.height+(arrayNutritionValueKeys.count*39));
    
}

- (IBAction)btnCartAction:(id)sender
{
    [self performSegueWithIdentifier:@"NavigateToCartViewController" sender:nil];
}


- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    //    if (textField.text.length == 1) {
    //        if ( [string isEqualToString:@""] )
    //            return NO;
    //    }
    if (textField.text.length >=5 && range.length == 0)
    {
        return NO; // return NO to not change text
    }
    else
    {
//    if ([[_arrayForProductInfo  valueForKey:@"product_unit"] isEqualToString:@"weight"])
//    {
//        if ([unit isEqualToString:@"per stuk"] || [unit isEqualToString:@"per zak"] || [unit isEqualToString:@"per doos"])
//        {
        if ([[_arrayForProductInfo  valueForKey:@"product_unit"] isEqualToString:@"stuk"] || [[_arrayForProductInfo  valueForKey:@"product_unit"] isEqualToString:@"zak"] || [[_arrayForProductInfo  valueForKey:@"product_unit"] isEqualToString:@"doos"])
           {
            if ([self checkForValidCharacterForWithout:string]) {
                
                string = [[string stringByReplacingOccurrencesOfString:@"." withString:@","]
                          stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
                
                NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
                newString =     [newString stringByTrimmingCharactersInSet:
                                 [NSCharacterSet whitespaceCharacterSet]];
                float sum=0.00 ;
                float vat=0.00;
                float orgPrice=0.00;
                NSString* cleanedString = [[[_arrayForProductInfo valueForKey:@"meal_price"] stringByReplacingOccurrencesOfString:@"," withString:@"."]
                                           stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
                
                NSString *strTextField =[[newString stringByReplacingOccurrencesOfString:@"," withString:@"."]
                                         stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
                
                vat = ([cleanedString floatValue]*[[_arrayForProductInfo  valueForKey:@"vat_name"] floatValue])/100;
                NSString *str =@"%";
                lblVat.text=[NSString stringWithFormat:@"VAT:%@%@ %@",[_arrayForProductInfo  valueForKey:@"vat_name"],str,[_arrayForProductInfo valueForKey:@"type"]];
                if ([[_arrayForProductInfo valueForKey:@"type"] isEqualToString:@"Excl."]) {
                    // sum = ([cleanedString floatValue]+vat)*[txtFld.text floatValue]  ;
                    sum = [cleanedString floatValue]+vat;
                    orgPrice =[cleanedString floatValue]+vat*1;
                    NSString *roundOff = [NSString stringWithFormat:@"%.2f",sum];
                    sum = [roundOff floatValue] *[strTextField floatValue];
                }
                else
                {
                    sum = [strTextField floatValue] * [cleanedString floatValue];
                    orgPrice =[cleanedString floatValue]*1;
                }
                NSString* strForPriceWithComma = [[[NSString stringWithFormat:@"%.2f",sum] stringByReplacingOccurrencesOfString:@"." withString:@","]
                                                  stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
                NSString* strForPriceWithComma2 = [[[NSString stringWithFormat:@"%.2f",orgPrice] stringByReplacingOccurrencesOfString:@"." withString:@","]
                                                   stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
                
                
                
                
                
                lblTotalPrice.text = [NSString stringWithFormat:@"€ %@",strForPriceWithComma];
                lblOriginalPrice.text =  [NSString stringWithFormat:@"€ %@",strForPriceWithComma2];
                return true;
            }
            else{
                return false;
            }
        }
        else
        {
            if ([self checkForValidCharacter:string]) {
                
                string = [[string stringByReplacingOccurrencesOfString:@"." withString:@","]
                          stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
                
                NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
                newString =     [newString stringByTrimmingCharactersInSet:
                                 [NSCharacterSet whitespaceCharacterSet]];
                float sum=0.00 ;
                float vat=0.00;
                float orgPrice=0.00;
                NSString* cleanedString = [[[_arrayForProductInfo valueForKey:@"meal_price"] stringByReplacingOccurrencesOfString:@"," withString:@"."]
                                           stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
                
                NSString *strTextField =[[newString stringByReplacingOccurrencesOfString:@"," withString:@"."]
                                         stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
                
                vat = ([cleanedString floatValue]*[[_arrayForProductInfo  valueForKey:@"vat_name"] floatValue])/100;
                NSString *str =@"%";
                lblVat.text=[NSString stringWithFormat:@"VAT:%@%@ %@",[_arrayForProductInfo  valueForKey:@"vat_name"],str,[_arrayForProductInfo valueForKey:@"type"]];
                if ([[_arrayForProductInfo valueForKey:@"type"] isEqualToString:@"Excl."]) {
                    // sum = ([cleanedString floatValue]+vat)*[txtFld.text floatValue]  ;
                    sum = [cleanedString floatValue]+vat;
                    orgPrice =[cleanedString floatValue]+vat*1;
                    NSString *roundOff = [NSString stringWithFormat:@"%.2f",sum];
                    sum = [roundOff floatValue] *[strTextField floatValue];
                }
                else
                {
                    sum = [strTextField floatValue] * [cleanedString floatValue];
                    orgPrice =[cleanedString floatValue]*1;
                }
                NSString* strForPriceWithComma = [[[NSString stringWithFormat:@"%.2f",sum] stringByReplacingOccurrencesOfString:@"." withString:@","]
                                                  stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
                NSString* strForPriceWithComma2 = [[[NSString stringWithFormat:@"%.2f",orgPrice] stringByReplacingOccurrencesOfString:@"." withString:@","]
                                                   stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
                
                
                
                
                
                lblTotalPrice.text = [NSString stringWithFormat:@"€ %@",strForPriceWithComma];
                lblOriginalPrice.text =  [NSString stringWithFormat:@"€ %@",strForPriceWithComma2];
                return true;
            }
            else{
                return false;
            }
        }
        
    //}
    //else
//    {
//        if ([self checkForValidCharacterForWithout:string]) {
//
//            string = [[string stringByReplacingOccurrencesOfString:@"." withString:@","]
//                      stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
//
//            NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
//            newString =     [newString stringByTrimmingCharactersInSet:
//                             [NSCharacterSet whitespaceCharacterSet]];
//            float sum=0.00 ;
//            float vat=0.00;
//            float orgPrice=0.00;
//            NSString* cleanedString = [[[_arrayForProductInfo valueForKey:@"meal_price"] stringByReplacingOccurrencesOfString:@"," withString:@"."]
//                                       stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
//
//            NSString *strTextField =[[newString stringByReplacingOccurrencesOfString:@"," withString:@"."]
//                                     stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
//
//            vat = ([cleanedString floatValue]*[[_arrayForProductInfo  valueForKey:@"vat_name"] floatValue])/100;
//            NSString *str =@"%";
//            lblVat.text=[NSString stringWithFormat:@"VAT:%@%@ %@",[_arrayForProductInfo  valueForKey:@"vat_name"],str,[_arrayForProductInfo valueForKey:@"type"]];
//            if ([[_arrayForProductInfo valueForKey:@"type"] isEqualToString:@"Excl."]) {
//                // sum = ([cleanedString floatValue]+vat)*[txtFld.text floatValue]  ;
//                sum = [cleanedString floatValue]+vat;
//                orgPrice =[cleanedString floatValue]+vat*1;
//                NSString *roundOff = [NSString stringWithFormat:@"%.2f",sum];
//                sum = [roundOff floatValue] *[strTextField floatValue];
//            }
//            else
//            {
//                sum = [strTextField floatValue] * [cleanedString floatValue];
//                orgPrice =[cleanedString floatValue]*1;
//            }
//
//            NSString* strForPriceWithComma = [[[NSString stringWithFormat:@"%.2f",sum] stringByReplacingOccurrencesOfString:@"." withString:@","]
//                                              stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
//            NSString* strForPriceWithComma2 = [[[NSString stringWithFormat:@"%.2f",orgPrice] stringByReplacingOccurrencesOfString:@"." withString:@","]
//                                               stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
//
//
//
//
//
//            lblTotalPrice.text = [NSString stringWithFormat:@"€ %@",strForPriceWithComma];
//            lblOriginalPrice.text =  [NSString stringWithFormat:@"€ %@",strForPriceWithComma2];
//            return true;
//        }
//        else
//        {
//            return false;
//        }
//
//    }
    }
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [txtQuantity resignFirstResponder];
}

-(BOOL)checkForValidCharacter:(NSString*)str
{
    NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:ACCEPTABLE_CHARACTERS] invertedSet];
    NSString *filtered = [[str componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
    if ([str isEqualToString:filtered])
    {
        return true;
    }
    else
    {
        return false;
    }
}
-(BOOL)checkForValidCharacterForWithout:(NSString*)str
{
    NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:ACCEPTABLE_CHARACTERSWITHOUT] invertedSet];
    NSString *filtered = [[str componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
    if ([str isEqualToString:filtered])
    {
        return true;
    }
    else
    {
        return false;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark -
#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return dicInfo.allKeys.count+1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier;
    UITableViewCell *cell ;
    if(indexPath.row==0)
    {
        
        CellIdentifier = @"cell1";
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        UILabel *lblKey = (UILabel *)[cell viewWithTag:101];
        lblKey.layer.borderColor = [UIColor colorWithRed:82/255.0f green:139/255.0f blue:11/255.0f alpha:1].CGColor;
        lblKey.layer.borderWidth = 1;
        lblKey.layer.masksToBounds = true;
        UILabel *lblValue = (UILabel *)[cell viewWithTag:102];
        lblValue.layer.borderColor = [UIColor colorWithRed:82/255.0f green:139/255.0f blue:11/255.0f alpha:1].CGColor;
        lblValue.layer.borderWidth = 1;
        lblValue.layer.masksToBounds = true;
        lblKey.text = @"Energy";
        lblValue.text = @"Value";
    }
    else
    {
        CellIdentifier = @"cell2";
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        UILabel *lblKey = (UILabel *)[cell viewWithTag:201];
        lblKey.layer.borderColor = [UIColor colorWithRed:82/255.0f green:139/255.0f blue:11/255.0f alpha:1].CGColor;
        lblKey.layer.borderWidth = 1;
        lblKey.layer.masksToBounds = true;
        UILabel *lblValue = (UILabel *)[cell viewWithTag:202];
        lblValue.layer.borderColor = [UIColor colorWithRed:82/255.0f green:139/255.0f blue:11/255.0f alpha:1].CGColor;
        lblValue.layer.borderWidth = 1;
        lblValue.layer.masksToBounds = true;
        lblKey.text = [dicInfo.allKeys objectAtIndex:indexPath.row-1];
        lblValue.text = [dicInfo.allValues objectAtIndex:indexPath.row-1];
        if (indexPath.row%2==0)
        {
            
            cell.backgroundColor = [UIColor clearColor];
        }
        else
        {
            cell.backgroundColor = RGB(203, 217, 161);
            
        }
    }
    
    
    return cell;
}
-(UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    static NSString *CellIdentifier;
    UITableViewCell *cell ;
    CellIdentifier = @"cell1";
    cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    UILabel *lblKey = (UILabel *)[cell viewWithTag:101];
    lblKey.layer.borderColor = [UIColor colorWithRed:82/255.0f green:139/255.0f blue:11/255.0f alpha:1].CGColor;
    lblKey.layer.borderWidth = 2;
    lblKey.layer.masksToBounds = true;
    UILabel *lblValue = (UILabel *)[cell viewWithTag:102];
    lblValue.layer.borderColor = [UIColor colorWithRed:82/255.0f green:139/255.0f blue:11/255.0f alpha:1].CGColor;
    lblValue.layer.borderWidth = 2;
    lblValue.layer.masksToBounds = true;
    return cell;
}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (IBAction)btnBackAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnAddToCartActiion:(id)sender
{
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"is_recipe"];
    [txtQuantity resignFirstResponder];
    float sum=0.00 ;
    float vat=0.00;
    float price=0.00;
    NSString *type=[_arrayForProductInfo  valueForKey:@"type"] ;
    NSString* cleanedString = [[[_arrayForProductInfo valueForKey:@"meal_price"] stringByReplacingOccurrencesOfString:@"," withString:@"."]
                               stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
    
    NSString *strTextField =[[txtQuantity.text stringByReplacingOccurrencesOfString:@"," withString:@"."]
                             stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
    
    vat = ([cleanedString floatValue]*[[_arrayForProductInfo  valueForKey:@"vat_name"] floatValue])/100;
    
    NSLog(@"%f",vat);
    
    if ([[_arrayForProductInfo  valueForKey:@"type"] isEqualToString:@"Excl."]) {
        // sum = ([cleanedString floatValue]+vat)*[txtFld.text floatValue]  ;
        sum = [cleanedString floatValue]+vat;
        price=sum;
        NSString *roundOff = [NSString stringWithFormat:@"%.2f",sum];
    }
    else
    {
        price=[cleanedString floatValue];
        sum = [cleanedString floatValue];
    }
    
    
    
    
    if (![lblTotalPrice.text isEqualToString:@"€ 0,00"]) {
        [[CustomClasssesForCoreData sharedManagerForCoreData] methodForSaveValues:_arrayForProductInfo   price:lblTotalPrice.text quantity:txtQuantity.text singlePrice:[NSString stringWithFormat:@"%.2f",price] type:type  onCompletion:^(NSArray *response){
            NSLog(@"%lu",(unsigned long)response.count);
        }];
        [self methodForGetTotalPrice];
    }
    else
    {
        
        [[AppDelegate shareInstance] showAlertWithErrorMessage: NSLocalizedString(@"Price should not be zero", @"Cancel")];
        
    }
    
}
-(void)methodForGetTotalPrice
{
    
    [[CustomClasssesForCoreData sharedManagerForCoreData] methodForFetchValues:nil onCompletion:^(NSArray *response)
     {
         NSLog(@"%@",response);
         arrayForCoreDatavalues = [response mutableCopy];
     }];
    NSString *count = [NSString stringWithFormat:@"%lu",(unsigned long)arrayForCoreDatavalues.count];
    [[NSUserDefaults standardUserDefaults]setObject:count forKey:@"cartCount"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"cartCount"]!=nil) {
        lblCount.text=[[NSUserDefaults standardUserDefaults] valueForKey:@"cartCount"];
    }
    else
    {
        lblCount.text=@"";
    }
}

- (IBAction)btnProfileAction:(id)sender
{
    if ( [[[NSUserDefaults standardUserDefaults] objectForKey:@"userData"] count])
    {
        UserProfileViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"UserProfileViewController"];
        [self.navigationController pushViewController:newView animated:YES];
    }
    else
    {
        LoginRegistrationViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginRegistrationViewController"];
        [self.navigationController pushViewController:newView animated:YES];
    }
}

- (IBAction)btnHomeAction:(id)sender
{
    ViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
    [self.navigationController pushViewController:newView animated:YES];
}

- (IBAction)btnMenuAction:(id)sender
{
    [self.menuContainerViewController toggleLeftSideMenuCompletion:nil];
}

- (IBAction)btnBottomBackAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnBottomCartAction:(id)sender
{
    CartViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"CartViewController"];
    [self.navigationController pushViewController:newView animated:YES];
}

- (IBAction)btnBottomHomeAction:(id)sender
{
    ViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
    [self.navigationController pushViewController:newView animated:YES];
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
