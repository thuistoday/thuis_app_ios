//
//  NeedHelpWebViewController.h
//  Thuis today
//
//  Created by IMMANENT on 30/06/17.
//  Copyright © 2017 IMMANENT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NeedHelpWebViewController : UIViewController
{
    IBOutlet UILabel *lblCount;
    
     IBOutlet UIWebView *webViewOutlet;
    
     IBOutlet UIActivityIndicatorView *activityIndicatorOutlet;
    
}

- (IBAction)btnCartAction:(id)sender;
- (IBAction)btnMenuAction:(id)sender;


- (IBAction)btnBottomBackAction:(id)sender;
- (IBAction)btnBottomCartAction:(id)sender;
- (IBAction)btnBottomHomeAction:(id)sender;

@property NSString *url;

@end
