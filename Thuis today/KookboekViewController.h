//
//  KookboekViewController.h
//  RecipeTinder8
//
//  Created by offshore_mac_1 on 11/10/17.
//  Copyright © 2017 offshore_mac_1. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BSStackView.h"
#import "TTFaveButton.h"
#import "MARKRangeSlider.h"
@interface KookboekViewController : UIViewController<BSStackViewProtocol>

@property (weak, nonatomic) IBOutlet UIView *subView;
@property (weak, nonatomic) IBOutlet UIView *FilterView;
@property (weak, nonatomic) IBOutlet UIImageView *image;
@property (weak, nonatomic) IBOutlet UILabel *label;
@property (weak, nonatomic) IBOutlet UIButton *btnFilterView;
@property (weak, nonatomic) IBOutlet UIView *textFieldView;

@property (weak, nonatomic) IBOutlet UIButton *btnFilter;
@property (weak, nonatomic) IBOutlet TTFaveButton *btnSpark;
@property (weak, nonatomic) IBOutlet MARKRangeSlider *rangeSlider;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UITextField *txtTag;

@property (nonatomic) IBInspectable UIColor *borderColor;
@property (nonatomic) IBInspectable CGFloat borderWidth;
@property (nonatomic) IBInspectable CGFloat cornerRadius;
@property (weak, nonatomic) IBOutlet UIButton *btnBacktoKookboek;
-(IBAction)btnFilterViewTouchupInside:(id)sender;
-(IBAction)BacktoKookboekTouchupInside:(id)sender;
-(IBAction)btnFilterTouchupInside:(id)sender;
- (IBAction)btnActionLeftSwipe:(id)sender;
- (IBAction)btnActionLike:(id)sender;
- (IBAction)btnActionRightSwipe:(id)sender;
- (IBAction)rangeSliderValueDidChange:(id)sender;
- (IBAction)btnActionFilterCategory:(id)sender;

@end
