//
//  ShopDetailViewController.h
//  Thuis today
//
//  Created by IMMANENT on 13/01/17.
//  Copyright © 2017 IMMANENT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShopDetailViewController : UIViewController<UITextFieldDelegate>
{
    
     IBOutlet UIView *viewForCategory;
    
     IBOutlet UITableView *tableViewForShopsList;
     IBOutlet UITableView *tableViewForCategory;
    
     IBOutlet UILabel *lblCount;
    
     IBOutlet UILabel *lblForShopName;
    
     IBOutlet UILabel *lblShopAddress;
    
     IBOutlet UIImageView *imgShopLogo;
    
     IBOutlet UILabel *lblShopDescription;
    
     IBOutlet UILabel *lblOpenCLose;
    
}
- (IBAction)btnCategoryAction:(id)sender;
- (IBAction)BtnOkAction:(id)sender;
- (IBAction)btnAddToCartAction:(id)sender;
- (IBAction)btnMenuAction:(id)sender;

- (IBAction)btnBottomBackAction:(id)sender;
- (IBAction)btnBottomCartAction:(id)sender;
- (IBAction)btnBottomHomeAction:(id)sender;




@property NSString *stringForShopId;
@property NSString *strForShopName;

@end
