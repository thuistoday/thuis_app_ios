//
//  ShopDetailViewController.m
//  Thuis today
//
//  Created by IMMANENT on 13/01/17.
//  Copyright © 2017 IMMANENT. All rights reserved.
//
//#define ACCEPTABLE_CHARACTERS @" ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_."
#define ACCEPTABLE_CHARACTERS @"0123456789,"
#define ACCEPTABLE_CHARACTERSWITHOUT @"0123456789"
#import <SDWebImage/UIImageView+WebCache.h>

#import "ShopDetailViewController.h"
#import "MFSideMenu.h"
@interface ShopDetailViewController ()
{
    float animatedViewHeight;
    NSMutableArray *arrayForSelectedIndexes ,*arrayForProductCategories,*arrayForStoreCategoriesIds,*arrayForProducts,*arrayForCoreDatavalues,*arrayForShop;
    BOOL checkViewForPopUpIsHidden,checkForPopUp;
    NSString *categorieIds;
    NSMutableDictionary *dicForTextFieldvalues;
    NSIndexPath * newIndexPath;
}
@end

@implementation ShopDetailViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    viewForCategory.hidden=YES;
    animatedViewHeight =viewForCategory.frame.size.height;
    arrayForSelectedIndexes =[[NSMutableArray alloc]init];
    arrayForProductCategories = [[NSMutableArray alloc]init];
    arrayForStoreCategoriesIds =[[NSMutableArray alloc]init];
    arrayForProducts = [[NSMutableArray alloc]init];
    dicForTextFieldvalues = [[NSMutableDictionary alloc]init];
    arrayForCoreDatavalues = [[NSMutableArray alloc]init];
    
    arrayForShop=[[NSMutableArray alloc]init];
    NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:@"restaurantData"];
    arrayForShop = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    lblShopAddress.text =[arrayForShop valueForKey:@"address_ar"];
     lblForShopName.text =[arrayForShop valueForKey:@"restaurant_name_ar"];
    lblShopDescription.text =[arrayForShop valueForKey:@"restaurant_description_ar"];
  
    NSLog(@"%@",arrayForShop);
    [imgShopLogo sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",baseUrlForImageProfile,[arrayForShop valueForKey:@"logo"]]]
                    placeholderImage:[UIImage imageNamed:@"place-holder@1x"]];
    NSCalendar* calender = [NSCalendar currentCalendar];
    NSDateComponents* component = [calender components:NSWeekdayCalendarUnit fromDate:[NSDate date]];
    NSLog(@"%ld",(long)component.weekday);
    
    if (component.weekday == 1) {
      lblOpenCLose.text = [NSString stringWithFormat:@"%@:%@",NSLocalizedString(@"Open-Close", @"Cancel"),[arrayForShop valueForKey:@"sun"]];
    }
    else if(component.weekday == 2)
    {
         lblOpenCLose.text = [NSString stringWithFormat:@"%@:%@",NSLocalizedString(@"Open-Close", @"Cancel"),[arrayForShop valueForKey:@"mon"]];
    }
    else if(component.weekday == 3)
    {
         lblOpenCLose.text = [NSString stringWithFormat:@"%@:%@",NSLocalizedString(@"Open-Close", @"tue"),[arrayForShop valueForKey:@"mon"]];
        
    }
    else if(component.weekday == 4)
    {
          lblOpenCLose.text = [NSString stringWithFormat:@"%@:%@",NSLocalizedString(@"Open-Close", @"tue"),[arrayForShop valueForKey:@"wed"]];
    }
    else if(component.weekday == 5)
    {
           lblOpenCLose.text = [NSString stringWithFormat:@"%@:%@",NSLocalizedString(@"Open-Close", @"tue"),[arrayForShop valueForKey:@"thr"]];
    }
    else if(component.weekday == 6)
    {
         lblOpenCLose.text = [NSString stringWithFormat:@"%@:%@",NSLocalizedString(@"Open-Close", @"tue"),[arrayForShop valueForKey:@"fri"]];
    }
    else if(component.weekday == 7)
    {
          lblOpenCLose.text = [NSString stringWithFormat:@"%@:%@",NSLocalizedString(@"Open-Close", @"tue"),[arrayForShop valueForKey:@"sat"]];
    }
    
    
     [self callWebServiceForGetAllProducts:@""];

}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    lblCount.layer.cornerRadius = lblCount.frame.size.width/2;
    lblCount.layer.masksToBounds=YES;
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"cartCount"]!=nil) {
        lblCount.text=[[NSUserDefaults standardUserDefaults] valueForKey:@"cartCount"];
    }
    else
    {
        lblCount.text=@"";
    }
    lblForShopName.text = [arrayForShop valueForKey:@"restaurant_name_ar"];

    [[NSUserDefaults standardUserDefaults]setObject:@"no" forKey:@"isCartView"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    [self callWebServiceForGetAllProductCategories];
   
   // [self callWebServiceForGetAllProducts:@""];
    checkForPopUp = FALSE;
}
-(void)callWebServiceForGetAllProductCategories
{
   NSDictionary *params = @{
               @"shop_id":[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"restaurant_id"]],
               };
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [[SingletonClass sharedManager] m_PostApiResponse:@"fetchProductCategory" paramDiction:params islogout:NO  onCompletion:^(NSDictionary *response)
         {
             NSDictionary *responseDic = (NSDictionary *)response;
             if ([[[responseDic objectForKey:@"payload"]valueForKey:@"status"]integerValue]==1)
             {
                 arrayForProductCategories = [[responseDic objectForKey:@"payload"] objectForKey:@"data"];
                 
                 [tableViewForCategory reloadData];
                 if (checkViewForPopUpIsHidden == true)
                 {
                     viewForCategory.hidden=NO;
                     checkViewForPopUpIsHidden = false;
                 }
             }
             else
             {
                 if (checkForPopUp != false)
                 {
                     viewForCategory.hidden=YES;
                     tableViewForShopsList.hidden=NO;
                    
                     [[AppDelegate shareInstance]showAlertWithErrorMessage:[[responseDic objectForKey:@"payload"] objectForKey:@"message"]];
                 }
                checkForPopUp = true;
            }
         }];
    });
}
-(void)callWebServiceForGetAllProducts:(NSString *)categorieId
{
    
    [[AppDelegate shareInstance]showActivityIndicator];
    NSDictionary *params;

  if (categorieIds == nil)
    {
        params = @{
                   @"shop_id":[[NSUserDefaults standardUserDefaults] valueForKey:@"restaurant_id"],
                   @"category_id":@""
                   };
    }
    else
    {
        params = @{
                   @"shop_id":[[NSUserDefaults standardUserDefaults] valueForKey:@"restaurant_id"],
                    @"category_id":categorieId
                   };
    }
    NSLog(@"%@",params);
    dispatch_async(dispatch_get_main_queue(), ^{
        [[SingletonClass sharedManager] m_PostApiResponse:@"fetchProduct" paramDiction:params  islogout:NO onCompletion:^(NSDictionary *response)
         {
             [[AppDelegate shareInstance]hideActivityIndicator];
             NSDictionary *responseDic = (NSDictionary *)response;
             if ([[[responseDic objectForKey:@"payload"]valueForKey:@"status"]integerValue]==1)
             {
                 arrayForProducts = [[responseDic objectForKey:@"payload"] objectForKey:@"data"];
                 for (int i=0; i<arrayForProducts.count; i++) {
                      [dicForTextFieldvalues setValue:@"" forKey:[NSString stringWithFormat:@"%d",i]];
                 }
                 NSLog(@"%@",dicForTextFieldvalues);
                 [tableViewForShopsList reloadData];
                 viewForCategory.hidden=YES;
             }
             else
             {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     [[AppDelegate shareInstance]hideActivityIndicator];
                     [[AppDelegate shareInstance] showAlertWithErrorMessage:[[responseDic objectForKey:@"payload"]valueForKey:@"message"]];
                     
                 });
             }
         }];
    });    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark -
#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
     if (tableView== tableViewForShopsList)
     {
         return arrayForProducts.count;
     }
     else{
         return arrayForProductCategories.count;
     }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier;
    UITableViewCell *cell ;
    if (tableView== tableViewForShopsList) {
        CellIdentifier = @"cell";
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        UILabel *lblTitle = (UILabel*)[cell viewWithTag:102];
        UILabel *lblPrice = (UILabel*)[cell viewWithTag:104];
        UILabel *lblProductUnit = (UILabel*)[cell viewWithTag:105];
        UILabel *lblVat = (UILabel*)[cell viewWithTag:202];
        UITextField *txtFld = (UITextField *)[cell viewWithTag:103];
        UIImageView *imgForShop = (UIImageView*)[cell viewWithTag:201];
        //            imgForShop.layer.cornerRadius = imgForShop.frame.size.width/2;
        //        imgForShop.layer.masksToBounds=YES;
        txtFld.text = [dicForTextFieldvalues valueForKey:[NSString stringWithFormat:@"%ld",(long)indexPath.row]];
        float sum=0.00;
        float vat=0.00;
        float orgPrice=0.00;
        NSString* cleanedString = [[[[arrayForProducts objectAtIndex:indexPath.row] valueForKey:@"meal_price"] stringByReplacingOccurrencesOfString:@"," withString:@"."]
                                   stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
        
        NSString *strTextField =[[txtFld.text stringByReplacingOccurrencesOfString:@"," withString:@"."]
                                 stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
        
        vat = ([cleanedString floatValue]*[[[arrayForProducts objectAtIndex:indexPath.row] valueForKey:@"vat_name"] floatValue])/100;
        NSString *str =@"%";
        lblVat.text=[NSString stringWithFormat:@"VAT:%@%@ %@",[[arrayForProducts objectAtIndex:indexPath.row] valueForKey:@"vat_name"],str,[[arrayForProducts objectAtIndex:indexPath.row] valueForKey:@"type"]];
        if ([[[arrayForProducts objectAtIndex:indexPath.row] valueForKey:@"type"] isEqualToString:@"Excl."]) {
            // sum = ([cleanedString floatValue]+vat)*[txtFld.text floatValue]  ;
            sum = [cleanedString floatValue]+vat;
            orgPrice =[cleanedString floatValue]+vat*1;
            NSString *roundOff = [NSString stringWithFormat:@"%.2f",sum];
            sum = [roundOff floatValue] *[strTextField floatValue];
        }
        else
        {
            sum = [strTextField floatValue] * [cleanedString floatValue];
            orgPrice =[cleanedString floatValue]*1;
        }
        NSString* strForOriginalPriceWithComma = [[[NSString stringWithFormat:@"%@: €%.2f",NSLocalizedString(@"Price", @"Cancel"),orgPrice] stringByReplacingOccurrencesOfString:@"." withString:@","]
                                                  stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
        
        lblVat.text = strForOriginalPriceWithComma;
        
        
        NSString* strForPriceWithComma = [[[NSString stringWithFormat:@"%.2f",sum] stringByReplacingOccurrencesOfString:@"." withString:@","]
                                          stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
        lblPrice.text = [NSString stringWithFormat:@"€ %@",strForPriceWithComma];
        
        lblTitle.text =[[arrayForProducts objectAtIndex:indexPath.row] valueForKey:@"meal_name_ar"];
        [imgForShop sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",baseUrlForImageProfile,[[arrayForProducts objectAtIndex:indexPath.row] valueForKey:@"meal_image"]]]
                      placeholderImage:[UIImage imageNamed:@"place-holder@1x"]];
        //        if ([[[arrayForProducts objectAtIndex:indexPath.row] valueForKey:@"product_unit"] isEqualToString:@"weight"]) {
        //            lblProductUnit.text=@"Kg";
        //        }
        //        else{
        //            lblProductUnit.text=@"Pcs";
        //        }
        lblProductUnit.text = [[arrayForProducts objectAtIndex:indexPath.row] valueForKey:@"actual_unit"];
        return cell;
    }
    else
    {
        CellIdentifier = @"Cell";
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        UIImageView *imgForCheck = (UIImageView*)[cell viewWithTag:101];
        
        UILabel *lblTitle = (UILabel*)[cell viewWithTag:102];
        lblTitle.text = [[arrayForProductCategories objectAtIndex:indexPath.row] valueForKey:@"category_name_ar"];
        if ([arrayForSelectedIndexes containsObject:[NSString stringWithFormat:@"%ld",(long)indexPath.row]])
        {
            imgForCheck.image=[UIImage imageNamed:@"green-checkbox@1x"];
        }
        else
        {
            imgForCheck.image=[UIImage imageNamed:@"check-box@1x"];
            
        }
        return cell;
    }
}

#pragma mark -
#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView ==tableViewForCategory) {
        if ([arrayForSelectedIndexes containsObject:[NSString stringWithFormat:@"%ld",(long)indexPath.row]]) {
            [arrayForSelectedIndexes removeObject:[NSString stringWithFormat:@"%ld",(long)indexPath.row]];
            [arrayForStoreCategoriesIds removeObject:[[arrayForProductCategories objectAtIndex:indexPath.row] valueForKey:@"category_id"]];
        }
        else
        {
            [arrayForSelectedIndexes addObject:[NSString stringWithFormat:@"%ld",(long)indexPath.row]];
            [arrayForStoreCategoriesIds addObject:[[arrayForProductCategories objectAtIndex:indexPath.row] valueForKey:@"category_id"]];
        }
        [tableView reloadData];
        
    }
    else{
        ProductDetailViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"ProductDetailViewController"];
        newView.arrayForProductInfo = [arrayForProducts objectAtIndex:indexPath.row];
        [self.navigationController pushViewController:newView animated:YES];
    }
    
}



- (IBAction)btnCategoryAction:(id)sender
{
    // viewForCategory.hidden=NO;
    if (arrayForProductCategories.count)
    {
        viewForCategory.hidden =NO;
        [tableViewForCategory reloadData];
    }
    else
    {
        checkViewForPopUpIsHidden=true;
        [self callWebServiceForGetAllProductCategories];
    }
    
}

- (IBAction)BtnOkAction:(id)sender
{
    viewForCategory.hidden=YES;
    tableViewForShopsList.hidden=NO;
    if (arrayForStoreCategoriesIds.count)
    {
        categorieIds= @"";
        categorieIds= [arrayForStoreCategoriesIds componentsJoinedByString:@","];
    }
    else
    {
        categorieIds= @"";
    }
    [self callWebServiceForGetAllProducts:categorieIds];
}

- (IBAction)btnAddToCartAction:(id)sender
{
    UITableViewCell* cell = (UITableViewCell*)[sender superview];
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:tableViewForShopsList];
    NSIndexPath *indexPath = [tableViewForShopsList indexPathForRowAtPoint:buttonPosition];
    UILabel *lblPrice = (UILabel*)[cell viewWithTag:104];
    UITextField *txtFld = (UITextField *)[cell viewWithTag:103];
    [txtFld resignFirstResponder];
    //    float sum=0.00;
    //    float vat=0.00;
    //    NSString* cleanedString = [[[[arrayForProducts objectAtIndex:indexPath.row] valueForKey:@"meal_price"] stringByReplacingOccurrencesOfString:@"," withString:@"."]
    //                               stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
    //
    //    NSString *strTextField =[[txtFld.text stringByReplacingOccurrencesOfString:@"," withString:@"."]
    //                             stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
    //
    //    vat = ([cleanedString floatValue]*[[[arrayForProducts objectAtIndex:indexPath.row] valueForKey:@"vat_name"] floatValue])/100;
    //
    //    NSLog(@"%f",vat);
    //
    //    if ([[[arrayForProducts objectAtIndex:indexPath.row] valueForKey:@"type"] isEqualToString:@"Excl."]) {
    //        // sum = ([cleanedString floatValue]+vat)*[txtFld.text floatValue]  ;
    //        sum = [cleanedString floatValue]+vat;
    //        NSString *roundOff = [NSString stringWithFormat:@"%.2f",sum];
    //    }
    //    else
    //    {
    //        sum = [cleanedString floatValue];
    //    }
    //
    //
    //
    //
    //    if (![lblPrice.text isEqualToString:@"€ 0,00"]) {
    //        [[CustomClasssesForCoreData sharedManagerForCoreData] methodForSaveValues:[[arrayForProducts objectAtIndex:indexPath.row] copy] price:lblPrice.text quantity:txtFld.text singlePrice:[NSString stringWithFormat:@"%.2f",sum] onCompletion:^(NSArray *response){
    //            NSLog(@"%lu",(unsigned long)response.count);
    //        }];
    //        [self methodForGetTotalPrice];
    //    }
    //    else
    //    {
    //
    //        [[AppDelegate shareInstance] showAlertWithErrorMessage: NSLocalizedString(@"Price should not be zero", @"Cancel")];
    //
    //    }
    
    
    
}

-(void)methodForGetTotalPrice
{
    [self.view endEditing:YES];
    [[CustomClasssesForCoreData sharedManagerForCoreData] methodForFetchValues:nil onCompletion:^(NSArray *response)
     {
         NSLog(@"%@",response);
         arrayForCoreDatavalues = [response mutableCopy];
    }];
    NSString *count = [NSString stringWithFormat:@"%lu",(unsigned long)arrayForCoreDatavalues.count];
    [[NSUserDefaults standardUserDefaults]setObject:count forKey:@"cartCount"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"cartCount"]!=nil) {
        lblCount.text=[[NSUserDefaults standardUserDefaults] valueForKey:@"cartCount"];
    }
    else
    {
        lblCount.text=@"";
    }
}

- (IBAction)btnMenuAction:(id)sender
{
    [self.menuContainerViewController toggleLeftSideMenuCompletion:nil];

}

- (IBAction)btnHomeAction:(id)sender
{
   
    ViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
    [self.navigationController pushViewController:newView animated:YES];
}

- (IBAction)btnProfileAction:(id)sender
{
    if ( [[[NSUserDefaults standardUserDefaults] objectForKey:@"userData"] count])
    {
        UserProfileViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"UserProfileViewController"];
        [self.navigationController pushViewController:newView animated:YES];
    }
    else
    {
        LoginRegistrationViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginRegistrationViewController"];
        [self.navigationController pushViewController:newView animated:YES];
    }
    
}


#pragma UITextField delegate
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
}
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [tableViewForShopsList reloadData];
    [self performSelector:@selector(addToCart:) withObject:textField afterDelay:0.5];
}
-(void) addToCart:(UITextField *) textField
{
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"is_recipe"];
    UITableViewCell* cell = (UITableViewCell*)textField.superview.superview;
    CGPoint buttonPosition = [textField.superview.superview convertPoint:CGPointZero toView:tableViewForShopsList];
    NSIndexPath *indexPath = [tableViewForShopsList indexPathForRowAtPoint:buttonPosition];
    UILabel *lblPrice = (UILabel*)[cell viewWithTag:104];
    UITextField *txtFld = (UITextField *)[cell viewWithTag:103];
    float sum=0.00;
    float vat=0.00;
    NSString *type=[[arrayForProducts objectAtIndex:indexPath.row] valueForKey:@"type"];
    NSString* cleanedString = [[[[arrayForProducts objectAtIndex:indexPath.row] valueForKey:@"meal_price"] stringByReplacingOccurrencesOfString:@"," withString:@"."]
                               stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
    
    NSString *strTextField =[[txtFld.text stringByReplacingOccurrencesOfString:@"," withString:@"."]
                             stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
    
    vat = ([cleanedString floatValue]*[[[arrayForProducts objectAtIndex:indexPath.row] valueForKey:@"vat_name"] floatValue])/100;
    
    NSLog(@"%f",vat);
    
    if ([[[arrayForProducts objectAtIndex:indexPath.row] valueForKey:@"type"] isEqualToString:@"Excl."]) {
        // sum = ([cleanedString floatValue]+vat)*[txtFld.text floatValue]  ;
        sum = [cleanedString floatValue]+vat;
        NSString *roundOff = [NSString stringWithFormat:@"%.2f",sum];
    }
    else
    {
        sum = [cleanedString floatValue];
    }
    
    
    
    
    if (![lblPrice.text isEqualToString:@"€ 0,00"]) {
        [[CustomClasssesForCoreData sharedManagerForCoreData] methodForSaveValues:[[arrayForProducts objectAtIndex:indexPath.row] copy] price:lblPrice.text quantity:txtFld.text singlePrice:[NSString stringWithFormat:@"%.2f",sum] type:type onCompletion:^(NSArray *response){
            NSLog(@"%lu",(unsigned long)response.count);
        }];
        [self methodForGetTotalPrice];
    }
    else
    {
        
        [[AppDelegate shareInstance] showAlertWithErrorMessage: NSLocalizedString(@"Price should not be zero", @"Cancel")];
        
    }
    
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    CGPoint origin = textField.frame.origin;
    CGPoint point = [textField.superview convertPoint:origin toView:tableViewForShopsList];
    newIndexPath = [tableViewForShopsList indexPathForRowAtPoint:point];
    
   // [tableViewForShopsList setContentOffset:CGPointMake(0, point.y+100)];
    
//    [tableViewForShopsList scrollToRowAtIndexPath:newIndexPath
//                         atScrollPosition:UITableViewScrollPositionNone
//                                 animated:NO];
    
    if ([[[arrayForProducts objectAtIndex:newIndexPath.row] valueForKey:@"product_unit"] isEqualToString:@"weight"])
    {
        if ([[[arrayForProducts objectAtIndex:newIndexPath.row] valueForKey:@"actual_unit"] isEqualToString:@"per stuk"] || [[[arrayForProducts objectAtIndex:newIndexPath.row] valueForKey:@"actual_unit"] isEqualToString:@"per zak"] || [[[arrayForProducts objectAtIndex:newIndexPath.row] valueForKey:@"actual_unit"] isEqualToString:@"per doos"])
        {
            if ([self checkForValidCharacterForWithout:string]) {
                
                string = [[string stringByReplacingOccurrencesOfString:@"." withString:@","]
                          stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
                
                NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
                newString =     [newString stringByTrimmingCharactersInSet:
                                 [NSCharacterSet whitespaceCharacterSet]];
                
                [dicForTextFieldvalues setObject:newString forKey:[NSString stringWithFormat:@"%ld",(long)newIndexPath.row]];
                
                //[tableViewForShopsList reloadData];
                
                double delayInSeconds = 0.1;
                dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
                dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                    [textField becomeFirstResponder];
                });
                return true;
            }
            else{
                return false;
            }
        }
        else
        {
            if ([self checkForValidCharacter:string]) {
                
                string = [[string stringByReplacingOccurrencesOfString:@"." withString:@","]
                          stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
                
                NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
                newString =     [newString stringByTrimmingCharactersInSet:
                                 [NSCharacterSet whitespaceCharacterSet]];
                
                [dicForTextFieldvalues setObject:newString forKey:[NSString stringWithFormat:@"%ld",(long)newIndexPath.row]];
                
                //[tableViewForShopsList reloadData];
                
                double delayInSeconds = 0.1;
                dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
                dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                    [textField becomeFirstResponder];
                });
                return true;
            }
            else{
                return false;
            }
        }
    }
    else
    {
        if ([self checkForValidCharacterForWithout:string]) {
            
            string = [[string stringByReplacingOccurrencesOfString:@"." withString:@","]
                      stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
            
            NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
            newString =     [newString stringByTrimmingCharactersInSet:
                             [NSCharacterSet whitespaceCharacterSet]];
            
            [dicForTextFieldvalues setObject:newString forKey:[NSString stringWithFormat:@"%ld",(long)newIndexPath.row]];
            
           // [tableViewForShopsList reloadData];
            
            double delayInSeconds = 0.1;
            dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
            dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                [textField becomeFirstResponder];
            });
            return true;
        }
        else
        {
            return false;
        }

    }
}


-(BOOL)checkForValidCharacter:(NSString*)str
{
   NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:ACCEPTABLE_CHARACTERS] invertedSet];
   NSString *filtered = [[str componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
    if ([str isEqualToString:filtered])
    {
        return true;
    }
    else
    {
        return false;
    }
}
-(BOOL)checkForValidCharacterForWithout:(NSString*)str
{
    NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:ACCEPTABLE_CHARACTERSWITHOUT] invertedSet];
    NSString *filtered = [[str componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
    if ([str isEqualToString:filtered])
    {
        return true;
    }
    else
    {
        return false;
    }
}
- (IBAction)btnBottomBackAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnBottomCartAction:(id)sender
{
    CartViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"CartViewController"];
    [self.navigationController pushViewController:newView animated:YES];
}

- (IBAction)btnBottomHomeAction:(id)sender
{
    ViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
    [self.navigationController pushViewController:newView animated:YES];
}


@end
