//
//  CheckoutViewController.h
//  Thuis today
//
//  Created by IMMANENT on 23/02/17.
//  Copyright © 2017 IMMANENT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MFSideMenu.h"

@interface CheckoutViewController : UIViewController
{
    
     IBOutlet UIDatePicker *datePicker;
    
     IBOutlet UIView *viewForDatePicker;
    
     IBOutlet UIToolbar *toolBarForDatePicker;
    
    
     IBOutlet UIButton *btnDuringOpeningHoursOutlet;
    
    
     IBOutlet UIButton *btnDuringWeekDaysOutlet;
    
     IBOutlet UIButton *
    btnPickUpAtLocation;
    __weak IBOutlet UITextField *txtCity;
    
    
    __weak IBOutlet UITextField *txtName;
     IBOutlet UIButton *btnCashOnDeliveryAction;
    
    __weak IBOutlet UITableView *tblSearch;
    
     IBOutlet UIButton *btnPaypalOutlet;
     IBOutlet UIButton *btnIdealOutlet;
    
     IBOutlet UIButton *btnOutletAddMore;
    
     IBOutlet UIButton *btnReverseOutlet;
     IBOutlet UIButton *btnProceedOutlet;
    
     IBOutlet UITableView *tblFields;
    
    
     IBOutlet UILabel *lblCount;
    
    IBOutlet UIPickerView *pickerFortime;
    
    IBOutlet UIView *viewForTime;
    
    IBOutlet UILabel *lblPrice;
    
     IBOutlet UILabel *lblDeliveryCharges;
    
     IBOutlet UILabel *lblServiceCharges;
    
     IBOutlet UIView *ViewForChangeAddress;
    
     IBOutlet UITableView *tblForAddressList;
     IBOutlet UITextField *txtFldForDeliveryAddress;
    
     IBOutlet UITextField *txtPostCode;
    
     IBOutlet UILabel *lblChoosePrevious;
}
@property (nonatomic) NSUInteger maxPreparationTime;

@property NSString *strForTotal;
@property NSString *strForCart;

- (IBAction)btnCancelAction:(id)sender;
- (IBAction)btnDoneAction:(id)sender;

- (IBAction)btnCartAction:(id)sender;
- (IBAction)btnMenuAction:(id)sender;

- (IBAction)btnActionDuringOpeningHours:(id)sender;
- (IBAction)btnDuringWeekDays:(id)sender;
- (IBAction)btnActionPickUpAtLocation:(id)sender;

- (IBAction)btnCashOnDeliveryAction:(id)sender;
- (IBAction)btnIdealAction:(id)sender;
- (IBAction)btnPaypalAction:(id)sender;

- (IBAction)btnAddMoreAction:(id)sender;
- (IBAction)btnProceedAction:(id)sender;
- (IBAction)btnReverseCart:(id)sender;

- (IBAction)btnDoneTimeAction:(id)sender;
- (IBAction)btnCancelTimeAction:(id)sender;
- (IBAction)btnActionForChangeAddress:(id)sender;

- (IBAction)btnCrossAction:(id)sender;
- (IBAction)btnUpdateAddress:(id)sender;


@end
