//
//  CheckoutViewController.m
//  Thuis today
//
//  Created by IMMANENT on 23/02/17.
//  Copyright © 2017 IMMANENT. All rights reserved.
//

#import "CheckoutViewController.h"

@interface CheckoutViewController ()<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>
{

    BOOL checkForDateMode,checkForFields,checkForAddress;
    NSMutableArray *arrayForPlaceholder,*arrayForHideFields,*arrayForuserData,*arrayForShopData,*arrayForTimeSlots,*arrayForAddress , *arraySearchData;
    NSMutableDictionary *dicForTextFieldvalues;
    NSIndexPath *newIndexPath;
    NSString *strForDate,*strForTime;
    NSArray *arrayForCoreDatavalues;
    float sum,deliveryRate;
    NSMutableDictionary *dicForCartInfo,*dicForFinalJson;
    NSMutableArray *arrayForCart;
    NSString *jsonString,*paymentMethod,*strAgentId,*strPostCode,*strShopType;
    NSArray *newArray;
    NSString *idOfTime;
    
}
@property (nonatomic,strong) NSMutableDictionary * dict_postalCodeDetails;
@property (weak, nonatomic) IBOutlet UITextField *txtFld_postCode;
@property (weak, nonatomic) IBOutlet UITextField *txtFld_StreetCode;
@property (weak, nonatomic) IBOutlet UITextField *txtFld_HouseCode;
@property (weak, nonatomic) IBOutlet UILabel *lbl_hiphenHouseCode;

@property (nonatomic) float serviceCharge;
@end

@implementation CheckoutViewController
@synthesize serviceCharge = _serviceCharge;

- (float)serviceCharge {
    NSString *str_serviceCharge = [[NSUserDefaults standardUserDefaults] valueForKey:kService_Charge];
    _serviceCharge = [[[str_serviceCharge stringByReplacingOccurrencesOfString:@"," withString:@"."]
                      stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]] floatValue];
    return _serviceCharge;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    [CommonMethods configureNavigationBarForViewController:self withTitle:@"Checkout"];
    viewForDatePicker.hidden =YES;
    viewForTime.hidden =YES;

    [self datepicker];
    arrayForPlaceholder = [[NSMutableArray alloc]initWithObjects:@"Name",@"Address",@"Postcode",@"Phone number",@"city",@"Email", nil];
     arrayForHideFields = [[NSMutableArray alloc]initWithObjects:@"Name",@"Address",@"Email", nil];
    arrayForuserData = [[NSMutableArray alloc]init];
    dicForTextFieldvalues = [[NSMutableDictionary alloc]init];
    arrayForShopData = [[NSMutableArray alloc]init];
    arraySearchData = [[NSMutableArray alloc]init];
    sum=0;
    arrayForCart = [[NSMutableArray alloc]init];
    dicForFinalJson = [[NSMutableDictionary alloc]init];
    arrayForTimeSlots = [[NSMutableArray alloc]init];
    arrayForAddress = [[NSMutableArray alloc]init];
    
  
    NSString *strServiceCharge=NSLocalizedString(@"Service charges", @"Cancel");
    
    NSString *strForServiceCharge = [[[NSString stringWithFormat:@"%@: %.2f Cent",strServiceCharge,self.serviceCharge] stringByReplacingOccurrencesOfString:@"." withString:@","]
                                     stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
    lblServiceCharges.text=strForServiceCharge;

    [self methodForFetchValuesFromCoreData];
    paymentMethod= @"CashOnDelivery";
    
     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(pushNotificationReceived) name:@"pushNotification" object:nil];
     [txtPostCode addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    
    if ( IDIOM == IPAD ) {
        tblSearch.frame = CGRectMake(tblSearch.frame.origin.x, tblSearch.frame.origin.y, tblSearch.frame.size.width, tblSearch.frame.size.height+100);
    }
    
}
-(void)pushNotificationReceived{
    
    if ( [[[NSUserDefaults standardUserDefaults] objectForKey:@"userData"] count])
    {
        UserProfileViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"UserProfileViewController"];
        [self.navigationController pushViewController:newView animated:YES];
    }
    else
    {
        LoginRegistrationViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginRegistrationViewController"];
        [self.navigationController pushViewController:newView animated:YES];
    }
    
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    self.dict_postalCodeDetails = [[NSMutableDictionary alloc] init];
    NSLog(@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"userData"]);
    
    arrayForuserData = [[NSUserDefaults standardUserDefaults] objectForKey:@"userData"];
    
    [dicForTextFieldvalues setObject:[arrayForuserData valueForKey:@"address"] forKey:@"address"];
     [dicForTextFieldvalues setObject:[arrayForuserData valueForKey:@"zipcode"] forKey:@"zipcode"];
    [dicForTextFieldvalues setObject:[arrayForuserData valueForKey:@"city"] forKey:@"city"];
    [dicForTextFieldvalues setObject:[arrayForuserData valueForKey:@"email"] forKey:@"email"];
    [dicForTextFieldvalues setObject:[arrayForuserData valueForKey:@"name"] forKey:@"name"];
   
    [dicForTextFieldvalues setObject:[arrayForuserData valueForKey:@"phone"] forKey:@"phone"];
    [self methodForCheckDelivery];

//    txtPostCode.text = [NSString stringWithFormat:@"%@",[arrayForuserData valueForKey:@"zipcode"]];
    [self hideHouseCodePart:YES];

    NSArray *arr = [[arrayForuserData valueForKey:@"zipcode"] componentsSeparatedByString:@"-"];
    if (arr.count == 2) {
        self.dict_postalCodeDetails = [NSMutableDictionary dictionaryWithDictionary: @{kPostalCode : [arr firstObject],kStreetCode : [arr objectAtIndex:1], kHouseNumber : @""}];
    }
    else if (arr.count == 3) {
        self.dict_postalCodeDetails = [NSMutableDictionary dictionaryWithDictionary:@{kPostalCode : [arr firstObject],kStreetCode : [arr objectAtIndex:1], kHouseNumber : [arr lastObject] }];
    }
    
    self.txtFld_postCode.text = [self.dict_postalCodeDetails valueForKey:kPostalCode];
    self.txtFld_StreetCode.text = [self.dict_postalCodeDetails valueForKey:kStreetCode];
    txtFldForDeliveryAddress.text = [NSString stringWithFormat:@"%@",[arrayForuserData valueForKey:@"address"]];
    txtName.text = [NSString stringWithFormat:@"%@",[arrayForuserData valueForKey:@"name"]];
    txtCity.text = [NSString stringWithFormat:@"%@",[arrayForuserData valueForKey:@"city"]];

    //[btnDuringOpeningHoursOutlet setBackgroundImage:[UIImage imageNamed:@"org-btn@1x"] forState:UIControlStateNormal];
    [btnDuringWeekDaysOutlet setBackgroundImage:[UIImage imageNamed:@"white-btn@1x"] forState:UIControlStateNormal];
    [btnPickUpAtLocation setBackgroundImage:[UIImage imageNamed:@"white-btn@1x"] forState:UIControlStateNormal];
    [btnCashOnDeliveryAction setBackgroundImage:[UIImage imageNamed:@"cash-icon@1x"] forState:UIControlStateNormal];
    [btnIdealOutlet setBackgroundImage:[UIImage imageNamed:@"ideal-uncheck@1x"] forState:UIControlStateNormal];
    [btnPaypalOutlet setBackgroundImage:[UIImage imageNamed:@"paypal-uncheck@1x"] forState:UIControlStateNormal];
    NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:@"restaurantData"];
    arrayForShopData = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    NSLog(@"%@",[arrayForShopData valueForKey:@"delivery_charges"]);
    if ([[arrayForShopData valueForKey:@"is_combination_shop"] integerValue]==1)
    {
        deliveryRate=[[[arrayForShopData valueForKey:@"delivery_charge"] stringByReplacingOccurrencesOfString:@"," withString:@"."] floatValue];
        btnPickUpAtLocation.hidden = YES;
        btnCashOnDeliveryAction.hidden =YES;
        [btnCashOnDeliveryAction setBackgroundImage:[UIImage imageNamed:@"cash-on-dilivery-uncheck@1x"] forState:UIControlStateNormal];
        [btnIdealOutlet setBackgroundImage:[UIImage imageNamed:@"deal-icon@1x"] forState:UIControlStateNormal];
        [btnPaypalOutlet setBackgroundImage:[UIImage imageNamed:@"paypal-uncheck@1x"] forState:UIControlStateNormal];
      //  serviceCharge = 0.25;
        paymentMethod= @"Ideal";
        NSString *str =NSLocalizedString(@"Checkout", @"Cancel");
        NSString *strTotal =NSLocalizedString(@"total", @"Cancel");
        NSString* cleanedString = [[[NSString stringWithFormat:@"%@ (%@: %.2f )",str,strTotal,[[[NSUserDefaults standardUserDefaults] valueForKey:@"totalPrice"] floatValue]+self.serviceCharge+deliveryRate] stringByReplacingOccurrencesOfString:@"." withString:@","]
                                   stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
        NSString *strServiceCharge=NSLocalizedString(@"Service charges", @"Cancel");
        
        NSString *strForServiceCharge = [[[NSString stringWithFormat:@"%@: %.2f Cent",strServiceCharge,self.serviceCharge] stringByReplacingOccurrencesOfString:@"." withString:@","]
                                         stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
        lblServiceCharges.text=strForServiceCharge;
        
        lblPrice.text =cleanedString;
    }
    else if ([[NSUserDefaults standardUserDefaults] boolForKey:@"is_recipe"])
    {
        NSLog(@"%@",[[[NSUserDefaults standardUserDefaults] valueForKey:@"delivery_charge"] stringByReplacingOccurrencesOfString:@"," withString:@"."]);
        deliveryRate=[[[[NSUserDefaults standardUserDefaults] valueForKey:@"delivery_charge"] stringByReplacingOccurrencesOfString:@"," withString:@"."] floatValue];
        btnPickUpAtLocation.hidden = NO;
        btnCashOnDeliveryAction.hidden =YES;
        [btnCashOnDeliveryAction setBackgroundImage:[UIImage imageNamed:@"cash-on-dilivery-uncheck@1x"] forState:UIControlStateNormal];
        [btnIdealOutlet setBackgroundImage:[UIImage imageNamed:@"deal-icon@1x"] forState:UIControlStateNormal];
        [btnPaypalOutlet setBackgroundImage:[UIImage imageNamed:@"paypal-uncheck@1x"] forState:UIControlStateNormal];
       // serviceCharge = 0.25;
        paymentMethod= @"Ideal";
        NSString *str =NSLocalizedString(@"Checkout", @"Cancel");
        NSString *strTotal =NSLocalizedString(@"total", @"Cancel");
        NSString* cleanedString = [[[NSString stringWithFormat:@"%@ (%@: %.2f )",str,strTotal,[[[NSUserDefaults standardUserDefaults] valueForKey:@"totalPrice"] floatValue]+self.serviceCharge+deliveryRate] stringByReplacingOccurrencesOfString:@"." withString:@","]
                                   stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
        NSString *strServiceCharge=NSLocalizedString(@"Service charges", @"Cancel");
        
        NSString *strForServiceCharge = [[[NSString stringWithFormat:@"%@: %.2f Cent",strServiceCharge,self.serviceCharge] stringByReplacingOccurrencesOfString:@"." withString:@","]
                                         stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
        lblServiceCharges.text=strForServiceCharge;
        
        lblPrice.text =cleanedString;
    }
   else
   {
        deliveryRate=[[[arrayForShopData valueForKey:@"delivery_charges_opening_hour"] stringByReplacingOccurrencesOfString:@"," withString:@"."] floatValue];
        btnPickUpAtLocation.hidden = NO;
        btnCashOnDeliveryAction.hidden =NO;
   }
   
    
    NSString *strDeliveryCharges=NSLocalizedString(@"Delivery charges", @"Cancel");
    NSString *strForDeliveryCharge = [[[NSString stringWithFormat:@"%@:%.2f",strDeliveryCharges,deliveryRate] stringByReplacingOccurrencesOfString:@"." withString:@","]
                                      stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
    lblDeliveryCharges.text=strForDeliveryCharge;

    strForTime =NSLocalizedString(@"As soon as possible", @"Cancel");
    strForDate =NSLocalizedString(@"As soon as possible", @"Cancel");
    lblCount.layer.cornerRadius = lblCount.frame.size.width/2;
    lblCount.layer.masksToBounds=YES;
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"cartCount"]!=nil) {
        lblCount.text=[[NSUserDefaults standardUserDefaults] valueForKey:@"cartCount"];
    }
    else
    {
        lblCount.text=@"";
    }
    NSString *str =NSLocalizedString(@"Checkout", @"Cancel");
        NSString *strTotal =NSLocalizedString(@"total", @"Cancel");
    NSString* cleanedString = [[[NSString stringWithFormat:@"%@ (%@: %.2f )",str,strTotal,[[[NSUserDefaults standardUserDefaults] valueForKey:@"totalPrice"] floatValue]+self.serviceCharge+deliveryRate] stringByReplacingOccurrencesOfString:@"." withString:@","]
                               stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
    lblPrice.text =cleanedString;
    ViewForChangeAddress.hidden = YES;
    tblForAddressList.hidden = YES;
}

-(void)methodForGetTotalPrice
{
    for (int i=0; i<arrayForCoreDatavalues.count; i++) {
        NSManagedObject *device = [arrayForCoreDatavalues objectAtIndex:i];
        NSString* cleanedString = [[[device valueForKey:@"product_price"] stringByReplacingOccurrencesOfString:@"€." withString:@""]
                                   stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
        NSLog(@"%@",cleanedString);
        sum = sum + [cleanedString floatValue];
        dicForCartInfo =[[NSMutableDictionary alloc]init];
        
        [dicForCartInfo setObject:[device valueForKey:@"product_id"] forKey:@"id"];
        [dicForCartInfo setObject:[device valueForKey:@"product_name"] forKey:@"name"];
        [dicForCartInfo setObject:cleanedString forKey:@"price"];
        [dicForCartInfo setObject:[device valueForKey:@"image_name"] forKey:@"coupon"];
        [dicForCartInfo setObject:[device valueForKey:@"type"] forKey:@"type"];
        [dicForCartInfo setObject:[device valueForKey:@"vat"] forKey:@"vat_name"];
        [dicForCartInfo setObject:cleanedString forKey:@"subtotal"];
        [dicForCartInfo setObject:[device valueForKey:@"single_price"] forKey:@"originalprice"];
        [dicForCartInfo setObject:[device valueForKey:@"product_id"] forKey:@"rowid"];
        [dicForCartInfo setObject:[device valueForKey:@"product_quantity"] forKey:@"qty"];
        [dicForCartInfo setObject:[device valueForKey:@"shop_id"] forKey:@"shopid"];
        [dicForCartInfo setObject:[device valueForKey:@"price_vat"] forKey:@"meal_price"];
        [dicForCartInfo setObject:[device valueForKey:@"product_unit"] forKey:@"itemWeightUnit"];
        [dicForCartInfo setObject:cleanedString forKey:@"itemPrice"];
        [arrayForCart addObject:dicForCartInfo];
    }
    
    for (int i = 0; i<arrayForCart.count; i++) {
        [dicForFinalJson setObject:[arrayForCart objectAtIndex:i] forKey:[NSString stringWithFormat:@"%@",[[arrayForCart objectAtIndex:i] valueForKey:@"id"]]];
    }
    NSError *error;
    NSData *jsonData2 = [NSJSONSerialization dataWithJSONObject:dicForFinalJson options:NSJSONWritingPrettyPrinted error:&error];
   jsonString = [[NSString alloc] initWithData:jsonData2 encoding:NSUTF8StringEncoding];
    NSLog(@"jsonData as string:\n%@", jsonString);
    
}

-(void)methodForFetchValuesFromCoreData
{
    
    [[CustomClasssesForCoreData sharedManagerForCoreData] methodForFetchValues:nil onCompletion:^(NSArray *response)
     {
         NSLog(@"%@",response);
         arrayForCoreDatavalues = response;
     }];
    NSString *count = [NSString stringWithFormat:@"%lu",(unsigned long)arrayForCoreDatavalues.count];
    [[NSUserDefaults standardUserDefaults]setObject:count forKey:@"cartCount"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"cartCount"]!=nil)
    {
        lblCount.text=[[NSUserDefaults standardUserDefaults] valueForKey:@"cartCount"];
    }
    else
    {
        lblCount.text=@"";
    }
    
    [self methodForGetTotalPrice];
}

-(void)datepicker
{
    if (checkForDateMode == false)
    {
        datePicker.datePickerMode = UIDatePickerModeDate;
        datePicker.minimumDate = [NSDate date];
        NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"nl_NL"];
        [datePicker setLocale:locale];
        [datePicker setValue:[UIColor blackColor] forKey:@"textColor"];

        datePicker.backgroundColor =[UIColor colorWithRed:252/255.0f green:128/255.0f blue:62/255.0f alpha:1];
    }
    else{
        datePicker.datePickerMode = UIDatePickerModeTime;
        datePicker.minimumDate = [NSDate date];
        datePicker.minuteInterval=30;
        NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"nl_NL"];
        [datePicker setLocale:locale];
        [datePicker setValue:[UIColor blackColor] forKey:@"textColor"];

        datePicker.backgroundColor =[UIColor colorWithRed:252/255.0f green:128/255.0f blue:62/255.0f alpha:1];
    }
   
}

#pragma mark -
#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == tblForAddressList) {
        return arrayForAddress.count;
    }
    else if(tableView == tblSearch)
    {
        return [arraySearchData count];
    }
    else
    {
        if (checkForFields ==false) {
            return 9;
        }
        else
        {
            return 6;
        }
    }
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == tblFields) {
        static NSString *CellIdentifier;
        UITableViewCell *cell ;
        if (indexPath.row ==0)
        {
            
            CellIdentifier = @"cell1";
            cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            UILabel *lblDate = (UILabel*)[cell viewWithTag:101];
            lblDate.text =strForDate;
            return cell;
        }
        else if(indexPath.row ==1){
            CellIdentifier = @"cell2";
            cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            UILabel *lblDate = (UILabel*)[cell viewWithTag:201];
            lblDate.text =strForTime;
            
            return cell;
        }
        else if(indexPath.row ==2){
            CellIdentifier = @"cell4";
            cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            UILabel *lblDate = (UILabel*)[cell viewWithTag:201];
            lblDate.text =strForTime;
            
            return cell;
        }
        else
        {
            CellIdentifier = @"cell3";
            cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            UITextField *txtField = (UITextField*)[cell viewWithTag:101];
            txtField.placeholder =[arrayForPlaceholder objectAtIndex:indexPath.row-3];
            
            // UIColor *color = [UIColor colorWithRed:210/255.0f green:45/255.0f blue:62/255.0f alpha:1];
            UIColor *color = [UIColor lightGrayColor];
            
            [txtField setValue:color forKeyPath:@"_placeholderLabel.textColor"];
            if (checkForFields == false) {
                txtField.placeholder =[arrayForPlaceholder objectAtIndex:indexPath.row-3];
                
                if (indexPath.row ==3) {
                    txtField.text =[dicForTextFieldvalues valueForKey:@"name"];
                }
                else if(indexPath.row ==4)
                {
                    txtField.text = [dicForTextFieldvalues valueForKey:@"address"];
                }
                else if(indexPath.row ==5)
                {
                    txtField.text = [dicForTextFieldvalues valueForKey:@"zipcode"];
                }
                else if(indexPath.row ==6)
                {
                    txtField.text = [dicForTextFieldvalues valueForKey:@"phone"];
                }
                else if(indexPath.row ==7)
                {
                    txtField.text = [dicForTextFieldvalues valueForKey:@"city"];
                }
                else if(indexPath.row ==8)
                {
                    txtField.text = [dicForTextFieldvalues valueForKey:@"email"];
                }
                // txtField.text =[dicForTextFieldvalues valueForKey:[NSString stringWithFormat:@"%ld",(long)indexPath.row]];
            }
            else
            {
                txtField.placeholder =[arrayForHideFields objectAtIndex:indexPath.row-3];
                if (indexPath.row ==3) {
                    txtField.text =[dicForTextFieldvalues valueForKey:@"name"];
                }
                else if(indexPath.row ==4)
                {
                    txtField.text = [dicForTextFieldvalues valueForKey:@"phone"];
                }
                else if(indexPath.row ==5)
                {
                    txtField.text = [dicForTextFieldvalues valueForKey:@"email"];
                }
            }
            return cell;
        }

    }
    else if (tableView == tblSearch)
    {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
        if (cell == nil)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"Cell"];
        }
        NSMutableDictionary *dict = [NSMutableDictionary new];
        dict = [arraySearchData objectAtIndex:indexPath.row];
        cell.textLabel.text =[NSString stringWithFormat:@"%@,%@",[dict objectForKey:@"straatnaam"],[dict objectForKey:@"huisnr"]];
        return cell;

    }
    else
    {
        static NSString *CellIdentifier;
        UITableViewCell *cell ;
      
            
        CellIdentifier = @"cell";
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
       UILabel *lblAddress = (UILabel*)[cell viewWithTag:101];
             UIView *viewBorder = (UIView*)[cell viewWithTag:201];
            viewBorder.layer.borderColor = [UIColor whiteColor].CGColor;
            viewBorder.layer.borderWidth = 2;
            viewBorder.layer.masksToBounds = true;
        lblAddress.text =[[arrayForAddress objectAtIndex:indexPath.row]valueForKey:@"address"];
        UILabel *lblPostcode = (UILabel*)[cell viewWithTag:102];
        lblPostcode.text =[[arrayForAddress objectAtIndex:indexPath.row]valueForKey:@"postcode"];
        return cell;

    }
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
     if (tableView == tblFields) {
         if ( IDIOM == IPAD ) {
             if (indexPath.row ==0 || indexPath.row == 1) {
                 return 110;
             }
             else if(indexPath.row == 2)
             {
                 return 60;
             }
             else{
                 return 90;
             }
         }else{
             if (indexPath.row ==0 || indexPath.row == 1) {
                 return 70;
             }
             else
             {
                 return 40;
             }
         }
     }
    else if (tableView == tblSearch)
    {
        if ( IDIOM == IPAD ) {
            return 70;
        }
        else
        return 44;
    }
    else
    {
       if ( IDIOM == IPAD )
       {
           return 140;
       }
        else
        {
            return 70;
        }
    }
}
#pragma mark -
#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == tblFields) {
        if (indexPath.row == 0) {
            checkForDateMode = false;
            [self datepicker];
            viewForDatePicker.hidden =NO;
            
        }
        else if(indexPath.row == 1)
        {
            if (![strForDate isEqualToString:NSLocalizedString(@"As soon as possible", @"Cancel")])
            {
                viewForTime.hidden =NO;
            }
            else
            {
                UIAlertController * alert=   [UIAlertController
                                              alertControllerWithTitle:nil
                                              message:@"Please select date first."
                                              preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                           {
                                               
                                           }];
                [alert addAction:okAction];
                [self presentViewController:alert animated:YES completion:nil];
            }
            
        }

    }
    else if (tableView == tblSearch)
    {
        [txtCity resignFirstResponder];
        [txtName resignFirstResponder];
        [txtPostCode resignFirstResponder];
        [txtFldForDeliveryAddress resignFirstResponder];
        [self hideHouseCodePart:YES];
        NSMutableDictionary *dict = [NSMutableDictionary new];
        dict = [arraySearchData objectAtIndex:indexPath.row];
        tblSearch.hidden = YES;
        txtPostCode.text=[NSString stringWithFormat:@"%@-%@-%@",[dict objectForKey:@"wijkcode"],[dict objectForKey:@"lettercombinatie"],[dict objectForKey:@"huisnr"]];
        txtCity.text = [dict objectForKey:@"plaatsnaam"];
        txtFldForDeliveryAddress.text = [NSString stringWithFormat:@"%@,%@",[dict objectForKey:@"straatnaam"],[dict objectForKey:@"huisnr"]];
    }
    else
    {
        
        [dicForTextFieldvalues setObject:[[arrayForAddress objectAtIndex:indexPath.row]valueForKey:@"address"] forKey:@"address"];
        [dicForTextFieldvalues setObject:[[arrayForAddress objectAtIndex:indexPath.row]valueForKey:@"postcode"]  forKey:@"zipcode"];
        NSArray *arr = [[arrayForuserData valueForKey:@"zipcode"] componentsSeparatedByString:@"-"];
        if (arr.count == 2) {
            self.dict_postalCodeDetails = [NSMutableDictionary dictionaryWithDictionary: @{kPostalCode : [arr firstObject],kStreetCode : [arr objectAtIndex:1], kHouseNumber : @""}];
        }
        else if (arr.count == 3) {
            self.dict_postalCodeDetails = [NSMutableDictionary dictionaryWithDictionary:@{kPostalCode : [arr firstObject],kStreetCode : [arr objectAtIndex:1], kHouseNumber : [arr lastObject] }];
        }
        [self hideHouseCodePart:YES];

        self.txtFld_postCode.text = [self.dict_postalCodeDetails valueForKey:kPostalCode];
        self.txtFld_StreetCode.text = [self.dict_postalCodeDetails valueForKey:kStreetCode];
        
        [tblFields reloadData];
        ViewForChangeAddress.hidden = YES;
    }
}

#pragma mark -
#pragma mark - Postal Code
- (IBAction)txtFld_PostCode_action:(UITextField *)sender {
    [self validatePostCode];
}

- (void)validatePostCode {
    if (self.txtFld_postCode.text.length >= 4) {
        if ([self checktext:self.txtFld_postCode.text forType:TextTypeNumeric]) {
            [self enableStreetCode:YES];
            [self.txtFld_StreetCode becomeFirstResponder];
        }
        else {
            [self enableStreetCode:NO];
            [self showAlertWithTitle:@"Wrong Input" message:@"Enter valid Post code. post code accepts only numbers"];
        }
    }
}

- (IBAction)txtFld_StreetCode_action:(UITextField *)sender {
    [self validateStreetCode];
}

- (void)validateStreetCode {
    if (self.txtFld_StreetCode.text.length >= 2) {
        if ([self checktext:self.txtFld_StreetCode.text forType:TextTypeAlphates]) {
            [self enableCityCode:YES];
            [self.txtFld_HouseCode becomeFirstResponder];
        }
        else {
            [self enableCityCode:NO];
            [self showAlertWithTitle:@"Wrong Input" message:@"Enter Valid street code"];
        }
    }
    else if([self.txtFld_StreetCode.text isEqualToString:@""]) {
        [self.txtFld_postCode becomeFirstResponder];
    }
}
- (IBAction)txtFld_HouseCode_action:(UITextField *)sender {
    [self validateHouseCode];
}
- (IBAction)buttonShowHouseCode:(UIButton *)sender {
    if (self.txtFld_postCode.text.length < 4) {
        [self.txtFld_postCode becomeFirstResponder];
    }
    else if (self.txtFld_StreetCode.text.length < 2) {
        [self.txtFld_StreetCode becomeFirstResponder];
    }
    else if (self.txtFld_HouseCode.text.length < 3) {
        [self.txtFld_HouseCode becomeFirstResponder];
        [self hideHouseCodePart:NO];
    }
}

- (void)validateHouseCode {
    if (self.txtFld_HouseCode.text.length >= 1) {
        if ([self checktext:self.txtFld_HouseCode.text forType:TextTypeNumeric]) {
            [self.txtFld_HouseCode resignFirstResponder];
            NSDictionary *postalCodeDetails = @{kPostalCode : self.txtFld_postCode.text, kStreetCode : self.txtFld_StreetCode.text, kHouseNumber : self.txtFld_HouseCode.text};
            [self searchAddressWithPostCodeDetails:postalCodeDetails];
            self.dict_postalCodeDetails = [NSMutableDictionary dictionaryWithDictionary:postalCodeDetails];
        }
        else {
            [self showAlertWithTitle:@"Wrong Input" message:@"Enter valid house number"];
        }
    }
    else if([self.txtFld_HouseCode.text isEqualToString:@""]) {
        [self.txtFld_HouseCode becomeFirstResponder];
    }
}

- (void)enableCityCode:(BOOL)value {
    self.txtFld_HouseCode.userInteractionEnabled = value;
    [self hideHouseCodePart:NO];
}

- (void)enableStreetCode:(BOOL)value {
    self.txtFld_StreetCode.userInteractionEnabled = value;
}

- (BOOL)checktext:(NSString *)txtFld_string forType:(TextType)textType{
    NSCharacterSet *textCharacterSet;
    if (textType == TextTypeNumeric) {
        textCharacterSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
    }
    else if (textType == TextTypeAlphates) {
        textCharacterSet = NSCharacterSet.letterCharacterSet;
    }
    NSCharacterSet *characterSetFromTextField = [NSCharacterSet characterSetWithCharactersInString:txtFld_string];
    
    BOOL stringIsValid = [textCharacterSet isSupersetOfSet:characterSetFromTextField];
    return stringIsValid;
}


- (void)searchAddressWithPostCodeDetails:(NSDictionary *)postCodeDetails {
    NSDictionary *params = @{
                             @"wijkcode":[postCodeDetails valueForKey:kPostalCode],
                             @"lettercombinatie":[postCodeDetails valueForKey:kStreetCode],
                             @"huisnr":[postCodeDetails valueForKey:kHouseNumber],
                             };
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [[SingletonClass sharedManager] m_PostAddressSuggestionApiResponse:@"delivery-agent-get-address" paramDiction:params onCompletion:^(NSDictionary *response)
         {
             NSDictionary *responseDic = (NSDictionary *)response;
             if ([[[responseDic objectForKey:@"payload"]valueForKey:@"status"]integerValue]==1)
             {
                 arraySearchData = [[responseDic objectForKey:@"payload"] objectForKey:@"data"];
                 tblSearch.hidden = NO;
                 [tblSearch reloadData];
             }
             else
             {
                 dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.6 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                     [self.view endEditing:YES];
                     [[AppDelegate shareInstance] showAlertWithErrorMessage:[[responseDic objectForKey:@"payload"] valueForKey:@"message"]];
                     tblSearch.hidden = YES;
                 });
             }
         }];
    });
}


- (void)showAlertWithTitle:(NSString *)alertTitle message:(NSString *)alertMessage {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:alertTitle message:alertMessage preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *OKBtn = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
    [alert addAction:OKBtn];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)hideHouseCodePart:(BOOL)value {
    
    self.lbl_hiphenHouseCode.hidden = value;
    self.txtFld_HouseCode.text = @"";
    self.txtFld_HouseCode.hidden = value;
    if ([[self.dict_postalCodeDetails allKeys] containsObject:kHouseNumber]) {
        [self.dict_postalCodeDetails removeObjectForKey:kHouseNumber];
    }
}


#pragma mark - UITextField delegates

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    if(textField == self.txtFld_StreetCode) {
        [self hideHouseCodePart:NO];
        if (txtFldForDeliveryAddress.text.length != 0) {
            txtFldForDeliveryAddress.text = @"";
        }
        if (txtCity.text.length != 0) {
            txtCity.text = @"";
        }
    }
    return YES;
}


-(void) textFieldDidChange:(UITextField *) theTextField
{
    if(txtPostCode.text.length < 6)
    {
        tblSearch.hidden = YES;
    }
    
    if ([txtPostCode.text rangeOfString:@"-"].location == NSNotFound)
    {
        if (txtPostCode.text.length > 6)
        {
            if (![self validPostcode1])
            {
                [[AppDelegate shareInstance] showAlertWithErrorMessage:NSLocalizedString(@"Enter Valid  Postcode.", @"Cancel")];
            }
            else
            {
                [self searchText:txtPostCode replacementString:txtPostCode.text];
            }
        }
    }
    else
    {
        if (txtPostCode.text.length > 8)
        {
            if (![self validPostcode])
            {
                [[AppDelegate shareInstance] showAlertWithErrorMessage:NSLocalizedString(@"Enter Valid  Postcode.", @"Cancel")];
            }
            else
            {
                [self searchText:txtPostCode replacementString:txtPostCode.text];
            }
        }
    }
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField != txtFldForDeliveryAddress && textField != self.txtFld_postCode && textField != self.txtFld_StreetCode && textField != self.txtFld_HouseCode) {
        CGPoint origin = textField.frame.origin;
        CGPoint point = [textField.superview convertPoint:origin toView:tblFields];
        newIndexPath = [tblFields indexPathForRowAtPoint:point];
        [textField setText:[textField.text stringByReplacingCharactersInRange:range withString:string]];
        
        if (newIndexPath.row ==3)
        {
            [dicForTextFieldvalues setObject:textField.text forKey:@"name"];
            
        }
        else if(newIndexPath.row ==4)
        {
            [dicForTextFieldvalues setObject:textField.text forKey:@"address"];
        }
        else if(newIndexPath.row ==5)
        {
            [dicForTextFieldvalues setObject:textField.text forKey:@"zipcode"];
        }
        else if(newIndexPath.row ==6)
        {
            
            if ([textField.text length] >= 10)
                textField.text = [textField.text substringToIndex:10];
            else
                textField.text = textField.text;
            [dicForTextFieldvalues setObject:textField.text forKey:@"phone"];
            if ([string rangeOfCharacterFromSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].location != NSNotFound)
            {
                return NO;
            }
            if(range.length + range.location > textField.text.length)
            {
                return NO;
            }
            
            NSUInteger newLength = [textField.text length] + [string length] - range.length;
            return newLength <=10;
        }
        else if(newIndexPath.row ==7)
        {
            [dicForTextFieldvalues setObject:textField.text forKey:@"city"];
        }
        else if(newIndexPath.row ==8)
        {
            [dicForTextFieldvalues setObject:textField.text forKey:@"email"];
        }
        [tblFields beginUpdates];
        [tblFields endUpdates];
        
        return true;

    }
    else
    {
        BOOL isValidate = NO;
        if(range.length + range.location > textField.text.length)
        {
            isValidate = NO;
        }
        
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        if(textField == self.txtFld_postCode) {
            isValidate =  (newLength <=4)?YES:NO;
            if (newLength == 5) {
                [self validatePostCode];
            }
        }
        else if (textField == self.txtFld_StreetCode) {
            isValidate =  (newLength <=2)?YES:NO;
            if (newLength == 3) {
                [self validateStreetCode];
            }
            
        }
        else if (textField == self.txtFld_HouseCode) {
            isValidate =  (newLength <=3)?YES:NO;
        }
        return isValidate;
    }
}

-(void) searchText:(UITextField *)textField replacementString:(NSString *)string
{
    NSString *tempStr = textField.text;
    NSString *postcode;
    NSString *street;
    NSString *houseNumber;
    if ([string rangeOfString:@"-"].location == NSNotFound)
    {
        if(tempStr.length == 7)
        {
            postcode = [tempStr substringWithRange:NSMakeRange(0,4)];
            street = [tempStr substringWithRange:NSMakeRange(4,2)];
            houseNumber = [tempStr substringWithRange:NSMakeRange(6,1)];
        }
        else if(tempStr.length == 8)
        {
            postcode = [tempStr substringWithRange:NSMakeRange(0,4)];
            street = [tempStr substringWithRange:NSMakeRange(4,2)];
            houseNumber = [tempStr substringWithRange:NSMakeRange(6,2)];
        }
        else if(tempStr.length == 9)
        {
            postcode = [tempStr substringWithRange:NSMakeRange(0,4)];
            street = [tempStr substringWithRange:NSMakeRange(4,2)];
            houseNumber = [tempStr substringWithRange:NSMakeRange(6,3)];
        }
        if(tempStr.length == 10)
        {
            postcode = [tempStr substringWithRange:NSMakeRange(0,4)];
            street = [tempStr substringWithRange:NSMakeRange(4,2)];
            houseNumber = [tempStr substringWithRange:NSMakeRange(6,4)];
        }
        
    }
    else
    {
        NSArray *arrTemp = [tempStr componentsSeparatedByString:@"-"];
        postcode = [arrTemp objectAtIndex:0];
        street = [arrTemp objectAtIndex:1];
        houseNumber = [arrTemp objectAtIndex:2];
    }

    
    NSDictionary *params = @{
                             @"wijkcode":postcode,
                             @"lettercombinatie":street,
                             @"huisnr":houseNumber,
                             @"shop_id":[[NSUserDefaults standardUserDefaults] valueForKey:@"restaurant_id"]
                             };
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [[SingletonClass sharedManager] m_PostAddressSuggestionApiResponse:@"delivery-agent-get-address_match" paramDiction:params onCompletion:^(NSDictionary *response)
         {
             NSDictionary *responseDic = (NSDictionary *)response;
             if ([[[responseDic objectForKey:@"payload"]valueForKey:@"status"]integerValue]==1)
             {
                 arraySearchData = [[responseDic objectForKey:@"payload"] objectForKey:@"data"];
                 tblSearch.hidden = NO;
                 [tblSearch reloadData];
             }
             else
             {
                 dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.6 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                     [self.view endEditing:YES];
                     [[AppDelegate shareInstance] showAlertWithErrorMessage:[[responseDic objectForKey:@"payload"] valueForKey:@"message"]];
                     tblSearch.hidden = YES;
                     
                     
                 });
             }
         }];
    });
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
//Method for validation for first name
-(BOOL)validPostcode
{
    NSString *emailid = txtPostCode.text;
    NSString *emailRegex = @"([0-9]{4})-[a-zA-Z]{2}-[0-9]{1,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:emailid];
}
-(BOOL)validPostcode1
{
    NSString *emailid = txtPostCode.text;
    NSString *emailRegex = @"([0-9]{4})[a-zA-Z]{2}[0-9]{1,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:emailid];
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [txtCity resignFirstResponder];
    [txtName resignFirstResponder];
    [txtPostCode resignFirstResponder];
    [txtFldForDeliveryAddress resignFirstResponder];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)btnCancelAction:(id)sender
{
    viewForDatePicker.hidden =YES;
    [tblFields reloadData];
}

- (IBAction)btnDoneAction:(id)sender
{
    viewForDatePicker.hidden =YES;
    
   // if (checkForDateMode == false)
    //{
        NSDate *chosen = [datePicker date];
        NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
        [dateformate setDateFormat:@"MM/dd/yyyy"]; // Date formater
        NSString *date = [dateformate stringFromDate:chosen]; // Convert date to string
        NSLog(@"date :%@",date);
        strForDate =date;
    [self methodForWebserviceTimeSlots];


    //}
//    else
//    {
//        NSDate *chosen = [datePicker date];
//        NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
//        [dateformate setDateFormat:@"hh:mm"]; // Date formater
//        NSString *date = [dateformate stringFromDate:chosen]; // Convert date to string
//        NSLog(@"date :%@",date);
//        strForTime =date;
//
//    }
    [tblFields reloadData];
}
#pragma mark - UIPickerDelegate and datasource methods
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)thePickerView {
    
    return 1;
}
- (NSInteger)pickerView:(UIPickerView *)thePickerView numberOfRowsInComponent:(NSInteger)component {
    
    return newArray.count;
}

- (NSAttributedString *)pickerView:(UIPickerView* )pickerView attributedTitleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    //NSString *title = [newArray objectAtIndex:row];
    NSDictionary *dict=[newArray objectAtIndex:row];
    strForTime = [dict valueForKey:@"time"];
    idOfTime=[dict valueForKey:@"id"];
    NSAttributedString *attString = [[NSAttributedString alloc] initWithString:strForTime attributes:@{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    return attString;
    
}
- (void)pickerView:(UIPickerView *)thePickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
   // txtFldForCategory.text =[arrayForCategories objectAtIndex:row];
    
}


//// tell the picker the width of each row for a given component
//- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component {
//    int sectionWidth = 300;
//    
//    return sectionWidth;
//}

- (IBAction)btnCartAction:(id)sender
{
    CartViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"CartViewController"];
    [self.navigationController pushViewController:newView animated:YES];
}

- (IBAction)btnMenuAction:(id)sender
{
    [self.menuContainerViewController toggleLeftSideMenuCompletion:nil];
}

- (IBAction)btnActionDuringOpeningHours:(id)sender
{
    //[btnDuringOpeningHoursOutlet setBackgroundImage:[UIImage imageNamed:@"org-btn@1x"] forState:UIControlStateNormal];
    [btnDuringWeekDaysOutlet setBackgroundImage:[UIImage imageNamed:@"white-btn@1x"] forState:UIControlStateNormal];
     [btnPickUpAtLocation setBackgroundImage:[UIImage imageNamed:@"white-btn@1x"] forState:UIControlStateNormal];
    checkForFields=false;
    if ([[arrayForShopData valueForKey:@"is_combination_shop"] integerValue]==1)
    {
        deliveryRate=[[[arrayForShopData valueForKey:@"delivery_charge"] stringByReplacingOccurrencesOfString:@"," withString:@"."] floatValue];
    }
    else if ([[NSUserDefaults standardUserDefaults] boolForKey:@"is_recipe"])
    {
        deliveryRate=[[[[NSUserDefaults standardUserDefaults] valueForKey:@"delivery_charge"] stringByReplacingOccurrencesOfString:@"," withString:@"."] floatValue];
    }
    else{
         deliveryRate =[[[arrayForShopData valueForKey:@"delivery_charges_opening_hour"] stringByReplacingOccurrencesOfString:@"," withString:@"."] floatValue];
    }
    
   
   // Checkout (total:E2/16)
    NSString *str =NSLocalizedString(@"Checkout", @"Cancel");
    NSString *strTotal =NSLocalizedString(@"total", @"Cancel");
    NSString* cleanedString = [[[NSString stringWithFormat:@"%@ (%@: %.2f )",str,strTotal,[[[NSUserDefaults standardUserDefaults] valueForKey:@"totalPrice"] floatValue]+self.serviceCharge+deliveryRate] stringByReplacingOccurrencesOfString:@"." withString:@","]
                               stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
    
    lblPrice.text =cleanedString;
    NSString *strDeliveryCharges=NSLocalizedString(@"Delivery charges", @"Cancel");
    NSString *strForDeliveryCharge = [[[NSString stringWithFormat:@"%@:%.2f",strDeliveryCharges,deliveryRate] stringByReplacingOccurrencesOfString:@"." withString:@","]
                                      stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
    lblDeliveryCharges.text=strForDeliveryCharge;
    [tblFields reloadData];
    
}

- (IBAction)btnDuringWeekDays:(id)sender
{
   // [btnDuringOpeningHoursOutlet setBackgroundImage:[UIImage imageNamed:@"white-btn@1x"] forState:UIControlStateNormal];
    [btnDuringWeekDaysOutlet setBackgroundImage:[UIImage imageNamed:@"org-btn@1x"] forState:UIControlStateNormal];
    [btnPickUpAtLocation setBackgroundImage:[UIImage imageNamed:@"white-btn@1x"] forState:UIControlStateNormal];
    checkForFields=false;
    if ([[arrayForShopData valueForKey:@"is_combination_shop"] integerValue]==1)
    {
        deliveryRate=[[[arrayForShopData valueForKey:@"delivery_charge"] stringByReplacingOccurrencesOfString:@"," withString:@"."] floatValue];
    }
    else if ([[NSUserDefaults standardUserDefaults] boolForKey:@"is_recipe"])
    {
        deliveryRate=[[[[NSUserDefaults standardUserDefaults] valueForKey:@"delivery_charge"] stringByReplacingOccurrencesOfString:@"," withString:@"."] floatValue];
    }
    else{
        deliveryRate =[[[arrayForShopData valueForKey:@"delivery_charges_opening_hour"] stringByReplacingOccurrencesOfString:@"," withString:@"."] floatValue];
    }
  
    NSString *str =NSLocalizedString(@"Checkout", @"Cancel");
    NSString *strTotal =NSLocalizedString(@"total", @"Cancel");
    NSString* cleanedString = [[[NSString stringWithFormat:@"%@ (%@: %.2f )",str,strTotal,[[[NSUserDefaults standardUserDefaults] valueForKey:@"totalPrice"] floatValue]+self.serviceCharge+deliveryRate] stringByReplacingOccurrencesOfString:@"." withString:@","]
                               stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
    
     lblPrice.text =cleanedString;
    NSString *strDeliveryCharges=NSLocalizedString(@"Delivery charges", @"Cancel");
    NSString *strForDeliveryCharge = [[[NSString stringWithFormat:@"%@:%.2f",strDeliveryCharges,deliveryRate] stringByReplacingOccurrencesOfString:@"." withString:@","]
                                      stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
    lblDeliveryCharges.text=strForDeliveryCharge;
    [tblFields reloadData];

}

- (IBAction)btnActionPickUpAtLocation:(id)sender {
   // [btnDuringOpeningHoursOutlet setBackgroundImage:[UIImage imageNamed:@"white-btn@1x"] forState:UIControlStateNormal];
    [btnDuringWeekDaysOutlet setBackgroundImage:[UIImage imageNamed:@"white-btn@1x"] forState:UIControlStateNormal];
    [btnPickUpAtLocation setBackgroundImage:[UIImage imageNamed:@"org-btn@1x"] forState:UIControlStateNormal];
    checkForFields=true;
     deliveryRate=0.00;
    NSString *str =NSLocalizedString(@"Checkout", @"Cancel");
    NSString *strTotal =NSLocalizedString(@"total", @"Cancel");
    NSString* cleanedString = [[[NSString stringWithFormat:@"%@ (%@: %.2f )",str,strTotal,[[[NSUserDefaults standardUserDefaults] valueForKey:@"totalPrice"] floatValue]+self.serviceCharge+deliveryRate] stringByReplacingOccurrencesOfString:@"." withString:@","]
                               stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
    lblPrice.text =cleanedString;
    NSString *strDeliveryCharges=NSLocalizedString(@"Delivery charges", @"Cancel");
    NSString *strForDeliveryCharge = [[[NSString stringWithFormat:@"%@:%.2f",strDeliveryCharges,deliveryRate] stringByReplacingOccurrencesOfString:@"." withString:@","]
                                      stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
    lblDeliveryCharges.text=strForDeliveryCharge;
    [tblFields reloadData];

}

- (IBAction)btnCashOnDeliveryAction:(id)sender
{
    [btnCashOnDeliveryAction setBackgroundImage:[UIImage imageNamed:@"cash-icon@1x"] forState:UIControlStateNormal];
        [btnIdealOutlet setBackgroundImage:[UIImage imageNamed:@"ideal-uncheck@1x"] forState:UIControlStateNormal];
    [btnPaypalOutlet setBackgroundImage:[UIImage imageNamed:@"paypal-uncheck@1x"] forState:UIControlStateNormal];
    float serviceCharge = 0.00;
      paymentMethod= @"CashOnDelivery";
    NSString *str =NSLocalizedString(@"Checkout", @"Cancel");
    NSString *strTotal =NSLocalizedString(@"total", @"Cancel");
    NSString* cleanedString = [[[NSString stringWithFormat:@"%@ (%@: %.2f )",str,strTotal,[[[NSUserDefaults standardUserDefaults] valueForKey:@"totalPrice"] floatValue]+serviceCharge+deliveryRate] stringByReplacingOccurrencesOfString:@"." withString:@","]
                               stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
    NSString *strServiceCharge=NSLocalizedString(@"Service charges", @"Cancel");
    
    NSString *strForServiceCharge = [[[NSString stringWithFormat:@"%@: %.2f Cent",strServiceCharge,serviceCharge] stringByReplacingOccurrencesOfString:@"." withString:@","]
                                     stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
    lblServiceCharges.text=strForServiceCharge;
     lblPrice.text =cleanedString;
}

- (IBAction)btnIdealAction:(id)sender
{
    [btnCashOnDeliveryAction setBackgroundImage:[UIImage imageNamed:@"cash-on-dilivery-uncheck@1x"] forState:UIControlStateNormal];
    [btnIdealOutlet setBackgroundImage:[UIImage imageNamed:@"deal-icon@1x"] forState:UIControlStateNormal];
    [btnPaypalOutlet setBackgroundImage:[UIImage imageNamed:@"paypal-uncheck@1x"] forState:UIControlStateNormal];
 //   serviceCharge = 0.25;
    paymentMethod= @"Ideal";
    NSString *str =NSLocalizedString(@"Checkout", @"Cancel");
    NSString *strTotal =NSLocalizedString(@"total", @"Cancel");
    NSString* cleanedString = [[[NSString stringWithFormat:@"%@ (%@: %.2f )",str,strTotal,[[[NSUserDefaults standardUserDefaults] valueForKey:@"totalPrice"] floatValue]+self.serviceCharge+deliveryRate] stringByReplacingOccurrencesOfString:@"." withString:@","]
                               stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
    NSString *strServiceCharge=NSLocalizedString(@"Service charges", @"Cancel");
    
    NSString *strForServiceCharge = [[[NSString stringWithFormat:@"%@: %.2f Cent",strServiceCharge,self.serviceCharge] stringByReplacingOccurrencesOfString:@"." withString:@","]
                                     stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
    lblServiceCharges.text=strForServiceCharge;

    lblPrice.text =cleanedString;
}

- (IBAction)btnPaypalAction:(id)sender {
    [btnCashOnDeliveryAction setBackgroundImage:[UIImage imageNamed:@"cash-on-dilivery-uncheck@1x"] forState:UIControlStateNormal];
    [btnIdealOutlet setBackgroundImage:[UIImage imageNamed:@"ideal-uncheck@1x"] forState:UIControlStateNormal];
    [btnPaypalOutlet setBackgroundImage:[UIImage imageNamed:@"paypal@1x"] forState:UIControlStateNormal];
   // serviceCharge = 0.30;
     paymentMethod= @"Paypal";
    NSString *str =NSLocalizedString(@"Checkout", @"Cancel");
    NSString *strTotal =NSLocalizedString(@"total", @"Cancel");
    NSString* cleanedString = [[[NSString stringWithFormat:@"%@ (%@: %.2f )",str,strTotal,[[[NSUserDefaults standardUserDefaults] valueForKey:@"totalPrice"] floatValue]+self.serviceCharge+deliveryRate] stringByReplacingOccurrencesOfString:@"." withString:@","]
                               stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
  
    
    
    NSString *strServiceCharge=NSLocalizedString(@"Service charges", @"Cancel");
    
    NSString *strForServiceCharge = [[[NSString stringWithFormat:@"%@: %.2f Cent",strServiceCharge,self.serviceCharge] stringByReplacingOccurrencesOfString:@"." withString:@","]
                                     stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
    lblServiceCharges.text=strForServiceCharge;

    lblPrice.text = cleanedString;
}

- (IBAction)btnAddMoreAction:(id)sender
{
    ShopDetailViewController*newView = [self.storyboard instantiateViewControllerWithIdentifier:@"ShopDetailViewController"];
    
    [self.navigationController pushViewController:newView animated:YES];

}

- (IBAction)btnProceedAction:(id)sender
{
    [self callWebServiceForAddProducts];
}

- (IBAction)btnReverseCart:(id)sender
{
    CartViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"CartViewController"];
    [self.navigationController pushViewController:newView animated:YES];
}

- (IBAction)btnDoneTimeAction:(id)sender
{
    viewForTime.hidden =YES;
     [tblFields reloadData];
}

- (IBAction)btnCancelTimeAction:(id)sender
{
    viewForTime.hidden =YES;
    [tblFields reloadData];
}

- (IBAction)btnActionForChangeAddress:(id)sender
{
    ViewForChangeAddress.hidden = NO;
    self.txtFld_HouseCode.text = @"";
    [self callWebServiceForGetAddresses];
}

- (IBAction)btnCrossAction:(id)sender
{
    [self.view endEditing:YES];
    ViewForChangeAddress.hidden = YES;

}

- (IBAction)btnUpdateAddress:(id)sender
{
   
    self.txtFld_postCode.text = [self.txtFld_postCode.text stringByTrimmingCharactersInSet:
                               [NSCharacterSet whitespaceCharacterSet]];
    self.txtFld_StreetCode.text =[self.txtFld_StreetCode.text stringByTrimmingCharactersInSet:
                                [NSCharacterSet whitespaceCharacterSet]];
    
    txtFldForDeliveryAddress.text =[txtFldForDeliveryAddress.text stringByTrimmingCharactersInSet:
                       [NSCharacterSet whitespaceCharacterSet]];
    txtName.text = [txtName.text stringByTrimmingCharactersInSet:
                       [NSCharacterSet whitespaceCharacterSet]];
    txtCity.text = [txtCity.text stringByTrimmingCharactersInSet:
                                    [NSCharacterSet whitespaceCharacterSet]];
    
    if (self.txtFld_postCode.text.length==0 || self.txtFld_StreetCode.text.length==0 || txtFldForDeliveryAddress.text.length==0 ||txtName.text.length==0 ||txtCity.text.length==0)
    {
          [[AppDelegate shareInstance] showAlertWithErrorMessage:NSLocalizedString(@"Please fill all fields.", @"Cancel")];
    }
    else
    {
        NSString *postalCode = [NSString stringWithFormat:@"%@-%@",self.txtFld_postCode.text,self.txtFld_StreetCode.text];
        NSDictionary *addressDict = @{
                                 @"address":[NSString stringWithFormat:@"%@",txtFldForDeliveryAddress.text],
                                 @"postcode":postalCode,
                                 };
         [self callWebServiceForAddNewAddress];
//        if (arrayForAddress.count > 0) {
//            if ([arrayForAddress containsObject:addressDict]) {
//                NSLog(@"Address already exist");
//            }
//            else {
//                [self callWebServiceForAddNewAddress];
//            }
//        }
//        else {
//            [self callWebServiceForAddNewAddress];
//        }
    }
  
  
}
#pragma mark - MethodForCall Webservice
- (void)methodForCheckDelivery {
    NSDictionary *params = @{ //[dicForTextFieldvalues valueForKey:@"zipcode"]
                             @"delivery_postcode": [dicForTextFieldvalues valueForKey:@"zipcode"],                             @"shopid" : [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"restaurant_id"]],
                             };
    NSLog(@"%@ Request - %@",kCheckDeliveryAddress,params);
    [[AppDelegate shareInstance] showActivityIndicator];

    dispatch_async(dispatch_get_main_queue(), ^{
        
        [[SingletonClass sharedManager] m_PostApiResponse:kCheckDeliveryAddress paramDiction:params islogout:NO  onCompletion:^(NSDictionary *response)
         {
             [[AppDelegate shareInstance] hideActivityIndicator];

             NSDictionary *responseDic = (NSDictionary *)response;
             if ([[[responseDic objectForKey:@"payload"] valueForKey:@"status"]integerValue]==0)
             {
                 [[AppDelegate shareInstance] hideActivityIndicator];
                 
                 UIAlertController * alert =   [UIAlertController
                                                alertControllerWithTitle:nil
                                                message:[[responseDic objectForKey:@"payload"] valueForKey:@"message"]
                                                preferredStyle:UIAlertControllerStyleAlert];
                 
                 UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                            {
                                            }];
                 [alert addAction:okAction];
                 [self presentViewController:alert animated:YES completion:nil];
                 
             }
             else {
                 
             }
             
         }];
    });
}


-(void) openHomeView
{
    ViewController*newView = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
    
    [self.navigationController pushViewController:newView animated:YES];
}
-(void)callWebServiceForAddProducts
{
    CLLocationCoordinate2D center = [self geoCodeUsingAddress:[dicForTextFieldvalues valueForKey:@"address"]];
    if ([[arrayForShopData valueForKey:@"is_combination_shop"] integerValue]==1)
    {
        strPostCode=[NSString stringWithFormat:@"%@",[arrayForShopData valueForKey:@"postal_code"]];
        strShopType = @"group";
        strAgentId = [NSString stringWithFormat:@"%@",[arrayForShopData valueForKey:@"agent_id"]];
    }
    else if ([[NSUserDefaults standardUserDefaults] boolForKey:@"is_recipe"])
    {
        strPostCode=[NSString stringWithFormat:@"%@",[arrayForShopData valueForKey:@"postal_code"]];
        strShopType = @"group";
        strAgentId = [NSString stringWithFormat:@"%@",[arrayForShopData valueForKey:@"agent_id"]];
    }
    else
    {
        strPostCode=@"";
        strShopType = @"";
        strAgentId = @"";
    }
    NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
    [dateformate setDateFormat:@"MM/dd/yyyy"]; // Date formater
    NSString *date = [dateformate stringFromDate:[NSDate date]];
    if (![strForDate isEqualToString:NSLocalizedString(@"As soon as possible", @"Cancel")]) {
        if (![strForTime isEqualToString:NSLocalizedString(@"As soon as possible", @"Cancel")]) {
            [[AppDelegate shareInstance] showActivityIndicator];
//            NSDate *datetoday = [NSDate date];
//            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
//            [formatter setDateFormat:@"HH:MM"];
//            NSString *timeString = [formatter stringFromDate:datetoday];
//            NSArray *arrTime =[strForTime componentsSeparatedByString:@"-"];
//            NSString *time1=[arrTime objectAtIndex:0];
//            NSString *time2=[arrTime objectAtIndex:1];
////            NSDate *date1= [formatter dateFromString:time1];
////            NSDate *date2 = [formatter dateFromString:time2];
//
//            NSComparisonResult result = [timeString compare:time1];
//             NSComparisonResult result1 = [timeString compare:time2];
//            if(result == NSOrderedDescending && result1 == NSOrderedAscending)
//            {
//                [[AppDelegate shareInstance] hideActivityIndicator];
//                NSLog(@"date1 is later than date2");
//                 [[AppDelegate shareInstance]showAlertWithErrorMessage:NSLocalizedString(@"Please select another time slot", @"Cancel")];
//            }
////            else if(result == NSOrderedAscending)
////            {
////                NSLog(@"date2 is later than date1");
////            }
//            else if(result ==NSOrderedSame || result1 ==NSOrderedSame)
//            {
//                [[AppDelegate shareInstance] hideActivityIndicator];
//                NSLog(@"date1 is equal to date2");
//                [[AppDelegate shareInstance]showAlertWithErrorMessage:NSLocalizedString(@"Please select another time slot", @"Cancel")];
//            }
//            else
//            {
                NSLog(@"%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"totalPrice"]);
                NSLog(@"%@",[NSString stringWithFormat:@"%f",self.serviceCharge]);
                NSLog(@"%@",[NSString stringWithFormat:@"%f",deliveryRate]);
                NSLog(@"%@",[NSString stringWithFormat:@"%.2f",[[[NSUserDefaults standardUserDefaults] valueForKey:@"totalPrice"] floatValue]+self.serviceCharge+deliveryRate]);
                NSDictionary *params = @{
                                         @"user_id":[arrayForuserData valueForKey:@"id"],
                                         @"shopid":[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"restaurant_id"]],
                                         @"total_payment":[NSString stringWithFormat:@"%.2f",[[[NSUserDefaults standardUserDefaults] valueForKey:@"totalPrice"] floatValue]+self.serviceCharge+deliveryRate],
                                         @"date":date,
                                         @"delivery_date":strForDate,
                                         //@"delivery_time":strForTime,
                                         @"delivery_time":idOfTime,
                                         @"quantity":[[NSUserDefaults standardUserDefaults] valueForKey:@"cartCount"],
                                         @"delivery_charge":[NSString stringWithFormat:@"%.2f",deliveryRate],
                                         @"payment_method":paymentMethod,
                                         @"service_charge":[NSString stringWithFormat:@"%f",self.serviceCharge],
                                         @"cart":jsonString,
                                         @"trans_id":@"",
                                         @"delivery_option":@"",
                                         @"agent_id":strAgentId,
                                         @"shop_type":strShopType,
                                         @"postcode":strPostCode,
                                         @"address":[dicForTextFieldvalues valueForKey:@"address"],
                                         @"delivery_postcode":[dicForTextFieldvalues valueForKey:@"zipcode"],
                                         @"lat":[NSString stringWithFormat:@"%f",center.latitude],
                                         @"lng":[NSString stringWithFormat:@"%f",center.longitude]
                                         };
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [[SingletonClass sharedManager] m_PostApiResponse:@"placeAnOrder" paramDiction:params islogout:NO  onCompletion:^(NSDictionary *response)
                     {
                         id<GAITracker> tracker = [GAI sharedInstance].defaultTracker;
                         [tracker set:kGAIScreenName value:@"User Login Screen"];
                         [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
                         
                         [tracker set:kGAIUserId value:[NSString stringWithFormat:@"%@", [arrayForuserData valueForKey:@"id"]]];
                         
                         
                         [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"User id"
                                                                               action:[NSString stringWithFormat:@"%@", [arrayForuserData valueForKey:@"id"]]
                                                                                label:nil
                                                                                value:nil] build]];
                         
                         [tracker send:[[GAIDictionaryBuilder createEventWithCategory:@"Shop id"
                                                                               action:[NSString stringWithFormat:@"%@", [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"restaurant_id"]]]
                                                                                label:nil
                                                                                value:nil] build]];
                         NSDictionary *responseDic = (NSDictionary *)response;
                         if ([[[responseDic objectForKey:@"payload"]valueForKey:@"status"]integerValue]==1)
                         {
                             [[AppDelegate shareInstance] hideActivityIndicator];
                             [[CustomClasssesForCoreData sharedManagerForCoreData] methodForDeleteAllValues:nil onCompletion:^(NSArray *response)
                              {
                                  [[NSUserDefaults standardUserDefaults]setObject:@"0" forKey:@"cartCount"];
                                  [[NSUserDefaults standardUserDefaults]synchronize];
                                  if ([[NSUserDefaults standardUserDefaults] valueForKey:@"cartCount"]!=nil)
                                  {
                                      lblCount.text=[[NSUserDefaults standardUserDefaults] valueForKey:@"cartCount"];
                                  }
                                  else
                                  {
                                      lblCount.text=@"";
                                  }
                              }];
                             
                             if ([[[[responseDic objectForKey:@"payload"] objectForKey:@"data"] valueForKey:@"payment_method"] isEqualToString:@"Ideal"])
                             {
                                 UIApplication *application = [UIApplication sharedApplication];
                                 NSURL *URL = [NSURL URLWithString:[[[responseDic objectForKey:@"payload"] objectForKey:@"data"] valueForKey:@"ideal_url"]];
                                 [application openURL:URL options:@{} completionHandler:^(BOOL success) {
                                     if (success) {
                                         NSLog(@"Opened url");
                                     }
                                 }];
                                 // [[NSUserDefaults standardUserDefaults] setValue:@"Your order has been placed." forKey:@"order_Status"];
                                 // [self performSelector:@selector(openHomeView) withObject:nil afterDelay:3.0];
                                 
                             }
                             else if([[[[responseDic objectForKey:@"payload"] objectForKey:@"data"] valueForKey:@"payment_method"] isEqualToString:@"Paypal"])
                             {
                                 UIApplication *application = [UIApplication sharedApplication];
                                 NSURL *URL = [NSURL URLWithString:[[[responseDic objectForKey:@"payload"] objectForKey:@"data"] valueForKey:@"paypal_url"]];
                                 [application openURL:URL options:@{} completionHandler:^(BOOL success) {
                                     if (success) {
                                         NSLog(@"Opened url");
                                     }
                                 }];
                                 //[[NSUserDefaults standardUserDefaults] setValue:@"Your order has been placed." forKey:@"order_Status"];
                                 // [self performSelector:@selector(openHomeView) withObject:nil afterDelay:3.0];
                                 
                             }
                             else
                             {
//                                 UIAlertController * alert=   [UIAlertController
//                                                               alertControllerWithTitle:nil
//                                                               message:@"Your order has been placed."
//                                                               preferredStyle:UIAlertControllerStyleAlert];
//
//                                 UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
//                                                            {
//                                                                //                                                            ViewController*newView = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
//                                                                //
//                                                                //                                                            [self.navigationController pushViewController:newView animated:YES];
//                                                            }];
//                                 [alert addAction:okAction];
//                                 [self presentViewController:alert animated:YES completion:nil];
//                                 [[AppDelegate shareInstance] hideActivityIndicator];
                                 [[AppDelegate shareInstance]showAlertWithErrorMessage:NSLocalizedString(@"Please select payment method", @"Cancel")];
                             }
                         }
                         else
                         {
                             [[AppDelegate shareInstance]showAlertWithErrorMessage:[[responseDic objectForKey:@"payload"] objectForKey:@"message"]];
                             [[AppDelegate shareInstance] hideActivityIndicator];
                         }
                     }];
                });
           // }
            
        }
        else{
             [[AppDelegate shareInstance]showAlertWithErrorMessage:NSLocalizedString(@"Please enter delivery date and time", @"Cancel")];
        }
    }
    else
    {
        [[AppDelegate shareInstance]showAlertWithErrorMessage:NSLocalizedString(@"Please enter delivery date and time", @"Cancel")];
    }
}
- (CLLocationCoordinate2D) geoCodeUsingAddress:(NSString *)address
{
    double latitude = 0, longitude = 0;
    NSString *esc_addr =  [address stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]];
    NSString *req = [NSString stringWithFormat:@"http://maps.google.com/maps/api/geocode/json?sensor=false&address=%@", esc_addr];
    NSString *result = [NSString stringWithContentsOfURL:[NSURL URLWithString:req] encoding:NSUTF8StringEncoding error:NULL];
    if (result) {
        NSScanner *scanner = [NSScanner scannerWithString:result];
        if ([scanner scanUpToString:@"\"lat\" :" intoString:nil] && [scanner scanString:@"\"lat\" :" intoString:nil]) {
            [scanner scanDouble:&latitude];
            if ([scanner scanUpToString:@"\"lng\" :" intoString:nil] && [scanner scanString:@"\"lng\" :" intoString:nil]) {
                [scanner scanDouble:&longitude];
            }
        }
    }
    CLLocationCoordinate2D center;
    center.latitude = latitude;
    center.longitude = longitude;
    return center;
}
- (void)methodForWebserviceTimeSlots
{
    NSLog(@"%@",strForDate);
    [[AppDelegate shareInstance] showActivityIndicator];
    NSDictionary *params = @{
                             @"opendate":strForDate,
                             @"shopid":[NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"restaurant_id"]],
                             kPreparation_time : [NSString stringWithFormat:@"%lu", self.maxPreparationTime]
                             };
    NSLog(@"%@",params);
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [[SingletonClass sharedManager] m_PostApiResponse:kCheckoutTimingWeekdays paramDiction:params islogout:NO  onCompletion:^(NSDictionary *response)
         {
             NSDictionary *responseDic = (NSDictionary *)response;
             if ([[[responseDic objectForKey:@"payload"]valueForKey:@"status"]integerValue]==1)
             {
                 //NSString *strArray  = [[responseDic objectForKey:@"payload"]valueForKey:@"data"];
           //NSDictionary *dict=
                 newArray = [[responseDic objectForKey:@"payload"]valueForKey:@"data"];//[strArray componentsSeparatedByString:@","];
                   NSLog(@"%@",newArray);
//                 NSString *title = [newArray objectAtIndex:row];
//                 strForTime = title;
                 [pickerFortime reloadAllComponents];
                 [tblFields reloadData];
                 [[AppDelegate shareInstance] hideActivityIndicator];
             }
             else
             {
                 UIAlertController * alert=   [UIAlertController
                                               alertControllerWithTitle:nil
                                               message:[[responseDic objectForKey:@"payload"]valueForKey:@"message"]
                                               preferredStyle:UIAlertControllerStyleAlert];
                 
                 UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                 {
                     
                 }];
                 [alert addAction:okAction];
                 [self presentViewController:alert animated:YES completion:nil];
                 [[AppDelegate shareInstance] hideActivityIndicator];
                 
                 
             }
         }];
    });
}

-(void)callWebServiceForAddNewAddress
{
    [self.view endEditing:YES];
    [[AppDelegate shareInstance] showActivityIndicator];
    NSString *postalCode = [NSString stringWithFormat:@"%@-%@",self.txtFld_postCode.text,self.txtFld_StreetCode.text];
    NSDictionary *params = @{
                             @"userid":[arrayForuserData valueForKey:@"id"],
                             @"address":[NSString stringWithFormat:@"%@",txtFldForDeliveryAddress.text],
                             @"postcode":postalCode,
                             @"name":[NSString stringWithFormat:@"%@",txtName.text],
                             @"city":[NSString stringWithFormat:@"%@",txtCity.text]
                             };
    NSLog(@"%@",params);
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [[SingletonClass sharedManager] m_PostApiResponse:@"saveextraaddress" paramDiction:params islogout:NO  onCompletion:^(NSDictionary *response)
         {
             NSDictionary *responseDic = (NSDictionary *)response;
             if ([[[responseDic objectForKey:@"payload"]valueForKey:@"status"]integerValue]==1)
             {
                 UIAlertController * alert=   [UIAlertController
                                               alertControllerWithTitle:nil
                                               message:@"Address updated successfully"
                                               preferredStyle:UIAlertControllerStyleAlert];
                 
                 UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                            {
                                                NSString *postalCode = [NSString stringWithFormat:@"%@-%@",self.txtFld_postCode.text,self.txtFld_StreetCode.text];

                                                [dicForTextFieldvalues setObject:txtFldForDeliveryAddress.text forKey:@"address"];
                                                [dicForTextFieldvalues setObject:postalCode  forKey:@"zipcode"];
                                                [dicForTextFieldvalues setObject:txtName.text forKey:@"name"];
                                                [dicForTextFieldvalues setObject:txtCity.text  forKey:@"city"];
                                                [tblFields reloadData];
                                                 ViewForChangeAddress.hidden = YES;
                                            }];
                 [alert addAction:okAction];
                 [self presentViewController:alert animated:YES completion:nil];
                
                 [[AppDelegate shareInstance] hideActivityIndicator];
             }
             else
             {
                 UIAlertController * alert=   [UIAlertController
                                               alertControllerWithTitle:nil
                                               message:[[responseDic objectForKey:@"payload"]valueForKey:@"message"]
                                               preferredStyle:UIAlertControllerStyleAlert];
                 
                 UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                            {
                                                [self.view endEditing:YES];
                                            }];
                 [alert addAction:okAction];
                 [self presentViewController:alert animated:YES completion:nil];
                 [[AppDelegate shareInstance] hideActivityIndicator];
                 
                 
             }
         }];
    });

}
-(void)callWebServiceForGetAddresses
{
    [[AppDelegate shareInstance] showActivityIndicator];
    NSDictionary *params = @{
                             @"userid":[arrayForuserData valueForKey:@"id"]
                             };
    NSLog(@"%@",params);
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [[SingletonClass sharedManager] m_PostApiResponse:@"getextradeliveryaddress" paramDiction:params islogout:NO  onCompletion:^(NSDictionary *response)
         {
             NSDictionary *responseDic = (NSDictionary *)response;
             
             if ([[[responseDic objectForKey:@"payload"]valueForKey:@"status"]integerValue]==1)
             {
                 tblForAddressList.hidden = NO;
                 lblChoosePrevious.hidden = NO;
                 arrayForAddress =[[responseDic objectForKey:@"payload"]valueForKey:@"data"];
                 NSLog(@"%@",[[arrayForAddress objectAtIndex:0]valueForKey:@"address"]);
                 [tblForAddressList reloadData];
                 [[AppDelegate shareInstance] hideActivityIndicator];
             }
             else
             {
                 tblForAddressList.hidden = YES;
                  lblChoosePrevious.hidden = YES;
                 UIAlertController * alert=   [UIAlertController
                                               alertControllerWithTitle:nil
                                               message:[[responseDic objectForKey:@"payload"]valueForKey:@"message"]
                                               preferredStyle:UIAlertControllerStyleAlert];
                 
                 UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                            {
                                                
                                            }];
                 [alert addAction:okAction];
                 [self presentViewController:alert animated:YES completion:nil];
                 [[AppDelegate shareInstance] hideActivityIndicator];
             }
         }];
    });
}
@end
