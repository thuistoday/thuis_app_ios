//
//  Product+CoreDataProperties.m
//  
//
//  Created by IMMANENT on 20/02/17.
//
//

#import "Product+CoreDataProperties.h"

@implementation Product (CoreDataProperties)

+ (NSFetchRequest<Product *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"Product"];
}

@dynamic category_id;
@dynamic product_id;
@dynamic product_name;
@dynamic product_unit;
@dynamic prouct_price;
@dynamic proudct_description_ar;
@dynamic shop_id;
@dynamic vat;
@dynamic vat_id;
@dynamic product_quantity;
@dynamic price_vat;
@dynamic single_price;
@dynamic image_name;
@dynamic type;

@end
