//
//  UserProfileViewController.m
//  Thuis today
//
//  Created by IMMANENT on 28/02/17.
//  Copyright © 2017 IMMANENT. All rights reserved.
//
#define RGB(r, g, b) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:1]
#define RGBA(r, g, b, a) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:a]
#import "UserProfileViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
@interface UserProfileViewController ()
{
    NSMutableArray *arrayForUserInfo,*arrayForOrderHistory,*mainArray;
      NSData *picData;
}
@end

@implementation UserProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [CommonMethods configureNavigationBarForViewController:self withTitle:@"My Account"];
    viewForUserInfo.layer.cornerRadius=8;
      viewForUserInfo.layer.borderWidth = 1.0;
    viewForUserInfo.layer.borderColor =[UIColor lightGrayColor].CGColor;
    viewForUserInfo.layer.masksToBounds = YES;
    
    imgForProPic.layer.cornerRadius=imgForProPic.frame.size.width/2;
    imgForProPic.layer.borderWidth = 1.0;
    imgForProPic.layer.borderColor =[UIColor lightGrayColor].CGColor;
    imgForProPic.layer.masksToBounds = YES;

    mainArray =[[NSMutableArray alloc]init];
    
    arrayForUserInfo = [[NSMutableArray alloc]init];
   _orderDetail=[[NSMutableArray alloc]init];

    arrayForUserInfo = [[NSUserDefaults standardUserDefaults] objectForKey:@"userData"];

    [imgForProPic sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",baseUrlForImageProfile,[arrayForUserInfo valueForKey:@"profile_pic"]]]
                    placeholderImage:[UIImage imageNamed:@"image_placeholder@1x"]];
    
    [self methodForCallWebServiceOrderHistory:0];
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [self refreshUserProfile];
    arrayForUserInfo = [[NSUserDefaults standardUserDefaults] objectForKey:@"userData"];
    lblCity.text=[arrayForUserInfo valueForKey:@"city"];
    lblName.text=[arrayForUserInfo valueForKey:@"name"];
    
    lblAddress.text=[arrayForUserInfo valueForKey:@"address"];
    
    lblPostCode.text=[arrayForUserInfo valueForKey:@"zipcode"];
    
    lblPhoneNumber.text=[arrayForUserInfo valueForKey:@"phone"];
    
    lblEmail.text=[arrayForUserInfo valueForKey:@"email"];
    
    lblCount.layer.cornerRadius = lblCount.frame.size.width/2;
    lblCount.layer.masksToBounds=YES;

    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"cartCount"]!=nil) {
        lblCount.text=[[NSUserDefaults standardUserDefaults] valueForKey:@"cartCount"];
    }
    else
    {
        lblCount.text=@"0";
    }
    [[NSUserDefaults standardUserDefaults]setObject:@"no" forKey:@"isCartView"];
    [[NSUserDefaults standardUserDefaults]synchronize];
   
}
#pragma mark -
#pragma mark - Webservice Calls
- (void)methodForCallWebServiceOrderHistory :(NSInteger)offset
{
        [[AppDelegate shareInstance] showActivityIndicator];
        NSDictionary *params = @{
                                 @"user_id":[arrayForUserInfo valueForKey:@"id"],
                                 @"offset":[NSString stringWithFormat:@"%ld",(long)offset]
                                 };
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [[SingletonClass sharedManager] m_PostApiResponse:@"orderHistory" paramDiction:params islogout:NO  onCompletion:^(NSDictionary *response)
             {
                 NSDictionary *responseDic = (NSDictionary *)response;
                 if ([[[responseDic objectForKey:@"payload"]valueForKey:@"status"]integerValue]==1)
                 {
                     arrayForOrderHistory= [[NSMutableArray alloc]init];
                     arrayForOrderHistory=[[responseDic objectForKey:@"payload"] objectForKey:@"data"];
                     if (arrayForOrderHistory.count) {
                         for (int i=0; i<arrayForOrderHistory.count; i++) {
                             [mainArray addObject:[arrayForOrderHistory objectAtIndex:i]];
                         }
                     }
                     [tblForHistory reloadData];
                     [[AppDelegate shareInstance] hideActivityIndicator];
                 }
                 else
                 {
                     [[AppDelegate shareInstance] hideActivityIndicator];
                     UIAlertController * alert=   [UIAlertController
                                                   alertControllerWithTitle:nil
                                                   message:[[responseDic objectForKey:@"payload"]valueForKey:@"message"]
                                                   preferredStyle:UIAlertControllerStyleAlert];
                     
//                     UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
//                         
//                         [self methodForCallWebServiceOrderHistory:0];
//                         
//                     }];
                     UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){
                         
                     }];
                     [alert addAction:cancelAction];
                    // [alert addAction:okAction];
                     [self presentViewController:alert animated:YES completion:nil];
                 }
             }];
        });
    
}
- (void)refreshUserProfile {
    [[AppDelegate shareInstance] showActivityIndicator];
    NSDictionary *params = @{
                             @"user_id":[[[NSUserDefaults standardUserDefaults] valueForKey:@"userData"] valueForKey:@"id"]
                             };
    NSLog(@"User profile requesr Params - %@",params);
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [[SingletonClass sharedManager] m_PostApiResponse:kGetUserEditProfile paramDiction:params islogout:NO  onCompletion:^(NSDictionary *response)
         {
             NSDictionary *responseDic = (NSDictionary *)response;
             if ([[[responseDic objectForKey:@"payload"]valueForKey:@"status"]integerValue]==1)
             {
                 [[AppDelegate shareInstance] hideActivityIndicator];
                 arrayForUserInfo = [[responseDic objectForKey:@"payload"] objectForKey:@"data"];
                
                 [[NSUserDefaults standardUserDefaults] setObject:arrayForUserInfo forKey:@"userData"];
                 [[NSUserDefaults standardUserDefaults] synchronize];
                 NSLog(@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"userData"]);
             }
             else
             {
                 [[AppDelegate shareInstance] hideActivityIndicator];
                 if ([[responseDic objectForKey:@"payload"]valueForKey:@"message"] != nil) {
                 UIAlertController * alert=   [UIAlertController
                                               alertControllerWithTitle:nil
                                               message:[[responseDic objectForKey:@"payload"]valueForKey:@"message"]
                                               preferredStyle:UIAlertControllerStyleAlert];
                 
             
                 UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){
                     
                 }];
                 [alert addAction:cancelAction];
                 [self presentViewController:alert animated:YES completion:nil];
                 }
             }
         }];
    });
    
}

#pragma mark -
#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return mainArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier;
    UITableViewCell *cell ;
    
        CellIdentifier = @"cell";
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    UILabel *lblDate=(UILabel *)[cell viewWithTag:101];
    UILabel *lblOrderNumber=(UILabel *)[cell viewWithTag:102];
    UILabel *lblPrice=(UILabel *)[cell viewWithTag:103];
    UILabel *lblStore=(UILabel *)[cell viewWithTag:104];
   // UILabel *lblStatus=(UILabel *)[cell viewWithTag:105];
    //lblStatus.layer.cornerRadius = 5;
    //lblStatus.layer.masksToBounds=YES;

    lblDate.text=[[mainArray objectAtIndex:indexPath.row] valueForKey:@"date"];
    lblOrderNumber.text=[[mainArray objectAtIndex:indexPath.row] valueForKey:@"id"];
    NSString* cleanedString = [[[[mainArray objectAtIndex:indexPath.row] valueForKey:@"total_payment"] stringByReplacingOccurrencesOfString:@"." withString:@","]
                               stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
    lblPrice.text=cleanedString;
    lblStore.text =[[mainArray objectAtIndex:indexPath.row] valueForKey:@"restaurant_name_ar"];
  
    
   // if ([[[mainArray objectAtIndex:indexPath.row] valueForKey:@"status"] isEqualToString:@"0"])
    //{
        //lblStatus.text =NSLocalizedString(@"Delivered", @"Cancel");
       //  R:- 0, G:-159, B:- 14
      //  lblStatus.backgroundColor = [UIColor colorWithRed:0/255.0f green:159/255.0f blue:14/255.0f alpha:1];
   //}
   // else if([[[mainArray objectAtIndex:indexPath.row] valueForKey:@"status"] isEqualToString:@"5"])
    //{
     
       // lblStatus.text =NSLocalizedString(@"Canceled", @"Cancel");
      //   lblStatus.backgroundColor = [UIColor colorWithRed:221/255.0f green:0/255.0f blue:20/255.0f alpha:1];
    //}
    //else if([[[mainArray objectAtIndex:indexPath.row] valueForKey:@"status"] isEqualToString:@"2"])
   // {
       // lblStatus.text =NSLocalizedString(@"Order placed", @"Cancel");
        // lblStatus.backgroundColor = [UIColor colorWithRed:213/255.0f green:159/255.0f blue:14/255.0f alpha:1];
    //}
    //else if([[[mainArray objectAtIndex:indexPath.row] valueForKey:@"status"] isEqualToString:@"3"])
   // {
     //   lblStatus.text =NSLocalizedString(@"Order approved", @"Cancel");
       //  lblStatus.backgroundColor = [UIColor colorWithRed:254/255.0f green:102/255.0f blue:3/255.0f alpha:1];
    //}
    //else if([[[mainArray objectAtIndex:indexPath.row] valueForKey:@"status"] isEqualToString:@"4"])
   // {
     //   lblStatus.text =NSLocalizedString(@"Order ready", @"Cancel");
      // lblStatus.backgroundColor = [UIColor colorWithRed:213/255.0f green:159/255.0f blue:14/255.0f alpha:1];
    //}
   // else if([[[mainArray objectAtIndex:indexPath.row] valueForKey:@"status"] isEqualToString:@"7"])
    //{
    //    lblStatus.text =NSLocalizedString(@"Transit", @"Cancel");
    //     lblStatus.backgroundColor = [UIColor colorWithRed:254/255.0f green:102/255.0f blue:3/255.0f alpha:1];
   // }
    if (arrayForOrderHistory.count==25) {
        if (mainArray.count-1 == indexPath.row)
        {
            [self methodForCallWebServiceOrderHistory:indexPath.row+1];
        }
    }
    if (indexPath.row%2==0)
    {
        cell.backgroundColor = [UIColor whiteColor];
    }
    else
    {
        cell.backgroundColor = RGB(241, 241, 241);
    }
    return cell;
}
//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    
//}
#pragma mark -
#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    OrderHistoryViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"OrderHistoryViewController"];
    //newView.strForOrderId = [[mainArray objectAtIndex:indexPath.row] valueForKey:@"id"];
    NSDictionary *dict=[mainArray objectAtIndex:indexPath.row];
    [_orderDetail addObject:dict];
    newView.OrderDetail=_orderDetail;
    [self.navigationController pushViewController:newView animated:YES];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)btnEditAction:(id)sender
{
   
}

- (IBAction)btnCartAction:(id)sender
{
    CartViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"CartViewController"];
    [self.navigationController pushViewController:newView animated:YES];
}

- (IBAction)btnActionMenu:(id)sender
{
    
    [self.menuContainerViewController toggleLeftSideMenuCompletion:nil];

}
- (IBAction)btnActionEditProfilePic:(id)sender
{
    [[AppDelegate shareInstance] hideActivityIndicator];
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:nil
                                  message:@""
                                  preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *CameraAction = [UIAlertAction actionWithTitle:@"Gallery" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        if ([UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypePhotoLibrary])
        {
            UIImagePickerController *controller = [[UIImagePickerController alloc] init];
            controller.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            controller.allowsEditing = NO;
            controller.delegate = self;
            [self.navigationController presentViewController: controller animated: YES completion: nil];
        }
    }];
    UIAlertAction *GalleryAction = [UIAlertAction actionWithTitle:@"Camera" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
    {
        if ([UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeCamera])
        {
            UIImagePickerController *controller = [[UIImagePickerController alloc] init];
            controller.sourceType = UIImagePickerControllerSourceTypeCamera;
            controller.allowsEditing = NO;
            controller.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType: UIImagePickerControllerSourceTypeCamera];
            controller.delegate = self;
            [self.navigationController presentViewController: controller animated: YES completion: nil];
        }

    }];
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel
 handler:^(UIAlertAction *action)
    {
       
    }];
    [alert addAction:CameraAction];
    [alert addAction:GalleryAction];
    [alert addAction:cancelAction];
    [self presentViewController:alert animated:YES completion:nil];
}

#pragma mark -
#pragma mark - Image Picker Delegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    
    UIImage *image = [info valueForKey: UIImagePickerControllerOriginalImage];
    
   // UIImage *comImage = [self compressImage:image];
//    NSData *compressedData = UIImageJPEGRepresentation(image, 0.01f);
//    UIImage *comImage = [UIImage imageWithData:compressedData];
    
    imgForProPic.image = image;
    
//    NSData *data = UIImageJPEGRepresentation(image, 0.99);
//    float maxFileSize = 1 * 512;
//    
//    //If the image is bigger than the max file size, try to bring it down to the max file size
//    if ([data length] > maxFileSize) {
//        data = UIImageJPEGRepresentation(image, maxFileSize/[data length]);
//    }
//    
//    image = [UIImage imageWithData:data];
//    
//    imgForProPic.image = image;
    
    [self.navigationController dismissViewControllerAnimated: YES completion: nil];
    if (image !=nil) {
        [self checkForImageUpload];
    }
    
}
- (UIImage *)compressImage:(UIImage *)image {
    
    NSData *imgData = UIImageJPEGRepresentation(image, 1); //1 it represents the quality of the image.
    NSLog(@"Size of Image(bytes):%ld",(unsigned long)[imgData length]);
    
    float actualHeight = image.size.height;
    float actualWidth = image.size.width;
    float maxHeight = 50;
    float maxWidth = 50;
    float imgRatio = actualWidth/actualHeight;
    float maxRatio = maxWidth/maxHeight;
    float compressionQuality = 0.9;//50 percent compression
    
    if (actualHeight > maxHeight || actualWidth > maxWidth){
        if(imgRatio < maxRatio){
            //adjust width according to maxHeight
            imgRatio = maxHeight / actualHeight;
            actualWidth = imgRatio * actualWidth;
            actualHeight = maxHeight;
        }
        else if(imgRatio > maxRatio){
            //adjust height according to maxWidth
            imgRatio = maxWidth / actualWidth;
            actualHeight = imgRatio * actualHeight;
            actualWidth = maxWidth;
        }
        else{
            actualHeight = maxHeight;
            actualWidth = maxWidth;
        }
    }
    
    CGRect rect = CGRectMake(0.0, 0.0, actualWidth, actualHeight);
    UIGraphicsBeginImageContext(rect.size);
    [image drawInRect:rect];
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    NSData *imageData = UIImageJPEGRepresentation(img, compressionQuality);
    UIGraphicsEndImageContext();
    
    NSLog(@"Size of Image(bytes):%ld",(unsigned long)[imageData length]);
    
    return [UIImage imageWithData:imageData];
}
-(void)checkForImageUpload
{
   // Are you sure to upload this picture.
    UIAlertController * alert=   [UIAlertController
                                  alertControllerWithTitle:nil
                                  message:NSLocalizedString(@"Are you sure to upload this picture.", @"Cancel")
                                  preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *UploadAction = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        [self methodForUploadPic];
    }];
     UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel
                                                         handler:^(UIAlertAction *action)
                                   {
                                       
                                   }];
    [alert addAction:UploadAction];
    [alert addAction:cancelAction];
    [self presentViewController:alert animated:YES completion:nil];
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker;
{
    [self.navigationController dismissViewControllerAnimated: YES completion: nil];
}
- (NSString *)contentTypeForImageData:(NSData *)data {
    uint8_t c;
    [data getBytes:&c length:1];
    
    switch (c) {
        case 0xFF:
            return @"jpeg";
            break;
        case 0x89:
            return @"png";
            break;
        case 0x47:
            return @"gif";
            break;
        case 0x49:
        case 0x4D:
            return @"tiff";
            break;
        case 0x25:
            return @"pdf";
            break;
        case 0xD0:
            return @"vnd";
            break;
        case 0x46:
            return @"plain";
            break;
        default:
            return @"octet-stream";
    }
    return nil;
}


-(void)methodForUploadPic
{
    [[AppDelegate shareInstance]showActivityIndicator];
    picData=UIImageJPEGRepresentation(imgForProPic.image, 0.3);
    NSString *strExt = [self contentTypeForImageData:picData];
    NSLog(@"%@",strExt);
    NSDictionary *params = @{
                             @"user_id":[arrayForUserInfo valueForKey:@"id"]
                             };
    
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [[SingletonClass sharedManager]m_UpdateGallery:@"editUser_image" paramDiction:params uploadPicData:picData coverPicData:nil imageName:@"profile_pic" extension:strExt onCompletion:^(NSDictionary *response)
         {
             NSDictionary *responseDic = (NSDictionary *)response;
             NSLog(@"%@",[[responseDic objectForKey:@"payload"] valueForKey:@"status"]);
             if ([[[responseDic objectForKey:@"payload"]valueForKey:@"status"]integerValue] == 1)
             {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     [[AppDelegate shareInstance]hideActivityIndicator];
                     arrayForUserInfo = [[responseDic objectForKey:@"payload"] objectForKey:@"data"];
                     [[NSUserDefaults standardUserDefaults] setObject:arrayForUserInfo forKey:@"userData"];
                     [[NSUserDefaults standardUserDefaults] synchronize];
                     NSLog(@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"userData"]);
                      [[AppDelegate shareInstance] showAlertWithErrorMessage:[[responseDic objectForKey:@"payload"]valueForKey:@"message"]];
                     [[NSNotificationCenter defaultCenter] postNotificationName:@"MyCacheUpdatedNotification" object:self];
                 });
                 
             }
             else
             {
                 dispatch_async(dispatch_get_main_queue(), ^{
                     [[AppDelegate shareInstance]hideActivityIndicator];
                     [[AppDelegate shareInstance] showAlertWithErrorMessage:[[responseDic objectForKey:@"payload"]valueForKey:@"message"]];
                 });
             }
         }];
    });
}
@end
