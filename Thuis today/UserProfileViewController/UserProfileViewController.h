//
//  UserProfileViewController.h
//  Thuis today
//
//  Created by IMMANENT on 28/02/17.
//  Copyright © 2017 IMMANENT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MFSideMenu.h"
@interface UserProfileViewController : UIViewController<UIImagePickerControllerDelegate, UINavigationControllerDelegate>
{
    
     IBOutlet UIView *viewForUserInfo;
    
     IBOutlet UIImageView *imgForProPic;
    
     IBOutlet UILabel *lblCity;
     IBOutlet UILabel *lblName;
    
     IBOutlet UILabel *lblAddress;
    
     IBOutlet UILabel *lblPostCode;
    
     IBOutlet UILabel *lblPhoneNumber;
    
     IBOutlet UILabel *lblEmail;
    
     IBOutlet UITableView *tblForHistory;
    
     IBOutlet UILabel *lblCount;
    
}

@property (nonatomic, strong) IBOutlet NSMutableArray *orderDetail;
- (IBAction)btnEditAction:(id)sender;
- (IBAction)btnCartAction:(id)sender;
- (IBAction)btnActionMenu:(id)sender;
- (IBAction)btnActionEditProfilePic:(id)sender;

@end
