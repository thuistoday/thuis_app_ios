//
//  Product+CoreDataProperties.h
//  
//
//  Created by IMMANENT on 20/02/17.
//
//

#import "Product+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface Product (CoreDataProperties)

+ (NSFetchRequest<Product *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *category_id;
@property (nullable, nonatomic, copy) NSString *product_id;
@property (nullable, nonatomic, copy) NSString *product_name;
@property (nullable, nonatomic, copy) NSString *product_unit;
@property (nullable, nonatomic, copy) NSString *prouct_price;
@property (nullable, nonatomic, copy) NSString *proudct_description_ar;
@property (nullable, nonatomic, copy) NSString *shop_id;
@property (nullable, nonatomic, copy) NSString *vat;
@property (nullable, nonatomic, copy) NSString *vat_id;
@property (nullable, nonatomic, copy) NSString *product_quantity;
@property (nullable, nonatomic, copy) NSString *price_vat;
@property (nullable, nonatomic, copy) NSString *single_price;
@property (nullable, nonatomic, copy) NSString *image_name;
@property (nullable, nonatomic, copy) NSString *type;


@end

NS_ASSUME_NONNULL_END
