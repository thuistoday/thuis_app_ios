//
//  AboutUsViewController.m
//  Thuis today
//
//  Created by offshore_mac_1 on 18/08/17.
//  Copyright © 2017 IMMANENT. All rights reserved.
//
#define URLEMail @"mailto:info@thuis.today?subject=FeedBack"
#import "AboutUsViewController.h"
#import "ContactUsViewController.h"
@interface AboutUsViewController ()
@property (weak, nonatomic) IBOutlet UILabel *lbl_versionNumber;

@end

@implementation AboutUsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [CommonMethods configureNavigationBarForViewController:self withTitle:@"OVER ONS"];
    // Do any additional setup after loading the view.
    self.lbl_versionNumber.text = [CommonMethods appVersionNumber];

}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [[NSUserDefaults standardUserDefaults]setObject:@"no" forKey:@"isCartView"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    lblCount.layer.cornerRadius = lblCount.frame.size.width/2;
    lblCount.layer.masksToBounds=YES;
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"cartCount"]!=nil) {
        lblCount.text=[[NSUserDefaults standardUserDefaults] valueForKey:@"cartCount"];
    }
    else
    {
        lblCount.text=@"";
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
    // Dispose of any resources that can be recreated.
}
#pragma mark - Button Actions
- (IBAction)btnBottomBackAction:(id)sender
{
    ViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
    [self.navigationController pushViewController:newView animated:NO];
    //[self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnBottomCartAction:(id)sender
{
    CartViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"CartViewController"];
    
    [self.navigationController pushViewController:newView animated:YES];
}

- (IBAction)btnBottomHomeAction:(id)sender
{
    ViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
    [self.navigationController pushViewController:newView animated:YES];
}
- (IBAction)btnCartAction:(id)sender
{
    CartViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"CartViewController"];
    [self.navigationController pushViewController:newView animated:YES];
}

- (IBAction)btnMenuAction:(id)sender
{
    [self.menuContainerViewController toggleLeftSideMenuCompletion:nil];
}
#pragma mark - UItextView Methods
- (BOOL)textView:(UITextView *)textView shouldInteractWithURL:(NSURL *)URL inRange:(NSRange)characterRange
{
    NSString *urlString = URL.absoluteString;
    if([urlString isEqualToString:@"tel:06%2053669745"])
    {
    
        
    }
    else
    {
        MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
        
        // his class should be the delegate of the mc
        mc.mailComposeDelegate = self;
        
        // set a mail subject ... but you do not need to do this :)
        [mc setSubject:@"FeedBack"];
        
        // set some basic plain text as the message body ... but you do not need to do this :)
        //[mc setMessageBody:@"This is an optional message body plain text!" isHTML:NO];
        
        // set some recipients ... but you do not need to do this :)
        [mc setToRecipients:[NSArray arrayWithObjects:@"info@thuis.today", nil]];
        
        
        if (!mc) {
            //When the device has not added mailViewController mail account is empty , the following present view controller causes the program to crash here
            NSLog(@"no email accounts are set up in your device");
        }
        else
        {
            // displaying our modal view controller on the screen with standard transition
            [self presentViewController:mc animated:YES completion:nil];
        }
        
    }
    
    
    
////    ContactUsViewController*LoginObj = [self.storyboard instantiateViewControllerWithIdentifier:@"ContactUsViewController"];
////
////    UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
////    NSArray *controllers = [NSArray arrayWithObject:LoginObj];
////    navigationController.viewControllers = controllers;
////    NSString *customURL = @"googlegmail://";
////    if ([[UIApplication sharedApplication]
////         canOpenURL:[NSURL URLWithString:customURL]])
////    {
////        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:customURL]];
////    }
////    else
////    {
////       [[UIApplication sharedApplication] openURL:[NSURL URLWithString:customURL]];
////    }
//
//    NSString *recipients = @"mailto:myemail@gmail.com?subject=subjecthere";
//    NSString *body = @"&body=bodyHere";
//    NSString *email = [NSString stringWithFormat:@"%@%@", recipients, body];
////
//    if ([[UIApplication sharedApplication]
//                  canOpenURL:[NSURL URLWithString:email]])
//             {
//                 [[UIApplication sharedApplication] openURL:[NSURL URLWithString:email]];
//             }
//             else
//             {
//                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:email]];
//             }
//
//    email = [email stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
//    MFMailComposeViewController *mailVC = [[MFMailComposeViewController alloc] init];
//    mailVC.mailComposeDelegate = self;
//    if ([MFMailComposeViewController canSendMail]) {
//
//    } else {
//        //This device cannot send email
//    }
//
    //[[UIApplication sharedApplication] openURL:[NSURL URLWithString:email]];
    
    
//    NSString *url = [URLEMail stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding ];
//    [[UIApplication sharedApplication]  openURL: [NSURL URLWithString: url]];
    [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
    return TRUE;
}
#pragma mark - MFMailComposeViewControllerDelegate Methode.
- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(nullable NSError *)error {
    
    switch (result) {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            
            break;
            
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            
            break;
            
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            
            break;
            
        case MFMailComposeResultFailed:
            NSLog(@"Mail sent failure: %@",error.description);
            
            break;
    }
    
    // Dismiss the mail compose view controller.
    [controller dismissViewControllerAnimated:true completion:nil];
    
}
@end
