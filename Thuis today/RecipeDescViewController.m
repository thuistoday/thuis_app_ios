//
//  RecipeDescViewController.m
//  RecipeTinder8
//
//  Created by offshore_mac_1 on 16/10/17.
//  Copyright © 2017 offshore_mac_1. All rights reserved.
//

#import "RecipeDescViewController.h"

@interface RecipeDescViewController ()
{
    BOOL isIngredient;
    NSMutableArray *arrayForCoreDatavalues;
}
@end

@implementation RecipeDescViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [CommonMethods configureNavigationBarForViewController:self withTitle:@"Shops for Rotterdam"];
    arrayForCoreDatavalues = [[NSMutableArray alloc]init];
    isIngredient = YES;
    lblCount.layer.cornerRadius = lblCount.frame.size.width/2;
    lblCount.layer.masksToBounds=YES;
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"cartCount"]!=nil) {
        lblCount.text=[[NSUserDefaults standardUserDefaults] valueForKey:@"cartCount"];
    }
    else
    {
        lblCount.text=@"";
    }
    myArray = [[[_recipeData objectAtIndex:_index] objectForKey:@"indg"] componentsSeparatedByString:@","];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(tableView.tag==0)
    {
        return 3;
    }
    else
    {
        return myArray.count;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{   //NSInteger *height;
    if(tableView.tag==0)
    {
        if (indexPath.row==0)
        {
            if (IDIOM == IPAD)
            {
                return 550;
            }
            else
            {
                return 322;
            }
        }
        else if (indexPath.row==1)
        {
            if (IDIOM == IPAD)
            {
                CGSize size = CGSizeMake(752,9999);
                CGRect textRect = [[[_recipeData objectAtIndex:_index] objectForKey:@"desc"] boundingRectWithSize:size
                                                                                                          options:NSStringDrawingUsesLineFragmentOrigin
                                                                                                       attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:15.0f]}
                                                                                                          context:nil];
                CGSize neededSize = textRect.size;
                return neededSize.height + 150;
            }
            else
            {
                CGSize size = CGSizeMake(300,9999);
                CGRect textRect = [[[_recipeData objectAtIndex:_index] objectForKey:@"desc"] boundingRectWithSize:size
                                                                                                          options:NSStringDrawingUsesLineFragmentOrigin
                                                                                                       attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:10.0f]}
                                                                                                          context:nil];
                CGSize neededSize = textRect.size;
                return neededSize.height + 80;
            }
        }
        else
        {
            if (isIngredient == YES)
            {
                return (myArray.count*44) + 5;
            }
            else
            {
                if (IDIOM == IPAD)
                {
                    CGSize size = CGSizeMake(752,9999);
                    CGRect textRect = [[[_recipeData objectAtIndex:_index] objectForKey:@"direction"] boundingRectWithSize:size
                                                                                                                   options:NSStringDrawingUsesLineFragmentOrigin
                                                                                                                attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:15.0f]}
                                                                                                                   context:nil];
                    CGSize neededSize = textRect.size;
                    return neededSize.height + 200;
                }
                else
                {
                CGSize size = CGSizeMake(300,9999);
                CGRect textRect = [[[_recipeData objectAtIndex:_index] objectForKey:@"direction"] boundingRectWithSize:size
                                                                                                               options:NSStringDrawingUsesLineFragmentOrigin
                                                                                                            attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:10.0f]}
                                                                                                               context:nil];
                CGSize neededSize = textRect.size;
                return neededSize.height + 120;
                }
            }
        }
    }
    else
    {
        return 44;
    }
    
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellIdentifier =@"RecipeImageCell";
    NSString *cellIdentifier1 =@"RecipeIDescCell";
    NSString *cellIdentifier2 =@"RecipeIngredientsCell";
    NSString *cellIdentifier3=@"subcell";
    UITableViewCell *cell;
    [tableView setTableFooterView:[[UIView alloc] initWithFrame:CGRectZero]];
    
    //[tableView setContentOffset:CGPointMake(0, tableView.contentSize.height) animated:YES];
    if(tableView.tag==0)
    {
        if (indexPath.row ==0)
        {
            cell =[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
            UIImageView *imgRecipe = (UIImageView *)[cell.contentView viewWithTag:1];
            UILabel *lblRecipeName = (UILabel *)[cell.contentView viewWithTag:2];
            UILabel *lblPrepTime = (UILabel *)[cell.contentView viewWithTag:3];
            UILabel *lblAddedBy = (UILabel *)[cell.contentView viewWithTag:4];
            
            lblRecipeName.text = [[_recipeData objectAtIndex:_index] objectForKey:@"recipe_name"];
            lblPrepTime.text = [NSString stringWithFormat:@"%@ min",[[_recipeData objectAtIndex:_index] objectForKey:@"preptime"]];
            lblAddedBy.text = [NSString stringWithFormat:@"By: %@",[[_recipeData objectAtIndex:_index] objectForKey:@"addedby"]];
            [imgRecipe sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",baseUrlForImageRecipe,[[_recipeData objectAtIndex:_index] objectForKey:@"image"]]]
                          placeholderImage:[UIImage imageNamed:@""]];
            
        }
        if (indexPath.row==1)
        {
            cell =[tableView dequeueReusableCellWithIdentifier:cellIdentifier1];
            UILabel *recipeDesc = (UILabel *)[cell.contentView viewWithTag:201];
            UILabel *recipeTitle = (UILabel *)[cell.contentView viewWithTag:202];
            UIImageView *imgSeperator = (UIImageView *)[cell.contentView viewWithTag:203];
            UITextField *txtServing = (UITextField *)[cell.contentView viewWithTag:204];
            UIButton *btnAddToCart = (UIButton *)[cell.contentView viewWithTag:205];
            
            txtServing.delegate = self;
            if ([[[_recipeData objectAtIndex:_index] objectForKey:@"desc"] length] == 0)
            {
                recipeDesc.hidden = YES;
                recipeTitle.hidden = YES;
                imgSeperator.hidden = YES;
            }
            else
            {
                recipeDesc.hidden = NO;
                recipeTitle.hidden = NO;
                imgSeperator.hidden = NO;
                
                CGRect textRect;
                if (IDIOM == IPAD)
                {
                    CGSize size = CGSizeMake(752,9999);
                    textRect = [[[_recipeData objectAtIndex:_index] objectForKey:@"desc"] boundingRectWithSize:size
                                                                                                       options:NSStringDrawingUsesLineFragmentOrigin
                                                                                                    attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:15.0f]}
                                                                                                       context:nil];
                }
                else
                {
                    CGSize size = CGSizeMake(300,9999);
                    textRect = [[[_recipeData objectAtIndex:_index] objectForKey:@"desc"] boundingRectWithSize:size
                                                                                                       options:NSStringDrawingUsesLineFragmentOrigin
                                                                                                    attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:10.0f]}
                                                                                                       context:nil];
                }
                
                CGSize neededSize = textRect.size;
                recipeDesc.frame = CGRectMake(recipeDesc.frame.origin.x, recipeDesc.frame.origin.y, recipeDesc.frame.size.width, neededSize.height );
                recipeDesc.text = [[_recipeData objectAtIndex:_index] objectForKey:@"desc"];
            }
            [btnAddToCart addTarget:self action:@selector(btnActionAddToCart:) forControlEvents:UIControlEventTouchUpInside];
        }
        if (indexPath.row==2)
        {
            cell =[tableView dequeueReusableCellWithIdentifier:cellIdentifier2];
            UIButton *btnIngredient = (UIButton *)[cell.contentView viewWithTag:301];
            UIButton *btnDirections = (UIButton *)[cell.contentView viewWithTag:302];
            UILabel *lblDirections = (UILabel *)[cell.contentView viewWithTag:303];
            UIImageView *imgBack = (UIImageView *)[cell.contentView viewWithTag:304];
            UITableView *tblIngredient = (UITableView *)[cell.contentView viewWithTag:1];
            
            
//            imgBack.layer.borderWidth = 1.0;
//            imgBack.layer.borderColor = [UIColor lightGrayColor].CGColor;
            
            tblIngredient.layer.borderWidth = 1.0;
            tblIngredient.layer.borderColor = [UIColor lightGrayColor].CGColor;
            
            lblDirections.layer.borderWidth = 1.0;
            lblDirections.layer.borderColor = [UIColor lightGrayColor].CGColor;
            
            btnIngredient.layer.borderWidth = 1.0;
            btnIngredient.layer.borderColor = [UIColor lightGrayColor].CGColor;
            
            btnDirections.layer.borderWidth = 1.0;
            btnDirections.layer.borderColor = [UIColor lightGrayColor].CGColor;
            
            if (isIngredient == YES)
            {
                tblIngredient.hidden = NO;
                lblDirections.hidden = YES;
                [btnIngredient setBackgroundColor:[UIColor lightGrayColor]];
                [btnDirections setBackgroundColor:[UIColor whiteColor]];
            }
            else
            {
                tblIngredient.hidden = YES;
                lblDirections.hidden = NO;
                [btnDirections setBackgroundColor:[UIColor lightGrayColor]];
                [btnIngredient setBackgroundColor:[UIColor whiteColor]];
                
                CGRect textRect;
                if (IDIOM == IPAD)
                {
                    CGSize size = CGSizeMake(752,9999);
                    textRect = [[[_recipeData objectAtIndex:_index] objectForKey:@"direction"] boundingRectWithSize:size
                                                                                                                   options:NSStringDrawingUsesLineFragmentOrigin
                                                                                                                attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:15.0f]}
                                                                                                                   context:nil];
                }
                else
                {
                    CGSize size = CGSizeMake(300,9999);
                    textRect = [[[_recipeData objectAtIndex:_index] objectForKey:@"direction"] boundingRectWithSize:size
                                                                                                                   options:NSStringDrawingUsesLineFragmentOrigin
                                                                                                                attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:10.0f]}
                                                                                                                   context:nil];
                }
                
        
                CGSize neededSize = textRect.size;
                lblDirections.frame = CGRectMake(lblDirections.frame.origin.x, lblDirections.frame.origin.y, lblDirections.frame.size.width, neededSize.height );
                lblDirections.text = [[_recipeData objectAtIndex:_index] objectForKey:@"direction"];
                
            }
            [btnDirections addTarget:self action:@selector(btnActionSelection:) forControlEvents:UIControlEventTouchUpInside];
            [btnIngredient addTarget:self action:@selector(btnActionSelection:) forControlEvents:UIControlEventTouchUpInside];

        }
        
    }
    else
    {
        cell =[tableView dequeueReusableCellWithIdentifier:cellIdentifier3];
        
        cell.textLabel.text = [myArray objectAtIndex:indexPath.row];
        cell.textLabel.font = [UIFont fontWithName:@"system" size:12];
        cell.textLabel.textColor = [UIColor lightGrayColor];
        
        
        
    }
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}
-(void)btnActionSelection:(id) sender
{
    if ([sender tag] == 301)
    {
        isIngredient = YES;
        [tableViewDesc reloadData];
    }
    else
    {
        isIngredient = NO;
        [tableViewDesc reloadData];
    }
}
-(void)btnActionAddToCart:(id) sender
{
    [[CustomClasssesForCoreData sharedManagerForCoreData] methodForDeleteAllValues:nil onCompletion:^(NSArray *response)
     {
         [[NSUserDefaults standardUserDefaults]setObject:@"0" forKey:@"cartCount"];
         [[NSUserDefaults standardUserDefaults]synchronize];
         if ([[NSUserDefaults standardUserDefaults] valueForKey:@"cartCount"]!=nil)
         {
             lblCount.text=[[NSUserDefaults standardUserDefaults] valueForKey:@"cartCount"];
         }
         else
         {
             lblCount.text=@"";
         }
     }];
    
    NSString *stockStr;
    NSMutableArray *arrayForProducts = [[_recipeData objectAtIndex:_index] objectForKey:@"rec_ind"];
    for (int i=0; i<[arrayForProducts count]; i++)
    {
        NSLog(@"%@",[arrayForProducts objectAtIndex:i]);
        if([[arrayForProducts objectAtIndex:i] objectForKey:@"stock"])
        {
            stockStr = [[arrayForProducts objectAtIndex:i] objectForKey:@"stock"];
        }
        else
        {
            float sum=0.00;
            float vat=0.00;
            NSString *type=[[arrayForProducts objectAtIndex:i] valueForKey:@"type"];
            NSString* cleanedString = [[arrayForProducts objectAtIndex:i] valueForKey:@"meal_price"];
            
            NSString *strTextField =[[arrayForProducts objectAtIndex:i] valueForKey:@"qty"];
            
            vat = ([cleanedString floatValue]*[[[arrayForProducts objectAtIndex:i] valueForKey:@"vat_name"] floatValue])/100;
            
            NSLog(@"%f",vat);
            
            if ([[[arrayForProducts objectAtIndex:i] valueForKey:@"type"] isEqualToString:@"Excl."]) {
                // sum = ([cleanedString floatValue]+vat)*[txtFld.text floatValue]  ;
                sum = [cleanedString floatValue]+vat;
                NSString *roundOff = [NSString stringWithFormat:@"%.2f",sum];
            }
            else
            {
                sum = [cleanedString floatValue];
            }
            
            
            [[CustomClasssesForCoreData sharedManagerForCoreData] methodForSaveValues:[[arrayForProducts objectAtIndex:i] copy] price:[[arrayForProducts objectAtIndex:i] objectForKey:@"total_price"] quantity:[[arrayForProducts objectAtIndex:i] valueForKey:@"qty"] singlePrice:[NSString stringWithFormat:@"%.2f",sum] type:type onCompletion:^(NSArray *response)
             {
                 NSLog(@"testttt = %lu",(unsigned long)response.count);
             }];
        }
    }
    [self methodForGetTotalPrice];
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"is_recipe"];
    [[NSUserDefaults standardUserDefaults] setObject:[[_recipeData objectAtIndex:_index] objectForKey:@"resturant_id"] forKey:@"restaurant_id"];
    [[NSUserDefaults standardUserDefaults] setObject:[[_recipeData objectAtIndex:_index] objectForKey:@"delivery_charge"] forKey:@"delivery_charge"];
    [[AppDelegate shareInstance] showAlertWithErrorMessage:[NSString stringWithFormat:@"%@ are not available",stockStr]];
}
-(void)methodForGetTotalPrice
{
    [self.view endEditing:YES];
    [[CustomClasssesForCoreData sharedManagerForCoreData] methodForFetchValues:nil onCompletion:^(NSArray *response)
     {
         NSLog(@"%@",response);
         arrayForCoreDatavalues = [response mutableCopy];
     }];
    NSString *count = [NSString stringWithFormat:@"%lu",(unsigned long)arrayForCoreDatavalues.count];
    [[NSUserDefaults standardUserDefaults]setObject:count forKey:@"cartCount"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"cartCount"]!=nil)
    {
        lblCount.text=[[NSUserDefaults standardUserDefaults] valueForKey:@"cartCount"];
    }
    else
    {
        lblCount.text=@"";
    }
}
#pragma mark - Button Actions
- (IBAction)btnActionBack:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)btnMenuAction:(id)sender
{
    [self.menuContainerViewController toggleLeftSideMenuCompletion:nil];
}
#pragma mark - UITextField Methods

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
@end
