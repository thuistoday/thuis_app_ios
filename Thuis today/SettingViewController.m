//
//  SettingViewController.m
//  Thuis today
//
//  Created by Apple on 04/04/17.
//  Copyright © 2017 IMMANENT. All rights reserved.
//

#import "SettingViewController.h"

@interface SettingViewController ()<UITableViewDataSource,UITableViewDelegate>

@end

@implementation SettingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [CommonMethods configureNavigationBarForViewController:self withTitle:@"Settings"];
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [[NSUserDefaults standardUserDefaults]setObject:@"no" forKey:@"isCartView"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    lblCount.layer.cornerRadius = lblCount.frame.size.width/2;
    lblCount.layer.masksToBounds=YES;
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"cartCount"]!=nil) {
        lblCount.text=[[NSUserDefaults standardUserDefaults] valueForKey:@"cartCount"];
    }
    else
    {
        lblCount.text=@"";
    }
}
#pragma mark -
#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ( [[[NSUserDefaults standardUserDefaults] objectForKey:@"userData"] count])
    {
        if([[[NSUserDefaults standardUserDefaults] valueForKey:@"Login_Type"] isEqualToString:@"facebook"])
        {
            return 1;
        }
        else
        return 2;
    }
    else
    {
        return 1;
    }
  
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier;
    UITableViewCell *cell ;
   
    if ( [[[NSUserDefaults standardUserDefaults] objectForKey:@"userData"] count])
    {
        if([[[NSUserDefaults standardUserDefaults] valueForKey:@"Login_Type"] isEqualToString:@"facebook"])
        {
             CellIdentifier = @"cell3";
                cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
                return cell;
        }
        else
        {
        if(indexPath.row ==0)
        {
            CellIdentifier = @"cell1";
            cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            return cell;
        }
        else
        { CellIdentifier = @"cell3";
            cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            return cell;
            
        }
        }
    }
    else
    {
            CellIdentifier = @"cell2";
            cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            return cell;
    }
    
}


- (CGFloat)tableView:(UITableView* )tableView heightForRowAtIndexPath:(NSIndexPath* )indexPath
{
    if ( IDIOM == IPAD )
    {
      
        return 110;
       
    }
    else
    {
      
        return 65;
    }
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if ( [[[NSUserDefaults standardUserDefaults] objectForKey:@"userData"] count])
    {
        if([[[NSUserDefaults standardUserDefaults] valueForKey:@"Login_Type"] isEqualToString:@"facebook"])
        {
//            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"userData"];
//            [[NSUserDefaults standardUserDefaults] synchronize];
//            [[CustomClasssesForCoreData sharedManagerForCoreData] methodForDeleteAllValues:nil onCompletion:^(NSArray *response)
//             {
//                 [[NSUserDefaults standardUserDefaults]setObject:@"0" forKey:@"cartCount"];;
//             }];
//            [[NSNotificationCenter defaultCenter] postNotificationName:@"MyCacheUpdatedNotification" object:self];
//            ViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
//            [self.navigationController pushViewController:newView animated:YES];
            
            [[AppDelegate shareInstance]showActivityIndicator];
            NSString *token;
            if([[NSUserDefaults standardUserDefaults] valueForKey:@"Device_Token"])
            {
                token = [[NSUserDefaults standardUserDefaults] valueForKey:@"Device_Token"];
            }
            else
            {
                token = @"123456";
            }
            NSDictionary *params = @{
                                     @"user_id":[[[NSUserDefaults standardUserDefaults] objectForKey:@"userData"] objectForKey:@"id"],
                                     @"token":token,
                                     };
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [[SingletonClass sharedManager] m_PostApiResponse:@"logout" paramDiction:params islogout:YES  onCompletion:^(NSDictionary *response)
                 {
                     NSDictionary *responseDic = (NSDictionary *)response;
                     if ([[[responseDic objectForKey:@"payload"]valueForKey:@"status"]integerValue]==1)
                     {
                         [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"userData"];
                         [[NSUserDefaults standardUserDefaults] synchronize];
                         [[CustomClasssesForCoreData sharedManagerForCoreData] methodForDeleteAllValues:nil onCompletion:^(NSArray *response)
                          {
                              [[NSUserDefaults standardUserDefaults]setObject:@"0" forKey:@"cartCount"];;
                          }];
                         [[NSNotificationCenter defaultCenter] postNotificationName:@"MyCacheUpdatedNotification" object:self];
                         ViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
                         [self.navigationController pushViewController:newView animated:YES];
                         [[AppDelegate shareInstance] showAlertWithErrorMessage:[[responseDic objectForKey:@"payload"] valueForKey:@"message"]];
                         [[AppDelegate shareInstance]hideActivityIndicator];
                     }
                     else
                     {
                         [[AppDelegate shareInstance]hideActivityIndicator];
                         dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.6 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                             [self.view endEditing:YES];
                             [[AppDelegate shareInstance] showAlertWithErrorMessage:[[responseDic objectForKey:@"payload"] valueForKey:@"message"]];
                         });
                         
                     }
                 }];
            });

        }
        else
        {
        if (indexPath.row==0) {
            ChangePasswordViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"ChangePasswordViewController"];
            [self.navigationController pushViewController:newView animated:YES];
        }
        else if(indexPath.row==1)
        {
//            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"userData"];
//            [[NSUserDefaults standardUserDefaults] synchronize];
//            [[CustomClasssesForCoreData sharedManagerForCoreData] methodForDeleteAllValues:nil onCompletion:^(NSArray *response)
//             {
//                 [[NSUserDefaults standardUserDefaults]setObject:@"0" forKey:@"cartCount"];;
//             }];
//            [[NSNotificationCenter defaultCenter] postNotificationName:@"MyCacheUpdatedNotification" object:self];
//            ViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
//            [self.navigationController pushViewController:newView animated:YES];
            
            
            [[AppDelegate shareInstance]showActivityIndicator];
            NSString *token;
            if([[NSUserDefaults standardUserDefaults] valueForKey:@"Device_Token"])
            {
                token = [[NSUserDefaults standardUserDefaults] valueForKey:@"Device_Token"];
            }
            else
            {
                token = @"123456";
            }
            NSDictionary *params = @{
                                     @"user_id":[[[NSUserDefaults standardUserDefaults] objectForKey:@"userData"] objectForKey:@"id"],
                                     @"token":token,
                                     };
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [[SingletonClass sharedManager] m_PostApiResponse:@"logout" paramDiction:params islogout:YES  onCompletion:^(NSDictionary *response)
                 {
                     NSDictionary *responseDic = (NSDictionary *)response;
                     if ([[[responseDic objectForKey:@"payload"]valueForKey:@"status"]integerValue]==1)
                     {
                         [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"userData"];
                         [[NSUserDefaults standardUserDefaults] synchronize];
                         [[CustomClasssesForCoreData sharedManagerForCoreData] methodForDeleteAllValues:nil onCompletion:^(NSArray *response)
                          {
                              [[NSUserDefaults standardUserDefaults]setObject:@"0" forKey:@"cartCount"];;
                          }];
                         [[NSNotificationCenter defaultCenter] postNotificationName:@"MyCacheUpdatedNotification" object:self];
                         ViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
                         [self.navigationController pushViewController:newView animated:YES];
                         [[AppDelegate shareInstance] showAlertWithErrorMessage:[[responseDic objectForKey:@"payload"] valueForKey:@"message"]];
                         [[AppDelegate shareInstance]hideActivityIndicator];
                     }
                     else
                     {
                         [[AppDelegate shareInstance]hideActivityIndicator];
                         dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.6 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                             [self.view endEditing:YES];
                             [[AppDelegate shareInstance] showAlertWithErrorMessage:[[responseDic objectForKey:@"payload"] valueForKey:@"message"]];
                         });
                         
                     }
                 }];
            });
        }
        }
    }
}


- (IBAction)btnProfileAction:(id)sender
{
    if ( [[[NSUserDefaults standardUserDefaults] objectForKey:@"userData"] count])
    {
        UserProfileViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"UserProfileViewController"];
        [self.navigationController pushViewController:newView animated:YES];
    }
    else
    {
        LoginRegistrationViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginRegistrationViewController"];
        [self.navigationController pushViewController:newView animated:YES];
    }
}

- (IBAction)btnHomeAction:(id)sender
{
    ViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
    [self.navigationController pushViewController:newView animated:YES];
}

- (IBAction)btnCartAction:(id)sender
{
    CartViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"CartViewController"];
    [self.navigationController pushViewController:newView animated:YES];
}

- (IBAction)btnMenuAction:(id)sender
{
      [self.menuContainerViewController toggleLeftSideMenuCompletion:nil];
}

- (IBAction)btnLanguage:(id)sender {
}
#pragma  -mark method for recieve memory warning
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)btnBottomBackAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnBottomCartAction:(id)sender
{
    CartViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"CartViewController"];
    [self.navigationController pushViewController:newView animated:YES];
}

- (IBAction)btnBottomHomeAction:(id)sender
{
    ViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
    [self.navigationController pushViewController:newView animated:YES];
}


@end
