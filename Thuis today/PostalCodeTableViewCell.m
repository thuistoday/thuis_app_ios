//
//  PostalCodeTableViewCell.m
//  Thuis today
//
//  Created by Rajeev Lochan Ranga on 26/02/18.
//  Copyright © 2018 Offshore Software Solutions. All rights reserved.
//

#import "PostalCodeTableViewCell.h"

@implementation PostalCodeTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


- (IBAction)txtFld_PostCode_action:(UITextField *)sender {
    if ([self.delegate respondsToSelector:@selector(txtFld_PostCode_action:forPostalCodeTableViewCell:)]) {
        [self.delegate txtFld_PostCode_action:sender forPostalCodeTableViewCell:self];
    }
}
- (IBAction)txtFld_streetCode_action:(UITextField *)sender {
    if ([self.delegate respondsToSelector:@selector(txtFld_streetCode_action:forPostalCodeTableViewCell:)]) {
        [self.delegate txtFld_streetCode_action:sender forPostalCodeTableViewCell:self];
    }
}

- (IBAction)txtFld_HouseCode_action:(UITextField *)sender {
    if ([self.delegate respondsToSelector:@selector(txtFld_HouseCode_action:forPostalCodeTableViewCell:)]) {
        [self.delegate txtFld_HouseCode_action:sender forPostalCodeTableViewCell:self];
    }
}
- (IBAction)buttonEdit_action:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(buttonEdit_action:forPostalCodeTableViewCell:)]) {
        [self.delegate buttonEdit_action:sender forPostalCodeTableViewCell:self];
    }
    
}

@end
