//
//  Product+CoreDataClass.h
//  
//
//  Created by IMMANENT on 20/02/17.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface Product : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "Product+CoreDataProperties.h"
