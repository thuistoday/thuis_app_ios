//
//  NewProductDetailsViewController.h
//  Thuis today
//
//  Created by offshore_mac_1 on 13/01/18.
//  Copyright © 2018 IMMANENT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewProductDetailsViewController : UIViewController
{
    
    IBOutlet UILabel *lblCount;
    IBOutlet UILabel *lblShopName;
    IBOutlet UILabel *lblDescription;
    IBOutlet UIImageView *imgShop;
    IBOutlet UILabel *lblShopAddress;
    IBOutlet UILabel *lblVat;
    IBOutlet UILabel *lblOriginalPrice;
    IBOutlet UILabel *lblTotalPrice;
    IBOutlet UITableView *tbInfo;
    
    IBOutlet UITextField *txtQuantity;
    
    __weak IBOutlet UIView *viewData;
    IBOutlet UILabel *lblUnit;
    IBOutlet UIImageView *imgProduct;
    
    IBOutlet UILabel *lblProductName;
    
    __weak IBOutlet UIScrollView *scrollView;
    IBOutlet UILabel *lblProductInfo;
}
- (IBAction)btnBackAction:(id)sender;
- (IBAction)btnAddToCartActiion:(id)sender;
- (IBAction)btnProfileAction:(id)sender;
- (IBAction)btnHomeAction:(id)sender;
- (IBAction)btnMenuAction:(id)sender;
- (IBAction)btnCartAction:(id)sender;

- (IBAction)btnBottomBackAction:(id)sender;
- (IBAction)btnBottomCartAction:(id)sender;
- (IBAction)btnBottomHomeAction:(id)sender;
@property NSArray *arrayForProductInfo;
@end
