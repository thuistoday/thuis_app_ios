//
//  SideMenuViewController.m
//  MFSideMenuDemo
//
//  Created by Michael Frederick on 3/19/12.

#import "SideMenuViewController.h"
#import "MFSideMenu.h"
#import "ViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "AboutUsViewController.h"
#import "PrivacyPolicyWebViewController.h"
@interface SideMenuViewController()
{
    NSMutableArray *arrayForTitle,*arrayForImages,*arrayForTitleNew,*arrayForImagesNew;
}
@end
@implementation SideMenuViewController
-(void)viewDidLoad
{
    arrayForTitle =[[NSMutableArray alloc]initWithObjects:NSLocalizedString(@"HOME", @"Cancel"),NSLocalizedString(@"Settings", @"Cancel"),NSLocalizedString(@"Need Help?", @"Cancel"),NSLocalizedString(@"ABOUT", @"Cancel"),NSLocalizedString(@"Privacy", @"Cancel"),  nil];
    arrayForImages = [[NSMutableArray alloc]initWithObjects:@"home-icon@1x",@"setting-icon@1x",@"help-icon",@"about-icon@1x",@"privacy-icon.png", nil];
    arrayForTitleNew =[[NSMutableArray alloc]initWithObjects:NSLocalizedString(@"HOME", @"Cancel"),NSLocalizedString(@"Need Help?", @"Cancel"),NSLocalizedString(@"ABOUT", @"Cancel"),NSLocalizedString(@"Privacy", @"Cancel"), nil];
    arrayForImagesNew = [[NSMutableArray alloc]initWithObjects:@"home-icon@1x",@"help-icon",@"about-icon@1x",@"privacy-icon.png", nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(cacheUpdated) name:@"MyCacheUpdatedNotification" object:nil];
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [tblSideMenu reloadData];
}
-(void)cacheUpdated
{
    [tblSideMenu reloadData];
}

#pragma mark -
#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if ( [[[NSUserDefaults standardUserDefaults] objectForKey:@"userData"] count])
    {
        return 6;
    }
    else
    {
        return 7;
    }
  
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *CellIdentifier;
  
    UITableViewCell *cell ;
    if ( [[[NSUserDefaults standardUserDefaults] objectForKey:@"userData"] count])
    {
        NSLog(@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"userData"]);
        if (indexPath.row ==0) {
            CellIdentifier = @"cell";
            cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            UIImageView *imgForProPic = (UIImageView*)[cell viewWithTag:303];
            //imgForProPic.frame = CGRectMake(38,4,100,100);
            imgForProPic.layer.cornerRadius=imgForProPic.frame.size.width/2;;
            imgForProPic.clipsToBounds = true;
            if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"userData"] objectForKey:@"profile_pic"]) {
                [imgForProPic sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",baseUrlForImageProfile,[[[NSUserDefaults standardUserDefaults] objectForKey:@"userData"] objectForKey:@"profile_pic"]]]
                                placeholderImage:[UIImage imageNamed:@"signin-icon@1x"]];
            }
        }
        else
        {
            CellIdentifier = @"cell2";
            cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            UIImageView *imgForProPic = (UIImageView*)[cell viewWithTag:301];
            imgForProPic.layer.cornerRadius=imgForProPic.frame.size.width/2;
            imgForProPic.layer.masksToBounds=YES;
            UILabel *lblTitle = (UILabel*)[cell viewWithTag:302];
            lblTitle.text =[arrayForTitle objectAtIndex:indexPath.row-1];
            imgForProPic.image =[UIImage imageNamed:[arrayForImages objectAtIndex:indexPath.row-1]];
        }

    }
    else
    {
        if (indexPath.row ==0) {
            CellIdentifier = @"cell";
            cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            UIImageView *imgForProPic = (UIImageView*)[cell viewWithTag:303];
            imgForProPic.layer.cornerRadius=imgForProPic.frame.size.width/2;
            imgForProPic.layer.masksToBounds=YES;
            imgForProPic.image =[UIImage imageNamed:@"signin-icon@1x"];
        }
        else if(indexPath.row ==1 ||indexPath.row ==2 )
        {
            CellIdentifier = @"cell1";
            cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            UILabel *imgForSignUp = (UILabel*)[cell viewWithTag:201];
            imgForSignUp.layer.cornerRadius= 5;
            imgForSignUp.layer.masksToBounds=YES;
            if (indexPath.row ==1) {
                imgForSignUp.text = NSLocalizedString(@"Sign In", @"Cancel");
                
            }
            else if(indexPath.row ==2)
            {
                imgForSignUp.text = NSLocalizedString(@"Register", @"Cancel");
            }
        }
        else
        {
            CellIdentifier = @"cell2";
            cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            UIImageView *imgForProPic = (UIImageView*)[cell viewWithTag:301];
            imgForProPic.layer.cornerRadius=imgForProPic.frame.size.width/2;
            imgForProPic.layer.masksToBounds=YES;
            UILabel *lblTitle = (UILabel*)[cell viewWithTag:302];
            lblTitle.text =[arrayForTitleNew objectAtIndex:indexPath.row-3];
            imgForProPic.image =[UIImage imageNamed:[arrayForImagesNew objectAtIndex:indexPath.row-3]];
        }

    }
    
    return cell;
}

#pragma mark -
#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ( [[[NSUserDefaults standardUserDefaults] objectForKey:@"userData"] count])
    {
        
        if(indexPath.row ==0)
        {
            if ( [[[NSUserDefaults standardUserDefaults] objectForKey:@"userData"] count])
            {
                UserProfileViewController *profileObj = [self.storyboard instantiateViewControllerWithIdentifier:@"UserProfileViewController"];
                
                UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
                NSArray *controllers = [NSArray arrayWithObject:profileObj];
                navigationController.viewControllers = controllers;
                [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
            }
            else
            {
                [[NSUserDefaults standardUserDefaults]setObject:@"no" forKey:@"isCartView"];
                [[NSUserDefaults standardUserDefaults]synchronize];
                LoginRegistrationViewController *LoginObj = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginRegistrationViewController"];
                
                UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
                NSArray *controllers = [NSArray arrayWithObject:LoginObj];
                navigationController.viewControllers = controllers;
                [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
                
            }
        }
        else if (indexPath.row==1)
        {
          //  if (IDIOM == IPAD) {
                ViewController*LoginObj = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
                
                UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
                NSArray *controllers = [NSArray arrayWithObject:LoginObj];
                navigationController.viewControllers = controllers;
                [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
//            }
//            else {
//                ViewController*LoginObj = [self.storyboard instantiateViewControllerWithIdentifier:@"DashBoardViewController"];
//
//                UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
//                NSArray *controllers = [NSArray arrayWithObject:LoginObj];
//                navigationController.viewControllers = controllers;
//                [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
//            }
        }
        else if (indexPath.row==2)
        {
            SettingViewController*LoginObj = [self.storyboard instantiateViewControllerWithIdentifier:@"SettingViewController"];
            UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
            NSArray *controllers = [NSArray arrayWithObject:LoginObj];
            navigationController.viewControllers = controllers;
            [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
        }
        else if(indexPath.row==3)
        {
            NeedAHelpViewController*LoginObj = [self.storyboard instantiateViewControllerWithIdentifier:@"NeedAHelpViewController"];
            
            UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
            NSArray *controllers = [NSArray arrayWithObject:LoginObj];
            navigationController.viewControllers = controllers;
            [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
        }
        else if(indexPath.row==4)
        {
            AboutUsViewController*LoginObj = [self.storyboard instantiateViewControllerWithIdentifier:@"AboutUsViewController"];
            
            UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
            NSArray *controllers = [NSArray arrayWithObject:LoginObj];
            navigationController.viewControllers = controllers;
            [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
        }
        else if(indexPath.row==5)
        {
            PrivacyPolicyWebViewController*LoginObj = [self.storyboard instantiateViewControllerWithIdentifier:@"PrivacyPolicyWebViewController"];
            
            UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
            NSArray *controllers = [NSArray arrayWithObject:LoginObj];
            navigationController.viewControllers = controllers;
            [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
        }
    }
    else
    {
        
        if(indexPath.row ==0)
        {
            if ( [[[NSUserDefaults standardUserDefaults] objectForKey:@"userData"] count])
            {
                UserProfileViewController *profileObj = [self.storyboard instantiateViewControllerWithIdentifier:@"UserProfileViewController"];
                
                UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
                NSArray *controllers = [NSArray arrayWithObject:profileObj];
                navigationController.viewControllers = controllers;
                [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
                
            }
            else
            {
                [[NSUserDefaults standardUserDefaults]setObject:@"no" forKey:@"isCartView"];
                [[NSUserDefaults standardUserDefaults]synchronize];
                LoginRegistrationViewController *LoginObj = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginRegistrationViewController"];
                
                UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
                NSArray *controllers = [NSArray arrayWithObject:LoginObj];
                navigationController.viewControllers = controllers;
                [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
                
            }
        }
        else if (indexPath.row==1)
        {
            LoginRegistrationViewController *LoginObj = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginRegistrationViewController"];
            LoginObj.checkForDisplayView = @"Login";
            UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
            NSArray *controllers = [NSArray arrayWithObject:LoginObj];
            navigationController.viewControllers = controllers;
            [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
            
        }
        else if(indexPath.row==2)
        {
            LoginRegistrationViewController *LoginObj = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginRegistrationViewController"];
            LoginObj.checkForDisplayView = @"Register";

            UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
            NSArray *controllers = [NSArray arrayWithObject:LoginObj];
            navigationController.viewControllers = controllers;
            [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
        }
        else if (indexPath.row==3)
        {
           // if (IDIOM == IPAD) {
                ViewController*LoginObj = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
                
                UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
                NSArray *controllers = [NSArray arrayWithObject:LoginObj];
                navigationController.viewControllers = controllers;
                [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
          //  }
//            else {
//                ViewController*LoginObj = [self.storyboard instantiateViewControllerWithIdentifier:@"DashBoardViewController"];
//
//                UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
//                NSArray *controllers = [NSArray arrayWithObject:LoginObj];
//                navigationController.viewControllers = controllers;
//                [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
//            }
            
        }
        else if(indexPath.row==4)
        {
            NeedAHelpViewController*LoginObj = [self.storyboard instantiateViewControllerWithIdentifier:@"NeedAHelpViewController"];
            
            UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
            NSArray *controllers = [NSArray arrayWithObject:LoginObj];
            navigationController.viewControllers = controllers;
            [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
        }
        else if(indexPath.row==5)
        {
            AboutUsViewController*LoginObj = [self.storyboard instantiateViewControllerWithIdentifier:@"AboutUsViewController"];
            
            UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
            NSArray *controllers = [NSArray arrayWithObject:LoginObj];
            navigationController.viewControllers = controllers;
            [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
        }
        else if(indexPath.row==6)
        {
            PrivacyPolicyWebViewController*LoginObj = [self.storyboard instantiateViewControllerWithIdentifier:@"PrivacyPolicyWebViewController"];
            
            UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
            NSArray *controllers = [NSArray arrayWithObject:LoginObj];
            navigationController.viewControllers = controllers;
            [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
        }
    }
    
    
    
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ( [[[NSUserDefaults standardUserDefaults] objectForKey:@"userData"] count])
    {
        if ( IDIOM == IPAD )
        {
            if (indexPath.row ==0) {
                return 140;
            }
            else
            {
                return 140;
            }
        }
        else
        {
            if (indexPath.row ==0)
            {
                return 63;
            }
            else
            {
                return 63;
            }
        }

    }
    else{
        if ( IDIOM == IPAD )
        {
            if (indexPath.row ==0) {
                return 140;
            }
            else if(indexPath.row ==1 ||indexPath.row ==2 )
            {
                return 50;
            }
            else
            {
                return 140;
            }
        }
        else
        {
            if (indexPath.row ==0)
            {
                return 63;
            }
            else if(indexPath.row ==1 ||indexPath.row ==2 )
            {
                return 30;
            }
            else
            {
                return 63;
            }
        }

    }
            return 140;
}
@end
