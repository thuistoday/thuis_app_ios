//
//  EditProfileViewController.h
//  Thuis today
//
//  Created by IMMANENT on 04/03/17.
//  Copyright © 2017 IMMANENT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MFSideMenu.h"
@interface EditProfileViewController : UIViewController
{
     IBOutlet UILabel *lblCountAction;
     IBOutlet UITableView *tblForFields;
     IBOutlet UIView *viewForReviewLabel;
}
- (IBAction)btnUpdateAction:(id)sender;
- (IBAction)btnMenuAction:(id)sender;
- (IBAction)btnCartAction:(id)sender;

@end
