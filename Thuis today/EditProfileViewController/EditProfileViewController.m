//
//  EditProfileViewController.m
//  Thuis today
//
//  Created by IMMANENT on 04/03/17.
//  Copyright © 2017 IMMANENT. All rights reserved.
//

#import "EditProfileViewController.h"
#import "PostalCodeTableViewCell.h"
#import "Constants.h"

@interface EditProfileViewController ()<UITextFieldDelegate, PostalCodeTableViewCellDelegate>
{
    NSMutableArray *arrayForPlaceholder,*arrayForuserData,*arraySearchData;
    NSMutableDictionary *dicForTextFieldvalues;
    NSIndexPath *newIndexPath;
    BOOL checkForEmptyField;
}
@property (weak, nonatomic) IBOutlet UITableView *tableSearch;
@property (nonatomic,strong) NSMutableDictionary * dict_postalCodeDetails;

@end

@implementation EditProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [CommonMethods configureNavigationBarForViewController:self withTitle:@"My Account"];
    arrayForPlaceholder = [[NSMutableArray alloc]initWithObjects:NSLocalizedString(@"Name", @"Cancel"),NSLocalizedString(@"Address", @"Cancel"),NSLocalizedString(@"Postcode", @"Cancel"),NSLocalizedString(@"city", @"Cancel"),NSLocalizedString(@"Phone number", @"Cancel"), nil];
    arrayForuserData = [[NSMutableArray alloc]init];
    arraySearchData = [[NSMutableArray alloc]init];
    
    dicForTextFieldvalues= [[NSMutableDictionary alloc]init];
    
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    viewForReviewLabel.layer.borderWidth = 1.0;
    viewForReviewLabel.layer.borderColor =[UIColor lightGrayColor].CGColor;
    viewForReviewLabel.layer.masksToBounds = YES;
    
    arrayForuserData =[[NSUserDefaults standardUserDefaults] objectForKey:@"userData"];
    
    [dicForTextFieldvalues setObject:[arrayForuserData valueForKey:@"address"] forKey:@"address"];
    [dicForTextFieldvalues setObject:[arrayForuserData valueForKey:@"city"] forKey:@"city"];
    [dicForTextFieldvalues setObject:[arrayForuserData valueForKey:@"name"] forKey:@"name"];
    [dicForTextFieldvalues setObject:[arrayForuserData valueForKey:@"zipcode"] forKey:@"zipcode"];
    [dicForTextFieldvalues setObject:[arrayForuserData valueForKey:@"phone"] forKey:@"phone"];
    
    NSArray *arr = [[arrayForuserData valueForKey:@"zipcode"] componentsSeparatedByString:@"-"];
    if (arr.count == 2) {
        self.dict_postalCodeDetails = [NSMutableDictionary dictionaryWithDictionary: @{kPostalCode : [arr firstObject],kStreetCode : [arr objectAtIndex:1], kHouseNumber : @""}];
    }
    else if (arr.count == 3) {
        self.dict_postalCodeDetails = [NSMutableDictionary dictionaryWithDictionary:@{kPostalCode : [arr firstObject],kStreetCode : [arr objectAtIndex:1], kHouseNumber : [arr lastObject] }];
    }
    
    lblCountAction.layer.cornerRadius = lblCountAction.frame.size.width/2;
    lblCountAction.layer.masksToBounds=YES;
    
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"cartCount"]!=nil) {
        lblCountAction.text=[[NSUserDefaults standardUserDefaults] valueForKey:@"cartCount"];
    }
    else
    {
        lblCountAction.text=@"";
    }
    [[NSUserDefaults standardUserDefaults]setObject:@"no" forKey:@"isCartView"];
    [[NSUserDefaults standardUserDefaults]synchronize];
}

#pragma mark -
#pragma mark - Postal Code

- (void)validatePostCode:(UITextField *)textField forCell:(PostalCodeTableViewCell *)Cell {
    if (textField.text.length >= 4) {
        if ([self checktext:Cell.txtFld_postCode.text forType:TextTypeNumeric]) {
            [self enableStreetCode:YES forCell:Cell];
            [Cell.txtFld_StreetCode becomeFirstResponder];
        }
        else {
            [self enableStreetCode:NO forCell:Cell];
            [self showAlertWithTitle:@"Wrong Input" message:@"Enter valid Post code. post code accepts only numbers"];
        }
    }
}


- (void)validateStreetCode:(UITextField *)textField forCell:(PostalCodeTableViewCell *)Cell {
    if (textField.text.length >= 2) {
        if ([self checktext:Cell.txtFld_StreetCode.text forType:TextTypeAlphates]) {
            [self enableCityCode:YES forCell:Cell];
            [Cell.txtFld_HouseCode becomeFirstResponder];
        }
        else {
            [self enableCityCode:NO forCell:Cell];
            [self showAlertWithTitle:@"Wrong Input" message:@"Enter Valid street code"];
        }
    }
    else if([Cell.txtFld_StreetCode.text isEqualToString:@""]) {
        [Cell.txtFld_postCode becomeFirstResponder];
    }
}


- (void)validateHouseCode:(UITextField *)textField forCell:(PostalCodeTableViewCell *)Cell{
    if (Cell.txtFld_HouseCode.text.length >= 1) {
        if ([self checktext:Cell.txtFld_HouseCode.text forType:TextTypeNumeric]) {
            [Cell.txtFld_HouseCode resignFirstResponder];
            NSDictionary *postalCodeDetails = @{kPostalCode : Cell.txtFld_postCode.text, kStreetCode :Cell.txtFld_StreetCode.text, kHouseNumber : Cell.txtFld_HouseCode.text};
            [self searchAddressWithPostCodeDetails:postalCodeDetails];
            self.dict_postalCodeDetails = [NSMutableDictionary dictionaryWithDictionary:postalCodeDetails];
        }
        else {
            [self showAlertWithTitle:@"Wrong Input" message:@"Enter valid house number"];
        }
    }
    else if([Cell.txtFld_HouseCode.text isEqualToString:@""]) {
        [Cell.txtFld_HouseCode becomeFirstResponder];
    }
}

- (void)enableCityCode:(BOOL)value forCell:(PostalCodeTableViewCell *)Cell{
    Cell.txtFld_HouseCode.userInteractionEnabled = value;
}

- (void)enableStreetCode:(BOOL)value forCell:(PostalCodeTableViewCell *)Cell{
    Cell.txtFld_StreetCode.userInteractionEnabled = value;
}

- (BOOL)checktext:(NSString *)txtFld_string forType:(TextType)textType{
    NSCharacterSet *textCharacterSet;
    if (textType == TextTypeNumeric) {
        textCharacterSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
    }
    else if (textType == TextTypeAlphates) {
        textCharacterSet = NSCharacterSet.letterCharacterSet;
    }
    NSCharacterSet *characterSetFromTextField = [NSCharacterSet characterSetWithCharactersInString:txtFld_string];
    
    BOOL stringIsValid = [textCharacterSet isSupersetOfSet:characterSetFromTextField];
    return stringIsValid;
}

- (void)showAlertWithTitle:(NSString *)alertTitle message:(NSString *)alertMessage {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:alertTitle message:alertMessage preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *OKBtn = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:nil];
    [alert addAction:OKBtn];
    [self presentViewController:alert animated:YES completion:nil];
}

- (void)searchAddressWithPostCodeDetails:(NSDictionary *)postCodeDetails {
    NSDictionary *params = @{
                             @"wijkcode":[postCodeDetails valueForKey:kPostalCode],
                             @"lettercombinatie":[postCodeDetails valueForKey:kStreetCode],
                             @"huisnr":[postCodeDetails valueForKey:kHouseNumber],
                             };
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [[SingletonClass sharedManager] m_PostAddressSuggestionApiResponse:@"delivery-agent-get-address" paramDiction:params onCompletion:^(NSDictionary *response)
         {
             NSDictionary *responseDic = (NSDictionary *)response;
             if ([[[responseDic objectForKey:@"payload"]valueForKey:@"status"]integerValue]==1)
             {
                 arraySearchData = [[responseDic objectForKey:@"payload"] objectForKey:@"data"];
                 self.tableSearch.hidden = NO;
                 [self.tableSearch reloadData];
             }
             else
             {
                 dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.6 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                     [self.view endEditing:YES];
                     [[AppDelegate shareInstance] showAlertWithErrorMessage:[[responseDic objectForKey:@"payload"] valueForKey:@"message"]];
                     self.tableSearch.hidden = YES;
                 });
             }
         }];
    });
}


- (void)hideHouseCodePart:(BOOL)value forCell:(PostalCodeTableViewCell *)Cell {
    
    Cell.lbl_hiphenHouseCode.hidden = value;
    Cell.txtFld_HouseCode.text = @"";
    Cell.txtFld_HouseCode.hidden = value;
    if ([[self.dict_postalCodeDetails allKeys] containsObject:kHouseNumber]) {
        [self.dict_postalCodeDetails removeObjectForKey:kHouseNumber];
    }
}

#pragma mark - PostalCodeTableViewCellDelegate
- (void)txtFld_PostCode_action:(UITextField *)textField forPostalCodeTableViewCell:(PostalCodeTableViewCell *)PostalCodeTableViewCell {
    [self validatePostCode:textField forCell:PostalCodeTableViewCell];
}

- (void)txtFld_streetCode_action:(UITextField *)textField forPostalCodeTableViewCell:(PostalCodeTableViewCell *)PostalCodeTableViewCell {
    [self validateStreetCode:textField forCell:PostalCodeTableViewCell];
}

- (void)txtFld_HouseCode_action:(UITextField *)textField forPostalCodeTableViewCell:(PostalCodeTableViewCell *)PostalCodeTableViewCell {
    [self validateHouseCode:textField forCell:PostalCodeTableViewCell];
}

- (void)buttonEdit_action:(UIButton *)sender forPostalCodeTableViewCell:(PostalCodeTableViewCell *)PostalCodeTableViewCell {
    PostalCodeTableViewCell.txtFld_postCode.userInteractionEnabled = YES;
    PostalCodeTableViewCell.txtFld_StreetCode.userInteractionEnabled = YES;
    [PostalCodeTableViewCell.txtFld_StreetCode becomeFirstResponder];
    PostalCodeTableViewCell.txtFld_HouseCode.userInteractionEnabled = YES;
}

#pragma mark -
#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if(tableView == self.tableSearch) {
        return [arraySearchData count];
    }
    else {
        return 6;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == self.tableSearch) {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
        if (cell == nil)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"Cell"];
        }
        NSMutableDictionary *dict = [NSMutableDictionary new];
        dict = [arraySearchData objectAtIndex:indexPath.row];
        cell.textLabel.text =[NSString stringWithFormat:@"%@,%@",[dict objectForKey:@"straatnaam"],[dict objectForKey:@"plaatsnaam"]];
        return cell;
    }
    else {
        static NSString *CellIdentifier;
        UITableViewCell *cell ;
        if (indexPath.row ==5) {
            CellIdentifier = @"cell1";
            cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
            return cell;
        }
        else
        {
            if(indexPath.row == 1) {
                PostalCodeTableViewCell *postCodeCell = (PostalCodeTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"PostalCodeTableViewCell"];
                postCodeCell.delegate = self;
                postCodeCell.txtFld_postCode.text = [self.dict_postalCodeDetails valueForKey:kPostalCode];
                postCodeCell.txtFld_StreetCode.text = [self.dict_postalCodeDetails valueForKey:kStreetCode];
                [self hideHouseCodePart:YES forCell:postCodeCell];
                return postCodeCell;
            }
            else
            {
                CellIdentifier = @"cell";
                cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
                UITextField *txtField=(UITextField *)[cell viewWithTag:101];
                UIImageView *image = (UIImageView*)[cell viewWithTag:201];
                txtField.placeholder =[arrayForPlaceholder objectAtIndex:indexPath.row];
                
                // UIColor *color = [UIColor colorWithRed:210/255.0f green:45/255.0f blue:62/255.0f alpha:1];
                UIColor *color = [UIColor lightGrayColor];
                
                [txtField setValue:color forKeyPath:@"_placeholderLabel.textColor"];
                txtField.placeholder =[arrayForPlaceholder objectAtIndex:indexPath.row];
                
                if (indexPath.row ==0) {
                    txtField.text =[dicForTextFieldvalues valueForKey:@"name"];
                    image.image=[UIImage imageNamed:@"user-icon.png"];
                    [txtField setKeyboardType:UIKeyboardTypeASCIICapable];
                    
                }
                else if(indexPath.row ==2)
                {
                    txtField.text = [dicForTextFieldvalues valueForKey:@"address"];
                    image.image=[UIImage imageNamed:@"address-map.png"];
                    [txtField setKeyboardType:UIKeyboardTypeASCIICapable];
                    txtField.userInteractionEnabled = NO;
                    
                }
                //        else if(indexPath.row ==2)
                //        {
                //            txtField.text = [dicForTextFieldvalues valueForKey:@"zipcode"];
                //            image.image=[UIImage imageNamed:@"location-icon.png"];
                //            [txtField setKeyboardType:UIKeyboardTypeASCIICapable];
                //
                //        }
                else if(indexPath.row ==3)
                {
                    txtField.userInteractionEnabled = NO;
                    txtField.text = [dicForTextFieldvalues valueForKey:@"city"];
                    image.image=[UIImage imageNamed:@"city-icon.png"];
                    [txtField setKeyboardType:UIKeyboardTypeASCIICapable];
                    
                }
                else if(indexPath.row ==4)
                {
                    txtField.text = [dicForTextFieldvalues valueForKey:@"phone"];
                    image.image=[UIImage imageNamed:@"phone-icon.png"];
                    [txtField setKeyboardType:UIKeyboardTypeNumberPad];
                }
            }
        }
        return cell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == self.tableSearch)
    {
        NSMutableDictionary *dict = [NSMutableDictionary new];
        dict = [arraySearchData objectAtIndex:indexPath.row];
        NSString *postCode=[NSString stringWithFormat:@"%@-%@",[dict objectForKey:@"wijkcode"],[dict objectForKey:@"lettercombinatie"]];
        self.tableSearch.hidden = YES;
        PostalCodeTableViewCell *Cell = [tblForFields cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
        [self hideHouseCodePart:YES forCell:Cell];
        
        [dicForTextFieldvalues setObject:[NSString stringWithFormat:@"%@",[dict objectForKey:@"straatnaam"]] forKey:@"address"];
        [dicForTextFieldvalues setObject:[dict objectForKey:@"plaatsnaam"] forKey:@"city"];
        //[dicForTextFieldvalues setObject:txtPostCodeValue forKey:@"zipcode"];
        [dicForTextFieldvalues setObject:postCode forKey:@"zipcode"];
        [tblForFields reloadData];
    }
}

#pragma mark - UITextField delegates
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    CGPoint origin = textField.frame.origin;
    CGPoint point = [textField.superview convertPoint:origin toView:tblForFields];
    NSIndexPath *postalCodeCell_IndexPath = [NSIndexPath indexPathForRow:1 inSection:0];
    newIndexPath = [tblForFields indexPathForRowAtPoint:point];
    if (newIndexPath  == postalCodeCell_IndexPath) {
        PostalCodeTableViewCell *postCodeCell = [tblForFields cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
        
        
        
        if(textField == postCodeCell.txtFld_StreetCode) {
            UITableViewCell *addressCell = [tblForFields cellForRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:0]];
            UITableViewCell *cityCell = [tblForFields cellForRowAtIndexPath:[NSIndexPath indexPathForRow:3 inSection:0]];
            
            UITextField *address_txtField=(UITextField *)[addressCell viewWithTag:101];
            address_txtField.text = @"";
            
            UITextField *city_txtField=(UITextField *)[cityCell viewWithTag:101];
            city_txtField.text = @"";
            [dicForTextFieldvalues setObject:@"" forKey:@"address"];
            [dicForTextFieldvalues setObject:@"" forKey:@"city"];

            [self hideHouseCodePart:NO forCell:postCodeCell];
            
        }
    }
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    CGPoint origin = textField.frame.origin;
    CGPoint point = [textField.superview convertPoint:origin toView:tblForFields];
    newIndexPath = [tblForFields indexPathForRowAtPoint:point];
    
    NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    newString =     [newString stringByTrimmingCharactersInSet:
                     [NSCharacterSet whitespaceCharacterSet]];
    if (newIndexPath.row ==0) {
        [dicForTextFieldvalues setObject:newString forKey:@"name"];
        
    }
    else if(newIndexPath.row ==2)
    {
        [dicForTextFieldvalues setObject:newString forKey:@"address"];
    }
    else if(newIndexPath.row ==1)
    {
        [dicForTextFieldvalues setObject:newString forKey:@"zipcode"];
        
        BOOL isValidate = NO;
        if(range.length + range.location > textField.text.length)
        {
            isValidate = NO;
        }
        
        PostalCodeTableViewCell *Cell = [tblForFields cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        if(textField == Cell.txtFld_postCode) {
            isValidate =  (newLength <=4)?YES:NO;
            if (newLength == 5) {
                [self validatePostCode:textField forCell:Cell];
            }
        }
        else if (textField == Cell.txtFld_StreetCode) {
            isValidate =  (newLength <=2)?YES:NO;
            if (newLength == 3) {
                [self validateStreetCode:textField forCell:Cell];
            }
            
        }
        else if (textField == Cell.txtFld_HouseCode) {
            isValidate =  (newLength <=2)?YES:NO;
            
        }
        return isValidate;
    }
    else if(newIndexPath.row ==3)
    {
        [dicForTextFieldvalues setObject:newString forKey:@"city"];
    }
    else if(newIndexPath.row ==4)
    {
        if ([newString length] >= 10)
            newString = [newString substringToIndex:10];
        else
            newString = newString;
        [dicForTextFieldvalues setObject:newString forKey:@"phone"];
        if ([string rangeOfCharacterFromSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].location != NSNotFound)
        {
            return NO;
        }
        if(range.length + range.location > textField.text.length)
        {
            return NO;
        }
        
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return newLength <=10;
        
        
        
    }
    [tblForFields beginUpdates];
    [tblForFields endUpdates];
    return true;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITableViewCell* cell = (UITableViewCell*)(tblForFields);
    UITextField *txtFld = (UITextField *)[cell viewWithTag:101];
    [txtFld resignFirstResponder];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)btnUpdateAction:(id)sender
{
    
    for (id currentValue in [dicForTextFieldvalues allValues])
    {
        if (![currentValue isEqualToString:@""])
        {
            checkForEmptyField = true;
        }
        else
        {
            checkForEmptyField = false;
            
            [[AppDelegate shareInstance]showAlertWithErrorMessage:NSLocalizedString(@"Please fill all fields.", @"Cancel")];
            return;
        }
    }
    if (checkForEmptyField == true)
    {
        [self methodForUpdatevalues];
    }
    else
    {
        
    }
}

- (IBAction)btnMenuAction:(id)sender
{
    [self.menuContainerViewController toggleLeftSideMenuCompletion:nil];
}

- (IBAction)btnCartAction:(id)sender
{
    CartViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"CartViewController"];
    [self.navigationController pushViewController:newView animated:YES];
}
-(void)methodForUpdatevalues
{
    [[AppDelegate shareInstance]showActivityIndicator];
    NSDictionary *params = @{
                             @"user_id":[arrayForuserData valueForKey:@"id"],
                             @"name":[dicForTextFieldvalues valueForKey:@"name"],
                             @"phone":[dicForTextFieldvalues valueForKey:@"phone"],
                             @"zipcode":[dicForTextFieldvalues valueForKey:@"zipcode"],
                             @"address":[dicForTextFieldvalues valueForKey:@"address"],
                             @"city":[dicForTextFieldvalues valueForKey:@"city"]
                             };
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [[SingletonClass sharedManager] m_PostApiResponse:@"edit_profile" paramDiction:params islogout:NO  onCompletion:^(NSDictionary *response)
         {
             NSDictionary *responseDic = (NSDictionary *)response;
             if ([[[responseDic objectForKey:@"payload"]valueForKey:@"status"]integerValue]==1)
             {
                 
                 
                 [[NSUserDefaults standardUserDefaults] setObject:[[responseDic objectForKey:@"payload"] objectForKey:@"data"] forKey:@"userData"];
                 [[NSUserDefaults standardUserDefaults] synchronize];
                 NSLog(@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"userData"]);
                 [[AppDelegate shareInstance]hideActivityIndicator];
                 [[AppDelegate shareInstance] hideActivityIndicator];
                 UIAlertController * alert=   [UIAlertController
                                               alertControllerWithTitle:nil
                                               message:[[responseDic objectForKey:@"payload"] valueForKey:@"message"]
                                               preferredStyle:UIAlertControllerStyleAlert];
                 UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                     [self.navigationController popViewControllerAnimated:YES];
                     
                 }];
                 [alert addAction:okAction];
                 [self presentViewController:alert animated:YES completion:nil];
                 
             }
             else
             {
                 //
                 [[AppDelegate shareInstance] hideActivityIndicator];
                 UIAlertController * alert=   [UIAlertController
                                               alertControllerWithTitle:nil
                                               message:NSLocalizedString(@"Unable to get detail please try again.", @"Cancel")
                                               preferredStyle:UIAlertControllerStyleAlert];
                 UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                     // [self methodForUpdatevalues];
                     
                 }];
                 [alert addAction:okAction];
                 [self presentViewController:alert animated:YES completion:nil];
             }
         }];
    });
}
@end

