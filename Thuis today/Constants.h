//
//  Constants.h
//  Thuis today
//
//  Created by Rajeev Lochan Ranga on 26/02/18.
//  Copyright © 2018 Offshore Software Solutions. All rights reserved.
//

#ifndef Constants_h
#define Constants_h

//URL constants
//#define baseUrl @"http://thuis.today/api/user/"
//#define baseUrlForImage @"http://main.thuis.today/uploads/"
//#define baseUrl @"http://52.7.27.60/accept/thuis_today/api/user/"
//#define baseUrlForImage @"http://52.7.27.60/accept/main_thuis_today/uploads/"
//#define baseUrlRecipe @"http://52.7.27.60/accept/thuis_today/"
//#define baseUrlForImageRecipe @"http://52.7.27.60/accept/main_thuis_today/uploads/recipe/"

//#define baseUrl @"http://52.7.27.60/test/thuis_today/api/user/"
//#define baseUrlForImage @"http://52.7.27.60/test/main_thuis_today/uploads/"
//#define baseUrlRecipe @"http://52.7.27.60/test/thuis_today/"
//#define baseUrlForImageRecipe @"http://52.7.27.60/test/main_thuis_today/uploads/recipe/"

//http://thuis.today/api/user/

//for Production Environment
//#define baseUrl @"http://thuis.today/api/user/"
//#define baseUrlForImage @"http://thuis.today/uploads/images/"
//#define baseUrlForImageProfile @"http://thuis.today/uploads/profile/"
//#define baseUrlRecipe @"http://52.7.27.60/test/thuis_today/"
//#define baseUrlForImageRecipe @"http://52.7.27.60/test/main_thuis_today/uploads/recipe/"

//For Acceptance (testing) Environment
#define baseUrl @"http://52.7.27.60/maintoday/api/user/"
#define baseUrlForImage @"http://52.7.27.60/maintoday/uploads/images/"
#define baseUrlRecipe @"http://52.7.27.60/test/maintoday/"
#define baseUrlForImageRecipe @"http://52.7.27.60/test/main_thuis_today/uploads/recipe/"
#define baseUrlForImageProfile @"http://52.7.27.60/maintoday/uploads/profile/"


//#define baseUrl @"http://52.7.27.60/maintest/api/user/"
//#define baseUrlForImage @"http://52.7.27.60/maintest/uploads/images/"
//#define baseUrlRecipe @"http://52.7.27.60/test/thuis_today/"
//#define baseUrlForImageRecipe @"http://52.7.27.60/test/main_thuis_today/uploads/recipe/"
//#define baseUrlForImageProfile @"http://52.7.27.60/maintest/uploads/profile/"

#define kGoogleAnalyticsTrackingID          @"UA-116250015-1"

#define kCheckDeliveryAddress               @"checkDelivery"
#define kCheckoutTimingWeekdays             @"checkouttimingweekdays"
#define kGetUserEditProfile                 @"profileinfo"

#define kUserId                             @"user_id"
#define kPreparation_time                   @"pre_time"



#define kPostalCode                  @"postalCode"
#define kStreetCode                  @"StreetCode"
#define kHouseNumber                 @"HouseNumber"

#define kService_Charge              @"service_charge"
#define kDelivery_Charge             @"delivery_charge"



#endif /* Constants_h */
