//
//  ProductDetailViewController.h
//  Thuis today
//
//  Created by IMMANENT on 25/05/17.
//  Copyright © 2017 IMMANENT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProductDetailViewController : UIViewController
{
    
     IBOutlet UILabel *lblCount;
     IBOutlet UILabel *lblShopName;
     IBOutlet UIImageView *imgShop;
     IBOutlet UILabel *lblShopAddress;
     IBOutlet UILabel *lblVat;
     IBOutlet UILabel *lblOriginalPrice;
     IBOutlet UILabel *lblTotalPrice;
     IBOutlet UITableView *tbInfo;
    
     IBOutlet UITextField *txtQuantity;
    
     IBOutlet UILabel *lblUnit;
     IBOutlet UIImageView *imgProduct;
    
     IBOutlet UILabel *lblProductName;
    
     IBOutlet UILabel *lblProductInfo;
}
- (IBAction)btnBackAction:(id)sender;
- (IBAction)btnAddToCartActiion:(id)sender;
- (IBAction)btnProfileAction:(id)sender;
- (IBAction)btnHomeAction:(id)sender;
- (IBAction)btnMenuAction:(id)sender;
- (IBAction)btnCartAction:(id)sender;

- (IBAction)btnBottomBackAction:(id)sender;
- (IBAction)btnBottomCartAction:(id)sender;
- (IBAction)btnBottomHomeAction:(id)sender;
@property NSArray *arrayForProductInfo;

@end
