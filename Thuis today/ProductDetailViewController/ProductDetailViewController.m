//
//  ProductDetailViewController.m
//  Thuis today
//
//  Created by IMMANENT on 25/05/17.
//  Copyright © 2017 IMMANENT. All rights reserved.
//
#define ACCEPTABLE_CHARACTERS @"0123456789,"
#define ACCEPTABLE_CHARACTERSWITHOUT @"0123456789"
#import "ProductDetailViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
@interface ProductDetailViewController ()<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>

{
    NSMutableArray *arrayForShop,*arrayForCoreDatavalues,*arrayNutritionValueKeys;
    NSString *unit;
    NSMutableDictionary *dicInfo;
    
}
@end

@implementation ProductDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    dicInfo = [NSMutableDictionary new];
    arrayForShop=[[NSMutableArray alloc]init];
    arrayNutritionValueKeys = [[NSMutableArray alloc]initWithObjects:@"Energy",@"which_saturated",@"single_unsaturated",@"multiply_unsaturated",@"Carbohydrates",@"sugars",@"Nutritional_fiber",@"Proteins",@"salt",@"vet", nil];
    
    NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:@"restaurantData"];
    arrayForShop = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    lblShopAddress.text =[arrayForShop valueForKey:@"address_ar"];
    lblShopName.text =[arrayForShop valueForKey:@"restaurant_name_ar"];
    [imgShop sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",baseUrlForImage,[arrayForShop valueForKey:@"logo"]]]
                   placeholderImage:[UIImage imageNamed:@"place-holder@1x"]];
    arrayForCoreDatavalues = [[NSMutableArray alloc]init];
    NSLog(@"%@",_arrayForProductInfo);
    lblCount.layer.cornerRadius = lblCount.frame.size.width/2;
    lblCount.layer.masksToBounds = YES;
    
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"cartCount"]!=nil) {
        lblCount.text=[[NSUserDefaults standardUserDefaults] valueForKey:@"cartCount"];
    }
    else
    {
        lblCount.text=@"";
    }
//    if ([[_arrayForProductInfo  valueForKey:@"product_unit"] isEqualToString:@"weight"]) {
//        lblUnit.text=@"Kg";
//        unit =@"Kg";
//        
//    }
//    else{
//        lblUnit.text=@"Pcs";
//           unit =@"Pcs";
//    }
    unit =[_arrayForProductInfo  valueForKey:@"actual_unit"];
    lblUnit.text = [_arrayForProductInfo  valueForKey:@"actual_unit"];
    [imgProduct sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",baseUrlForImage,[_arrayForProductInfo valueForKey:@"meal_image"]]]
               placeholderImage:[UIImage imageNamed:@"place-holder@1x"]];
    
    NSString *str =@"%";
    lblVat.text=[NSString stringWithFormat:@"VAT:%@%@ %@",[_arrayForProductInfo valueForKey:@"vat_name"],str,[_arrayForProductInfo  valueForKey:@"type"]];
    
    lblProductName.text=[_arrayForProductInfo  valueForKey:@"meal_name_ar"];
    
   
    float sum=0.00 ;
    float vat=0.00;
    float orgPrice=0.00;
    NSString* cleanedString = [[[_arrayForProductInfo valueForKey:@"meal_price"] stringByReplacingOccurrencesOfString:@"," withString:@"."]
                               stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
    
    
    vat = ([cleanedString floatValue]*[[_arrayForProductInfo  valueForKey:@"vat_name"] floatValue])/100;

    if ([[_arrayForProductInfo valueForKey:@"type"] isEqualToString:@"Excl."]) {
        // sum = ([cleanedString floatValue]+vat)*[txtFld.text floatValue]  ;
        sum = [cleanedString floatValue]+vat;
        orgPrice =[cleanedString floatValue]+vat*1;
        NSString *roundOff = [NSString stringWithFormat:@"%.2f",sum];
        sum = [roundOff floatValue] *1;
    }
    else
    {
        sum = 1 * [cleanedString floatValue];
        orgPrice =[cleanedString floatValue]*1;
    }
    NSString* strForPriceWithComma = [[[NSString stringWithFormat:@"%.2f",sum] stringByReplacingOccurrencesOfString:@"." withString:@","]
                                      stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
    NSString* strForPriceWithComma2 = [[[NSString stringWithFormat:@"%.2f",orgPrice] stringByReplacingOccurrencesOfString:@"." withString:@","]
                                       stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
    
    lblOriginalPrice.text = [NSString stringWithFormat:@"€ %@/%@",strForPriceWithComma2,unit];
    txtQuantity.text = @"";
    lblTotalPrice.text =[NSString stringWithFormat:@"€ %@",strForPriceWithComma];
    
    NSArray *keys =  [NSArray array];
    NSDictionary *dict = [NSDictionary new];

    dict = [_arrayForProductInfo mutableCopy];
    keys = [dict allKeys];
    for (int i = 0; i < [arrayNutritionValueKeys count]; i++)
    {
        if ([keys containsObject:[arrayNutritionValueKeys objectAtIndex:i]])
        {
            [dicInfo setObject:[_arrayForProductInfo valueForKey:[arrayNutritionValueKeys objectAtIndex:i]] forKey:[arrayNutritionValueKeys objectAtIndex:i]];
        }
    }
    
   // dicInfo = [_arrayForProductInfo mutableCopy];
    
    if ([[_arrayForProductInfo valueForKey:@"additional_info"]intValue ]==0) {
        tbInfo.hidden = YES;
        lblProductInfo.hidden =YES;
    }
    else
        
    {
         tbInfo.hidden = NO;
         lblProductInfo.hidden =NO;
    }
    [tbInfo setTableFooterView:[[UIView alloc] initWithFrame:CGRectMake(0,0,0,0)]];
    
    
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
//    if (textField.text.length == 1) {
//        if ( [string isEqualToString:@""] )
//            return NO;
//    }
   
    if ([[_arrayForProductInfo  valueForKey:@"product_unit"] isEqualToString:@"weight"])
    {
        if ([unit isEqualToString:@"per stuk"] || [unit isEqualToString:@"per zak"] || [unit isEqualToString:@"per doos"])
        {
            if ([self checkForValidCharacterForWithout:string]) {
                
                string = [[string stringByReplacingOccurrencesOfString:@"." withString:@","]
                          stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
                
                NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
                newString =     [newString stringByTrimmingCharactersInSet:
                                 [NSCharacterSet whitespaceCharacterSet]];
                float sum=0.00 ;
                float vat=0.00;
                float orgPrice=0.00;
                NSString* cleanedString = [[[_arrayForProductInfo valueForKey:@"meal_price"] stringByReplacingOccurrencesOfString:@"," withString:@"."]
                                           stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
                
                NSString *strTextField =[[newString stringByReplacingOccurrencesOfString:@"," withString:@"."]
                                         stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
                
                vat = ([cleanedString floatValue]*[[_arrayForProductInfo  valueForKey:@"vat_name"] floatValue])/100;
                NSString *str =@"%";
                lblVat.text=[NSString stringWithFormat:@"VAT:%@%@ %@",[_arrayForProductInfo  valueForKey:@"vat_name"],str,[_arrayForProductInfo valueForKey:@"type"]];
                if ([[_arrayForProductInfo valueForKey:@"type"] isEqualToString:@"Excl."]) {
                    // sum = ([cleanedString floatValue]+vat)*[txtFld.text floatValue]  ;
                    sum = [cleanedString floatValue]+vat;
                    orgPrice =[cleanedString floatValue]+vat*1;
                    NSString *roundOff = [NSString stringWithFormat:@"%.2f",sum];
                    sum = [roundOff floatValue] *[strTextField floatValue];
                }
                else
                {
                    sum = [strTextField floatValue] * [cleanedString floatValue];
                    orgPrice =[cleanedString floatValue]*1;
                }
                NSString* strForPriceWithComma = [[[NSString stringWithFormat:@"%.2f",sum] stringByReplacingOccurrencesOfString:@"." withString:@","]
                                                  stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
                NSString* strForPriceWithComma2 = [[[NSString stringWithFormat:@"%.2f",orgPrice] stringByReplacingOccurrencesOfString:@"." withString:@","]
                                                   stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
                
                
                
                
                
                lblTotalPrice.text = [NSString stringWithFormat:@"€ %@",strForPriceWithComma];
                lblOriginalPrice.text =  [NSString stringWithFormat:@"€ %@",strForPriceWithComma2];
                return true;
            }
            else{
                return false;
            }
        }
        else
        {
            if ([self checkForValidCharacter:string]) {
                
                string = [[string stringByReplacingOccurrencesOfString:@"." withString:@","]
                          stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
                
                NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
                newString =     [newString stringByTrimmingCharactersInSet:
                                 [NSCharacterSet whitespaceCharacterSet]];
                float sum=0.00 ;
                float vat=0.00;
                float orgPrice=0.00;
                NSString* cleanedString = [[[_arrayForProductInfo valueForKey:@"meal_price"] stringByReplacingOccurrencesOfString:@"," withString:@"."]
                                           stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
                
                NSString *strTextField =[[newString stringByReplacingOccurrencesOfString:@"," withString:@"."]
                                         stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
                
                vat = ([cleanedString floatValue]*[[_arrayForProductInfo  valueForKey:@"vat_name"] floatValue])/100;
                NSString *str =@"%";
                lblVat.text=[NSString stringWithFormat:@"VAT:%@%@ %@",[_arrayForProductInfo  valueForKey:@"vat_name"],str,[_arrayForProductInfo valueForKey:@"type"]];
                if ([[_arrayForProductInfo valueForKey:@"type"] isEqualToString:@"Excl."]) {
                    // sum = ([cleanedString floatValue]+vat)*[txtFld.text floatValue]  ;
                    sum = [cleanedString floatValue]+vat;
                    orgPrice =[cleanedString floatValue]+vat*1;
                    NSString *roundOff = [NSString stringWithFormat:@"%.2f",sum];
                    sum = [roundOff floatValue] *[strTextField floatValue];
                }
                else
                {
                    sum = [strTextField floatValue] * [cleanedString floatValue];
                    orgPrice =[cleanedString floatValue]*1;
                }
                NSString* strForPriceWithComma = [[[NSString stringWithFormat:@"%.2f",sum] stringByReplacingOccurrencesOfString:@"." withString:@","]
                                                  stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
                NSString* strForPriceWithComma2 = [[[NSString stringWithFormat:@"%.2f",orgPrice] stringByReplacingOccurrencesOfString:@"." withString:@","]
                                                   stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
                
                
                
                
                
                lblTotalPrice.text = [NSString stringWithFormat:@"€ %@",strForPriceWithComma];
                lblOriginalPrice.text =  [NSString stringWithFormat:@"€ %@",strForPriceWithComma2];
                return true;
            }
            else{
                return false;
            }
        }
        
    }
    else
    {
        if ([self checkForValidCharacterForWithout:string]) {
            
            string = [[string stringByReplacingOccurrencesOfString:@"." withString:@","]
                      stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
            
            NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
            newString =     [newString stringByTrimmingCharactersInSet:
                             [NSCharacterSet whitespaceCharacterSet]];
            float sum=0.00 ;
            float vat=0.00;
            float orgPrice=0.00;
            NSString* cleanedString = [[[_arrayForProductInfo valueForKey:@"meal_price"] stringByReplacingOccurrencesOfString:@"," withString:@"."]
                                       stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
            
            NSString *strTextField =[[newString stringByReplacingOccurrencesOfString:@"," withString:@"."]
                                     stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
            
            vat = ([cleanedString floatValue]*[[_arrayForProductInfo  valueForKey:@"vat_name"] floatValue])/100;
            NSString *str =@"%";
            lblVat.text=[NSString stringWithFormat:@"VAT:%@%@ %@",[_arrayForProductInfo  valueForKey:@"vat_name"],str,[_arrayForProductInfo valueForKey:@"type"]];
            if ([[_arrayForProductInfo valueForKey:@"type"] isEqualToString:@"Excl."]) {
                // sum = ([cleanedString floatValue]+vat)*[txtFld.text floatValue]  ;
                sum = [cleanedString floatValue]+vat;
                orgPrice =[cleanedString floatValue]+vat*1;
                NSString *roundOff = [NSString stringWithFormat:@"%.2f",sum];
                sum = [roundOff floatValue] *[strTextField floatValue];
            }
            else
            {
                sum = [strTextField floatValue] * [cleanedString floatValue];
                orgPrice =[cleanedString floatValue]*1;
            }
            
            NSString* strForPriceWithComma = [[[NSString stringWithFormat:@"%.2f",sum] stringByReplacingOccurrencesOfString:@"." withString:@","]
                                              stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
            NSString* strForPriceWithComma2 = [[[NSString stringWithFormat:@"%.2f",orgPrice] stringByReplacingOccurrencesOfString:@"." withString:@","]
                                              stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
            
            
            
            
            
            lblTotalPrice.text = [NSString stringWithFormat:@"€ %@",strForPriceWithComma];
            lblOriginalPrice.text =  [NSString stringWithFormat:@"€ %@",strForPriceWithComma2];
            return true;
        }
        else
        {
            return false;
        }
        
    }
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [txtQuantity resignFirstResponder];
}

-(BOOL)checkForValidCharacter:(NSString*)str
{
    NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:ACCEPTABLE_CHARACTERS] invertedSet];
    NSString *filtered = [[str componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
    if ([str isEqualToString:filtered])
    {
        return true;
    }
    else
    {
        return false;
    }
}
-(BOOL)checkForValidCharacterForWithout:(NSString*)str
{
    NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:ACCEPTABLE_CHARACTERSWITHOUT] invertedSet];
    NSString *filtered = [[str componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
    if ([str isEqualToString:filtered])
    {
        return true;
    }
    else
    {
        return false;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark -
#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return dicInfo.allKeys.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier;
    UITableViewCell *cell ;
    CellIdentifier = @"cell2";
            cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    UILabel *lblKey = (UILabel *)[cell viewWithTag:201];
//    lblKey.layer.borderColor = [UIColor colorWithRed:82/255.0f green:139/255.0f blue:11/255.0f alpha:1].CGColor;
//    lblKey.layer.borderWidth = 2;
//    lblKey.layer.masksToBounds = true;
    UILabel *lblValue = (UILabel *)[cell viewWithTag:202];
//    lblValue.layer.borderColor = [UIColor colorWithRed:82/255.0f green:139/255.0f blue:11/255.0f alpha:1].CGColor;
//    lblValue.layer.borderWidth = 2;
//    lblValue.layer.masksToBounds = true;
    lblKey.text = [dicInfo.allKeys objectAtIndex:indexPath.row];
    lblValue.text = [dicInfo.allValues objectAtIndex:indexPath.row];
    
    return cell;
}
-(UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    static NSString *CellIdentifier;
    UITableViewCell *cell ;
    CellIdentifier = @"cell1";
    cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    UILabel *lblKey = (UILabel *)[cell viewWithTag:101];
    lblKey.layer.borderColor = [UIColor colorWithRed:82/255.0f green:139/255.0f blue:11/255.0f alpha:1].CGColor;
    lblKey.layer.borderWidth = 2;
    lblKey.layer.masksToBounds = true;
    UILabel *lblValue = (UILabel *)[cell viewWithTag:102];
    lblValue.layer.borderColor = [UIColor colorWithRed:82/255.0f green:139/255.0f blue:11/255.0f alpha:1].CGColor;
    lblValue.layer.borderWidth = 2;
    lblValue.layer.masksToBounds = true;
    return cell;
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnBackAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnAddToCartActiion:(id)sender
{
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"is_recipe"];
    [txtQuantity resignFirstResponder];
    float sum=0.00 ;
    float vat=0.00;
    NSString *type=[_arrayForProductInfo  valueForKey:@"type"] ;
    NSString* cleanedString = [[[_arrayForProductInfo valueForKey:@"meal_price"] stringByReplacingOccurrencesOfString:@"," withString:@"."]
                               stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
    
    NSString *strTextField =[[txtQuantity.text stringByReplacingOccurrencesOfString:@"," withString:@"."]
                             stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
    
    vat = ([cleanedString floatValue]*[[_arrayForProductInfo  valueForKey:@"vat_name"] floatValue])/100;
    
    NSLog(@"%f",vat);
    
    if ([[_arrayForProductInfo  valueForKey:@"type"] isEqualToString:@"Excl."]) {
        // sum = ([cleanedString floatValue]+vat)*[txtFld.text floatValue]  ;
        sum = [cleanedString floatValue]+vat;
        NSString *roundOff = [NSString stringWithFormat:@"%.2f",sum];
    }
    else
    {
        sum = [cleanedString floatValue];
    }
    
    
    
    
    if (![lblTotalPrice.text isEqualToString:@"€ 0,00"]) {
        [[CustomClasssesForCoreData sharedManagerForCoreData] methodForSaveValues:_arrayForProductInfo   price:lblTotalPrice.text quantity:txtQuantity.text singlePrice:[NSString stringWithFormat:@"%.2f",sum] type:type onCompletion:^(NSArray *response){
            NSLog(@"%lu",(unsigned long)response.count);
        }];
        [self methodForGetTotalPrice];
    }
    else
    {
        
        [[AppDelegate shareInstance] showAlertWithErrorMessage: NSLocalizedString(@"Price should not be zero", @"Cancel")];
        
    }
    
}
-(void)methodForGetTotalPrice
{
    
    [[CustomClasssesForCoreData sharedManagerForCoreData] methodForFetchValues:nil onCompletion:^(NSArray *response)
     {
         NSLog(@"%@",response);
         arrayForCoreDatavalues = [response mutableCopy];
     }];
    NSString *count = [NSString stringWithFormat:@"%lu",(unsigned long)arrayForCoreDatavalues.count];
    [[NSUserDefaults standardUserDefaults]setObject:count forKey:@"cartCount"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"cartCount"]!=nil) {
        lblCount.text=[[NSUserDefaults standardUserDefaults] valueForKey:@"cartCount"];
    }
    else
    {
        lblCount.text=@"";
    }
}
- (IBAction)btnProfileAction:(id)sender
{
    if ( [[[NSUserDefaults standardUserDefaults] objectForKey:@"userData"] count])
    {
        UserProfileViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"UserProfileViewController"];
        [self.navigationController pushViewController:newView animated:YES];
    }
    else
    {
        LoginRegistrationViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginRegistrationViewController"];
        [self.navigationController pushViewController:newView animated:YES];
    }
}

- (IBAction)btnHomeAction:(id)sender
{
    ViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
    [self.navigationController pushViewController:newView animated:YES];
}

- (IBAction)btnMenuAction:(id)sender
{
      [self.menuContainerViewController toggleLeftSideMenuCompletion:nil];
}

- (IBAction)btnCartAction:(id)sender
{
    CartViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"CartViewController"];
    [self.navigationController pushViewController:newView animated:YES];
}

- (IBAction)btnBottomBackAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnBottomCartAction:(id)sender
{
    CartViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"CartViewController"];
    [self.navigationController pushViewController:newView animated:YES];
}

- (IBAction)btnBottomHomeAction:(id)sender
{
    ViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
    [self.navigationController pushViewController:newView animated:YES];
}

@end
