//
//  CartViewController.h
//  Thuis today
//
//  Created by IMMANENT on 14/01/17.
//  Copyright © 2017 IMMANENT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MFSideMenu.h"

@interface CartViewController : UIViewController
{
    
    __weak IBOutlet UIView *viewUpdateQty;
    __weak IBOutlet UITextField *txtFldupdateQty;
    IBOutlet UIScrollView *scrollView;
    
    IBOutlet UILabel *lblCartCount;
    
     IBOutlet UITableView *tableViewForCart;
    
     IBOutlet UIButton *btnAddMoreoutlet;
    
     IBOutlet UIView *viewForCartPrice;
    
     IBOutlet UILabel *lblTotalPrice;
    
     IBOutlet UILabel *lblCount;
}
- (IBAction)btnCancelAction:(id)sender;

- (IBAction)btnUpdateQtyAction:(id)sender;
- (IBAction)btnCloseUpdatePopUpAction:(id)sender;
- (IBAction)btnProfileAction:(id)sender;
- (IBAction)btnHomeAction:(id)sender;
- (IBAction)btnHelpAction:(id)sender;
- (IBAction)btnMenuAction:(id)sender;
- (IBAction)btnCrossAction:(id)sender;
- (IBAction)btnActionCheckOut:(id)sender;
- (IBAction)btnClearAllAction:(id)sender;
- (IBAction)btnAddMoreAction:(id)sender;

- (IBAction)btnBottomBackAction:(id)sender;
- (IBAction)btnBottomCartAction:(id)sender;
- (IBAction)btnBottomHomeAction:(id)sender;

@end
