//
//  CartViewController.m
//  Thuis today
//
//  Created by IMMANENT on 14/01/17.
//  Copyright © 2017 IMMANENT. All rights reserved.
//

#import "CartViewController.h"
#define ACCEPTABLE_CHARACTERS @"0123456789_,"
#define ACCEPTABLE_CHARACTERSWITHOUT @"0123456789_"
@interface CartViewController ()<UITextFieldDelegate>
{
    NSArray *arrayForCoreDatavalues;
    float sum;
    NSMutableArray *arrayForCart;
    NSString *strUnit;
    NSManagedObject *device;
}
@property (nonatomic) NSUInteger maxPreparationTime;
@end

@implementation CartViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [CommonMethods configureNavigationBarForViewController:self withTitle:@"My Cart"];
    // Do any additional setup after loading the view.
    lblCartCount.layer.cornerRadius = lblCartCount.frame.size.width/2;
    lblCartCount.layer.masksToBounds=YES;
   
    //textField.keyboardType = UIKeyboardTypeNamePhonePad;
    sum=0;
    [self methodForFetchValuesFromCoreData];
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
//    viewForCartPrice.layer.borderWidth = 1.0;
//    viewForCartPrice.layer.borderColor =[UIColor lightGrayColor].CGColor;
//    viewForCartPrice.layer.masksToBounds = YES;
    [self methodForGetTotalPrice];
    lblCount.layer.cornerRadius = lblCount.frame.size.width/2;
    lblCount.layer.masksToBounds=YES;
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"cartCount"]!=nil) {
        lblCount.text=[[NSUserDefaults standardUserDefaults] valueForKey:@"cartCount"];
    }
    else
    {
        lblCount.text=@"";
    }
    [[NSUserDefaults standardUserDefaults]setObject:@"no" forKey:@"isCartView"];
    [[NSUserDefaults standardUserDefaults]synchronize];
}
-(void)methodForGetTotalPrice
{
    sum=0.00;
    NSLog(@"%lu",(unsigned long)arrayForCoreDatavalues.count);
       for (int i=0; i<arrayForCoreDatavalues.count; i++) {
          NSManagedObject *device = [arrayForCoreDatavalues objectAtIndex:i];
           NSString* cleanedString = [[[device valueForKey:@"product_price"] stringByReplacingOccurrencesOfString:@"," withString:@"."]
                                      stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
           NSLog(@"%@",cleanedString);
       sum = sum + [cleanedString floatValue];
           
           
        }
    NSLog(@"%@",arrayForCart);
    
    [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%.2f",sum] forKey:@"totalPrice"];
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    NSString *str =NSLocalizedString(@"Total Price", @"Cancel");
    NSString* cleanedString = [[[NSString stringWithFormat:@"%@ :€ %.2f  ",str,sum] stringByReplacingOccurrencesOfString:@"." withString:@","]
                               stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
    lblTotalPrice.text =cleanedString;

}
-(void)methodForFetchValuesFromCoreData
{
    [[CustomClasssesForCoreData sharedManagerForCoreData] methodForFetchValues:nil onCompletion:^(NSArray *response)
     {
         NSLog(@"%@",response);
         
         arrayForCoreDatavalues = response;
        self.maxPreparationTime = [self getMaxPreparationTimeForProducts:arrayForCoreDatavalues];
         [self methodForGetTotalPrice];
         [tableViewForCart reloadData];
     }];
    NSString *count = [NSString stringWithFormat:@"%lu",(unsigned long)arrayForCoreDatavalues.count];
    [[NSUserDefaults standardUserDefaults]setObject:count forKey:@"cartCount"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"cartCount"]!=nil) {
        lblCount.text=[[NSUserDefaults standardUserDefaults] valueForKey:@"cartCount"];
    }
    else
    {
        lblCount.text=@"";
    }
}

- (NSInteger)getMaxPreparationTimeForProducts:(NSArray *)productsList {
    NSInteger maxTime ;
    NSArray *preparationTimeArray = [productsList valueForKeyPath:kPreparation_time];
    maxTime = [[preparationTimeArray valueForKeyPath:@"@max.intValue"] integerValue];
    return maxTime;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return arrayForCoreDatavalues.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier;
    UITableViewCell *cell ;
    
    CellIdentifier = @"cell";
    cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    UILabel *lblProductName = (UILabel *)[cell viewWithTag:101];
      UITextField *lblProductQuantity = (UITextField *)[cell viewWithTag:102];
       UILabel *lblProductUnit = (UILabel *)[cell viewWithTag:103];
     UILabel *lblProductPrice = (UILabel *)[cell viewWithTag:104];
    NSManagedObject *device = [arrayForCoreDatavalues objectAtIndex:indexPath.row];
    
    lblProductName.text = [NSString stringWithFormat:@"%@",[device valueForKey:@"product_name"]];
    //lblProductQuantity.text =[NSString stringWithFormat:@"%@",[device valueForKey:@"product_quantity"]];
    
  
//    if ([[device valueForKey:@"product_unit"] isEqualToString:@"weight"]) {
//        lblProductUnit.text=@"Kg";
//    }
//    else{
//         lblProductUnit.text=@"Pcs";
//    }
    lblProductUnit.text = [device valueForKey:@"product_unit"];
    NSLog(@"%@",[NSString stringWithFormat:@"%@",[device valueForKey:@"product_price"]]);
    if ([[NSString stringWithFormat:@"%@",[NSString stringWithFormat:@"%@",[device valueForKey:@"product_quantity"]]] containsString:@"."])
    {
        NSString* cleanedString = [[[NSString stringWithFormat:@"%@",[NSString stringWithFormat:@"%@",[device valueForKey:@"product_quantity"]]] stringByReplacingOccurrencesOfString:@"." withString:@","]
                                   stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
         lblProductQuantity.text =[NSString stringWithFormat:@"%@",cleanedString];

    }
    else
    {
        lblProductQuantity.text =[NSString stringWithFormat:@"%@",[device valueForKey:@"product_quantity"]];

    }
    
    
      lblProductPrice.text = [NSString stringWithFormat:@"%@",[device valueForKey:@"product_price"]];
        return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
   // price_vat product_id
      device = [arrayForCoreDatavalues objectAtIndex:indexPath.row];
    strUnit = [device valueForKey:@"product_unit"];
    NSLog(@"%@",[NSString stringWithFormat:@"%@",[device valueForKey:@"price_vat"]]);
    
    
    
    UIAlertController *alert= [UIAlertController
                               alertControllerWithTitle:nil
                               message:@"Update Quantity"
                               preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                               handler:^(UIAlertAction * action){
                                                   //Do Some action here
                                                   UITextField *textField = alert.textFields[0];
                                                   textField.keyboardType=UIKeyboardTypeNumberPad;
                                                   NSLog(@"text was %@", textField.text);
                                                   NSString *str = [device valueForKey:@"single_price"];
                                                   NSString* updatedPrice = [[str stringByReplacingOccurrencesOfString:@"," withString:@"."]
                                                                             stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
                                                   NSString* updatedText = [[textField.text stringByReplacingOccurrencesOfString:@"," withString:@"."]
                                                                             stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
                                                   if ([updatedText isEqualToString:@"0"]) {
                                                       updatedText = @"1";
                                                       textField.text=@"1";
                                                   }
                                                   
                                                   float newValue = [updatedPrice floatValue] * [updatedText floatValue];
                                                   
                                                   NSLog(@"%f",newValue);
                                                   
                                                   NSLog(@"%@",[NSString stringWithFormat:@"%.2f",newValue]);
                                                   
                                                   NSString* finalPrice = [[[NSString stringWithFormat:@"%.2f",newValue] stringByReplacingOccurrencesOfString:@"." withString:@","]
                                                                             stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
                                                   updatedText=[[[NSString stringWithFormat:@"%@,",updatedText] stringByReplacingOccurrencesOfString:@"." withString:@","]stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];;
                                                   updatedText =[updatedText stringByTrimmingCharactersInSet:
                                                                     [NSCharacterSet whitespaceCharacterSet]];
                                                   
                                                   
                                                   if (![updatedText isEqualToString:@""]) {
                                                       [[CustomClasssesForCoreData sharedManagerForCoreData] methodForUpdateValues:[NSString stringWithFormat:@"%@",[device valueForKey:@"product_id"]] price:[NSString stringWithFormat:@"€ %@",finalPrice] quantity:textField.text onCompletion:^(BOOL response){
                                                           if (response == true) {
                                                               [self methodForFetchValuesFromCoreData];
                                                           }
                                                       }];
                                                   }
                                                   
                                               }];
    UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction * action) {
    
                                                       NSLog(@"cancel btn");
    
                                                       [alert dismissViewControllerAnimated:YES completion:nil];
    
                                                   }];
    
    [alert addAction:ok];
    [alert addAction:cancel];
    
    [alert addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"placeHolderText";
        textField.keyboardType = UIKeyboardTypeNumberPad;
        textField.delegate = self;
        if ([[NSString stringWithFormat:@"%@",[NSString stringWithFormat:@"%@",[device valueForKey:@"product_quantity"]]] containsString:@"."])
        {
            NSString* cleanedString = [[[NSString stringWithFormat:@"%@",[NSString stringWithFormat:@"%@",[device valueForKey:@"product_quantity"]]] stringByReplacingOccurrencesOfString:@"." withString:@","]
                                       stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
            textField.text =[NSString stringWithFormat:@"%@",cleanedString];
            NSString *me = @".0";
            //NSString *target=@"abcdetoBe";
            if([self substring:me existsInString:cleanedString]) {
                NSLog(@"It exists!");
            }
            else {
                NSLog(@"It does not exist!");
            }
        }
        else
        {
            NSString *me = @",0";
            //NSString *target=@"abcdetoBe";
            if([self substring:me existsInString:[NSString stringWithFormat:@"%@",[device valueForKey:@"product_quantity"]]]) {
                NSLog(@"It exists!");
                NSString* cleanedString = [[[NSString stringWithFormat:@"%@",[NSString stringWithFormat:@"%@",[device valueForKey:@"product_quantity"]]] stringByReplacingOccurrencesOfString:@",0" withString:@""]
                                           stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
                textField.text =[NSString stringWithFormat:@"%@",cleanedString];
            }
            else {
               textField.text =[NSString stringWithFormat:@"%@",[device valueForKey:@"product_quantity"]];
            }
            
        }
    }];
    
    [self presentViewController:alert animated:YES completion:nil];
    
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField.text.length >=5 && range.length == 0)
{
    return NO; // return NO to not change text
}
else
{
    //if ([strUnit isEqualToString:@"weight"])
    if ([strUnit isEqualToString:@"stuk"]||[strUnit isEqualToString:@"zak"]||[strUnit isEqualToString:@"doos"]||[strUnit isEqualToString:@"1 stuk"]||[strUnit isEqualToString:@"1 zak"]||[strUnit isEqualToString:@"1 doos"])
    {
        if ([self checkForValidCharacterForWithout:string]) {
            
            string = [[string stringByReplacingOccurrencesOfString:@"." withString:@","]
                      stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
            
            NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
            newString =     [newString stringByTrimmingCharactersInSet:
                             [NSCharacterSet whitespaceCharacterSet]];
            
        return true;
        }
        else{
            return false;
        }
        
    }
    else
    {
        if ([self checkForValidCharacter:string]) {
            
            string = [[string stringByReplacingOccurrencesOfString:@"." withString:@","]
                      stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
            
            NSString *newString = [textField.text stringByReplacingCharactersInRange:range withString:string];
            newString =     [newString stringByTrimmingCharactersInSet:
                             [NSCharacterSet whitespaceCharacterSet]];
            
            return true;
        }
        else
        {
            return false;
        }
        
    }
}
}

-(BOOL)substring:(NSString *)substr existsInString:(NSString *)str {
    if(!([str rangeOfString:substr options:NSCaseInsensitiveSearch].length==0)) {
        return YES;
    }
    
    return NO;
}
-(BOOL)checkForValidCharacter:(NSString*)str
{
    NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:ACCEPTABLE_CHARACTERS] invertedSet];
    NSString *filtered = [[str componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
    if ([str isEqualToString:filtered])
    {
        return true;
    }
    else
    {
        return false;
    }
}
-(BOOL)checkForValidCharacterForWithout:(NSString*)str
{
    NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:ACCEPTABLE_CHARACTERSWITHOUT] invertedSet];
    NSString *filtered = [[str componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
    if ([str isEqualToString:filtered])
    {
        return true;
    }
    else
    {
        return false;
    }
}


- (IBAction)btnUpdateQtyAction:(id)sender {
}

- (IBAction)btnCloseUpdatePopUpAction:(id)sender {
}

- (IBAction)btnProfileAction:(id)sender {
    if ( [[[NSUserDefaults standardUserDefaults] objectForKey:@"userData"] count])
    {
        UserProfileViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"UserProfileViewController"];
        [self.navigationController pushViewController:newView animated:YES];
    }
    else
    {
        LoginRegistrationViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginRegistrationViewController"];
        [self.navigationController pushViewController:newView animated:YES];
    }
}

- (IBAction)btnHomeAction:(id)sender {
    ViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
    [self.navigationController pushViewController:newView animated:YES];
}

- (IBAction)btnHelpAction:(id)sender
{
    
}

- (IBAction)btnMenuAction:(id)sender
{
    [self.menuContainerViewController toggleLeftSideMenuCompletion:nil];
}

- (IBAction)btnCrossAction:(id)sender
{
    UITableViewCell* cell = (UITableViewCell*)[sender superview];
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:tableViewForCart];
    NSIndexPath *indexPath = [tableViewForCart indexPathForRowAtPoint:buttonPosition];
    NSManagedObject *device = [arrayForCoreDatavalues objectAtIndex:indexPath.row];
    NSString *productId = [NSString stringWithFormat:@"%@",[device valueForKey:@"product_id"]];
    [[CustomClasssesForCoreData sharedManagerForCoreData] methodForDeleteValues:productId onCompletion:^(NSArray *response)
     {
         [self methodForFetchValuesFromCoreData];
     }];
}

- (IBAction)btnActionCheckOut:(id)sender
{
    if (arrayForCoreDatavalues.count) {
        if ( [[[NSUserDefaults standardUserDefaults] objectForKey:@"userData"] count])
        {
            CheckoutViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"CheckoutViewController"];
            newView.maxPreparationTime = self.maxPreparationTime;
            [self.navigationController pushViewController:newView animated:YES];
        }
        else
        {
            UIAlertController * alert=   [UIAlertController
                                          alertControllerWithTitle:nil
                                          message:NSLocalizedString(@"Check Out Message", @"Cancel")
                                          preferredStyle:UIAlertControllerStyleAlert];
            //
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                       {
                                           [[NSUserDefaults standardUserDefaults]setObject:@"yes" forKey:@"isCartView"];
                                           [[NSUserDefaults standardUserDefaults]synchronize];
                                           LoginRegistrationViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginRegistrationViewController"];
                                           [self.navigationController pushViewController:newView animated:YES];
                                       }];
            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){
                
            }];
            [alert addAction:cancelAction];
            [alert addAction:okAction];
            [self presentViewController:alert animated:YES completion:nil];
           
        }
    }
    else
    {
        [[AppDelegate shareInstance]showAlertWithErrorMessage:NSLocalizedString(@"please add products in cart", @"Cancel")];
    }
}

- (IBAction)btnClearAllAction:(id)sender
{
    [[CustomClasssesForCoreData sharedManagerForCoreData] methodForDeleteAllValues:nil onCompletion:^(NSArray *response)
     {
        [self methodForFetchValuesFromCoreData];
     }];
     lblTotalPrice.text =[NSString stringWithFormat:@"Total Price :€ 0,00"];
}

- (IBAction)btnAddMoreAction:(id)sender
{
    if ( [[NSUserDefaults standardUserDefaults] valueForKey:@"restaurant_id"] !=nil) {
        ShopDetailViewController*newView = [self.storyboard instantiateViewControllerWithIdentifier:@"ShopDetailViewController"];
        
        [self.navigationController pushViewController:newView animated:YES];
    }
    else
    {
        ViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
        
        [self.navigationController pushViewController:newView animated:YES];
    }
}

- (IBAction)btnBottomBackAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnBottomCartAction:(id)sender
{
    CartViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"CartViewController"];
    [self.navigationController pushViewController:newView animated:YES];
}

- (IBAction)btnBottomHomeAction:(id)sender
{
    ViewController *newView = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
    [self.navigationController pushViewController:newView animated:YES];
}

-(void) ChangeQuantity
{
  strUnit = [device valueForKey:@"product_unit"];
//    UITextField *textField = alert.textFields[0];
//    textField.keyboardType=UIKeyboardTypeNumberPad;
    NSLog(@"text was %@", txtFldupdateQty.text);
    NSString *str = [device valueForKey:@"single_price"];
    
    NSString* updatedPrice = [[str stringByReplacingOccurrencesOfString:@"," withString:@"."]
                              stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
    NSString* updatedText = [[txtFldupdateQty.text stringByReplacingOccurrencesOfString:@"," withString:@"."]
                             stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
    if ([updatedText isEqualToString:@"0"]) {
        updatedText = @"1";
        txtFldupdateQty.text=@"1";
    }
    
    float newValue = [updatedPrice floatValue] * [updatedText floatValue];
    
    NSLog(@"%f",newValue);
    
    NSLog(@"%@",[NSString stringWithFormat:@"%.2f",newValue]);
    
    NSString* finalPrice = [[[NSString stringWithFormat:@"%.2f",newValue] stringByReplacingOccurrencesOfString:@"." withString:@","]
                            stringByTrimmingCharactersInSet: [NSCharacterSet symbolCharacterSet]];
    updatedText =[updatedText stringByTrimmingCharactersInSet:
                  [NSCharacterSet whitespaceCharacterSet]];
    
    
    if (![updatedText isEqualToString:@""]) {
        [[CustomClasssesForCoreData sharedManagerForCoreData] methodForUpdateValues:[NSString stringWithFormat:@"%@",[device valueForKey:@"product_id"]] price:[NSString stringWithFormat:@"€ %@",finalPrice] quantity:txtFldupdateQty.text onCompletion:^(BOOL response){
            if (response == true) {
                [self methodForFetchValuesFromCoreData];
            }
        }];
    }

 }
         - (IBAction)btnCancelAction:(id)sender {
            }
@end
