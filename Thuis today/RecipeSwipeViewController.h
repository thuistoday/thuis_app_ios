//
//  RecipeSwipeViewController.h
//  RecipeTinder8
//
//  Created by offshore_mac_1 on 17/10/17.
//  Copyright © 2017 offshore_mac_1. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RecipeSwipeViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *lblPrepTime;
@property (weak, nonatomic) IBOutlet UILabel *lblAddedBy;
@property (weak, nonatomic) IBOutlet UILabel *lblLikes;
@property (weak, nonatomic) IBOutlet UILabel *lblRecipeName;
@property (weak, nonatomic) IBOutlet UIImageView *imgRecipe;

@property (retain, nonatomic) NSMutableArray *recipeData;
@property (nonatomic) NSInteger index;
@end
