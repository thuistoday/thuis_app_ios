//
//  SplashViewController.m
//  Delivery Agent - Thuis today
//
//  Created by Offshore on 9/20/17.
//  Copyright © 2017 Offshore. All rights reserved.
//

#import "SplashViewController.h"
#import "UIImage+animatedGIF.h"
@interface SplashViewController ()

@end

@implementation SplashViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSURL *url;
    if ( IDIOM == IPAD ) {
        url = [[NSBundle mainBundle] URLForResource:@"splash-v4-ipad" withExtension:@"gif"];
        
    }
    else {
        if ([UIScreen mainScreen].bounds.size.height == 812) {
            url = [[NSBundle mainBundle] URLForResource:@"splash-v4-iphonex" withExtension:@"gif"];
        }
        else {
            url = [[NSBundle mainBundle] URLForResource:@"splash-v4" withExtension:@"gif"];
        }
    }
    _imgView.image = [UIImage animatedImageWithAnimatedGIFURL:url];
    [self performSelector:@selector(loadMainView) withObject:nil afterDelay:5.0];
}
-(void)loadMainView
{
    if ( IDIOM == IPAD ) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Storyboard" bundle:[NSBundle mainBundle]];
        ViewController *homeView = [storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
        [self.navigationController pushViewController:homeView animated:YES];
    }
    else{
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]];
        ViewController *homeView = [storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
        [self.navigationController pushViewController:homeView animated:YES];
    
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

@end
